create extension postgis; -- JA EXISTE NA IMAGEM DO POSTGIS
create extension pg_trgm;
create function public.fn_remove_acentuacao(character varying) returns character varying
    immutable
    cost 1
    language sql
as
$$
SELECT TRANSLATE($1, 'áéíóúàèìòùãõâêîôôäëïöüçÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÄËÏÖÜÇ', 'aeiouaeiouaoaeiooaeioucAEIOUAEIOUAOAEIOOAEIOUC')
$$;

alter function public.fn_remove_acentuacao(varchar) owner to postgres;

create schema salve;
alter schema salve owner to postgres;

create schema taxonomia;
alter schema taxonomia owner to postgres;

create user usr_salve;
create user usr_sintax;


create sequence salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq
    as integer;

alter sequence salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq owner to postgres;

create sequence salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq
    as integer;

alter sequence salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq owner to postgres;

create sequence salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq
    as integer;

alter sequence salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq owner to postgres;

create sequence salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq;

alter sequence salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq owner to postgres;

create sequence salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq
    as integer;

alter sequence salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq owner to postgres;

create table if not exists taxonomia.nivel_taxonomico
(
    sq_nivel_taxonomico    serial
        constraint pk_nivel_taxonomico
            primary key,
    de_nivel_taxonomico    text                  not null,
    nu_grau_taxonomico     integer               not null,
    in_nivel_intermediario boolean default false not null
        constraint chk_nivel_taxonomico_nivel
            check (in_nivel_intermediario = ANY (ARRAY [true, false])),
    co_nivel_taxonomico    text                  not null
);

comment on column taxonomia.nivel_taxonomico.nu_grau_taxonomico is 'Squencia numérica que identifica a ordem hierárquica entre os níveis taxonômicos 10,20,30,40...';

comment on column taxonomia.nivel_taxonomico.in_nivel_intermediario is 'Identifica se o nivel taxonômico é um nivel fora da sequência padrão REFICOFAGE';

comment on column taxonomia.nivel_taxonomico.co_nivel_taxonomico is 'Código do nível taxonômico sem acento e em caixa alta para ser utilizado como constantes nas consultas no lugar de do sq_nivel e nu_ordem_taxonomica';

alter table taxonomia.nivel_taxonomico
    owner to postgres;

grant usage on sequence taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq to usr_salve;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.nivel_taxonomico to usr_salve;

-- xxxxxxxxxxxxxxxxx

grant delete, insert, references, select, trigger, truncate, update on taxonomia.nivel_taxonomico to usr_sintax;

create table if not exists salve.bioma
(
    sq_bioma     serial
        constraint pk_bioma
            primary key,
    no_bioma     varchar(50)                         not null,
    ge_bioma     public.geometry
        constraint enforce_dims_the_geom
            check (public.st_ndims(ge_bioma) = 2)
        constraint enforce_geotype_the_geom
            check ((public.geometrytype(ge_bioma) = 'MULTIPOLYGON'::text) OR (ge_bioma IS NULL))
        constraint enforce_srid_the_geom
            check (public.st_srid(ge_bioma) = 4674),
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

alter table salve.bioma
    owner to postgres;

grant select, update, usage on sequence salve.bioma_sq_bioma_seq to usr_salve;

create index if not exists ix_geo_bioma
    on salve.bioma using gist (ge_bioma)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.bioma to usr_salve;

create table if not exists salve.character_entity
(
    name text not null
        primary key,
    ch   char
        unique
);

comment on table salve.character_entity is 'Tabela de entidades html utilizada na convesão para caracteres convencionais';

alter table salve.character_entity
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.character_entity to usr_salve;

create table if not exists salve.ciclo_avaliacao
(
    sq_ciclo_avaliacao serial
        constraint pk_ciclo_avaliacao
            primary key,
    de_ciclo_avaliacao text,
    nu_ano             smallint                 not null,
    in_situacao        char                     not null,
    in_oficina         char default 'S'::bpchar not null,
    in_publico         char
);

comment on column salve.ciclo_avaliacao.in_oficina is 'Indica se as fichas do ciclo poderão iniciar fase consulta ou oficina';

comment on column salve.ciclo_avaliacao.in_publico is 'Informa se as inforamcoes do ciclo estao disponiveis para o modulo publico do SALVE.';

alter table salve.ciclo_avaliacao
    owner to postgres;

create unique index if not exists ixu_ciclo_avaliacao_pk
    on salve.ciclo_avaliacao (sq_ciclo_avaliacao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_avaliacao to usr_salve;

create table if not exists salve.dados_apoio
(
    sq_dados_apoio       serial
        constraint pk_dados_apoio
            primary key,
    sq_dados_apoio_pai   bigint
        constraint fk_dados_dados_pai
            references salve.dados_apoio
            on update cascade on delete restrict,
    ds_dados_apoio       text not null,
    cd_dados_apoio       text,
    de_dados_apoio_ordem text,
    cd_sistema           text,
    cd_dados_apoio_nivel varchar(20)
);

comment on column salve.dados_apoio.de_dados_apoio_ordem is 'Valor utilizado para definr a ordem de listagem dos registros';

comment on column salve.dados_apoio.cd_sistema is 'Código único utilizado pelo sistema para aplicação de regras de negócio';

comment on column salve.dados_apoio.cd_dados_apoio_nivel is 'Campo utilizado para criar hierarquia entre os registros, quando necessário, os itens da tabela independente da ordem de apresentação definida pela coluna cd_dados_apoio_ordem';

alter table salve.dados_apoio
    owner to postgres;

grant select, update, usage on sequence salve.dados_apoio_sq_dados_apoio_seq to usr_salve;

create index if not exists ix_dados_apoio_cd_dados_apoio
    on salve.dados_apoio (cd_dados_apoio)
    tablespace pg_default;

create index if not exists ix_dados_apoio_cd_sistema
    on salve.dados_apoio (cd_sistema)
    tablespace pg_default;

create index if not exists ix_dados_apoio_ds_dados_apoio
    on salve.dados_apoio (ds_dados_apoio)
    tablespace pg_default;

create index if not exists ix_dados_apoio_sq_dados_apoio_pai
    on salve.dados_apoio (sq_dados_apoio_pai)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.dados_apoio to usr_salve;

create table if not exists salve.dados_apoio_geo
(
    sq_dados_apoio_geo serial
        constraint pk_dados_apoio_geo
            primary key,
    sq_dados_apoio     bigint
        constraint fk_dados_apoio
            references salve.dados_apoio
            on update cascade on delete cascade,
    cd_geo             text,
    tx_geo             text,
    ge_geo             public.geometry
        constraint enforce_dims_geom
            check (public.st_ndims(ge_geo) = 2)
        constraint enforce_geotype_geom
            check (public.geometrytype(ge_geo) = 'MULTIPOLYGON'::text)
        constraint enforce_srid_geom
            check (public.st_srid(ge_geo) = 4674)
);

comment on table salve.dados_apoio_geo is 'Tabela para armazenamento das informações georefenciadas da tabela de apoio.';

comment on column salve.dados_apoio_geo.sq_dados_apoio_geo is 'Chave primaria e sequencial';

comment on column salve.dados_apoio_geo.sq_dados_apoio is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.dados_apoio_geo.cd_geo is 'Código identificador do shape ou polígono';

comment on column salve.dados_apoio_geo.tx_geo is 'Informações textuais sobre o shape ou polígono';

comment on column salve.dados_apoio_geo.ge_geo is 'Polígono';

alter table salve.dados_apoio_geo
    owner to postgres;

grant select, update, usage on sequence salve.dados_apoio_geo_sq_dados_apoio_geo_seq to usr_salve;

create index if not exists idx_dados_apoio_geo_geom
    on salve.dados_apoio_geo using gist (ge_geo)
    tablespace pg_default;

create index if not exists ix_dados_apoio_geo_dados_apoio
    on salve.dados_apoio_geo (sq_dados_apoio)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.dados_apoio_geo to usr_salve;

create table if not exists salve.efeito_ameaca
(
    sq_efeito_ameaca serial
        constraint pk_efeito_ameaca
            primary key,
    sq_efeito        bigint
        constraint fk_efeito_ameaca_efeito
            references salve.dados_apoio
            on update cascade on delete cascade,
    sq_ameaca        bigint
        constraint fk_efeito_ameaca_ameaca
            references salve.dados_apoio
            on update cascade on delete cascade
);

comment on table salve.efeito_ameaca is 'Tabela para restringir os efeitos por ameaca.';

comment on column salve.efeito_ameaca.sq_efeito_ameaca is 'Chave primaria sequencial';

comment on column salve.efeito_ameaca.sq_efeito is 'Chave estrangeira da tabela apoio TB_EFEITO_PRIM';

comment on column salve.efeito_ameaca.sq_ameaca is 'Chave estrangeira da tabela TB_CRITERIO_AMEACA_IUCN';

alter table salve.efeito_ameaca
    owner to postgres;

create index if not exists ix_efeito_ameaca_ameaca
    on salve.efeito_ameaca (sq_ameaca);

comment on index salve.ix_efeito_ameaca_ameaca is 'Indice pela coluna sq_ameaca';

grant delete, insert, references, select, trigger, truncate, update on salve.efeito_ameaca to usr_salve;

create table if not exists salve.esfera
(
    sq_esfera serial
        constraint pk_esfera
            primary key,
    no_esfera text not null
);

comment on table salve.esfera is 'Tabela que armazena a esfera de atividade, ou seja, o domínio de ação das Unidades Organizacionais. A esfera das Unidades Organizacionais e Unidades de Conservação pode ser federal, estadual e municipal.';

comment on column salve.esfera.sq_esfera is 'Chave primária da tabela';

comment on column salve.esfera.no_esfera is 'Coluna que armazena o nome da esfera da Unidade Organizacional.';

alter table salve.esfera
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.esfera to usr_salve;

create table if not exists salve.ficha_uso
(
    sq_ficha_uso serial
        constraint pk_ficha_uso
            primary key,
    sq_uso       bigint not null,
    sq_ficha     bigint not null,
    tx_ficha_uso text
);

alter table salve.ficha_uso
    owner to postgres;

create index if not exists ix_ficha_uso_ficha
    on salve.ficha_uso (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_uso_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_uso to usr_salve;

create table if not exists salve.informativo
(
    sq_informativo   serial
        constraint pk_informativo
            primary key,
    nu_informativo   integer,
    dt_informativo   date not null,
    in_publicado     char,
    de_tema          text,
    tx_informativo   text,
    no_arquivo       varchar(255),
    de_local_arquivo varchar(255),
    de_tipo_conteudo varchar(50),
    dt_inicio_alerta date,
    dt_fim_alerta    date,
    dt_inclusao      timestamp default CURRENT_TIMESTAMP,
    dt_alteracao     timestamp default CURRENT_TIMESTAMP
);

comment on table salve.informativo is 'Tabela para armazenar os informativos sobre os sistema';

comment on column salve.informativo.nu_informativo is 'Numero do informativo informado pelo usuario';

comment on column salve.informativo.dt_informativo is 'Data elaboracao do informativo';

comment on column salve.informativo.in_publicado is 'Infomacao S/N se o informativo esta publicado para ser visualizado pelos usuarios';

comment on column salve.informativo.de_tema is 'Breve descricao ou o tema do informativo';

comment on column salve.informativo.tx_informativo is 'Descricao completa do informativo';

comment on column salve.informativo.no_arquivo is 'Nome do arquivo pdf anexado quando houver';

comment on column salve.informativo.de_local_arquivo is 'Nome do arquivo no disco, gerado a partir do hash do conteudo do arquivo';

comment on column salve.informativo.de_tipo_conteudo is 'Conteudo mime baseado no conteudo do arquivo';

comment on column salve.informativo.dt_inicio_alerta is 'Data inicio para exibicao automatica ao efetuar login no sistema';

comment on column salve.informativo.dt_fim_alerta is 'Data final para exibicao automatica ao efetuar login no sistema';

comment on column salve.informativo.dt_inclusao is 'Data de inclusao do informativo no sistema';

comment on column salve.informativo.dt_alteracao is 'Data da ultima alteracao do informativo';

alter table salve.informativo
    owner to postgres;

grant select, update, usage on sequence salve.informativo_sq_informativo_seq to usr_salve;

create unique index if not exists ix_informativo_nu_informativo
    on salve.informativo (nu_informativo);

comment on index salve.ix_informativo_nu_informativo is 'Indice pelo número do informativo';

grant delete, insert, references, select, trigger, truncate, update on salve.informativo to usr_salve;

create table if not exists salve.iucn_acao_conservacao
(
    sq_iucn_acao_conservacao       serial
        constraint pk_iucn_acao_conservacao
            primary key,
    cd_iucn_acao_conservacao       text not null,
    ds_iucn_acao_conservacao       text not null,
    ds_iucn_acao_conservacao_ordem text,
    dt_inclusao                    timestamp default CURRENT_TIMESTAMP,
    dt_alteracao                   timestamp default CURRENT_TIMESTAMP
);

comment on column salve.iucn_acao_conservacao.cd_iucn_acao_conservacao is 'Codigo da acao de conservacao. Ex: 1, 1.1, 1.2 etc';

comment on column salve.iucn_acao_conservacao.ds_iucn_acao_conservacao is 'Descricao da acao de conservacao';

comment on column salve.iucn_acao_conservacao.ds_iucn_acao_conservacao_ordem is 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';

comment on column salve.iucn_acao_conservacao.dt_inclusao is 'Data e hora que o registro foi criado no banco de dados';

comment on column salve.iucn_acao_conservacao.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_acao_conservacao
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_acao_conservacao to usr_salve;

create table if not exists salve.iucn_acao_conservacao_apoio
(
    sq_iucn_acao_conservacao_apoio serial
        constraint pk_iucn_acao_conservacao_apoio
            primary key,
    sq_iucn_acao_conservacao       bigint                              not null
        constraint fk_iucn_acao_cons_apoio_iucn_acao_cons
            references salve.iucn_acao_conservacao
            on update cascade on delete restrict,
    sq_dados_apoio                 bigint                              not null
        constraint fk_iucn_acao_cons_apoio_dados_apoio
            references salve.dados_apoio
            on update cascade on delete restrict,
    dt_inclusao                    timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao                   timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao_apoio is 'Chave primaria sequencial da tabela iucn_acao_conservacao_apoio';

comment on column salve.iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao is 'Chave estrangeira da tabela iucn_acao_conservacao';

comment on column salve.iucn_acao_conservacao_apoio.sq_dados_apoio is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.iucn_acao_conservacao_apoio.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_acao_conservacao_apoio.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_acao_conservacao_apoio
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_acao_conservacao_apoio to usr_salve;

create table if not exists salve.iucn_ameaca
(
    sq_iucn_ameaca       serial
        constraint pk_iucn_ameaca
            primary key,
    cd_iucn_ameaca       text not null,
    ds_iucn_ameaca       text not null,
    ds_iucn_ameaca_ordem text,
    dt_inclusao          timestamp default CURRENT_TIMESTAMP,
    dt_alteracao         timestamp default CURRENT_TIMESTAMP
);

comment on column salve.iucn_ameaca.cd_iucn_ameaca is 'Codigo da ameaca. Ex: 1, 1.1, 1.2 etc';

comment on column salve.iucn_ameaca.ds_iucn_ameaca is 'Descricao da ameaca';

comment on column salve.iucn_ameaca.ds_iucn_ameaca_ordem is 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';

comment on column salve.iucn_ameaca.dt_inclusao is 'Data e hora que o registro foi criado no banco de dados';

comment on column salve.iucn_ameaca.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_ameaca
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_ameaca to usr_salve;

create table if not exists salve.iucn_ameaca_apoio
(
    sq_iucn_ameaca_apoio serial
        constraint pk_iucn_ameaca_apoio
            primary key,
    sq_iucn_ameaca       bigint                              not null
        constraint fk_iucn_ameaca_apoio_iucn_ameaca
            references salve.iucn_ameaca
            on update cascade on delete restrict,
    sq_dados_apoio       bigint                              not null
        constraint fk_iucn_ameaca_apoio_dados_apoio
            references salve.dados_apoio
            on update cascade on delete restrict,
    dt_inclusao          timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao         timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_ameaca_apoio.sq_iucn_ameaca_apoio is 'Chave primaria sequencial da tabela iucn_ameaca_apoio';

comment on column salve.iucn_ameaca_apoio.sq_iucn_ameaca is 'Chave estrangeira da tabela iucn_ameaca';

comment on column salve.iucn_ameaca_apoio.sq_dados_apoio is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.iucn_ameaca_apoio.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_ameaca_apoio.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_ameaca_apoio
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_ameaca_apoio to usr_salve;

create table if not exists salve.iucn_habitat
(
    sq_iucn_habitat       serial
        constraint pk_iucn_habitat
            primary key,
    cd_iucn_habitat       text not null,
    ds_iucn_habitat       text not null,
    ds_iucn_habitat_ordem text,
    dt_inclusao           timestamp default CURRENT_TIMESTAMP,
    dt_alteracao          timestamp default CURRENT_TIMESTAMP
);

comment on column salve.iucn_habitat.cd_iucn_habitat is 'Codigo da habitat. Ex: 1, 1.1, 1.2 etc';

comment on column salve.iucn_habitat.ds_iucn_habitat is 'Descricao da habitat';

comment on column salve.iucn_habitat.ds_iucn_habitat_ordem is 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';

comment on column salve.iucn_habitat.dt_inclusao is 'Data e hora que o registro foi criado no banco de dados';

comment on column salve.iucn_habitat.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_habitat
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_habitat to usr_salve;

create table if not exists salve.iucn_habitat_apoio
(
    sq_iucn_habitat_apoio serial
        constraint pk_iucn_habitat_apoio
            primary key,
    sq_iucn_habitat       bigint                              not null
        constraint fk_iucn_habitat_apoio_iucn_habitat
            references salve.iucn_habitat
            on update cascade on delete restrict,
    sq_dados_apoio        bigint                              not null
        constraint fk_iucn_habitat_apoio_dados_apoio
            references salve.dados_apoio
            on update cascade on delete restrict,
    dt_inclusao           timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao          timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_habitat_apoio.sq_iucn_habitat_apoio is 'Chave primaria sequencial da tabela iucn_habitat_apoio';

comment on column salve.iucn_habitat_apoio.sq_iucn_habitat is 'Chave estrangeira da tabela iucn_habitat';

comment on column salve.iucn_habitat_apoio.sq_dados_apoio is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.iucn_habitat_apoio.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_habitat_apoio.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_habitat_apoio
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_habitat_apoio to usr_salve;

create table if not exists salve.iucn_motivo_mudanca
(
    sq_iucn_motivo_mudanca       serial
        constraint pk_iucn_motivo_mudanca
            primary key,
    cd_iucn_motivo_mudanca       text not null,
    ds_iucn_motivo_mudanca       text not null,
    ds_iucn_motivo_mudanca_ordem text,
    dt_inclusao                  timestamp default CURRENT_TIMESTAMP,
    dt_alteracao                 timestamp default CURRENT_TIMESTAMP
);

comment on column salve.iucn_motivo_mudanca.cd_iucn_motivo_mudanca is 'Codigo do motivo da mudanca. Ex: 1, 1.1, 1.2 etc';

comment on column salve.iucn_motivo_mudanca.ds_iucn_motivo_mudanca is 'Descricao do motivo da mudanca';

comment on column salve.iucn_motivo_mudanca.ds_iucn_motivo_mudanca_ordem is 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';

comment on column salve.iucn_motivo_mudanca.dt_inclusao is 'Data e hora que o registro foi criado no banco de dados';

comment on column salve.iucn_motivo_mudanca.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_motivo_mudanca
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_motivo_mudanca to usr_salve;

create table if not exists salve.iucn_motivo_mudanca_apoio
(
    sq_iucn_motivo_mudanca_apoio serial
        constraint pk_iucn_motivo_mudanca_apoio
            primary key,
    sq_iucn_motivo_mudanca       bigint                              not null
        constraint fk_iucn_motivo_mudanca_apoio_iucn_motivo_mudanca
            references salve.iucn_motivo_mudanca
            on update cascade on delete restrict,
    sq_dados_apoio               bigint                              not null
        constraint fk_iucn_motivo_mudanca_apoio_dados_apoio
            references salve.dados_apoio
            on update cascade on delete restrict,
    dt_inclusao                  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao                 timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca_apoio is 'Chave primaria sequencial da tabela iucn_motivo_mudanca_apoio';

comment on column salve.iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca is 'Chave estrangeira da tabela iucn_motivo_mudanca';

comment on column salve.iucn_motivo_mudanca_apoio.sq_dados_apoio is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.iucn_motivo_mudanca_apoio.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_motivo_mudanca_apoio.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_motivo_mudanca_apoio
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_motivo_mudanca_apoio to usr_salve;

create table if not exists salve.iucn_pesquisa
(
    sq_iucn_pesquisa       serial
        constraint pk_iucn_pesquisa
            primary key,
    cd_iucn_pesquisa       text                                not null,
    ds_iucn_pesquisa       text                                not null,
    ds_iucn_pesquisa_ordem text,
    dt_inclusao            timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao           timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_pesquisa.cd_iucn_pesquisa is 'Codigo. Ex: 1, 1.1, 1.2 etc';

comment on column salve.iucn_pesquisa.ds_iucn_pesquisa is 'Descricao da pesquisa';

comment on column salve.iucn_pesquisa.ds_iucn_pesquisa_ordem is 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';

comment on column salve.iucn_pesquisa.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_pesquisa.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_pesquisa
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_pesquisa to usr_salve;

create table if not exists salve.iucn_pesquisa_apoio
(
    sq_iucn_pesquisa_apoio serial
        constraint pk_iucn_pesquisa_apoio
            primary key,
    sq_iucn_pesquisa       bigint                              not null
        constraint fk_iucn_pes_nec_apo_iucn_pes_nec
            references salve.iucn_pesquisa
            on update cascade on delete restrict,
    sq_dados_apoio         bigint                              not null
        constraint fk_iucn_pes_nec_apo_dad_apo
            references salve.dados_apoio
            on update cascade on delete restrict,
    dt_inclusao            timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao           timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_pesquisa_apoio.sq_iucn_pesquisa_apoio is 'Chave primaria sequencial da tabela iucn_pesquisa_apoio';

comment on column salve.iucn_pesquisa_apoio.sq_iucn_pesquisa is 'Chave estrangeira da tabela iucn_pesquisa';

comment on column salve.iucn_pesquisa_apoio.sq_dados_apoio is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.iucn_pesquisa_apoio.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_pesquisa_apoio.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_pesquisa_apoio
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_pesquisa_apoio to usr_salve;

create table if not exists salve.iucn_uso
(
    sq_iucn_uso       serial
        constraint pk_iucn_uso
            primary key,
    cd_iucn_uso       text                                not null,
    ds_iucn_uso       text                                not null,
    ds_iucn_uso_ordem text                                not null,
    dt_inclusao       timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao      timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.iucn_uso.cd_iucn_uso is 'Codigo do uso. Ex: 1, 1.1, 1.2 etc';

comment on column salve.iucn_uso.ds_iucn_uso is 'Descricao do uso';

comment on column salve.iucn_uso.ds_iucn_uso_ordem is 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';

comment on column salve.iucn_uso.dt_inclusao is 'Data e hora que o registro foi adicionado no banco de dados';

comment on column salve.iucn_uso.dt_alteracao is 'Data e hora da ultima alteracao do registro';

alter table salve.iucn_uso
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_uso to usr_salve;

create table if not exists salve.pais
(
    sq_pais      serial
        constraint pk_pais
            primary key,
    no_pais      varchar(50)                         not null,
    co_pais      varchar(3),
    ge_pais      public.geometry
        constraint enforce_dims_the_geom
            check (public.st_ndims(ge_pais) = 2)
        constraint enforce_geotype_the_geom
            check ((public.geometrytype(ge_pais) = 'MULTIPOLYGON'::text) OR (ge_pais IS NULL))
        constraint enforce_srid_the_geom
            check (public.st_srid(ge_pais) = 4674),
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

alter table salve.pais
    owner to postgres;

grant select, update, usage on sequence salve.pais_sq_pais_seq to usr_salve;

create index if not exists ix_pais_geo
    on salve.pais using gist (ge_pais)
    tablespace pg_default;

create unique index if not exists ixu_pais_cod_iso
    on salve.pais (co_pais)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.pais to usr_salve;

create table if not exists salve.perfil
(
    sq_perfil    serial
        constraint pk_perfil
            primary key,
    no_perfil    text                                not null,
    cd_sistema   text                                not null,
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP
);

comment on table salve.perfil is 'Tabela para armazenar os perfis de acesso do sistema';

comment on column salve.perfil.sq_perfil is 'Chave primária e sequencial';

comment on column salve.perfil.no_perfil is 'Nome do perfil';

comment on column salve.perfil.cd_sistema is 'Código interno para utilização do sistema';

comment on column salve.perfil.dt_inclusao is 'Data de inclusão no sistema';

comment on column salve.perfil.dt_alteracao is 'Data da última alteração';

alter table salve.perfil
    owner to postgres;

grant select, update, usage on sequence salve.perfil_sq_perfil_seq to usr_salve;

create unique index if not exists ixu_perfil_codigo
    on salve.perfil (cd_sistema)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.perfil to usr_salve;

create table if not exists salve.pessoa
(
    sq_pessoa    serial
        constraint pk_pessoa
            primary key,
    no_pessoa    text                                not null,
    de_email     text,
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.pessoa is 'Tabela para armazenar as pessoas físicas e jurídicas do sistema';

comment on column salve.pessoa.sq_pessoa is 'Chave primária e sequencial';

comment on column salve.pessoa.no_pessoa is 'Nome completo da pessoa';

comment on column salve.pessoa.de_email is 'Email eletrônico da pessoa';

comment on column salve.pessoa.dt_inclusao is 'Data de inclusão no banco de dados';

comment on column salve.pessoa.dt_alteracao is 'Data da última alteração';

alter table salve.pessoa
    owner to postgres;

grant select, update, usage on sequence salve.pessoa_sq_pessoa_seq to usr_salve;

create table if not exists salve.instituicao
(
    sq_pessoa      bigint                  not null
        constraint pk_instituicao
            primary key
        constraint fk_instituicao_pessoa
            references salve.pessoa
            on update restrict on delete restrict,
    sq_pessoa_pai  bigint
        constraint fk_instituicao_pai
            references salve.instituicao
            on update restrict on delete cascade,
    sq_tipo        bigint
        constraint fk_instituicao_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sg_instituicao text,
    nu_cnpj        varchar(14),
    no_contato     text,
    nu_telefone    text,
    dt_alteracao   timestamp default CURRENT_TIMESTAMP,
    st_interna     boolean   default false not null
);

comment on table salve.instituicao is 'Tabela para armazenar as instituições, unidades organizacionais, pessoas jurídicas, ucs etc';

comment on column salve.instituicao.sq_pessoa is 'Chave primária estrangeira da tabela pessoa';

comment on column salve.instituicao.sq_pessoa_pai is 'Chave auto-relacionamento para criação da hierarquia entre as instituições';

comment on column salve.instituicao.sq_tipo is 'Chave estrangeira da tabela dados_apoio';

comment on column salve.instituicao.sg_instituicao is 'Sigla da intituição/departamento ou da unidade organizacional';

comment on column salve.instituicao.nu_cnpj is 'Número do CNPJ';

comment on column salve.instituicao.no_contato is 'Nome da pessoa para contato na instituição';

comment on column salve.instituicao.nu_telefone is 'Número do(s) telefone(s) da instituição';

comment on column salve.instituicao.dt_alteracao is 'Data da última alteração';

comment on column salve.instituicao.st_interna is 'Indicar se a instituição faz ou não parte da estrutura organizacional';

alter table salve.instituicao
    owner to postgres;

create table if not exists salve.ciclo_consulta
(
    sq_ciclo_consulta  serial
        constraint pk_ciclo_consulta
            primary key,
    sq_ciclo_avaliacao bigint              not null
        constraint fk_ciclo_cons_ciclo
            references salve.ciclo_avaliacao
            on update restrict on delete cascade,
    sq_tipo_consulta   bigint              not null
        constraint fk_ciclo_cons_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    dt_inicio          date                not null,
    dt_fim             date                not null,
    tx_email           text,
    de_consulta        text,
    de_titulo_consulta text,
    sq_unidade_org     bigint default 3164 not null
        constraint fk_ciclo_cons_unidade_org
            references salve.instituicao
            on update restrict on delete restrict,
    sg_consulta        text
);

comment on column salve.ciclo_consulta.sq_tipo_consulta is 'Ampla - todos podem contribuir ou Restrita-somente para os especialistas';

comment on column salve.ciclo_consulta.sq_unidade_org is 'Id da unidade organizacional responsÃ¡vel pela consulta';

comment on column salve.ciclo_consulta.sg_consulta is 'Nome da consulta que será exibido na consulta Ampla / Direta';

alter table salve.ciclo_consulta
    owner to postgres;

create index if not exists ix_ciclo_consulta_ciclo_avaliacao
    on salve.ciclo_consulta (sq_ciclo_avaliacao)
    tablespace pg_default;

comment on index salve.ix_ciclo_consulta_ciclo_avaliacao is 'Indice coluna id do ciclo avaliacao';

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_consulta to usr_salve;

create index if not exists ix_instituicao_pai_fk
    on salve.instituicao (sq_pessoa_pai)
    tablespace pg_default;

create unique index if not exists ixu_instituicao_cnpj
    on salve.instituicao (nu_cnpj)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.instituicao to usr_salve;

create index if not exists ix_pessoa_email
    on salve.pessoa (de_email)
    tablespace pg_default;

create index if not exists ix_trgm_pessoa_nome
    on salve.pessoa using gin (fn_remove_acentuacao(no_pessoa::character varying) public.gin_trgm_ops)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.pessoa to usr_salve;

create table if not exists salve.pessoa_fisica
(
    sq_pessoa      bigint not null
        constraint pk_pessoa_fisica
            primary key
        constraint fk_pessoa_fisica_pessoa
            references salve.pessoa
            on update restrict on delete restrict,
    sq_instituicao bigint
        constraint fk_pessoa_fisica_instituicao
            references salve.instituicao
            on update cascade on delete restrict,
    nu_cpf         varchar(11),
    dt_alteracao   timestamp default CURRENT_TIMESTAMP
);

comment on table salve.pessoa_fisica is 'Tabela para armazenar as pessoas físicas do sistema';

comment on column salve.pessoa_fisica.sq_pessoa is 'Chave estrangeira e primária';

comment on column salve.pessoa_fisica.sq_instituicao is 'Chave estrangeira da tabela instituicao';

comment on column salve.pessoa_fisica.nu_cpf is 'Número do CPF';

comment on column salve.pessoa_fisica.dt_alteracao is 'Data da última alteração';

alter table salve.pessoa_fisica
    owner to postgres;

create table if not exists salve.ciclo_consulta_pessoa
(
    sq_ciclo_consulta_pessoa serial
        constraint pk_ciclo_consulta_pessoa
            primary key,
    sq_ciclo_consulta        bigint not null
        constraint fk_ciclo_cons_pessoa_ciclo_cons
            references salve.ciclo_consulta
            on update restrict on delete cascade,
    sq_pessoa                bigint not null
        constraint fk_ciclo_cons_pessoa_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    sq_situacao              bigint not null
        constraint fk_ciclo_cons_pessoa_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    de_email                 text,
    dt_envio                 date,
    de_observacao            text,
    sq_instituicao           bigint
        constraint fk_ciclo_cons_pessoa_instituicao
            references salve.instituicao
            on update cascade on delete restrict
);

comment on column salve.ciclo_consulta_pessoa.de_observacao is 'Pode ser utilizado para  dizer que a pessoa ?um especialista na hora de avaliar o seu coment?io.';

comment on column salve.ciclo_consulta_pessoa.sq_instituicao is 'Informar a instituicao do convidado da consulta ou revisao';

alter table salve.ciclo_consulta_pessoa
    owner to postgres;

create index if not exists ix_ciclo_consulta_pessoa_ciclo
    on salve.ciclo_consulta_pessoa (sq_ciclo_consulta)
    tablespace pg_default;

comment on index salve.ix_ciclo_consulta_pessoa_ciclo is 'Indice coluna id do ciclo consulta';

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_consulta_pessoa to usr_salve;

create table if not exists salve.iucn_transmissao
(
    sq_iucn_transmissao serial
        constraint pk_iucn_transmissao
            primary key,
    sq_pessoa           bigint                              not null
        constraint fk_iucn_transmissao_sq_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_transmissao      timestamp default CURRENT_TIMESTAMP not null,
    no_arquivo_zip      text,
    tx_log              text
);

comment on column salve.iucn_transmissao.sq_iucn_transmissao is 'Chave primária sequencial da tabela iucn_transmissao';

comment on column salve.iucn_transmissao.sq_pessoa is 'Nome da pessoa/usuario que realizou a transmissao';

comment on column salve.iucn_transmissao.dt_transmissao is 'Data que foi feita a transmissão do arquivo zip para o sis/iucn';

comment on column salve.iucn_transmissao.no_arquivo_zip is 'Nome do arquivo zip transmitido localizado no diretorio /data/salve/iucn';

comment on column salve.iucn_transmissao.tx_log is 'Lista de erros encontrados na criacao do arquivo zip para envio ao sis/iucn';

alter table salve.iucn_transmissao
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_transmissao to usr_salve;

create table if not exists salve.oficina
(
    sq_oficina             serial
        constraint pk_oficina
            primary key,
    sq_unidade_org         bigint
        constraint fk_ficina_unid_org
            references salve.instituicao
            on update restrict on delete restrict,
    sq_ciclo_avaliacao     bigint              not null
        constraint fk_oficina_ciclo_aval
            references salve.ciclo_avaliacao
            on update restrict on delete cascade,
    sq_tipo_oficina        bigint              not null
        constraint fk_oficina_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_pessoa              bigint
        constraint fk_oficina_pessoa_pf
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    no_oficina             text                not null,
    sg_oficina             text                not null,
    de_local               text                not null,
    dt_inicio              date                not null,
    dt_fim                 date                not null,
    tx_encaminhamento      text,
    tx_email_participantes text,
    de_email_ponto_focal   text,
    tx_email_validadores   text,
    sq_situacao            bigint default 1041 not null,
    de_link_sei            text,
    sq_fonte_recurso       bigint
        constraint fk_oficina_fonte_recurso
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on column salve.oficina.sq_unidade_org is 'Id do unidade organizacional que está organizando a oficina. Oficina de validação não terá unidade.';

comment on column salve.oficina.sq_pessoa is 'Id do ponto focal. Oficina de validação não terá o ponto focal';

comment on column salve.oficina.tx_email_participantes is 'Cópia do email enviado aos participantes';

comment on column salve.oficina.de_email_ponto_focal is 'email do ponto focal que enviou os emails aos participantes';

comment on column salve.oficina.sq_situacao is 'Situação da oficina: aberta ou encerrada';

comment on column salve.oficina.de_link_sei is 'Link para visualizar documento no sistema SEI';

comment on column salve.oficina.sq_fonte_recurso is 'Id da fonte de recurso utilizada para realizacao da oficina.';

alter table salve.oficina
    owner to postgres;

create table if not exists salve.ciclo_avaliacao_validador
(
    sq_ciclo_avaliacao_validador serial
        constraint pk_ciclo_avaliacao_validador
            primary key,
    sq_validador                 bigint not null
        constraint fk_ciclo_aval_validador
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    sq_situacao_convite          bigint not null
        constraint fk_ciclo_aval_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ciclo_avaliacao           bigint not null
        constraint fk_ciclo_aval_ciclo_aval_valid
            references salve.ciclo_avaliacao
            on update restrict on delete cascade,
    de_email                     text,
    dt_email                     timestamp with time zone,
    dt_resposta                  timestamp with time zone,
    dt_validade_convite          date,
    no_usuario                   text,
    de_hash                      text,
    sq_oficina                   bigint
        constraint fk_ciclo_aval_validador_oficina
            references salve.oficina
            on update restrict on delete cascade
);

comment on column salve.ciclo_avaliacao_validador.dt_resposta is 'Data em que o validador respondeu ao convite aceitando ou n?';

comment on column salve.ciclo_avaliacao_validador.de_hash is 'Hash de resposta automática pela api';

comment on column salve.ciclo_avaliacao_validador.sq_oficina is 'Chave estrangeira da relação com a tabela oficina';

alter table salve.ciclo_avaliacao_validador
    owner to postgres;

create index if not exists ix_ciclo_ava_valid_hash
    on salve.ciclo_avaliacao_validador (de_hash)
    tablespace pg_default;

create index if not exists ix_ciclo_ava_valid_validador
    on salve.ciclo_avaliacao_validador (sq_validador)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_avaliacao_validador to usr_salve;

create index if not exists ix_oficina_sq_tipo
    on salve.oficina (sq_ciclo_avaliacao, sq_tipo_oficina);

create unique index if not exists ixu_oficina_pk
    on salve.oficina (sq_oficina)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.oficina to usr_salve;

create table if not exists salve.oficina_anexo
(
    sq_oficina_anexo serial
        constraint pk_oficina_anexo
            primary key,
    sq_oficina       bigint                                 not null
        constraint fk_oficina_anexo_oficina
            references salve.oficina
            on update restrict on delete cascade,
    sq_pessoa        bigint                                 not null
        constraint fk_oficina_anexo_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    no_arquivo       text                                   not null,
    de_tipo_conteudo text                                   not null,
    de_local_arquivo text                                   not null,
    de_legenda       text,
    dt_inclusao      timestamp with time zone default now() not null,
    in_doc_final     boolean                  default false
);

comment on table salve.oficina_anexo is 'Anexos da oficina, docs, fotos';

comment on column salve.oficina_anexo.in_doc_final is 'Informar se o documento anexo é o documento final da oficina ou não';

alter table salve.oficina_anexo
    owner to postgres;

create index if not exists ix_oficina_anexo_oficina
    on salve.oficina_anexo (sq_oficina)
    tablespace pg_default;

comment on index salve.ix_oficina_anexo_oficina is 'Indice coluna id da oficina';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_anexo to usr_salve;

create table if not exists salve.oficina_memoria
(
    sq_oficina_memoria serial
        constraint pk_oficina_memoria
            primary key,
    sq_oficina         bigint not null
        constraint fk_oficina_memoria_oficina
            references salve.oficina
            on update restrict on delete cascade,
    tx_memoria         text,
    dt_inicio_oficina  date   not null,
    dt_fim_oficina     date   not null,
    de_local_oficina   text,
    de_salas           text
);

comment on table salve.oficina_memoria is 'Tabela para registro da memória da reunião preparatória';

comment on column salve.oficina_memoria.dt_inicio_oficina is 'Data proposta para inicio da oficina de validacao';

comment on column salve.oficina_memoria.dt_fim_oficina is 'Data proposta para finalizar a oficina de validacao';

comment on column salve.oficina_memoria.de_local_oficina is 'Registrar o local de realizaÃ§Ã£o da oficina';

comment on column salve.oficina_memoria.de_salas is 'Número de salas reservadas para a realizaÃ§Ã£o da oficina';

alter table salve.oficina_memoria
    owner to postgres;

create index if not exists ix_oficina_memoria_oficina
    on salve.oficina_memoria (sq_oficina)
    tablespace pg_default;

comment on index salve.ix_oficina_memoria_oficina is 'Indice coluna id da oficina';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_memoria to usr_salve;

create table if not exists salve.oficina_memoria_anexo
(
    sq_oficina_memoria_anexo serial
        constraint pk_oficina_memoria_anexo
            primary key,
    sq_oficina_memoria       bigint                                 not null
        constraint fk_oficina_memoria_anexo_oficina
            references salve.oficina_memoria
            on update restrict on delete cascade,
    sq_pessoa                bigint                                 not null
        constraint fk_oficina_memoria_anexo_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    no_arquivo               text                                   not null,
    de_tipo_conteudo         text                                   not null,
    de_local_arquivo         text                                   not null,
    de_legenda               text,
    dt_inclusao              timestamp with time zone default now() not null
);

comment on table salve.oficina_memoria_anexo is 'Tabela para armazenamento dos anexos da memória da reunião preparatória.';

alter table salve.oficina_memoria_anexo
    owner to postgres;

create index if not exists ix_oficina_memoria_anexo_oficina
    on salve.oficina_memoria_anexo (sq_oficina_memoria)
    tablespace pg_default;

comment on index salve.ix_oficina_memoria_anexo_oficina is 'Indice coluna id da oficina_memoria';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_memoria_anexo to usr_salve;

create table if not exists salve.oficina_memoria_participante
(
    sq_oficina_memoria_participante integer default nextval('salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq'::regclass) not null
        constraint pk_oficina_memoria_participante
            primary key,
    sq_oficina_memoria              bigint                                                                                                     not null
        constraint fk_oficina_memoria_participante_oficina
            references salve.oficina_memoria
            on update restrict on delete cascade,
    sq_papel                        bigint                                                                                                     not null,
    no_participante                 text                                                                                                       not null
);

comment on table salve.oficina_memoria_participante is 'Tabela para armazenamento dos participantes da memoria da reunião preparatória.';

alter table salve.oficina_memoria_participante
    owner to postgres;

alter sequence salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq owned by salve.oficina_memoria_participante.sq_oficina_memoria_participante;

create index if not exists ix_oficina_mem_part_ofic_mem_papel_pessoa
    on salve.oficina_memoria_participante (sq_oficina_memoria, sq_papel, no_participante)
    tablespace pg_default;

comment on index salve.ix_oficina_mem_part_ofic_mem_papel_pessoa is 'Indice chave estrangeira oficina memória, papel e nome participante';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_memoria_participante to usr_salve;

create table if not exists salve.oficina_participante
(
    sq_oficina_participante serial
        constraint pk_oficina_participante
            primary key,
    sq_oficina              bigint not null
        constraint fk_oficina_part_oficina
            references salve.oficina
            on delete cascade,
    sq_pessoa               bigint not null
        constraint fk_oficina_part_pessoa
            references salve.pessoa
            on delete restrict,
    sq_situacao             bigint
        constraint fk_oficina_part_situacao
            references salve.dados_apoio
            on delete restrict,
    sq_instituicao          bigint not null
        constraint fk_oficina_participante_instituicao
            references salve.instituicao
            on update restrict on delete restrict,
    de_email                text   not null,
    dt_email                timestamp,
    de_assunto_email        text
);

alter table salve.oficina_participante
    owner to postgres;

create table if not exists salve.oficina_anexo_assinatura
(
    sq_oficina_anexo_assinatura serial
        constraint pk_oficina_anexo_assinatura
            primary key,
    sq_oficina_anexo            bigint
        constraint fk_oficina_anexo_assinatura_oficina_anexo
            references salve.oficina_anexo
            on update cascade on delete cascade,
    sq_oficina_participante     bigint
        constraint fk_oficina_anexo_assinatura_oficina_participante
            references salve.oficina_participante
            on update cascade on delete cascade,
    de_hash                     text,
    dt_assinatura               timestamp,
    dt_email                    timestamp,
    tx_observacao               text
);

comment on table salve.oficina_anexo_assinatura is 'Tabela para registrar a assinatura eletronica dos participantes da oficina';

comment on column salve.oficina_anexo_assinatura.sq_oficina_anexo_assinatura is 'Chave primaria auto incrementada';

comment on column salve.oficina_anexo_assinatura.sq_oficina_anexo is 'Chave estrangeira da tabela oficina_anexo';

comment on column salve.oficina_anexo_assinatura.sq_oficina_participante is 'Chave estrangeira da tabela oficina_participante';

comment on column salve.oficina_anexo_assinatura.de_hash is 'Hash para confirmacao da assinatura via link enviado por email';

comment on column salve.oficina_anexo_assinatura.dt_assinatura is 'Data que o participante assinou o documento eletronicamente.';

comment on column salve.oficina_anexo_assinatura.dt_email is 'Data do envio do email para os participante com o link da assinatura eletronica.';

comment on column salve.oficina_anexo_assinatura.tx_observacao is 'Campo para registro de observacoes sobre a assinatura eletronica';

alter table salve.oficina_anexo_assinatura
    owner to postgres;

create index if not exists ix_oficina_anexo_assinatura_hash
    on salve.oficina_anexo_assinatura (de_hash);

comment on index salve.ix_oficina_anexo_assinatura_hash is 'Indice para otimizar a pesquisa realizada pelo modulo de assinatura eletronica ao clicar no hash.';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_anexo_assinatura to usr_salve;

create index if not exists ix_oficina_part_oficina
    on salve.oficina_participante (sq_oficina)
    tablespace pg_default;

comment on index salve.ix_oficina_part_oficina is 'Indice coluna id da oficina';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_participante to usr_salve;

create unique index if not exists ixu_pessoa_fisica_cpf
    on salve.pessoa_fisica (nu_cpf)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.pessoa_fisica to usr_salve;

create table if not exists salve.planilha
(
    sq_planilha        serial
        constraint pk_planilha
            primary key,
    sq_ciclo_avaliacao bigint
        constraint fk_planilha_ciclo_avaliacao
            references salve.ciclo_avaliacao
            on update restrict on delete cascade,
    sq_unidade_org     bigint                not null
        constraint fk_planilha_unidade_org
            references salve.instituicao
            on update restrict on delete restrict,
    sq_pessoa          bigint                not null
        constraint fk_planilha_usuario
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    no_arquivo         text                  not null,
    de_caminho         text                  not null,
    de_comentario      text,
    dt_envio           timestamp             not null,
    no_contexto        text,
    tx_log             text,
    nu_percentual      integer,
    in_processando     boolean default false not null
);

alter table salve.planilha
    owner to postgres;

create index if not exists ix_planilha_unidade_org
    on salve.planilha (sq_unidade_org)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.planilha to usr_salve;

create table if not exists salve.planilha_linha
(
    sq_planilha_linha  serial
        constraint pk_planilha_linha
            primary key,
    sq_planilha        bigint not null
        constraint fk_planilha_linha_planilha
            references salve.planilha
            on update restrict on delete cascade,
    tx_nome_cientifico text,
    nu_linha           bigint,
    tx_erros           text
);

alter table salve.planilha_linha
    owner to postgres;

create index if not exists ix_planilha_linha_planilha
    on salve.planilha_linha (sq_planilha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.planilha_linha to usr_salve;

create table if not exists salve.plano_acao
(
    sq_plano_acao serial
        constraint pk_plano_acao
            primary key,
    sg_plano_acao text,
    tx_plano_acao text
);

alter table salve.plano_acao
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.plano_acao to usr_salve;

create table if not exists salve.regiao
(
    sq_regiao    serial
        constraint pk_regiao
            primary key,
    no_regiao    varchar(20)                         not null,
    nu_ordem     integer,
    ge_regiao    public.geometry
        constraint enforce_dims_the_geom
            check (public.st_ndims(ge_regiao) = 2)
        constraint enforce_geotype_the_geom
            check ((public.geometrytype(ge_regiao) = 'MULTIPOLYGON'::text) OR (ge_regiao IS NULL))
        constraint enforce_srid_the_geom
            check (public.st_srid(ge_regiao) = 4674),
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

alter table salve.regiao
    owner to postgres;

grant select, update, usage on sequence salve.regiao_sq_regiao_seq to usr_salve;

create table if not exists salve.estado
(
    sq_estado    serial
        constraint pk_estado
            primary key,
    sg_estado    char(2)                             not null,
    sq_pais      bigint                              not null
        constraint fk_estado_pais
            references salve.pais
            on update cascade on delete restrict,
    no_estado    varchar(50)                         not null,
    ge_estado    public.geometry
        constraint enforce_geotype_ge_estado
            check ((public.geometrytype(ge_estado) = 'MULTIPOLYGON'::text) OR (ge_estado IS NULL))
        constraint enforce_srid_ge_estado
            check (public.st_srid(ge_estado) = 4674)
        constraint estado_ge_estado_check
            check (public.st_ndims(ge_estado) = 2),
    sq_regiao    bigint
        constraint fk_estado_regiao
            references salve.regiao
            on update cascade on delete restrict,
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

alter table salve.estado
    owner to postgres;

grant select, update, usage on sequence salve.estado_sq_estado_seq to usr_salve;

create index if not exists ix_estado_geo
    on salve.estado using gist (ge_estado)
    tablespace pg_default;

create unique index if not exists ixu_estado_nome
    on salve.estado (no_estado)
    tablespace pg_default;

create unique index if not exists ixu_estado_sigla
    on salve.estado (sg_estado)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.estado to usr_salve;

create table if not exists salve.municipio
(
    sq_municipio serial
        constraint pk_municipio
            primary key,
    sq_estado    bigint                              not null
        constraint fk_municipio_estado
            references salve.estado
            on update cascade on delete cascade,
    no_municipio text                                not null,
    co_ibge      integer                             not null,
    ge_municipio public.geometry
        constraint enforce_dims_the_geom
            check (public.st_ndims(ge_municipio) = 2)
        constraint enforce_geotype_the_geom
            check ((public.geometrytype(ge_municipio) = 'MULTIPOLYGON'::text) OR (ge_municipio IS NULL))
        constraint enforce_srid_the_geom
            check (public.st_srid(ge_municipio) = 4674),
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

alter table salve.municipio
    owner to postgres;

grant select, update, usage on sequence salve.municipio_sq_municipio_seq to usr_salve;

create index if not exists ix_municipio_estado_fk
    on salve.municipio (sq_estado)
    tablespace pg_default;

create index if not exists ix_municipio_geo
    on salve.municipio using gist (ge_municipio)
    tablespace pg_default;

create index if not exists ix_municipio_nome
    on salve.municipio (no_municipio)
    tablespace pg_default;

create unique index if not exists ixu_municipio_ibge
    on salve.municipio (co_ibge)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.municipio to usr_salve;

create index if not exists idx_geo_regiao
    on salve.regiao using gist (ge_regiao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.regiao to usr_salve;

create table if not exists salve.registro
(
    sq_registro serial
        constraint pk_registro
            primary key,
    ge_registro public.geometry                            not null,
    dt_inclusao timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.registro is 'tabela das coordenadas do registro de ocorrência (ponto) cadastrado no SALVE';

comment on column salve.registro.sq_registro is 'chave primária sequencial';

alter table salve.registro
    owner to postgres;

create index if not exists ux_registro_geo
    on salve.registro using gist (ge_registro)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro to usr_salve;

create table if not exists salve.registro_bacia
(
    sq_registro_bacia serial
        constraint pk_registro_bacia
            primary key,
    sq_registro       bigint                              not null
        constraint fk_reg_bacia_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_bacia_geo      bigint                              not null
        constraint fk_reg_bacia_geo
            references salve.dados_apoio_geo
            on update restrict on delete restrict,
    dt_inclusao       timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.registro_bacia is 'Tabela para registro da Bacia Hidrográfica que o ponto intersecta';

comment on column salve.registro_bacia.sq_registro_bacia is 'Chave primária sequencial';

comment on column salve.registro_bacia.sq_registro is 'Chave estrangeira tabela Registro';

comment on column salve.registro_bacia.sq_bacia_geo is 'Chave estrangeira tabela Dados_apoio_geo';

comment on column salve.registro_bacia.dt_inclusao is 'Data de inclusão no banco de dados';

alter table salve.registro_bacia
    owner to postgres;

create index if not exists ix_fk_reg_bacia_bacia
    on salve.registro_bacia (sq_bacia_geo)
    tablespace pg_default;

create index if not exists ix_fk_reg_bacia_reg
    on salve.registro_bacia (sq_registro)
    tablespace pg_default;

create unique index if not exists ixu_reg_bacia_reg_bacia
    on salve.registro_bacia (sq_registro, sq_bacia_geo)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_bacia to usr_salve;

create table if not exists salve.registro_bioma
(
    sq_registro bigint                              not null
        constraint pk_registro_bioma
            primary key
        constraint fk_reg_bioma_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_bioma    bigint                              not null
        constraint fk_reg_bioma_bioma
            references salve.bioma
            on update restrict on delete restrict,
    dt_inclusao timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.registro_bioma is 'tabela para registro do Bioma que o ponto intersecta';

comment on column salve.registro_bioma.sq_registro is 'chave estrangeira tabela Registro';

comment on column salve.registro_bioma.sq_bioma is 'chave estrangeira tabela Bioma';

comment on column salve.registro_bioma.dt_inclusao is 'data de inclusão no banco de dados';

alter table salve.registro_bioma
    owner to postgres;

create index if not exists ix_fk_reg_bioma_bioma
    on salve.registro_bioma (sq_bioma)
    tablespace pg_default;

create unique index if not exists ixu_reg_bioma_reg_bioma
    on salve.registro_bioma (sq_registro, sq_bioma)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_bioma to usr_salve;

create table if not exists salve.registro_estado
(
    sq_registro bigint                              not null
        constraint pk_registro_estado
            primary key
        constraint fk_reg_estado_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_estado   bigint                              not null
        constraint fk_reg_estado_estado
            references salve.estado
            on update restrict on delete restrict,
    dt_inclusao timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.registro_estado is 'tabela para registro do Estado que o ponto intersecta';

comment on column salve.registro_estado.sq_registro is 'chave estrangeira tabela Registro';

comment on column salve.registro_estado.sq_estado is 'chave estrangeira tabela Estado';

comment on column salve.registro_estado.dt_inclusao is 'data de inclusão no banco de dados';

alter table salve.registro_estado
    owner to postgres;

create index if not exists ix_fk_reg_est_est
    on salve.registro_estado (sq_estado)
    tablespace pg_default;

create unique index if not exists ixu_reg_est_reg_est
    on salve.registro_estado (sq_registro, sq_estado)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_estado to usr_salve;

create table if not exists salve.registro_municipio
(
    sq_registro  bigint                              not null
        constraint pk_registro_municipio
            primary key
        constraint fk_reg_municipio_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_municipio bigint                              not null
        constraint fk_reg_municipio_mun
            references salve.municipio
            on update restrict on delete restrict,
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.registro_municipio.sq_registro is 'Chave estrangeira tabela Registro';

comment on column salve.registro_municipio.sq_municipio is 'Chave estrangeira tabela Municipio';

comment on column salve.registro_municipio.dt_inclusao is 'Data de inclusão no banco de dados';

alter table salve.registro_municipio
    owner to postgres;

create index if not exists ix_fk_reg_mun_mun
    on salve.registro_municipio (sq_municipio)
    tablespace pg_default;

create unique index if not exists ixu_reg_mun_reg_mun
    on salve.registro_municipio (sq_registro, sq_municipio)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_municipio to usr_salve;

create table if not exists salve.registro_pais
(
    sq_registro bigint                              not null
        constraint pk_registro_pais
            primary key
        constraint fk_reg_pais_reg
            references salve.registro
            on update restrict on delete restrict,
    sq_pais     bigint                              not null
        constraint fk_reg_pais_pais
            references salve.pais
            on update restrict on delete restrict,
    dt_inclusao timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.registro_pais.sq_registro is 'chave estrangeira tabela Registro';

comment on column salve.registro_pais.sq_pais is 'chave estrangeira tabela Pais';

comment on column salve.registro_pais.dt_inclusao is 'data de inclusão no banco de dados';

alter table salve.registro_pais
    owner to postgres;

create index if not exists ix_fk_reg_pais_pais
    on salve.registro_pais (sq_pais)
    tablespace pg_default;

create unique index if not exists ixu_reg_pais_reg_pais
    on salve.registro_pais (sq_registro, sq_pais)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_pais to usr_salve;

create table if not exists salve.registro_rppn
(
    sq_registro bigint                              not null
        constraint pk_registro_rppn
            primary key
        constraint fk_reg_rppn_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_rppn     bigint                              not null
        constraint fk_reg_rppn_rppn
            references salve.instituicao
            on update restrict on delete restrict,
    dt_inclusao timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.registro_rppn is 'Tabela para registro da RPPN que o ponto intersecta';

comment on column salve.registro_rppn.sq_registro is 'Chave primária sequencial';

comment on column salve.registro_rppn.sq_rppn is 'Chave estrangeira tabela RPPN';

comment on column salve.registro_rppn.dt_inclusao is 'Data de inclusão no banco de dados';

alter table salve.registro_rppn
    owner to postgres;

create index if not exists ix_reg_rppn_reg
    on salve.registro_rppn (sq_rppn)
    tablespace pg_default;

create unique index if not exists ixu_reg_rppn_reg_rppn
    on salve.registro_rppn (sq_registro, sq_rppn)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_rppn to usr_salve;

create table if not exists salve.registro_uc_federal
(
    sq_registro_uc_federal serial,
    sq_registro            bigint                              not null
        constraint fk_reg_uc_fed_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_uc_federal          bigint                              not null
        constraint fk_reg_uc_fed_uc
            references salve.instituicao
            on update restrict on delete restrict,
    dt_inclusao            timestamp default CURRENT_TIMESTAMP not null,
    constraint pk_registro_uc_federal
        primary key (sq_registro_uc_federal, sq_registro)
);

comment on table salve.registro_uc_federal is 'Tabela para registro da(s) Unidade(s) de Conservação Federal que o ponto intersecta';

comment on column salve.registro_uc_federal.sq_registro_uc_federal is 'Chave primária sequencial';

comment on column salve.registro_uc_federal.sq_registro is 'Chave estrangeira tabela Registro';

comment on column salve.registro_uc_federal.sq_uc_federal is 'Chave estrangeira tabela Unidade_org';

comment on column salve.registro_uc_federal.dt_inclusao is 'Data de inclusão no banco de dados';

alter table salve.registro_uc_federal
    owner to postgres;

create index if not exists ix_fk_reg_uc_fed_reg
    on salve.registro_uc_federal (sq_registro)
    tablespace pg_default;

create index if not exists ix_fk_reg_uc_fed_uc
    on salve.registro_uc_federal (sq_uc_federal)
    tablespace pg_default;

create unique index if not exists ixu_registro_uc_federal_reg_uc
    on salve.registro_uc_federal (sq_registro, sq_uc_federal)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_uc_federal to usr_salve;

create table if not exists salve.terra_indigena
(
    gid        serial
        primary key,
    the_geom   public.geometry(MultiPolygon, 4674),
    terrai_cod numeric(16, 6),
    terrai_nom varchar(254),
    terrai_pro varchar(254),
    status_ter varchar(254),
    fase_descr varchar(254),
    tipo_terra varchar(254),
    terrai_fai varchar(254),
    terrai_obs varchar(254),
    terrai_a_1 varchar(254),
    situa_desc varchar(254),
    tipo_area_ varchar(254),
    terrai_per varchar(254),
    terrai_pop varchar(254)
);

alter table salve.terra_indigena
    owner to postgres;

create table if not exists salve.registro_terra_indigena
(
    sq_registro_terra_indigena bigserial
        constraint pk_registro_terra_indigena
            primary key,
    sq_registro                bigint                              not null
        constraint fk_reg_terra_indigena_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_terra_indigena          bigint                              not null
        constraint fk_reg_terra_indigena
            references salve.terra_indigena
            on update restrict on delete restrict,
    dt_inclusao                timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.registro_terra_indigena is 'Tabela para registro da Terra Indigena que o ponto intersecta';

comment on column salve.registro_terra_indigena.sq_registro is 'Chave estrangeira tabela Registro';

comment on column salve.registro_terra_indigena.sq_terra_indigena is 'Chave estrangeira tabela geo.terra_indigena';

comment on column salve.registro_terra_indigena.dt_inclusao is 'Data de inclusão no banco de dados';

alter table salve.registro_terra_indigena
    owner to postgres;

create index if not exists ix_fk_reg_terra_indigena
    on salve.registro_terra_indigena (sq_terra_indigena)
    tablespace pg_default;

create index if not exists ix_fk_reg_terra_indigena_reg
    on salve.registro_terra_indigena (sq_registro)
    tablespace pg_default;

create unique index if not exists ixu_reg_terra_indigena_reg
    on salve.registro_terra_indigena (sq_registro, sq_terra_indigena)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_terra_indigena to usr_salve;

create index if not exists terra_indigena_the_geom_geom_idx
    on salve.terra_indigena using gist (the_geom)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.terra_indigena to usr_salve;

create table if not exists salve.traducao_dados_apoio
(
    sq_traducao_dados_apoio serial
        constraint pk_traducao_dados_apoio
            primary key,
    sq_dados_apoio          bigint                              not null
        constraint fk_dados_apoio_trad_dados_apoio
            references salve.dados_apoio
            on update cascade on delete cascade,
    tx_traduzido            text                                not null,
    tx_revisado             text                                not null,
    dt_inclusao             timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao            timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.traducao_dados_apoio is 'tabela para registrar as traducoes realizadas no registros da tabela de apoio';

comment on column salve.traducao_dados_apoio.sq_traducao_dados_apoio is 'chave primaria da tabela dados_apoio_traducao';

comment on column salve.traducao_dados_apoio.sq_dados_apoio is 'chave estrangeira da tabela dados_apoio';

comment on column salve.traducao_dados_apoio.tx_traduzido is 'texto, palarva ou frase apos a traducao';

comment on column salve.traducao_dados_apoio.tx_revisado is 'texto, palarva ou frase apos a revisao da traducao automatica';

comment on column salve.traducao_dados_apoio.dt_inclusao is 'data da criacao do registro no banco de dados';

comment on column salve.traducao_dados_apoio.dt_alteracao is 'data da ultima alteracao realizada no registro';

alter table salve.traducao_dados_apoio
    owner to postgres;

create index if not exists ix_fk_trad_dados_apoio
    on salve.traducao_dados_apoio (sq_dados_apoio)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_dados_apoio to usr_salve;

create table if not exists salve.traducao_pan
(
    sq_traducao_pan serial
        constraint pk_traducao_pan
            primary key,
    sq_plano_acao   bigint                              not null
        constraint fk_traducao_pan_plano_acao
            references salve.plano_acao
            on update restrict on delete restrict,
    tx_traduzido    text                                not null,
    tx_revisado     text                                not null,
    dt_inclusao     timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao    timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.traducao_pan.sq_traducao_pan is 'Chave primária sequencial da tabela traducao_pan';

comment on column salve.traducao_pan.sq_plano_acao is 'Chave estrangeira da tabela plano acao';

comment on column salve.traducao_pan.tx_traduzido is 'Texto, palarva ou frase apos a traducao';

comment on column salve.traducao_pan.tx_revisado is 'Texto, palarva ou frase apos a revisao da traducao automatica';

comment on column salve.traducao_pan.dt_inclusao is 'Data da criacao do registro no banco de dados';

comment on column salve.traducao_pan.dt_alteracao is 'Data da ultima alteracao realizada no registro';

alter table salve.traducao_pan
    owner to postgres;

create index if not exists ix_fk_traducao_pan_plano_acao
    on salve.traducao_pan (sq_plano_acao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_pan to usr_salve;

create table if not exists salve.traducao_tabela
(
    sq_traducao_tabela bigserial
        constraint pk_traducao_tabela
            primary key,
    no_tabela          text                                not null,
    no_coluna          text                                not null,
    ds_coluna          text                                not null,
    dt_inclusao        timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao       timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.traducao_tabela is 'tabela com relacao de tabelas e suas colunas que deverao ser traduzidas para possibilitar o acompanhamento do andamento da revisao';

comment on column salve.traducao_tabela.sq_traducao_tabela is 'chave primaria sequencial da tabela traducao_tabela';

comment on column salve.traducao_tabela.no_tabela is 'nome fisico da tabela no banco de dados';

comment on column salve.traducao_tabela.no_coluna is 'nome fisico da coluna tera seu conteudo traduzido';

comment on column salve.traducao_tabela.ds_coluna is 'descricao da coluna para identificacao no contexto da ficha e exibir mensagem ao usuario';

comment on column salve.traducao_tabela.dt_inclusao is 'data da criacao do registro no banco de dados';

comment on column salve.traducao_tabela.dt_alteracao is 'data da ultima alteracao realizada no registro';

alter table salve.traducao_tabela
    owner to postgres;

create table if not exists salve.traducao_revisao
(
    sq_traducao_revisao bigserial
        constraint pk_traducao_revisao
            primary key,
    sq_traducao_tabela  bigint                              not null
        constraint fk_trad_rev_trad_tab
            references salve.traducao_tabela
            on update cascade on delete cascade,
    sq_registro         bigint                              not null,
    sq_pessoa           bigint
        constraint fk_trad_rev_pf
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    tx_traduzido        text                                not null,
    dt_revisao          timestamp,
    dt_inclusao         timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao        timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.traducao_revisao is 'tabela para armazenamento do autor da traducao realizadas no campo de uma trabela';

comment on column salve.traducao_revisao.sq_traducao_revisao is 'chave primaria sequencial da tabela traducao_revisao';

comment on column salve.traducao_revisao.sq_traducao_tabela is 'chave primaria sequencial da tabela traducao_tabela';

comment on column salve.traducao_revisao.sq_registro is 'id do registro traduzido';

comment on column salve.traducao_revisao.sq_pessoa is 'id do usuario que realizou a revisao da traducao automática';

comment on column salve.traducao_revisao.tx_traduzido is 'texto traduzido';

comment on column salve.traducao_revisao.dt_revisao is 'data que a traducao automatica foi revisada';

comment on column salve.traducao_revisao.dt_inclusao is 'data da criacao do registro no banco de dados';

comment on column salve.traducao_revisao.dt_alteracao is 'data da ultima alteracao realizada no registro';

alter table salve.traducao_revisao
    owner to postgres;

create index if not exists ix_fk_trad_revisao_trad_tab
    on salve.traducao_revisao (sq_traducao_tabela, sq_registro)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_revisao to usr_salve;

create table if not exists salve.traducao_revisao_hist
(
    sq_traducao_revisao_hist bigserial
        constraint pk_traducao_revisao_hist
            primary key,
    sq_traducao_revisao      bigint                              not null
        constraint fk_trad_rev_hist_trad_rev
            references salve.traducao_revisao
            on update cascade on delete cascade,
    sq_pessoa                bigint                              not null
        constraint fk_trad_rev_hist_pf
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    tx_traduzido             text                                not null,
    dt_revisao               timestamp,
    dt_inclusao              timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.traducao_revisao_hist is 'tabela para armazenamento do historico das revisões dos textos da ficha';

comment on column salve.traducao_revisao_hist.sq_traducao_revisao_hist is 'chave primaria sequencial da tabela traducao_revisao_hist';

comment on column salve.traducao_revisao_hist.sq_traducao_revisao is 'chave estrangeira da tabela traducao_revisa';

comment on column salve.traducao_revisao_hist.sq_pessoa is 'id do usuario que realizou a alteracao no texto ou na data da revisao';

comment on column salve.traducao_revisao_hist.tx_traduzido is 'texto traduzido que foi alterado';

comment on column salve.traducao_revisao_hist.dt_revisao is 'data que a traducao automatica foi revisada';

comment on column salve.traducao_revisao_hist.dt_inclusao is 'data da criacao do registro no banco de dados';

alter table salve.traducao_revisao_hist
    owner to postgres;

create index if not exists ix_fk_trad_revisao_hist_trad_tab
    on salve.traducao_revisao_hist (sq_traducao_revisao, dt_inclusao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_revisao_hist to usr_salve;

create unique index if not exists ixu_traducao_tabela_coluna
    on salve.traducao_tabela (no_tabela, no_coluna)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_tabela to usr_salve;

create table if not exists salve.traducao_texto
(
    sq_traducao_texto bigserial
        constraint pk_traducao_texto
            primary key,
    tx_original       text                                not null,
    tx_traduzido      text                                not null,
    tx_revisado       text                                not null,
    dt_inclusao       timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao      timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.traducao_texto is 'tabela para armazenamento das traducoes dos textos, palavras e frases utilizadas no sistema salve para integração com o sistema sis/iucn';

comment on column salve.traducao_texto.sq_traducao_texto is 'chave primaria sequencial da tabela traducao_texto';

comment on column salve.traducao_texto.tx_original is 'texto, palavra ou frase no formato original antes da traducao';

comment on column salve.traducao_texto.tx_traduzido is 'texto, palarva ou frase apos a traducao';

comment on column salve.traducao_texto.tx_revisado is 'texto, palarva ou frase apos a revisao da traducao automatica';

comment on column salve.traducao_texto.dt_inclusao is 'data da criacao do registro no banco de dados';

comment on column salve.traducao_texto.dt_alteracao is 'data da ultima alteracao realizada no registro';

alter table salve.traducao_texto
    owner to postgres;

create unique index if not exists ix_traducao_texto_tx_original
    on salve.traducao_texto (tx_original)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_texto to usr_salve;

create table if not exists salve.trilha_auditoria
(
    sq_trilha_auditoria bigserial
        constraint pk_trilha_auditoria
            primary key,
    dt_log              timestamp    not null,
    de_ip               varchar(15),
    de_env              varchar(20)  not null,
    no_pessoa           varchar(50)  not null,
    nu_cpf              text,
    no_modulo           varchar(100) not null,
    no_acao             varchar(50)  not null,
    tx_parametros       text
);

comment on table salve.trilha_auditoria is 'Tabela para registro das acoes de inclusoes, alteracoes e exclusoes realizadas pelos usuarios.';

comment on column salve.trilha_auditoria.sq_trilha_auditoria is 'Chave primaria e sequencial';

comment on column salve.trilha_auditoria.dt_log is 'Data e hora da acao realizada pelo usuario';

comment on column salve.trilha_auditoria.de_ip is 'Endereco IP do usuario';

comment on column salve.trilha_auditoria.de_env is 'Ambiente em que a acao ocorreu. Ex: PRODUCAO/DESENV/TEST/HOMOLOG';

comment on column salve.trilha_auditoria.no_pessoa is 'Nome do usuario que realizou a acao';

comment on column salve.trilha_auditoria.nu_cpf is 'Numero do CPF do usuario que realiou a acao';

comment on column salve.trilha_auditoria.no_modulo is 'Nome do modulo do sistema que recebeu a acao';

comment on column salve.trilha_auditoria.no_acao is 'Nome da acao executada';

comment on column salve.trilha_auditoria.tx_parametros is 'Nomes e valores dos parametros enviados para execucao da acao no formato JSON';

alter table salve.trilha_auditoria
    owner to postgres;

grant select, update, usage on sequence salve.trilha_auditoria_sq_trilha_auditoria_seq to usr_salve;

create index if not exists ix_trilha_auditoria_modulo
    on salve.trilha_auditoria (no_modulo);

comment on index salve.ix_trilha_auditoria_modulo is 'Indice pelo nome do modulo';

create index if not exists ix_trilha_auditoria_cpf
    on salve.trilha_auditoria (nu_cpf);

comment on index salve.ix_trilha_auditoria_cpf is 'Indice pelo numero do cpf';

grant delete, insert, references, select, trigger, truncate, update on salve.trilha_auditoria to usr_salve;

create table if not exists salve.unidade_conservacao
(
    sq_pessoa    bigint not null
        constraint pk_unidade_conservacao
            primary key
        constraint fk_unidade_conservacao_instituicao
            references salve.instituicao
            on update cascade on delete cascade,
    sq_esfera    bigint not null
        constraint fk_unidade_conservacao_esfera
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo      bigint not null
        constraint fk_unidade_conservacao_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    cd_cnuc      text,
    ge_uc        public.geometry
        constraint enforce_geotype_geom
            check ((public.geometrytype(ge_uc) = 'MULTIPOLYGON'::text) OR (ge_uc IS NULL))
        constraint enforce_srid_geom
            check (public.st_srid(ge_uc) = 4674),
    dt_alteracao timestamp default CURRENT_TIMESTAMP
);

comment on column salve.unidade_conservacao.sq_pessoa is 'chave primária sequencial';

comment on column salve.unidade_conservacao.sq_esfera is 'chave estrangeira da tabela dados_apoio';

comment on column salve.unidade_conservacao.sq_tipo is 'chave estrangeira da tabela dados_apoio';

comment on column salve.unidade_conservacao.cd_cnuc is 'codigo do cadastro nacional de unidades de conservacao - cnuc';

comment on column salve.unidade_conservacao.ge_uc is 'polígono da unidade de conservacao';

comment on column salve.unidade_conservacao.dt_alteracao is 'data da última alteração';

alter table salve.unidade_conservacao
    owner to postgres;

create table if not exists salve.abrangencia
(
    sq_abrangencia     serial
        constraint pk_abrangencia
            primary key,
    sq_municipio       bigint
        constraint fk_abrangencia_municipio
            references salve.municipio
            on update restrict on delete restrict,
    sq_uc_federal      bigint
        constraint fk_abrangencia_uc_federal
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_estado          bigint
        constraint fk_abrangencia_estado
            references salve.estado
            on update restrict on delete restrict,
    sq_uc_nao_federal  bigint
        constraint fk_abrangencia_uc_nao_federal
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_rppn            bigint
        constraint fk_abrangencia_rppn
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_pais            bigint
        constraint fk_abrangencia_pais
            references salve.pais
            on update restrict on delete restrict,
    sq_regiao          bigint
        constraint fk_abrangencia_regiao
            references salve.regiao
            on update restrict on delete restrict,
    sq_dados_apoio     bigint
        constraint fk_abrangencia_apoio
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_abrangencia_pai bigint
        constraint fk_abrangencia_pai
            references salve.abrangencia
            on update restrict on delete cascade,
    sq_tipo            bigint
);

comment on table salve.abrangencia is 'Tabela para criacao das abrangencias. Ex: Estado, Municipio, Bioma etc';

comment on column salve.abrangencia.sq_abrangencia_pai is 'Auto relacionamento';

alter table salve.abrangencia
    owner to postgres;

create index if not exists ix_abrangencia_pai
    on salve.abrangencia (sq_abrangencia_pai)
    tablespace pg_default;

create index if not exists ix_abrangencia_tipo
    on salve.abrangencia (sq_tipo)
    tablespace pg_default;

create unique index if not exists ixu_abrangencia_unique
    on salve.abrangencia (sq_municipio, sq_uc_federal, sq_estado, sq_uc_nao_federal, sq_rppn, sq_pais, sq_dados_apoio)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.abrangencia to usr_salve;

create table if not exists salve.registro_uc_estadual
(
    sq_registro             bigint                              not null
        constraint fk_reg_uc_est_reg
            references salve.registro
            on update cascade on delete cascade,
    sq_uc_estadual          bigint                              not null
        constraint fk_reg_uc_est_uc
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    dt_inclusao             timestamp default CURRENT_TIMESTAMP not null,
    sq_registro_uc_estadual bigserial
        constraint pk_registro_uc_estadual
            primary key
);

comment on table salve.registro_uc_estadual is 'tabela para registro da Unidade de Conservação Estadual que o ponto intersecta';

comment on column salve.registro_uc_estadual.sq_registro is 'chave estrangeira tabela Registro';

comment on column salve.registro_uc_estadual.sq_uc_estadual is 'chave estrangeira tabela MMA_uc_nao_federal ';

comment on column salve.registro_uc_estadual.dt_inclusao is 'data de inclusão no banco de dados';

alter table salve.registro_uc_estadual
    owner to postgres;

create index if not exists ix_fk_reg_uc_est_reg
    on salve.registro_uc_estadual (sq_registro)
    tablespace pg_default;

create index if not exists ix_fk_reg_uc_est_uc
    on salve.registro_uc_estadual (sq_uc_estadual)
    tablespace pg_default;

create unique index if not exists ixu_reg_uc_est_reg_uc
    on salve.registro_uc_estadual (sq_registro, sq_uc_estadual)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.registro_uc_estadual to usr_salve;

create index if not exists ix_unidade_conservacao_geo
    on salve.unidade_conservacao using gist (ge_uc)
    tablespace pg_default;

create index if not exists ix_unidade_conservacao_tipo_fk
    on salve.unidade_conservacao (sq_tipo)
    tablespace pg_default;

create unique index if not exists ixu_unidade_conservacao_cnuc
    on salve.unidade_conservacao (cd_cnuc)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.unidade_conservacao to usr_salve;

create table if not exists salve.user_jobs
(
    sq_user_jobs          bigserial
        constraint pk_user_jobs
            primary key,
    sq_pessoa             bigint not null
        constraint fk_user_jobs_sq_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    de_rotulo             text,
    de_andamento          text,
    vl_max                integer,
    vl_atual              integer,
    js_executar           text,
    de_erro               text,
    de_mensagem           text,
    dt_inclusao           timestamp default CURRENT_TIMESTAMP,
    dt_inicio             timestamp default CURRENT_TIMESTAMP,
    dt_fim                timestamp,
    dt_ultima_atualizacao timestamp,
    id_elemento           text,
    de_email              text,
    de_email_assunto      text,
    no_arquivo_anexo      text,
    st_email_enviado      boolean,
    st_cancelado          boolean   default false,
    st_executado          boolean   default false
);

comment on column salve.user_jobs.sq_user_jobs is 'Chave primaria e sequencial da tabela user_jobs';

comment on column salve.user_jobs.sq_pessoa is 'Chave estrangeira da tabela pessoa_fisica';

comment on column salve.user_jobs.de_rotulo is 'Descricao do trabalho que esta sendo realizado em segundo plano';

comment on column salve.user_jobs.de_andamento is 'Texto do andamento que sera exibida quando o usuario colocar o mouse sobre a tarefa e que sera atualizada durante a execucao da tarefa';

comment on column salve.user_jobs.vl_max is 'Valor maximo utilizado para calcular o percentual executado';

comment on column salve.user_jobs.vl_atual is 'Valor atual utilizado para calcular o percentual executado';

comment on column salve.user_jobs.js_executar is 'Codigo javascript que sera executado no navegador quando terminar';

comment on column salve.user_jobs.de_erro is 'Descricao do erro encontrado durante a execucao';

comment on column salve.user_jobs.de_mensagem is 'Mensagem de retorno ou email';

comment on column salve.user_jobs.dt_inclusao is 'Data e hora de criacao da tarefa';

comment on column salve.user_jobs.dt_inicio is 'Data e hora que a tarefa foi iniciada';

comment on column salve.user_jobs.dt_fim is 'Data e hora que a tarefa foi finalizada';

comment on column salve.user_jobs.dt_ultima_atualizacao is 'Data e hora que a tarefa sofreu a ultima atualizacao';

comment on column salve.user_jobs.id_elemento is 'Id do elemento html que devera ser atualizdo com o andamento atual da tarefa';

comment on column salve.user_jobs.de_email is 'Email do usuario para envio do arquivo/mensagem ao finalizar a tarefa';

comment on column salve.user_jobs.de_email_assunto is 'Assunto do email';

comment on column salve.user_jobs.no_arquivo_anexo is 'Nome do arquivo que devera ser enviado como anexo para o usuario';

comment on column salve.user_jobs.st_email_enviado is 'Indica se o e-mail ja foi enviado ou nao';

comment on column salve.user_jobs.st_cancelado is 'Informar se a terefa foi cancelada pelo usuario';

comment on column salve.user_jobs.st_executado is 'Informar se o job ja esta executado para evitar reprocessamento';

alter table salve.user_jobs
    owner to postgres;

grant select, update, usage on sequence salve.user_jobs_sq_user_jobs_seq to usr_salve;

create unique index if not exists ix_sq_user_job_pk
    on salve.user_jobs (sq_user_jobs)
    tablespace pg_default;

create index if not exists ix_user_jobs_sq_pessoa_fk
    on salve.user_jobs (sq_pessoa)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.user_jobs to usr_salve;

create table if not exists salve.uso
(
    sq_uso                  serial
        constraint pk_uso
            primary key,
    sq_uso_pai              bigint
        constraint fk_uso_uso_pai
            references salve.uso
            on update cascade on delete restrict,
    sq_criterio_ameaca_iucn bigint
        constraint fk_uso_ameaca_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    ds_uso                  text not null,
    co_uso                  text,
    nu_ordem                text,
    sq_iucn_uso             bigint
        constraint fk_uso_sq_iucn_uso
            references salve.iucn_uso
            on update cascade on delete restrict
);

comment on column salve.uso.nu_ordem is 'Ordem de exibição na tela';

comment on column salve.uso.sq_iucn_uso is 'Chave estrangeira da tabela iucn_uso';

alter table salve.uso
    owner to postgres;

create table if not exists salve.traducao_uso
(
    sq_traducao_uso serial
        constraint pk_traducao_uso
            primary key,
    sq_uso          bigint                              not null
        constraint fk_uso_trad_uso
            references salve.uso
            on update cascade on delete cascade,
    tx_traduzido    text                                not null,
    tx_revisado     text                                not null,
    dt_inclusao     timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao    timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.traducao_uso is 'tabela para registrar as traducoes realizadas na tabela de usos';

comment on column salve.traducao_uso.sq_traducao_uso is 'chave primaria da tabelatraducao_uso';

comment on column salve.traducao_uso.sq_uso is 'chave estrangeira da tabela uso';

comment on column salve.traducao_uso.tx_traduzido is 'texto apos a traducao';

comment on column salve.traducao_uso.tx_revisado is 'texto, palarva ou frase apos a revisao da traducao automatica';

comment on column salve.traducao_uso.dt_inclusao is 'data da criacao do registro no banco de dados';

comment on column salve.traducao_uso.dt_alteracao is 'data da ultima alteracao realizada no registro';

alter table salve.traducao_uso
    owner to postgres;

create index if not exists ix_fk_trad_uso
    on salve.traducao_uso (sq_uso)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_uso to usr_salve;

create index if not exists ix_uso_criterio
    on salve.uso (sq_criterio_ameaca_iucn)
    tablespace pg_default;

create index if not exists ix_uso_pai
    on salve.uso (sq_uso_pai)
    tablespace pg_default;

create index if not exists ix_uso_sq_iucn_uso
    on salve.uso (sq_iucn_uso)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.uso to usr_salve;

create table if not exists salve.usuario
(
    sq_pessoa        bigint                  not null
        constraint pk_usuario
            primary key
        constraint fk_usuario_pessoa_fisica
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    de_senha         text                    not null,
    st_ativo         boolean   default false not null,
    de_hash          text,
    dt_alteracao     timestamp default CURRENT_TIMESTAMP,
    dt_ultimo_acesso timestamp,
    no_instituicao   text,
    sg_instituicao   text
);

comment on table salve.usuario is 'Tabela para armazenar os usuários do sistema';

comment on column salve.usuario.sq_pessoa is 'Chave primária e estrangeira da tabela pessoa física';

comment on column salve.usuario.de_senha is 'Senha de acesso ao sistema no formato MD5';

comment on column salve.usuario.dt_alteracao is 'Data da última alteração';

comment on column salve.usuario.dt_ultimo_acesso is 'Data do último acesso ao sistema';

comment on column salve.usuario.no_instituicao is 'Nome da instituicao que o usuario pertence';

comment on column salve.usuario.sg_instituicao is 'Sigla da instituicao que o usuario pertence';

alter table salve.usuario
    owner to postgres;

create table if not exists salve.informativo_pessoa
(
    sq_informativo_pessoa serial
        primary key,
    sq_informativo        bigint                              not null
        constraint fk_informativo_pessoa_informativo
            references salve.informativo
            on update cascade on delete cascade,
    sq_pessoa             bigint                              not null
        constraint fk_informativo_pessoa_usuario
            references salve.usuario
            on update restrict on delete restrict,
    dt_visualizacao       timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.informativo_pessoa is 'Tabela de controle das visualizacoes dos informativos pelos usuarios';

comment on column salve.informativo_pessoa.sq_informativo is 'Id do informativo - chave estrangeira';

comment on constraint fk_informativo_pessoa_informativo on salve.informativo_pessoa is 'Chave estrangeira da tabela salve.informativo';

comment on column salve.informativo_pessoa.sq_pessoa is 'Chave estrangeira da tabela usuario. Usuario que visualizou o informativo';

comment on column salve.informativo_pessoa.dt_visualizacao is 'Data de visualizacao do informativo pela pessoa';

alter table salve.informativo_pessoa
    owner to postgres;

create index if not exists ix_informativo_pessoa_informativo
    on salve.informativo_pessoa (sq_informativo, sq_pessoa);

comment on index salve.ix_informativo_pessoa_informativo is 'Indice da chave estrangeira tabela informativo';

create index if not exists ix_informativo_pessoa_pessoa_fk
    on salve.informativo_pessoa (sq_pessoa);

comment on index salve.ix_informativo_pessoa_pessoa_fk is 'Indice chave estrangeira da tabela pessoa';

grant delete, insert, references, select, trigger, truncate, update on salve.informativo_pessoa to usr_salve;

grant delete, insert, references, select, trigger, truncate, update on salve.usuario to usr_salve;

create table if not exists salve.usuario_perfil
(
    sq_usuario_perfil serial
        constraint pk_usuario_perfil
            primary key,
    sq_pessoa         bigint                              not null
        constraint fk_usuario_perfil_usuario
            references salve.usuario
            on update restrict on delete cascade,
    sq_perfil         bigint                              not null
        constraint fk_usuario_perfil_perfil
            references salve.perfil
            on update restrict on delete restrict,
    dt_inclusao       timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.usuario_perfil is 'Tabela para armazenar os perfis de acesso dos usuários';

comment on column salve.usuario_perfil.sq_usuario_perfil is 'Chave primária e sequencial';

comment on column salve.usuario_perfil.sq_pessoa is 'Chave estrangeira da tabela pessoa física';

comment on column salve.usuario_perfil.sq_perfil is 'Chave estrangeira da tabela perfil';

comment on column salve.usuario_perfil.dt_inclusao is 'Data de inclusão no sistema';

alter table salve.usuario_perfil
    owner to postgres;

grant select, update, usage on sequence salve.usuario_perfil_sq_usuario_perfil_seq to usr_salve;

create unique index if not exists ixu_usuario_perfil_pessoa_fk
    on salve.usuario_perfil (sq_pessoa, sq_perfil)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.usuario_perfil to usr_salve;

create table if not exists salve.usuario_perfil_instituicao
(
    sq_usuario_perfil_instituicao serial
        constraint pk_usuario_perfil_instituicao
            primary key,
    sq_usuario_perfil             bigint                              not null
        constraint fk_usuario_perfil_inst_usu_perf
            references salve.usuario_perfil
            on update restrict on delete cascade,
    sq_instituicao                bigint                              not null
        constraint fk_usuario_perfil_inst_inst
            references salve.instituicao
            on update restrict on delete cascade,
    dt_inclusao                   timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.usuario_perfil_instituicao.sq_usuario_perfil is 'Chave primária e sequencial';

comment on column salve.usuario_perfil_instituicao.sq_instituicao is 'Chave estrangeira da tabela instituição';

comment on column salve.usuario_perfil_instituicao.dt_inclusao is 'Data de inclusão no banco de dados';

alter table salve.usuario_perfil_instituicao
    owner to postgres;

grant select, update, usage on sequence salve.usuario_perfil_instituicao_sq_usuario_perfil_instituicao_seq to usr_salve;

create unique index if not exists ixu_usuario_perfil_instituicao
    on salve.usuario_perfil_instituicao (sq_instituicao, sq_usuario_perfil)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.usuario_perfil_instituicao to usr_salve;

create table if not exists salve.web_instituicao
(
    sq_web_instituicao serial
        constraint pk_web_instituicao
            primary key,
    no_instituicao     text      not null,
    sq_unidade_org     bigint
        constraint fk_web_instituicao_unidade_org
            references salve.instituicao
            on update restrict on delete restrict,
    sg_instituicao     text,
    dt_inclusao        timestamp not null
);

alter table salve.web_instituicao
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.web_instituicao to usr_salve;

create table if not exists salve.web_role
(
    sq_web_role serial
        constraint pk_web_role
            primary key,
    no_role     text not null
        constraint ct_web_role_no_role
            unique
);

alter table salve.web_role
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.web_role to usr_salve;

create table if not exists salve.web_usuario
(
    sq_web_usuario     serial
        constraint pk_web_usuario
            primary key,
    sq_web_instituicao bigint
        constraint fk_web_usuario_instituicao
            references salve.instituicao
            on update restrict on delete restrict,
    no_usuario         text                  not null,
    de_email           text                  not null,
    de_senha           text                  not null,
    de_hash_ativacao   text,
    st_conta_bloqueada boolean default false not null,
    st_conta_expirada  boolean default false not null,
    st_senha_expirada  boolean default false not null,
    st_conta_ativada   boolean default false not null,
    dt_ultimo_acesso   timestamp,
    dt_inclusao        timestamp             not null,
    nu_tentativas      integer
);

comment on column salve.web_usuario.nu_tentativas is 'Número de tentativas de acesso com erro de senha ou usuário';

alter table salve.web_usuario
    owner to postgres;

create unique index if not exists ixu_web_usuario_email
    on salve.web_usuario (de_email)
    tablespace pg_default;

comment on index salve.ixu_web_usuario_email is 'Indice unico para evitar duplicidade de emails';

grant delete, insert, references, select, trigger, truncate, update on salve.web_usuario to usr_salve;

create table if not exists salve.web_usuario_role
(
    sq_web_usuario_role serial
        constraint pk_web_usuario_role
            primary key,
    sq_web_usuario      bigint
        constraint fk_usuario_role_usuario
            references salve.web_usuario
            on update cascade on delete cascade,
    sq_web_role         bigint    not null
        constraint fk_usuario_role
            references salve.web_role
            on update cascade on delete cascade,
    dt_inclusao         timestamp not null
);

alter table salve.web_usuario_role
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.web_usuario_role to usr_salve;

create table if not exists taxonomia.grupo
(
    sq_grupo serial
        constraint pk_grupo
            primary key,
    de_grupo text not null,
    co_grupo text
);

comment on table taxonomia.grupo is 'Identificar o grupo em que a situação se aplica.  Ex:
a) "HISTORICO" para Situações ref. aos históricos
b) "NOME" para Situações ref. ao nomes dos Taxons
';

alter table taxonomia.grupo
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.grupo to usr_salve;

create table if not exists taxonomia.situacao
(
    sq_situacao serial
        constraint pk_situacao
            primary key,
    sq_grupo    bigint not null
        constraint fk_situacao_grupo
            references taxonomia.grupo
            on update restrict on delete restrict,
    de_situacao text   not null,
    co_situacao text
);

comment on table taxonomia.situacao is 'Registra a situação do registro. Ex:
a) Inclusão
b) Alteração no nome do táxon
c) Desativação
d) Reclassificação
e) Cadastramento anterior
f) Cadastro equivalente
g) Alteração de outros dados
h) Reativação
i) Sinonímia
j) Alteração nos dados de autoria da descrição (autor/ano)
etc...';

alter table taxonomia.situacao
    owner to postgres;

create table if not exists taxonomia.taxon
(
    sq_taxon            serial
        constraint pk_taxon
            primary key,
    sq_taxon_pai        bigint
        constraint fk_taxon_ref_taxon_pai
            references taxonomia.taxon,
    sq_nivel_taxonomico bigint                  not null
        constraint fk_taxon_nivel_taxonomico
            references taxonomia.nivel_taxonomico
            on update restrict on delete restrict,
    sq_situacao_nome    bigint                  not null
        constraint fk_taxon_situacao_nome
            references taxonomia.situacao
            on update restrict on delete restrict,
    no_taxon            text                    not null,
    st_ativo            boolean                 not null,
    no_autor            text,
    nu_ano              integer,
    in_ocorre_brasil    boolean   default false not null
        constraint chk_taxon_ocorre_brasil
            check (in_ocorre_brasil = ANY (ARRAY [true, false])),
    de_autor_ano        text,
    json_trilha         json,
    ts_ultima_alteracao timestamp default now() not null
);

comment on column taxonomia.taxon.json_trilha is 'Coluna para registrar a estrutura taxonomica no formato json para otimização em consultas e exibição da hierarquia taxonômica';

comment on column taxonomia.taxon.ts_ultima_alteracao is 'Adiciona a data alteracao do registro.';

alter table taxonomia.taxon
    owner to postgres;

create table if not exists salve.ficha
(
    sq_ficha                         serial
        constraint pk_ficha
            primary key,
    sq_unidade_org                   bigint                      not null
        constraint fk_ficha_unidade_org
            references salve.instituicao
            on update restrict on delete restrict,
    sq_usuario                       integer                     not null,
    sq_taxon                         bigint                      not null
        constraint fk_ficha_taxon
            references taxonomia.taxon
            on update restrict on delete restrict,
    sq_situacao_ficha                bigint                      not null
        constraint fk_ficha_situacao_ficha
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ciclo_avaliacao               bigint                      not null
        constraint fk_ficha_ciclo_avaliacao
            references salve.ciclo_avaliacao
            on update restrict on delete restrict,
    nm_cientifico                    text                        not null,
    ds_notas_taxonomicas             text,
    ds_diagnostico_morfologico       text,
    st_endemica_brasil               char
        constraint ckc_st_endemica_brasi_ficha
            check (st_endemica_brasil = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    ds_distribuicao_geo_nacional     text,
    ds_distribuicao_geo_global       text,
    nu_min_altitude                  integer,
    nu_max_altitude                  integer,
    nu_min_batimetria                integer,
    nu_max_batimetria                integer,
    ds_historia_natural              text,
    st_habito_aliment_especialista   char,
    ds_habito_alimentar              text,
    st_restrito_habitat_primario     char,
    st_especialista_micro_habitat    char
        constraint ckc_ficha_st_espec_micro_habitat
            check (st_especialista_micro_habitat = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    ds_especialista_micro_habitat    text,
    st_variacao_sazonal_habitat      char
        constraint ckc_ficha_st_var_sazo_habit
            check (st_variacao_sazonal_habitat = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    st_difer_macho_femea_habitat     char
        constraint ckc_ficha_st_dif_macho_femea_habit
            check (st_difer_macho_femea_habitat = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    ds_diferenca_macho_femea         text,
    ds_uso_habitat                   text,
    ds_interacao                     text,
    vl_intervalo_nascimento          text,
    vl_tempo_gestacao                text,
    vl_tamanho_prole                 text,
    vl_maturidade_sexual_femea       numeric(4, 1),
    vl_maturidade_sexual_macho       numeric(4, 1),
    sq_unid_maturidade_sexual_macho  bigint
        constraint fk_ficha_mat_sexual_macho
            references salve.dados_apoio
            on update restrict on delete restrict,
    vl_peso_femea                    numeric(6, 1),
    vl_peso_macho                    bigint,
    vl_comprimento_femea             numeric(6, 1),
    sq_grupo                         bigint
        constraint fk_ficha_grupo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_posicao_trofica               bigint
        constraint fk_ficha_posicao_trofica
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_modo_reproducao               bigint
        constraint fk_ficha_mod_reprod
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_sistema_acasalamento          bigint
        constraint fk_ficha_sis_acasalamento
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unid_intervalo_nascimento     bigint
        constraint fk_ficha_interv_nasc
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unid_tempo_gestacao           bigint
        constraint fk_ficha_unid_tempo_gest
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unid_maturidade_sexual_femea  bigint
        constraint fk_ficha_mat_sexual_femea
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unidade_peso_femea            bigint,
    sq_unidade_peso_macho            bigint,
    sq_medida_comprimento_femea      bigint
        constraint fk_ficha_med_comp_femea
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_medida_comprimento_macho      bigint
        constraint fk_ficha_med_comp_macho
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unidade_senilid_rep_femea     bigint
        constraint fk_ficha_unid_senelid_rep_femea
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unidade_senilid_rep_macho     bigint
        constraint fk_ficha_unid_senel_rep_macho
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unidade_longevidade_femea     bigint
        constraint fk_ficha_unid_long_femea
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_unidade_longevidade_macho     bigint
        constraint fk_ficha_unid_long_macho
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tendencia_populacional        bigint
        constraint fk_ficha_tend_populacional
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_periodo_taxa_cresc_popul      bigint
        constraint fk_ficha_perio_tax_cres_pop
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tendencia_eoo                 bigint
        constraint fk_ficha_tendencia_eoo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tendencia_aoo                 bigint
        constraint fk_ficha_tend_aoo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_medida_tempo_geracional       bigint
        constraint fk_ficha_tempo_geracional
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo_redu_popu_pass_rev       bigint
        constraint fk_ficha_tip_redu_pop_pass_rev
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo_redu_popu_pass           bigint
        constraint fk_ficha_tip_redu_pop_pass
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo_redu_popu_futura         bigint
        constraint fk_ficha_tip_red_pop_futura
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo_redu_popu_pass_futura    bigint
        constraint fk_ficha_tip_red_pop_pas_fut
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_declinio_populacional         bigint
        constraint fk_ficha_declinio_populacional
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tenden_num_individuo_maduro   bigint
        constraint fk_ficha_tend_num_indiv_maduro
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_perc_declinio_populacional    bigint
        constraint fk_ficha_perc_decli_populacional
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tendencia_num_subpopulacao    bigint
        constraint fk_ficha_tend_num_subpop
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tenden_num_localizacoes       bigint
        constraint fk_ficha_tend_num_localizacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tenden_qualidade_habitat      bigint
        constraint fk_ficha_tend_qual_habitat
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_prob_extincao_brasil          bigint
        constraint fk_ficha_prob_ext_brasil
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo_conectividade            bigint
        constraint fk_ficha_tip_conectividade
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_categoria_final               bigint
        constraint fk_ficha_cat_final
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_categoria_iucn                bigint
        constraint fk_ficha_cat_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_categoria_iucn_sugestao       bigint
        constraint fk_ficha_cat_iucn_sugest
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tendencia_imigracao           bigint
        constraint fk_ficha_tend_imigracao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_padrao_deslocamento           bigint
        constraint fk_ficha_padrao_deslocamento
            references salve.dados_apoio
            on update restrict on delete restrict,
    vl_comprimento_macho             numeric(6, 1),
    vl_senilidade_reprodutiva_femea  numeric(5, 1),
    vl_senilidade_reprodutiva_macho  numeric(5, 1),
    vl_longevidade_femea             numeric(5, 1),
    vl_longevidade_macho             numeric(5, 1),
    ds_reproducao                    text,
    ds_razao_sexual                  text,
    ds_taxa_mortalidade              text,
    vl_taxa_cresc_populacional       integer,
    ds_caracteristica_genetica       text,
    ds_populacao                     text,
    ds_ameaca                        text,
    ds_uso                           text,
    st_presenca_lista_vigente        char
        constraint ckc_ficha_st_presenca_lista_vigente
            check (st_presenca_lista_vigente = ANY (ARRAY ['S'::bpchar, 'N'::bpchar])),
    ds_acao_conservacao              text,
    ds_presenca_uc                   text,
    ds_pesquisa_exist_necessaria     text,
    st_ocorrencia_marginal           char,
    st_elegivel_avaliacao            char,
    st_limitacao_taxonomica_aval     char,
    vl_eoo                           numeric(10, 2),
    st_flutuacao_eoo                 char
        constraint ckc_ficha_st_flutuacao_eoo
            check (st_flutuacao_eoo = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    vl_aoo                           numeric(10, 2),
    st_flutuacao_aoo                 char
        constraint ckc_ficha_st_flutuacao_aoo
            check (st_flutuacao_aoo = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    ds_justificativa_aoo_eoo         text,
    st_localidade_tipo_conhecida     char,
    st_regiao_bem_amostrada          char,
    vl_tempo_geracional              numeric(6, 2),
    ds_metod_calc_tempo_geracional   text,
    nu_reducao_popul_passada_rev     integer,
    nu_reducao_popul_passada         integer,
    nu_reducao_popul_futura          bigint,
    nu_reducao_popul_pass_futura     bigint,
    st_populacao_fragmentada         char,
    nu_individuo_maduro              integer,
    st_flut_extrema_indiv_maduro     char,
    nu_subpopulacao                  integer,
    st_flut_extrema_sub_populacao    char
        constraint ckc_ficha_st_flut_extrema_sub_pop
            check (st_flut_extrema_sub_populacao = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    nu_indivi_subpopulacao_max       integer,
    nu_indivi_subpopulacao_perc      integer,
    nu_localizacoes                  integer,
    st_flutuacao_num_localizacoes    char,
    st_ameaca_futura                 char,
    ds_metodo_calc_perc_extin_br     text,
    st_declinio_br_popul_exterior    char
        constraint ckc_st_declinio_br_po_ficha
            check ((st_declinio_br_popul_exterior IS NULL) OR
                   (st_declinio_br_popul_exterior = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    ds_conectividade_pop_exterior    text,
    ds_criterio_aval_iucn            text,
    ds_justificativa                 text,
    ds_just_mudanca_categoria        text,
    ds_pendencias                    text,
    st_categoria_ok                  char
        constraint ckc_ficha_st_categoria_ok
            check ((st_categoria_ok IS NULL) OR
                   (st_categoria_ok = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    st_criterio_aval_iucn_ok         char
        constraint ckc_ficha_st_crit_aval_iucn_ok
            check ((st_criterio_aval_iucn_ok IS NULL) OR
                   (st_criterio_aval_iucn_ok = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    ds_criterio_aval_iucn_sugerido   text,
    ds_justificativa_validacao       text,
    ds_criterio_aval_iucn_final      text,
    ds_justificativa_final           text,
    ds_issn                          text,
    ds_doi                           text,
    ds_citacao                       text,
    st_migratoria                    char,
    st_possivelmente_extinta         char,
    st_favorecido_conversao_habitats char,
    st_tem_registro_areas_amplas     char,
    st_possui_ampla_dist_geografica  char,
    st_frequente_inventario_eoo      char,
    sq_medida_comprimento_femea_max  bigint
        constraint fk_ficha_med_comp_femea_max
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_medida_comprimento_macho_max  bigint
        constraint fk_ficha_med_comp_macho_max
            references salve.dados_apoio
            on update restrict on delete restrict,
    vl_comprimento_macho_max         numeric(6, 1),
    vl_comprimento_femea_max         numeric(6, 1),
    dt_aceite_validacao              timestamp,
    sq_usuario_alteracao             bigint
        constraint fk_ficha_usuario_alteracao
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_alteracao                     timestamp,
    vl_percentual_preenchimento      integer default 0,
    dt_publicacao                    timestamp,
    sq_usuario_publicacao            bigint
        constraint fk_ficha_pessoa_fisica_publicacao
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    st_manter_lc                     boolean,
    st_transferida                   boolean,
    st_possivelmente_extinta_final   char
        constraint chk_ficha_pex_final
            check (st_possivelmente_extinta_final = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    sq_ficha_ciclo_anterior          bigint
        constraint fk_ficha_sq_ficha_ciclo_anterior
            references salve.ficha
            on update cascade on delete set null,
    sq_subgrupo                      bigint
        constraint fk_ficha_subgrupo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_grupo_salve                   bigint
        constraint fk_ficha_grupo_salve
            references salve.dados_apoio
            on update restrict on delete restrict,
    st_localidade_desconhecida       char,
    ds_historico_avaliacao           text,
    ds_equipe_tecnica                text,
    ds_colaboradores                 text,
    st_sem_uf                        char    default 'N'::bpchar not null,
    ds_justificativa_eoo             text,
    ds_justificativa_aoo             text
);

comment on table salve.ficha is 'Os campos nu_max_altitude, nu_min_altitude, nu_min_batimetria, nu_max_batimetria são informados em metros';

comment on column salve.ficha.st_especialista_micro_habitat is 'Valores aceitos: S,N,D sendo S=Sim, N=Não e D=Desconhecido';

comment on column salve.ficha.st_variacao_sazonal_habitat is 'Valores aceitos: S,N,D sendo S=Sim, N=Não e D=Desconhecido';

comment on column salve.ficha.st_difer_macho_femea_habitat is 'Valores aceitos: S,N,D sendo S=Sim, N=Não e D=Desconhecido';

comment on column salve.ficha.sq_grupo is 'Coluna para informar o grupo avaliado. (tb_grupo)';

comment on column salve.ficha.sq_padrao_deslocamento is 'Padrão de deslocamento das aves migratórias';

comment on column salve.ficha.ds_razao_sexual is '1:2, 1:1,';

comment on column salve.ficha.st_presenca_lista_vigente is 'S/N';

comment on column salve.ficha.vl_eoo is 'em Km2';

comment on column salve.ficha.st_flutuacao_eoo is 'S/N/D';

comment on column salve.ficha.vl_aoo is 'em Km2';

comment on column salve.ficha.st_flutuacao_aoo is 'S/N/D';

comment on column salve.ficha.st_flut_extrema_sub_populacao is 'S/N/D';

comment on column salve.ficha.st_declinio_br_popul_exterior is 'S/N/D';

comment on column salve.ficha.st_categoria_ok is 'S/N - Se não tem que informar a sugestão';

comment on column salve.ficha.st_criterio_aval_iucn_ok is 'S/N - Se não tem que informar a sugestão';

comment on column salve.ficha.st_migratoria is 'A espécie é migratória (a população inteira, ou parte significativa, migra para outros países)?';

comment on column salve.ficha.st_possivelmente_extinta is 'Preenchido com S/N/D durante a oficina - para Sim, Não ou Desconhecido';

comment on column salve.ficha.dt_aceite_validacao is 'Data de encerramento da validação da ficha, quando a categoria, critério são definitivamente aceitos';

comment on column salve.ficha.sq_usuario_alteracao is 'Id do usuário que fez a última alteração';

comment on column salve.ficha.dt_alteracao is 'Data da última alteração';

comment on column salve.ficha.vl_percentual_preenchimento is 'guardar o percentual de preenchimento da ficha já calculado';

comment on column salve.ficha.dt_publicacao is 'Data de publicação da ficha no portal da biodiversidade';

comment on column salve.ficha.sq_usuario_publicacao is 'Id do usuário que fez a publicação';

comment on constraint fk_ficha_pessoa_fisica_publicacao on salve.ficha is 'Chave estrangeira com a tabela pessoa_fisica para usuário que publicou a ficha';

comment on column salve.ficha.st_manter_lc is 'Informa se a ficha manteve a categoria LC após a oficina de validação';

comment on column salve.ficha.st_transferida is 'Informar que a ficha foi transferida de uma oficina para a outra';

comment on column salve.ficha.st_possivelmente_extinta_final is 'Preenchido com S/N/D na avaliação final - para Sim, Não ou Desconhecido';

comment on column salve.ficha.sq_ficha_ciclo_anterior is 'Identificar a ficha do ciclo anterior quando hover mudança no taxon da ficha atual.';

comment on column salve.ficha.sq_subgrupo is 'Coluna para informar o subgrupo avaliado. (tb_subgrupo)';

comment on column salve.ficha.sq_grupo_salve is 'Coluna para informar o grupo taxonomico no salve. (tb_grupo_salve)';

comment on column salve.ficha.st_localidade_desconhecida is 'A especie nao possui uma localizacao conhecida no mapa';

comment on column salve.ficha.ds_historico_avaliacao is 'Texto livre para informacoes sobre o historico da avaliacoes ocorridas';

comment on column salve.ficha.ds_equipe_tecnica is 'Registrar os nomes das pessoas que fizeram parte da equipe técnica na elaboração da ficha';

comment on column salve.ficha.ds_colaboradores is 'Registrar os nomes das pessoas que contribuiram na elaboração da ficha';

comment on column salve.ficha.st_sem_uf is 'Informar que a espécie pode nao pussuir um Estado definido. Dominio S ou N';

comment on column salve.ficha.ds_justificativa_eoo is 'Informar a memoria de calculo ou a justificativa do valor da EOO';

comment on column salve.ficha.ds_justificativa_aoo is 'Informar a memoria de calculo ou a justificativa do valor da AOO';

alter table salve.ficha
    owner to postgres;

create table if not exists salve.ciclo_consulta_ficha
(
    sq_ciclo_consulta_ficha serial
        constraint pk_ciclo_consulta_ficha
            primary key,
    sq_ciclo_consulta       bigint not null
        constraint fk_ciclo_consulta_ficha_ciclo
            references salve.ciclo_consulta
            on update restrict on delete cascade,
    sq_ficha                bigint
        constraint fk_ciclo_consulta_ficha_ficha
            references salve.ficha
            on update restrict on delete cascade
);

alter table salve.ciclo_consulta_ficha
    owner to postgres;

create index if not exists ix_ciclo_consulta_ficha_ciclo
    on salve.ciclo_consulta_ficha (sq_ciclo_consulta)
    tablespace pg_default;

comment on index salve.ix_ciclo_consulta_ficha_ciclo is 'Indice coluna id do ciclo consulta';

create index if not exists ix_ciclo_consulta_ficha_ficha
    on salve.ciclo_consulta_ficha (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ciclo_consulta_ficha_ficha is 'Indice coluna id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_consulta_ficha to usr_salve;

create table if not exists salve.ciclo_consulta_pessoa_ficha
(
    sq_ciclo_consulta_pessoa_ficha serial
        constraint pk_ciclo_consulta_pessoa_ficha
            primary key,
    sq_ciclo_consulta_pessoa       bigint not null
        constraint fk_ciclo_cons_pes_ficha_pes
            references salve.ciclo_consulta_pessoa
            on update restrict on delete cascade,
    sq_ciclo_consulta_ficha        bigint not null
        constraint fk_ciclo_cons_pes_ficha_ficha
            references salve.ciclo_consulta_ficha
            on update restrict on delete cascade
);

alter table salve.ciclo_consulta_pessoa_ficha
    owner to postgres;

create index if not exists ix_fk_cic_con_pes_fic_cic_con_fic
    on salve.ciclo_consulta_pessoa_ficha (sq_ciclo_consulta_ficha)
    tablespace pg_default;

comment on index salve.ix_fk_cic_con_pes_fic_cic_con_fic is 'Indice da chave estrangeira da tabela ciclo_consulta_ficha';

create index if not exists ix_fk_cic_con_pes_fic_cic_con_pes
    on salve.ciclo_consulta_pessoa_ficha (sq_ciclo_consulta_pessoa)
    tablespace pg_default;

comment on index salve.ix_fk_cic_con_pes_fic_cic_con_pes is 'Indice da chave estrangeira da tabela ciclo_consulta_pessoa';

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_consulta_pessoa_ficha to usr_salve;

create index if not exists idx_trgm_ficha_nm_cientifico
    on salve.ficha using gin (nm_cientifico public.gin_trgm_ops)
    tablespace pg_default;

create index if not exists ix_ficha_ciclo_anterior
    on salve.ficha (sq_ficha_ciclo_anterior)
    tablespace pg_default;

comment on index salve.ix_ficha_ciclo_anterior is 'Indice pelo ID da ficha no ciclo anterior';

create index if not exists ix_ficha_ciclo_pessoa_altaracao_data
    on salve.ficha (sq_ciclo_avaliacao, sq_usuario_alteracao, dt_alteracao)
    tablespace pg_default;

create index if not exists ix_ficha_doi
    on salve.ficha (sq_ciclo_avaliacao, ds_doi)
    tablespace pg_default;

create index if not exists ix_ficha_grupo_e_subgrupo
    on salve.ficha (sq_grupo, sq_subgrupo)
    tablespace pg_default;

create index if not exists ix_ficha_grupo_salve
    on salve.ficha (sq_grupo_salve)
    tablespace pg_default;

comment on index salve.ix_ficha_grupo_salve is 'Indice pela grupo dos recortes utilizados no modulo publico';

create index if not exists ix_ficha_sq_categoria_final
    on salve.ficha (sq_categoria_final)
    tablespace pg_default;

comment on index salve.ix_ficha_sq_categoria_final is 'Indice pela categoria final da ficha para selecionar as fichas avaliadas';

create index if not exists ix_ficha_sq_ciclo_avaliacao
    on salve.ficha (sq_ciclo_avaliacao)
    tablespace pg_default;

create index if not exists ix_ficha_sq_situacao
    on salve.ficha (sq_situacao_ficha)
    tablespace pg_default;

create index if not exists ix_ficha_sq_taxon
    on salve.ficha (sq_taxon)
    tablespace pg_default;

create index if not exists ix_ficha_sq_unidade_org
    on salve.ficha (sq_unidade_org)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha to usr_salve;

create table if not exists salve.ficha_acao_conservacao
(
    sq_ficha_acao_conservacao    serial
        constraint pk_ficha_acao_conservacao
            primary key,
    sq_ficha                     bigint not null
        constraint fk_ficha_acao_cons_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_acao_conservacao          bigint not null
        constraint fk_ficha_acao_conservacao_acao_conservacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_situacao_acao_conservacao bigint not null
        constraint fk_ficha_acao_cons_sit_acao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_plano_acao                bigint
        constraint fk_ficha_acao_cons_pan
            references salve.plano_acao
            on update restrict on delete restrict,
    sq_tipo_ordenamento          bigint
        constraint fk_ficha_acao_cons_ordenam
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_ficha_acao_conservacao    text
);

alter table salve.ficha_acao_conservacao
    owner to postgres;

create index if not exists ix_ficha_acao_conservacao_ficha
    on salve.ficha_acao_conservacao (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_acao_conservacao_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_acao_conservacao to usr_salve;

create table if not exists salve.ficha_ambiente_restrito
(
    sq_ficha_ambiente_restrito serial
        constraint pk_ficha_ambiente_restrito
            primary key,
    sq_ficha                   bigint not null
        constraint fk_tb_ficha_rest_habit_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_restricao_habitat       bigint not null
        constraint fk_tb_ficha_rest_habit_rest_habit
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_ficha_restricao_habitat text,
    de_restricao_habitat_outro text
);

alter table salve.ficha_ambiente_restrito
    owner to postgres;

create index if not exists ix_ficha_ambiente_restrito_ficha
    on salve.ficha_ambiente_restrito (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_ambiente_restrito_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ambiente_restrito to usr_salve;

create table if not exists salve.ficha_ameaca
(
    sq_ficha_ameaca         serial
        constraint pk_ficha_ameaca
            primary key,
    sq_ficha                bigint not null
        constraint fk_ficha_ameaca_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_criterio_ameaca_iucn bigint not null
        constraint fk_ficha_ameaca_crit_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_ficha_ameaca         text,
    nu_peso                 integer,
    sq_referencia_temporal  bigint
        constraint fk_ficha_ameaca_ref_temporal
            references salve.dados_apoio
            on update cascade on delete restrict
);

comment on column salve.ficha_ameaca.nu_peso is 'Peso atribuido ao item para gerar informacoes para o PRIM. Ex: 1=Secundario ou 2=Principal';

comment on column salve.ficha_ameaca.sq_referencia_temporal is 'Referencia temporal da ameaca, atual, passada ou potencial';

alter table salve.ficha_ameaca
    owner to postgres;

create index if not exists ix_ficha_ameaca_ficha
    on salve.ficha_ameaca (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_ameaca_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ameaca to usr_salve;

create table if not exists salve.ficha_ameaca_efeito
(
    sq_ficha_ameaca_efeito serial
        constraint pk_ficha_ameaca_efeito
            primary key,
    sq_ficha_ameaca        bigint not null
        constraint fk_ficha_ameaca_efeito_ameaca
            references salve.ficha_ameaca
            on update restrict on delete cascade,
    sq_efeito              bigint not null
        constraint fk_ficha_ameaca_efeito_efeito
            references salve.dados_apoio
            on update restrict on delete restrict,
    nu_peso                integer
);

comment on column salve.ficha_ameaca_efeito.sq_ficha_ameaca is 'Id da ameaca atribuida na ficha';

comment on column salve.ficha_ameaca_efeito.sq_efeito is 'Id do efeito cadastrado na tabela dados_apoio';

comment on column salve.ficha_ameaca_efeito.nu_peso is 'Peso atribuido ao item para gerar informacoes para o PRIM. Ex: 1=Secundario ou 2=Principal';

alter table salve.ficha_ameaca_efeito
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ameaca_efeito to usr_salve;

create table if not exists salve.ficha_anexo
(
    sq_ficha_anexo   serial
        constraint pk_ficha_anexo
            primary key,
    sq_ficha         bigint                   not null
        constraint fk_ficha_anexo_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_contexto      bigint                   not null
        constraint fk_ficha_anexo_contexto
            references salve.dados_apoio
            on update restrict on delete restrict,
    de_legenda       text                     not null,
    no_arquivo       varchar(255)             not null,
    de_tipo_conteudo varchar(50)              not null,
    de_local_arquivo varchar(255)             not null,
    in_principal     char default 'N'::bpchar not null,
    dt_anexo         timestamp
);

comment on column salve.ficha_anexo.in_principal is 'No caso de um mapa este campo indica se ele vai ou não ser impresso ou visto na avaliação';

comment on column salve.ficha_anexo.dt_anexo is 'Data do arquivo anexado';

alter table salve.ficha_anexo
    owner to postgres;

create index if not exists ix_ficha_anexo_ficha
    on salve.ficha_anexo (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_anexo_ficha is 'Indice coluna id ficha';

create index if not exists ix_ficha_area_relev_ficha
    on salve.ficha_anexo (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_area_relev_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_anexo to usr_salve;

create table if not exists salve.ficha_aoo
(
    sq_ficha_aoo serial
        constraint pk_ficha_aoo
            primary key,
    sq_ficha     bigint                              not null
        constraint fk_ficha_aoo_ficha
            references salve.ficha
            on update restrict on delete cascade,
    ge_aoo       public.geometry,
    dt_inclusao  timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.ficha_aoo.ge_aoo is 'Colecao de poligons 2x2 e desenhos feitos pelo usuario para calcular a Area de Ocupacao da especie';

alter table salve.ficha_aoo
    owner to postgres;

create index if not exists ix_ficha_aoo_ficha
    on salve.ficha_aoo (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_aoo_ficha is 'Indice coluna id ficha - chave estrangeira';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_aoo to usr_salve;

create table if not exists salve.ficha_area_relevancia
(
    sq_ficha_area_relevancia serial
        constraint pk_ficha_area_relev
            primary key,
    sq_ficha                 bigint not null
        constraint fk_ficha_area_relev_tb_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_tipo_relevancia       bigint not null
        constraint fk_ficha_area_relev_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_estado                bigint not null,
    tx_relevancia            text,
    tx_local                 text,
    sq_uc_federal            bigint
        constraint fk_ficha_area_relev_uc_federal
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_uc_estadual           bigint
        constraint fk_ficha_area_relev_uc_estadual
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_rppn                  bigint
        constraint fk_ficha_area_relev_rppn
            references salve.unidade_conservacao
            on update restrict on delete restrict
);

alter table salve.ficha_area_relevancia
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_area_relevancia to usr_salve;

create table if not exists salve.ficha_area_relevancia_municipio
(
    sq_ficha_area_relevancia_municipio integer default nextval('salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq'::regclass) not null
        constraint pk_ficha_area_relevancia_municipio
            primary key,
    sq_municipio                       bigint                                                                                                     not null,
    sq_ficha_area_relevancia           bigint                                                                                                     not null
        constraint fk_area_relev_municipio_area_relevancia
            references salve.ficha_area_relevancia
            on update restrict on delete cascade,
    tx_relevancia_municipio            text
);

alter table salve.ficha_area_relevancia_municipio
    owner to postgres;

alter sequence salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq owned by salve.ficha_area_relevancia_municipio.sq_ficha_area_relevancia_municipio;

create index if not exists ix_fi_are_rel_muni_fi_are_rel
    on salve.ficha_area_relevancia_municipio (sq_ficha_area_relevancia)
    tablespace pg_default;

comment on index salve.ix_fi_are_rel_muni_fi_are_rel is 'Indice coluna id ficha_area_relevancia';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_area_relevancia_municipio to usr_salve;

create table if not exists salve.ficha_area_vida
(
    sq_ficha_area_vida serial
        constraint pk_ficha_area_vida
            primary key,
    sq_ficha           bigint not null
        constraint fk_ficha_area_vida_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_unidade_area    bigint not null
        constraint fk_ficha_area_vida_unid
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_local           text   not null,
    vl_area            text   not null,
    nu_ano             smallint,
    tx_area            text
);

alter table salve.ficha_area_vida
    owner to postgres;

create index if not exists ix_ficha_area_vida_ficha
    on salve.ficha_area_vida (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_area_vida_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_area_vida to usr_salve;

create table if not exists salve.ficha_autor
(
    sq_ficha_autor serial
        constraint pk_ficha_autor
            primary key,
    sq_autor       bigint not null
        constraint fk_ficha_autor_autor
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ficha       bigint not null
        constraint fk_ficha_autor_ficha
            references salve.ficha
            on update restrict on delete cascade
);

alter table salve.ficha_autor
    owner to postgres;

create index if not exists ix_ficha_autor_ficha
    on salve.ficha_autor (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_autor_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_autor to usr_salve;

create table if not exists salve.ficha_avaliacao_regional
(
    sq_ficha_avaliacao_regional serial
        constraint pk_ficha_avaliacao_regional
            primary key,
    sq_ficha                    bigint  not null
        constraint fk_ficha_aval_reg_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_abrangencia              bigint
        constraint fk_ficha_aval_reg_abrangencia
            references salve.abrangencia
            on update restrict on delete restrict,
    sq_categoria_iucn           bigint  not null
        constraint fk_ficha_aval_reg_categ
            references salve.dados_apoio
            on update restrict on delete restrict,
    nu_ano_avaliacao            integer not null,
    de_criterio_avaliacao_iucn  text,
    tx_justificativa_avaliacao  text,
    st_possivelmente_extinta    varchar(1)
);

comment on table salve.ficha_avaliacao_regional is 'Informacoes sobre as avaliacoes regionais realizadas para a especie';

comment on column salve.ficha_avaliacao_regional.st_possivelmente_extinta is 'informar se a espécie está possivelmente extinta. S ou N';

alter table salve.ficha_avaliacao_regional
    owner to postgres;

create index if not exists ix_ficha_avaliacao_reg_ficha
    on salve.ficha_avaliacao_regional (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_avaliacao_reg_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_avaliacao_regional to usr_salve;

create table if not exists salve.ficha_bacia
(
    sq_ficha_bacia serial
        constraint pk_ficha_bacia
            primary key,
    sq_ficha       bigint not null
        constraint fk_ficha_bacia_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_bacia       bigint not null
        constraint fk_ficha_bacia_bacia
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on table salve.ficha_bacia is 'tabela de bacias hidrograficas onde a especie ocorre';

alter table salve.ficha_bacia
    owner to postgres;

create index if not exists ix_ficha_bacia_bacia
    on salve.ficha_bacia (sq_bacia)
    tablespace pg_default;

create unique index if not exists ixu_ficha_bacia_ficha_bacia
    on salve.ficha_bacia (sq_ficha, sq_bacia)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_bacia to usr_salve;

create table if not exists salve.ficha_bioma
(
    sq_ficha_bioma serial
        constraint pk_ficha_bioma
            primary key,
    sq_ficha       bigint not null
        constraint fk_ficha_bioma_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_bioma       bigint not null
        constraint fk_ficha_bioma_bioma
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on table salve.ficha_bioma is 'tabela de biomas onde a especie ocorre';

alter table salve.ficha_bioma
    owner to postgres;

create index if not exists ix_ficha_bioma_bioma
    on salve.ficha_bioma (sq_bioma)
    tablespace pg_default;

create unique index if not exists ixu_ficha_bioma_ficha_bioma
    on salve.ficha_bioma (sq_ficha, sq_bioma)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_bioma to usr_salve;

create table if not exists salve.ficha_colaboracao
(
    sq_ficha_colaboracao    serial
        constraint pk_ficha_colaboracao
            primary key,
    sq_ciclo_consulta_ficha bigint
        constraint fk_ficha_colaboracao_consulta
            references salve.ciclo_consulta_ficha
            on update restrict on delete cascade,
    sq_situacao             bigint not null
        constraint fk_ficha_colaboracao_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_web_usuario          bigint not null
        constraint fk_ficha_co_fk_ficha__web_usua
            references salve.pessoa
            on update restrict on delete restrict,
    no_campo_ficha          text   not null,
    ds_colaboracao          text,
    ds_ref_bib              text,
    dt_inclusao             timestamp default now(),
    dt_alteracao            timestamp,
    ds_rotulo               text,
    de_rotulo               text,
    sq_tipo_consulta        bigint
        constraint fk_ficha_colaboracao_tipo_consulta
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_nao_aceita           text
);

comment on column salve.ficha_colaboracao.ds_rotulo is 'Nome que será exibido na tela';

comment on column salve.ficha_colaboracao.de_rotulo is 'Nome do campo que será exibido na tela';

comment on column salve.ficha_colaboracao.sq_tipo_consulta is 'código do tipo da consulta em que a colaboração foi feita. Ex: Direta, Ampla ou Revisão';

comment on column salve.ficha_colaboracao.tx_nao_aceita is 'Campo utilizado para justificar a nao aceitacao da colaboracao.';

alter table salve.ficha_colaboracao
    owner to postgres;

create index if not exists ix_ficha_colab_ciclo_consulta_ficha
    on salve.ficha_colaboracao (sq_ciclo_consulta_ficha)
    tablespace pg_default;

create unique index if not exists ixu_ficha_colaboracao
    on salve.ficha_colaboracao (sq_ciclo_consulta_ficha, sq_web_usuario, no_campo_ficha)
    tablespace pg_default;

comment on index salve.ixu_ficha_colaboracao is 'Não permitir duplicidade de colaborações';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_colaboracao to usr_salve;

create table if not exists salve.ficha_consulta_anexo
(
    sq_ficha_consulta_anexo serial
        constraint pk_ficha_consulta_anexo
            primary key,
    sq_ciclo_consulta_ficha bigint
        constraint fk_ficha_consulta_anexo_consulta
            references salve.ciclo_consulta_ficha
            on update restrict on delete cascade,
    sq_web_usuario          bigint       not null
        constraint fk_ficha_consulta_usuario
            references salve.pessoa
            on update restrict on delete restrict,
    sq_situacao             bigint       not null
        constraint fk_ficha_consulta_anexo_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    no_arquivo              varchar(200) not null,
    de_local_arquivo        varchar(200) not null,
    de_tipo_conteudo        varchar(100) not null,
    dt_inclusao             timestamp default CURRENT_TIMESTAMP
);

comment on column salve.ficha_consulta_anexo.sq_ficha_consulta_anexo is 'chave primaria sequencial';

comment on column salve.ficha_consulta_anexo.sq_ciclo_consulta_ficha is 'chave estrangeira referente a consulta que a ficha esta';

comment on column salve.ficha_consulta_anexo.sq_web_usuario is 'chave estrangeira do usuario web que enviou o anexo';

comment on column salve.ficha_consulta_anexo.sq_situacao is 'chave estrangeira da situacao do anexo, se ja foi aceito ou nao';

comment on column salve.ficha_consulta_anexo.no_arquivo is 'nome do arquivo enviado';

comment on column salve.ficha_consulta_anexo.de_local_arquivo is 'nome do arquivo no disco';

comment on column salve.ficha_consulta_anexo.de_tipo_conteudo is 'tipo mime do arquivo:doc,xls,pdf etc';

comment on column salve.ficha_consulta_anexo.dt_inclusao is 'data de inclusao na tabela';

alter table salve.ficha_consulta_anexo
    owner to postgres;

create index if not exists ix_ficha_consulta_anexo_ciclo_consulta_ficha
    on salve.ficha_consulta_anexo (sq_ciclo_consulta_ficha)
    tablespace pg_default;

create unique index if not exists ixu_ficha_consulta_anexo
    on salve.ficha_consulta_anexo (sq_ciclo_consulta_ficha, sq_web_usuario, no_arquivo)
    tablespace pg_default;

comment on index salve.ixu_ficha_consulta_anexo is 'Não permitir duplicidade de colaborações';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_consulta_anexo to usr_salve;

create table if not exists salve.ficha_declinio_populacional
(
    sq_ficha_declinio_populacional serial
        constraint pk_ficha_declinio_populacional
            primary key,
    sq_declinio_populacional       bigint not null
        constraint fk_ficha_declinio_populacional_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ficha                       bigint
        constraint fk_ficha_declinio_populacional_ficha
            references salve.ficha
            on delete cascade
);

alter table salve.ficha_declinio_populacional
    owner to postgres;

create index if not exists ix_ficha_decl_pop_ficha
    on salve.ficha_declinio_populacional (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_decl_pop_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_declinio_populacional to usr_salve;

create table if not exists salve.ficha_eoo
(
    sq_ficha_eoo       serial
        primary key,
    sq_ficha           bigint                              not null
        constraint fk_ficha_eoo_ficha
            references salve.ficha
            on update restrict on delete cascade,
    ge_eoo             public.geometry,
    dt_inclusao        timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao       timestamp default CURRENT_TIMESTAMP not null,
    nu_hydroshed_level integer
);

comment on table salve.ficha_eoo is 'Tabela para armazenamento do polígono da extensao de ocorrencia - EOO da especie';

comment on column salve.ficha_eoo.ge_eoo is 'Poligono da Extensao de Ocorrencia da especie';

comment on column salve.ficha_eoo.nu_hydroshed_level is 'Nivel da hydroshed utilizada no calculo da EOO';

alter table salve.ficha_eoo
    owner to postgres;

create index if not exists ix_ficha_eoo_ficha
    on salve.ficha_eoo (sq_ficha);

comment on index salve.ix_ficha_eoo_ficha is 'Indice coluna id ficha - chave estrangeira';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_eoo to usr_salve;

create table if not exists salve.ficha_habitat
(
    sq_ficha_habitat serial
        constraint pk_ficha_habitat
            primary key,
    sq_ficha         bigint not null
        constraint fk_tb_ficha_habitat_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_habitat       bigint not null
        constraint fk_ficha_habitat_habitat
            references salve.dados_apoio
            on update restrict on delete restrict
);

alter table salve.ficha_habitat
    owner to postgres;

create index if not exists ix_ficha_hab_ficha
    on salve.ficha_habitat (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_hab_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_habitat to usr_salve;

create table if not exists salve.ficha_habito_alimentar
(
    sq_ficha_habito_alimentar serial
        constraint pk_ficha_habito_alimentar
            primary key,
    sq_tipo_habito_alimentar  bigint not null
        constraint fk_ficha_habito_alimentar_tipo_habito_alimentar
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ficha                  bigint not null
        constraint fk_ficha_habito_alimentar_ficha
            references salve.ficha
            on update restrict on delete cascade,
    ds_ficha_habito_alimentar text
);

alter table salve.ficha_habito_alimentar
    owner to postgres;

create index if not exists ix_ficha_hab_alim_ficha
    on salve.ficha_habito_alimentar (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_hab_alim_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_habito_alimentar to usr_salve;

create table if not exists salve.ficha_habito_alimentar_esp
(
    sq_ficha_habito_alimentar_esp serial
        constraint pk_ficha_habito_alimentar_esp
            primary key,
    sq_ficha                      bigint not null
        constraint fk_tb_ficha_habito_alim_esp_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_taxon                      bigint not null
        constraint fk_tb_ficha_habito_alimentar_esp_taxon
            references taxonomia.taxon
            on update restrict on delete restrict,
    sq_categoria_iucn             bigint
        constraint fk_tb_ficha_habito_alimentar_esp_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    ds_ficha_habito_alimentar_esp text
);

alter table salve.ficha_habito_alimentar_esp
    owner to postgres;

create index if not exists ix_ficha_hab_alim_esp_ficha
    on salve.ficha_habito_alimentar_esp (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_hab_alim_esp_ficha is 'Indice coluna id ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_habito_alimentar_esp to usr_salve;

create table if not exists salve.ficha_hist_situacao_excluida
(
    sq_ficha_hist_situacao_excluida integer   default nextval('salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq'::regclass) not null
        constraint pk_ficha_hist_situacao_excluida
            primary key,
    sq_ficha                        bigint                                                                                                       not null
        constraint fk_ficha_hist_situacao_excluida_ficha
            references salve.ficha
            on update cascade on delete cascade,
    sq_usuario_inclusao             bigint                                                                                                       not null,
    ds_justificativa                text                                                                                                         not null,
    dt_inclusao                     timestamp default CURRENT_TIMESTAMP                                                                          not null
);

comment on column salve.ficha_hist_situacao_excluida.sq_ficha_hist_situacao_excluida is 'Chave primaria sequencial da tabela';

comment on column salve.ficha_hist_situacao_excluida.sq_ficha is 'Chave estrangeira da tabela ficha';

comment on column salve.ficha_hist_situacao_excluida.sq_usuario_inclusao is 'Chave estrangeira da tabela corporativa pessoa_fisica da pessoa que fez a inclusao';

comment on column salve.ficha_hist_situacao_excluida.ds_justificativa is 'Texto da justificativa da exclusao';

comment on column salve.ficha_hist_situacao_excluida.dt_inclusao is 'Data de inclusao no banco de dados';

alter table salve.ficha_hist_situacao_excluida
    owner to postgres;

alter sequence salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq owned by salve.ficha_hist_situacao_excluida.sq_ficha_hist_situacao_excluida;

create index if not exists ix_fk_ficha_hist_situacao_excluida_sq_ficha
    on salve.ficha_hist_situacao_excluida (sq_ficha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_hist_situacao_excluida to usr_salve;

create table if not exists salve.ficha_historico_avaliacao
(
    sq_ficha_historico_avaliacao serial
        constraint pk_ficha_historico_avaliacao
            primary key,
    sq_ficha                     bigint   not null
        constraint fk_ficha_hist_aval_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_tipo_avaliacao            bigint   not null
        constraint fk_icha_hist_aval_tipo_aval
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_abrangencia               bigint
        constraint fk_ficha_hist_aval_abrang
            references salve.abrangencia
            on update restrict on delete restrict,
    sq_tipo_abrangencia          bigint
        constraint fk_ficha_hist_aval_tipo_abrang
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_categoria_iucn            bigint
        constraint fk_tb_ficha_hist_aval_cat_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    nu_ano_avaliacao             smallint not null,
    de_criterio_avaliacao_iucn   text,
    tx_justificativa_avaliacao   text,
    tx_regiao_abrangencia        text,
    no_regiao_outra              text,
    sq_taxon                     bigint
        constraint fk_ficha_hist_aval_taxon
            references taxonomia.taxon
            on update restrict on delete restrict,
    st_possivelmente_extinta     char,
    de_categoria_outra           text
);

comment on column salve.ficha_historico_avaliacao.sq_tipo_abrangencia is 'Registra o tipo da abrangencia ( Estado, Bioma, costa, ecoregiao...) em dados_apoio tb_tipo_abrangencia';

comment on column salve.ficha_historico_avaliacao.sq_taxon is 'Chave estrangeira tabela taxonomia.taxon';

comment on column salve.ficha_historico_avaliacao.st_possivelmente_extinta is 'Preenchido com S/N durante a oficina - para Sim ou NÃ£o';

comment on column salve.ficha_historico_avaliacao.de_categoria_outra is 'Quando a categoria nÃ£o for existir na tabela de categorias.';

alter table salve.ficha_historico_avaliacao
    owner to postgres;

create index if not exists ix_ficha_his_aval_taxon
    on salve.ficha_historico_avaliacao (sq_taxon)
    tablespace pg_default;

comment on index salve.ix_ficha_his_aval_taxon is 'Indice pelo tipo da avaliação';

create index if not exists ix_ficha_his_aval_tipo
    on salve.ficha_historico_avaliacao (sq_tipo_avaliacao, sq_taxon)
    tablespace pg_default;

comment on index salve.ix_ficha_his_aval_tipo is 'Indice pelo tipo da avaliação';

create index if not exists ix_ficha_hist_aval_sq_ficha
    on salve.ficha_historico_avaliacao (sq_ficha)
    tablespace pg_default;

create index if not exists ix_ficha_hist_aval_tipo
    on salve.ficha_historico_avaliacao (sq_tipo_avaliacao)
    tablespace pg_default;

comment on index salve.ix_ficha_hist_aval_tipo is 'Indice pelo tipo da avalição';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_historico_avaliacao to usr_salve;

create table if not exists salve.ficha_interacao
(
    sq_ficha_interacao serial
        constraint pk_ficha_interacao
            primary key,
    sq_ficha           bigint not null
        constraint fk_tb_ficha_inter_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_tipo_interacao  bigint not null
        constraint fk_tb_ficha_inter_tipo_inter
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_classificacao   bigint
        constraint fk_tb_ficha_inter_classif
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_taxon           bigint not null
        constraint fk_tb_ficha_inter_taxon
            references taxonomia.taxon
            on update restrict on delete restrict,
    sq_categoria_iucn  bigint
        constraint fk_tb_ficha_inter_cat_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    ds_ficha_interacao text
);

alter table salve.ficha_interacao
    owner to postgres;

create index if not exists ix_ficha_interacao_ficha
    on salve.ficha_interacao (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_interacao_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_interacao to usr_salve;

create table if not exists salve.ficha_lista_convencao
(
    sq_ficha_lista_convencao serial
        constraint pk_ficha_lista_convencao
            primary key,
    sq_ficha                 bigint not null
        constraint fk_ficha_lista_conven_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_lista_convencao       bigint not null
        constraint fk_ficha_lista_conven_lista
            references salve.dados_apoio
            on update restrict on delete restrict,
    nu_ano                   smallint
);

alter table salve.ficha_lista_convencao
    owner to postgres;

create index if not exists ix_ficha_lista_conv_ficha
    on salve.ficha_lista_convencao (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_lista_conv_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_lista_convencao to usr_salve;

create table if not exists salve.ficha_modulo_publico
(
    sq_ficha_modulo_publico bigserial
        constraint pk_ficha_modulo_publico
            primary key,
    sq_ficha                bigint                              not null
        constraint fk_ficha_modulo_publico_ficha
            references salve.ficha
            on update cascade on delete cascade,
    st_exibir               boolean   default false             not null,
    dt_alteracao            timestamp default CURRENT_TIMESTAMP not null,
    sq_pessoa               bigint
        constraint fk_ficha_modulo_publico_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict
);

comment on column salve.ficha_modulo_publico.sq_ficha_modulo_publico is 'Chave primaria sequencial da tabela ';

comment on column salve.ficha_modulo_publico.sq_ficha is 'Chave estrangeira da tabela ficha';

comment on column salve.ficha_modulo_publico.st_exibir is 'Indicar se a ficha esta ou nao visivel no modulo publico';

comment on column salve.ficha_modulo_publico.dt_alteracao is 'Data que a ficha foi colocada ou retirada do modulo publico';

comment on column salve.ficha_modulo_publico.sq_pessoa is 'Id do usuario que colocou ou retirou a ficha do modulo publico';

alter table salve.ficha_modulo_publico
    owner to postgres;

create index if not exists ix_ficha_modulo_publico_ficha
    on salve.ficha_modulo_publico (sq_ficha);

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_modulo_publico to usr_salve;

create table if not exists salve.ficha_mudanca_categoria
(
    sq_ficha_mudanca_categoria serial
        constraint pk_ficha_mudanca_categoria
            primary key,
    sq_ficha                   bigint not null
        constraint tb_ficha_mudanca_cat_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_motivo_mudanca          bigint not null
        constraint tb_ficha_mudanca_cat_motivo
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_ficha_mudanca_categoria text
);

alter table salve.ficha_mudanca_categoria
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_mudanca_categoria to usr_salve;

create table if not exists salve.ficha_multimidia
(
    sq_ficha_multimidia serial
        constraint pk_ficha_multimidia
            primary key,
    sq_ficha            bigint                not null
        constraint fk_ficha_multim_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_tipo             bigint                not null
        constraint fk_ficha_multimidia_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_datum            bigint
        constraint fk_ficha_multim_datum
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_situacao         bigint                not null
        constraint fk_ficha_multimidia_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    de_legenda          text                  not null,
    tx_multimidia       text,
    no_autor            text                  not null,
    de_email_autor      text,
    dt_elaboracao       timestamp(4),
    no_arquivo          text                  not null,
    no_arquivo_disco    text                  not null,
    dt_inclusao         timestamp             not null,
    in_principal        boolean default false not null,
    de_url              text,
    ge_multimidia       public.geometry,
    dt_aprovacao        timestamp(4),
    sq_pessoa_aprovou   bigint,
    tx_nao_aceito       text,
    dt_reprovacao       timestamp,
    sq_pessoa_reprovou  bigint,
    in_destaque         boolean default false not null
);

comment on column salve.ficha_multimidia.sq_tipo is 'Tipo som, imagem ou video';

comment on column salve.ficha_multimidia.de_legenda is 'Descricao curta da imagem, som ou video';

comment on column salve.ficha_multimidia.tx_multimidia is 'Descricao detalhada da imagem, som ou video';

comment on column salve.ficha_multimidia.no_arquivo is 'Nome do arquivo original';

comment on column salve.ficha_multimidia.no_arquivo_disco is 'Hash do arquivo salvo no disco';

comment on column salve.ficha_multimidia.in_principal is 'Cada ficha podera ter somente uma foto marcada como principal. Esta sera a foto impressa na ficha.';

comment on column salve.ficha_multimidia.de_url is 'Url da imagem, video no youtube ou do som quando a mídia não ficar armazenada no icmbio';

comment on column salve.ficha_multimidia.ge_multimidia is 'Coordenada da foto, video ou som do animal';

comment on column salve.ficha_multimidia.dt_aprovacao is 'Data e hora que a foto foi aprovada para visualizacao publica';

comment on column salve.ficha_multimidia.sq_pessoa_aprovou is 'Id da pessoa que aprovou a foto';

comment on column salve.ficha_multimidia.tx_nao_aceito is 'Descricao do motivo porque o registro nao foi aceito.';

comment on column salve.ficha_multimidia.dt_reprovacao is 'Data e hora que o registro foi reprovado.';

comment on column salve.ficha_multimidia.sq_pessoa_reprovou is 'Id da pessoa que reprovou o registro';

alter table salve.ficha_multimidia
    owner to postgres;

create table if not exists salve.ficha_consulta_foto
(
    sq_ficha_consulta_foto  bigserial
        constraint pk_ficha_consulta_foto
            primary key,
    sq_ciclo_consulta_ficha bigint                              not null
        constraint fk_ficha_consulta_foto_ciclo_consulta
            references salve.ciclo_consulta_ficha
            on update restrict on delete cascade,
    sq_web_usuario          bigint                              not null
        constraint fk_ficha_consulta_foto_web_usuario
            references salve.pessoa
            on update restrict on delete cascade,
    sq_ficha_multimidia     bigint                              not null
        constraint fk_ficha_consulta_foto_ficha_multimidia
            references salve.ficha_multimidia
            on update restrict on delete cascade,
    in_tipo_termo_resp      text      default 'USO_IRRESTRITO'::text,
    dt_aceite_termo_resp    timestamp default CURRENT_TIMESTAMP not null,
    nu_ip_computador        text                                not null,
    no_navegador            text                                not null
);

comment on table salve.ficha_consulta_foto is 'Tabela para registrar as fotos enviadas pelas consultas diretas/amplas';

comment on column salve.ficha_consulta_foto.sq_ciclo_consulta_ficha is 'chave extrangeira da tabela ciclo_consulta_ficha';

comment on column salve.ficha_consulta_foto.sq_web_usuario is 'chave extrangeira da tabela web_usuario';

comment on column salve.ficha_consulta_foto.sq_ficha_multimidia is 'chave extrangeira da tabela ficha_multimidia';

comment on column salve.ficha_consulta_foto.in_tipo_termo_resp is 'informar qual tipo de termo de responsabilidade o usuario concordou';

comment on column salve.ficha_consulta_foto.dt_aceite_termo_resp is 'data em que o usuario concordou com o termo de responsabilidade';

comment on column salve.ficha_consulta_foto.nu_ip_computador is 'o ip do computador que o usuario estava utilizando quando concordou com o termo de responsabilidade';

comment on column salve.ficha_consulta_foto.no_navegador is 'o navegador web que o usuario estava utilizando quando concordou com o termo de responsabilidade';

alter table salve.ficha_consulta_foto
    owner to postgres;

create index if not exists ix_ficha_consulta_foto_ciclo_consulta
    on salve.ficha_consulta_foto (sq_ciclo_consulta_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_consulta_foto_ciclo_consulta is 'Indice chave extrangeira da tabela ciclo_consulta_ficha';

create index if not exists ix_ficha_consulta_foto_ficha_multimidia
    on salve.ficha_consulta_foto (sq_ficha_multimidia)
    tablespace pg_default;

comment on index salve.ix_ficha_consulta_foto_ficha_multimidia is 'Indice chave extrangeira da tabela ficha_multimidia';

create index if not exists ix_ficha_consulta_foto_web_usuario
    on salve.ficha_consulta_foto (sq_web_usuario)
    tablespace pg_default;

comment on index salve.ix_ficha_consulta_foto_web_usuario is 'Indice chave extrangeira da tabela web_usuario';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_consulta_foto to usr_salve;

create index if not exists ix_ficha_multimidia_ficha
    on salve.ficha_multimidia (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_multimidia_ficha is 'Indice coluna id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_multimidia to usr_salve;

create table if not exists salve.ficha_nome_comum
(
    sq_ficha_nome_comum serial
        constraint pk_ficha_nome_comum
            primary key,
    sq_ficha            bigint not null
        constraint fk_ficha_nome_comum_ficha
            references salve.ficha
            on update restrict on delete cascade,
    no_comum            text   not null,
    de_regiao_lingua    text
);

alter table salve.ficha_nome_comum
    owner to postgres;

create index if not exists idx_trgm_ficha_nome_comum_no_comum
    on salve.ficha_nome_comum using gin (fn_remove_acentuacao(no_comum::character varying) public.gin_trgm_ops)
    tablespace pg_default;

create index if not exists ix_ficha_nome_comum_ficha
    on salve.ficha_nome_comum (sq_ficha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_nome_comum to usr_salve;

create table if not exists salve.ficha_ocorrencia
(
    sq_ficha_ocorrencia               serial
        constraint pk_ficha_ocorrencia
            primary key,
    sq_ficha                          bigint                not null
        constraint fk_ficha_ocorr_ficha
            references salve.ficha
            on update restrict on delete cascade
        constraint fk_ficha_ocorr_ref_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_tipo_registro                  bigint
        constraint fk_ficha_ocorr_tipo_reg
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ref_aproximacao                bigint
        constraint fk_ficha_ocorr_ref_aprox
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_datum                          bigint
        constraint fk_ficha_ocorr_datum
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_precisao_coordenada            bigint
        constraint fk_ficha_ocorr_prec_coord
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_formato_coord_original         bigint
        constraint fk_ficha_ocorr_form_original
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_continente                     bigint
        constraint fk_ficha_ocorr_continente
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_pais                           bigint
        constraint fk_ficha_ocorr_pais
            references salve.pais
            on update restrict on delete restrict,
    sq_qualificador_val_taxon         bigint
        constraint fk_ficha_ocorr_q_val_taxon
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_prazo_carencia                 bigint
        constraint fk_ficha_ocorr_prazo_caren
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_estado                         bigint
        constraint fk_ficha_ocorr_estado
            references salve.estado
            on update restrict on delete restrict,
    sq_municipio                      bigint
        constraint fk_ficha_ocorr_municipio
            references salve.municipio
            on update restrict on delete restrict,
    sq_metodo_aproximacao             bigint
        constraint fk_ficha_ocorr_met_aprox
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_taxon_citado                   bigint
        constraint fk_ficha_ocorr_tax_citado
            references taxonomia.taxon
            on update restrict on delete restrict,
    sq_bacia_hidrografica             bigint
        constraint fk_ficha_ocorr_bacia
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_bioma                          bigint
        constraint fk_ficha_ocorr_bioma
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_uc_federal                     bigint
        constraint fk_ficha_ocorr_uc_federal
            references salve.instituicao
            on update restrict on delete restrict,
    sq_uc_estadual                    bigint
        constraint fk_ficha_ocorr_uc_estadual
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_rppn                           bigint
        constraint fk_ficha_ocorr_rppn
            references salve.unidade_conservacao
            on update restrict on delete restrict,
    sq_habitat                        bigint
        constraint fk_ficha_ocorr_habitat
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_pessoa_compilador              bigint,
    sq_pessoa_revisor                 bigint,
    sq_pessoa_validador               bigint,
    sq_contexto                       bigint
        constraint fk_ficha_ocorr_contexto
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_tipo_abrangencia               bigint
        constraint fk_ficha_ocorr_tipo_abrangencia
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_divisao_administrativa         bigint
        constraint fk_ficha_ocorr_div_adm
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ecoregiao                      bigint
        constraint fk_ficha_ocorr_ecoregiao
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_metodo_aproximacao             text,
    tx_local                          text,
    no_localidade                     text,
    dt_registro                       date,
    hr_registro                       text,
    vl_elevacao_min                   numeric(10, 2),
    vl_elevacao_max                   numeric(10, 2),
    vl_profundidade_min               numeric(10, 2),
    vl_profundidade_max               numeric(10, 2),
    de_identificador                  text,
    dt_identificacao                  date,
    de_centro_responsavel             text,
    in_presenca_atual                 char,
    tx_tombamento                     text,
    tx_instituicao                    text,
    dt_compilacao                     date,
    dt_revisao                        date,
    dt_validacao                      date,
    nu_dia_registro                   integer,
    nu_mes_registro                   integer,
    nu_ano_registro                   integer,
    in_data_desconhecida              char,
    tx_nao_utilizado_avaliacao        text,
    in_utilizado_avaliacao            char,
    dt_invalidado                     timestamp,
    ge_ocorrencia                     public.geometry,
    sq_abrangencia                    bigint,
    dt_inclusao                       timestamp,
    sq_pessoa_inclusao                bigint
        constraint fk_ficha_ocorrencia_pessoa_inclusao
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    sq_situacao                       bigint
        constraint fk_ficha_ocorrencia_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    dt_alteracao                      timestamp,
    sq_pessoa_alteracao               bigint
        constraint fk_ficha_ocorrencia_pessoa_alt
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    nu_dia_registro_fim               integer,
    nu_mes_registro_fim               integer,
    nu_ano_registro_fim               integer,
    id_origem                         varchar(50),
    tx_observacao                     text,
    tx_nao_aceita                     text,
    nu_altitude                       integer,
    in_sensivel                       char    default 'N'::bpchar,
    in_processamento                  varchar(10),
    sq_motivo_nao_utilizado_avaliacao bigint
        constraint fk_ficha_ocorr_motivo_nao_util_avaliacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    st_adicionado_apos_avaliacao      boolean default false not null,
    sq_situacao_avaliacao             bigint  default 1331  not null
        constraint fk_ficha_ocorr_sit_avaliacao
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on column salve.ficha_ocorrencia.sq_contexto is 'Definir o contexto da ocorrência, sendo: uf, bioma, bacia hidrografica ou registro de ocorrência';

comment on column salve.ficha_ocorrencia.sq_tipo_abrangencia is 'Registrar o tipo da abrangência. ex: uf,bioma,bacia etc ';

comment on column salve.ficha_ocorrencia.tx_local is 'Observações sobre o local ou descrição do local';

comment on column salve.ficha_ocorrencia.in_presenca_atual is 'Indicação da presença atual da espécie na coordenada.';

comment on column salve.ficha_ocorrencia.in_data_desconhecida is 'informação se a data/ano do registro da ocorrência são desconhecidos ou não.';

comment on column salve.ficha_ocorrencia.tx_nao_utilizado_avaliacao is 'Justificativa quando não for utilizado na avaliação';

comment on column salve.ficha_ocorrencia.in_utilizado_avaliacao is 'Informa se o registro foi ou não utilizado na avaliação';

comment on column salve.ficha_ocorrencia.dt_invalidado is 'Data que a coordenada foi marcada como inválida';

comment on column salve.ficha_ocorrencia.sq_situacao is 'informar se o registro esta aceito, nao aceito, nao validado ou resolver em oficina';

comment on column salve.ficha_ocorrencia.nu_dia_registro_fim is 'Dia final do período do registro';

comment on column salve.ficha_ocorrencia.nu_mes_registro_fim is 'Mês final do período do registro';

comment on column salve.ficha_ocorrencia.nu_ano_registro_fim is 'Ano final do período de registro';

comment on column salve.ficha_ocorrencia.id_origem is 'Identificador único do registro na tabela de origem para as ocorrências importadas de outros bancos de dados';

comment on column salve.ficha_ocorrencia.tx_observacao is 'Campo para informações adicionais sobre o registro de ocorrência.';

comment on column salve.ficha_ocorrencia.tx_nao_aceita is 'Campo utilizado para justificar a nao aceitacao da colaboracao.';

comment on column salve.ficha_ocorrencia.nu_altitude is 'valor da altitude em que a especie foi localizada';

comment on column salve.ficha_ocorrencia.in_sensivel is 'informar se o ponto da ocorrência é um dado sensivel para divulgacao publica';

comment on column salve.ficha_ocorrencia.in_processamento is 'informação para controle do processamento de atualizacao do estado, bioma e ucs na ficha da especie.';

comment on column salve.ficha_ocorrencia.sq_motivo_nao_utilizado_avaliacao is 'Id do motivo do registro nao ter sido utilizado na avaliacao.';

comment on column salve.ficha_ocorrencia.st_adicionado_apos_avaliacao is 'true/false se o registro foi adicionado após a avaliação';

comment on column salve.ficha_ocorrencia.sq_situacao_avaliacao is 'Informar a situação do registro durante a avaliação. Ex: utilizado, nao utilizado etc';

alter table salve.ficha_ocorrencia
    owner to postgres;

create table if not exists salve.ficha_ameaca_regiao
(
    sq_ficha_ameaca_regiao serial
        constraint pk_ficha_ameaca_regiao
            primary key,
    sq_ficha_ameaca        bigint
        constraint fk_ficha_ameaca_regiao_ameaca
            references salve.ficha_ameaca
            on update restrict on delete cascade,
    sq_ficha_ocorrencia    bigint not null
        constraint fk_ficha_ameaca_regiao_ocorrencia
            references salve.ficha_ocorrencia
            on update restrict on delete cascade
);

comment on table salve.ficha_ameaca_regiao is 'Registrar as regiões da Ameaça';

alter table salve.ficha_ameaca_regiao
    owner to postgres;

create index if not exists ix_ficha_ame_reg_ficha_ameaca
    on salve.ficha_ameaca_regiao (sq_ficha_ameaca)
    tablespace pg_default;

comment on index salve.ix_ficha_ame_reg_ficha_ameaca is 'Indice coluna id ficha_ameaca';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ameaca_regiao to usr_salve;

create index if not exists ix_ficha_ocorrencia_contexto
    on salve.ficha_ocorrencia (sq_contexto)
    tablespace pg_default;

create index if not exists ix_ficha_ocorrencia_geo_gist
    on salve.ficha_ocorrencia using gist (ge_ocorrencia)
    tablespace pg_default;

create index if not exists ix_ficha_ocorrencia_importacao
    on salve.ficha_ocorrencia (sq_ficha, ge_ocorrencia, sq_prazo_carencia, sq_datum, sq_formato_coord_original)
    tablespace pg_default;

comment on index salve.ix_ficha_ocorrencia_importacao is 'Indice utilizado para pesquisa de duplicidade de registros no processo de importacao das planilhas';

create index if not exists ix_ficha_ocorrencia_sq_ficha
    on salve.ficha_ocorrencia (sq_ficha, sq_contexto);

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia to usr_salve;

create table if not exists salve.ficha_ocorrencia_consulta
(
    sq_ficha_ocorrencia_consulta serial
        constraint pk_ficha_ocorrencia_consulta
            primary key,
    sq_ciclo_consulta_ficha      bigint not null
        constraint fk_ficha_oco_cons_ciclo_cons
            references salve.ciclo_consulta_ficha
            on update restrict on delete cascade,
    sq_ficha_ocorrencia          bigint not null
        constraint fk_ficha_oco_cons_ocorrencia
            references salve.ficha_ocorrencia
            on update restrict on delete cascade,
    sq_web_usuario               bigint not null
        constraint fk_ficha_oco_cons_usuario
            references salve.pessoa
            on update restrict on delete restrict,
    tx_observacao                text,
    sq_tipo_consulta             bigint
        constraint fk_ficha_ocorrencia_consulta_tipo_consulta
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_situacao                  bigint
        constraint fk_ficha_oco_consulta_situacao
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on column salve.ficha_ocorrencia_consulta.sq_tipo_consulta is 'código do tipo da consulta em que a colaboração foi feita. Ex: Direta, Ampla ou Revisão';

comment on column salve.ficha_ocorrencia_consulta.sq_situacao is 'Situacao da colaboracao do ponto antes e apos a consolidacao';

alter table salve.ficha_ocorrencia_consulta
    owner to postgres;

create table if not exists salve.ficha_consulta_multimidia
(
    sq_ficha_consulta_multimidia serial
        constraint pk_ficha_consulta_multimidia
            primary key,
    sq_ficha_ocorrencia_consulta bigint not null
        constraint fk_ficha_cons_midia_oco_cons
            references salve.ficha_ocorrencia_consulta
            on update restrict on delete cascade,
    sq_ficha_multimidia          bigint not null
        constraint fk_ficha_cons_midia_ficha_midia
            references salve.ficha_multimidia
            on update restrict on delete cascade
);

alter table salve.ficha_consulta_multimidia
    owner to postgres;

create index if not exists ix_ficha_consulta_multimidia_ficha_multimidia
    on salve.ficha_consulta_multimidia (sq_ficha_multimidia);

comment on index salve.ix_ficha_consulta_multimidia_ficha_multimidia is 'Indice chave extrangeira da tabela ficha_multimidia';

create index if not exists ix_ficha_consulta_multimidia_oco_cons
    on salve.ficha_consulta_multimidia (sq_ficha_ocorrencia_consulta);

comment on index salve.ix_ficha_consulta_multimidia_oco_cons is 'Indice chave extrangeira da tabela ficha_ocorrencia_consulta';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_consulta_multimidia to usr_salve;

create index if not exists ix_fic_oco_cons_cic_con_ficha
    on salve.ficha_ocorrencia_consulta (sq_ciclo_consulta_ficha)
    tablespace pg_default;

comment on index salve.ix_fic_oco_cons_cic_con_ficha is 'Indice pelo id da ciclo_consulta_ficha';

create index if not exists ix_ficha_oco_cons_ficha_ocorr
    on salve.ficha_ocorrencia_consulta (sq_ficha_ocorrencia)
    tablespace pg_default;

comment on index salve.ix_ficha_oco_cons_ficha_ocorr is 'Indice pelo id da ficha_ocorrencia';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_consulta to usr_salve;

create table if not exists salve.ficha_ocorrencia_multimidia
(
    sq_ficha_ocorrencia_multimidia serial
        constraint pk_ficha_ocorrencia_multimidia
            primary key,
    sq_ficha_ocorrencia            bigint not null
        constraint fk_ficha_oco_multimidia_ficha_oco
            references salve.ficha_ocorrencia
            on update restrict on delete cascade,
    sq_ficha_multimidia            bigint not null
        constraint fk_ficha_oco_multimidia_ficha_midia
            references salve.ficha_multimidia
            on update restrict on delete cascade
);

comment on column salve.ficha_ocorrencia_multimidia.sq_ficha_ocorrencia is 'Chave extrangeira da tabela ficha_ocorrencia';

comment on column salve.ficha_ocorrencia_multimidia.sq_ficha_multimidia is 'Chave extrangeira da tabela ficha_multimidia';

alter table salve.ficha_ocorrencia_multimidia
    owner to postgres;

create index if not exists ix_ficha_ocorrencia_multimidia_ficha_multimidia
    on salve.ficha_ocorrencia_multimidia (sq_ficha_multimidia);

comment on index salve.ix_ficha_ocorrencia_multimidia_ficha_multimidia is 'Indice chave extrangeira da tabela ficha_multimidia';

create index if not exists ix_ficha_ocorrencia_multimidia_ficha_ocorrencia
    on salve.ficha_ocorrencia_multimidia (sq_ficha_ocorrencia);

comment on index salve.ix_ficha_ocorrencia_multimidia_ficha_ocorrencia is 'Indice chave extrangeira da tabela ficha_ocorrencia';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_multimidia to usr_salve;

create table if not exists salve.ficha_ocorrencia_portalbio
(
    sq_ficha_ocorrencia_portalbio     serial
        constraint pk_ficha_oco_portalbio
            primary key,
    sq_ficha                          bigint                      not null
        constraint fk_ficha_oco_portalbio_ficha
            references salve.ficha
            on update restrict on delete cascade,
    de_uuid                           text                        not null,
    no_cientifico                     text                        not null,
    no_local                          text,
    no_instituicao                    text,
    no_autor                          text,
    dt_ocorrencia                     date,
    dt_carencia                       date,
    de_ameaca                         text,
    tx_ocorrencia                     text                        not null,
    ge_ocorrencia                     public.geometry                    not null,
    sq_situacao                       bigint
        constraint fk_ficha_ocorrencia_portalbio_situacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_pessoa_validador               bigint
        constraint fk_ficha_oco_portalbio_validador
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_validacao                      timestamp,
    sq_pessoa_revisor                 bigint
        constraint fk_ficha_oco_portalbio_revisor
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_revisao                        timestamp,
    in_utilizado_avaliacao            varchar(1),
    tx_nao_utilizado_avaliacao        text,
    tx_observacao                     text,
    id_origem                         bigint,
    dt_alteracao                      timestamp,
    no_base_dados                     varchar(50),
    sq_pais                           bigint,
    sq_estado                         bigint,
    sq_municipio                      bigint,
    sq_bioma                          bigint,
    sq_uc_federal                     bigint,
    sq_uc_estadual                    bigint,
    sq_rppn                           bigint,
    in_processamento                  varchar(10),
    sq_bacia_hidrografica             bigint,
    sq_motivo_nao_utilizado_avaliacao bigint,
    in_data_desconhecida              char,
    tx_nao_aceita                     text,
    st_adicionado_apos_avaliacao      boolean default false       not null,
    sq_situacao_avaliacao             bigint  default 1331        not null
        constraint fk_ficha_oco_port_sit_avaliacao
            references salve.dados_apoio
            on update restrict on delete restrict,
    in_presenca_atual                 char    default 'S'::bpchar not null
);

comment on table salve.ficha_ocorrencia_portalbio is 'Registrar as ocorrêcias do portalbio que foram utilizadas ou não na avaliação';

comment on column salve.ficha_ocorrencia_portalbio.sq_situacao is 'Situação do registro';

comment on column salve.ficha_ocorrencia_portalbio.sq_pessoa_validador is 'Pessoa que validou o registro';

comment on column salve.ficha_ocorrencia_portalbio.dt_validacao is 'Data e hora da validacao';

comment on column salve.ficha_ocorrencia_portalbio.sq_pessoa_revisor is 'Pessoa que fez a revisao do registro';

comment on column salve.ficha_ocorrencia_portalbio.dt_revisao is 'Data e hora da revisao';

comment on column salve.ficha_ocorrencia_portalbio.in_utilizado_avaliacao is 'Indica se o registro foi ou nao utilizado na avaliacao da especie';

comment on column salve.ficha_ocorrencia_portalbio.tx_nao_utilizado_avaliacao is 'Justificativa de não utilização do registro no processo de avaliação';

comment on column salve.ficha_ocorrencia_portalbio.tx_observacao is 'Campo para informações adicionais sobre o registro de ocorrência.';

comment on column salve.ficha_ocorrencia_portalbio.id_origem is 'Identificador Ãºnico do registro na base de dados de origem.';

comment on column salve.ficha_ocorrencia_portalbio.dt_alteracao is 'Data da Ãºltima dt_alteracao do registro';

comment on column salve.ficha_ocorrencia_portalbio.no_base_dados is 'Nome da base de dados de origem da informaÃ§Ã£o. Ex: SISBIO,ARA,JBRJ';

comment on column salve.ficha_ocorrencia_portalbio.in_processamento is 'Informação para controle do processamento de atualizacao do estado, bioma e ucs na ficha da especie.';

comment on column salve.ficha_ocorrencia_portalbio.sq_bacia_hidrografica is 'Bacia hidrografica que o ponto pertence';

comment on column salve.ficha_ocorrencia_portalbio.sq_motivo_nao_utilizado_avaliacao is 'Id do motivo do registro nao ter sido utilizado na avaliacao.';

comment on column salve.ficha_ocorrencia_portalbio.in_data_desconhecida is 'Informar se a data do registro da ocorrência é desconhecida ou não: S/N.';

comment on column salve.ficha_ocorrencia_portalbio.tx_nao_aceita is 'Motivo pelo qual a ocorrencia nao foi aceita na avaliacao';

comment on column salve.ficha_ocorrencia_portalbio.st_adicionado_apos_avaliacao is 'True/False se o registro foi adicionado após a avaliação';

comment on column salve.ficha_ocorrencia_portalbio.sq_situacao_avaliacao is 'Informar a situação do registro durante a avaliação. Ex: utilizado, nao utilizado etc';

alter table salve.ficha_ocorrencia_portalbio
    owner to postgres;

create index if not exists ix_ficha_oco_port_ficha
    on salve.ficha_ocorrencia_portalbio (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_oco_port_ficha is 'Indice pelo id da ficha';

create index if not exists ix_ficha_oco_portalbio_id_origem
    on salve.ficha_ocorrencia_portalbio (sq_ficha, no_base_dados, id_origem);

create index if not exists ix_ficha_ocorrencia_portalbio_geo_gist
    on salve.ficha_ocorrencia_portalbio using gist (ge_ocorrencia)
    tablespace pg_default;

create unique index if not exists ixu_ficha_oco_portalbio_coord
    on salve.ficha_ocorrencia_portalbio (sq_ficha, no_autor, dt_ocorrencia, ge_ocorrencia)
    tablespace pg_default;

create unique index if not exists ixu_ficha_oco_portalbio_uuid
    on salve.ficha_ocorrencia_portalbio (sq_ficha, de_uuid)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_portalbio to usr_salve;

create table if not exists salve.ficha_ocorrencia_portalbio_reg
(
    sq_ficha_ocorrencia_portalbio_reg bigint    default nextval('salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq'::regclass) not null
        constraint pk_ficha_oco_portalbio_reg
            primary key,
    sq_ficha_ocorrencia_portalbio     bigint                                                                                                       not null
        constraint fk_ficha_oco_por_reg_fic_oco_por
            references salve.ficha_ocorrencia_portalbio
            on delete cascade,
    sq_registro                       bigint                                                                                                       not null
        constraint fk_ficha_oco_por_reg_reg
            references salve.registro
            on delete cascade,
    dt_inclusao                       timestamp default CURRENT_TIMESTAMP                                                                          not null
);

comment on table salve.ficha_ocorrencia_portalbio_reg is 'tabela para integração da ocorrencia do portalbio com a tabela de pontos';

alter table salve.ficha_ocorrencia_portalbio_reg
    owner to postgres;

alter sequence salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq owned by salve.ficha_ocorrencia_portalbio_reg.sq_ficha_ocorrencia_portalbio_reg;

create index if not exists ix_ficha_oco_por_reg_fic_oco_por
    on salve.ficha_ocorrencia_portalbio_reg (sq_ficha_ocorrencia_portalbio)
    tablespace pg_default;

create index if not exists ix_ficha_oco_por_reg_reg
    on salve.ficha_ocorrencia_portalbio_reg (sq_registro)
    tablespace pg_default;

create unique index if not exists ixu_ficha_oco_por_reg_oco_reg
    on salve.ficha_ocorrencia_portalbio_reg (sq_ficha_ocorrencia_portalbio, sq_registro)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_portalbio_reg to usr_salve;

create table if not exists salve.ficha_ocorrencia_registro
(
    sq_ficha_ocorrencia_registro bigserial
        constraint pk_ficha_ocorrencia_registro
            primary key,
    sq_ficha_ocorrencia          bigint                              not null
        constraint fk_ficha_oco_reg_ficha_oco
            references salve.ficha_ocorrencia
            on delete cascade,
    sq_registro                  bigint                              not null
        constraint fk_ficha_oco_reg_reg
            references salve.registro
            on delete cascade,
    dt_inclusao                  timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.ficha_ocorrencia_registro is 'tabela para integração da ocorrencia com a tabela pontos (registro)';

alter table salve.ficha_ocorrencia_registro
    owner to postgres;

create index if not exists ix_ficha_oco_reg_ficha_oco
    on salve.ficha_ocorrencia_registro (sq_ficha_ocorrencia)
    tablespace pg_default;

create index if not exists ix_ficha_oco_reg_reg
    on salve.ficha_ocorrencia_registro (sq_registro)
    tablespace pg_default;

create unique index if not exists ixu_ficha_oco_reg_oco_reg
    on salve.ficha_ocorrencia_registro (sq_ficha_ocorrencia, sq_registro)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_registro to usr_salve;

create table if not exists salve.ficha_ocorrencia_validacao
(
    sq_ficha_ocorrencia_validacao serial
        constraint pk_ficha_ocorrencia_validacao
            primary key,
    sq_ficha_ocorrencia           bigint
        constraint fk_ficha_oco_val_ocorrencia
            references salve.ficha_ocorrencia
            on update restrict on delete cascade,
    sq_ficha_ocorrencia_portalbio bigint
        constraint fk_ficha_oco_val_portalbio
            references salve.ficha_ocorrencia_portalbio
            on update restrict on delete cascade,
    sq_web_usuario                bigint
        constraint fk_ficha_oco_val_usuario
            references salve.pessoa
            on update restrict on delete restrict,
    in_valido                     char
        constraint ck_ficha_oco_in_valido
            check ((in_valido IS NULL) OR (in_valido = ANY (ARRAY ['S'::bpchar, 'N'::bpchar]))),
    tx_observacao                 text,
    dt_inclusao                   timestamp not null,
    sq_ciclo_consulta_ficha       bigint
        constraint fk_ficha_oco_val_ciclo_consulta_ficha
            references salve.ciclo_consulta_ficha
            on delete cascade,
    sq_situacao                   bigint
        constraint fk_ficha_oco_val_ciclo_situacao
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on table salve.ficha_ocorrencia_validacao is 'tabela para registrar as sugestoes de validacao da coordenada se e valida ou nao pelos validadores no modulo consulta externa';

comment on column salve.ficha_ocorrencia_validacao.in_valido is 'S/N';

comment on column salve.ficha_ocorrencia_validacao.sq_situacao is 'Situacao da colaboracao antes e apos a consolidacao';

alter table salve.ficha_ocorrencia_validacao
    owner to postgres;

create index if not exists ix_ficha_oco_val_ciclo_consulta_ficha
    on salve.ficha_ocorrencia_validacao (sq_ciclo_consulta_ficha)
    tablespace pg_default;

create index if not exists ix_ficha_oco_val_ficha_oco
    on salve.ficha_ocorrencia_validacao (sq_ficha_ocorrencia)
    tablespace pg_default;

comment on index salve.ix_ficha_oco_val_ficha_oco is 'Indice pelo id da ficha_ocorrencia';

create index if not exists ix_ficha_oco_val_ficha_oco_port
    on salve.ficha_ocorrencia_validacao (sq_ficha_ocorrencia_portalbio)
    tablespace pg_default;

comment on index salve.ix_ficha_oco_val_ficha_oco_port is 'Indice pelo id da ficha_ocorrencia_portalbio';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_validacao to usr_salve;

create table if not exists salve.ficha_pendencia
(
    sq_ficha_pendencia  serial
        constraint pk_ficha_pendencia
            primary key,
    sq_usuario          bigint                   not null
        constraint fk_ficha_pendencia_usuario
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    sq_ficha            bigint                   not null
        constraint fk_ficha_pendencia_ficha
            references salve.ficha
            on update restrict on delete cascade,
    de_assunto          text,
    tx_pendencia        text                     not null,
    dt_pendencia        date                     not null,
    st_pendente         char default 'S'::bpchar not null
        constraint ckc_st_pendente_ficha_pe
            check (st_pendente = ANY (ARRAY ['S'::bpchar, 'N'::bpchar])),
    sq_contexto         bigint
        constraint fk_ficha_pendencia_contexto
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_usuario_resolveu bigint
        constraint fk_ficha_pendencia_usr_resolveu
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_resolvida        timestamp,
    sq_usuario_revisou  bigint
        constraint fk_ficha_pendencia_usr_revisou
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_revisao          timestamp
);

comment on column salve.ficha_pendencia.sq_contexto is 'Informa em qual contexto a pendência foi gerada. Ex: cadastro, validação, oficina etc.';

comment on column salve.ficha_pendencia.sq_usuario_resolveu is 'id do usuario que marcou a pendência como resolvida';

comment on column salve.ficha_pendencia.dt_resolvida is 'data que a pendência foi resolvida';

comment on column salve.ficha_pendencia.sq_usuario_revisou is 'id do usuario que confirmou se a pendencia foi resolvida';

comment on column salve.ficha_pendencia.dt_revisao is 'data que o usuario confirmou que a pendencia foi removida';

alter table salve.ficha_pendencia
    owner to postgres;

create index if not exists ix_ficha_pendencia_ficha
    on salve.ficha_pendencia (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_pendencia_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_pendencia to usr_salve;

create table if not exists salve.ficha_pendencia_email
(
    sq_ficha_pendencia_email bigserial
        constraint ficha_pendencia_email_pk
            primary key,
    sq_ficha_pendencia       bigint                              not null
        constraint fk_ficha_pendencia_email_sq_ficha_pendencia
            references salve.ficha_pendencia
            on update restrict on delete cascade,
    dt_envio                 date,
    tx_log                   text,
    dt_inclusao              timestamp default CURRENT_TIMESTAMP not null
);

comment on column salve.ficha_pendencia_email.sq_ficha_pendencia_email is 'Chave primaria sequencial da tabela ficha_pendencia_email';

comment on column salve.ficha_pendencia_email.sq_ficha_pendencia is 'Chave estrangeira da tabela ficha_pendencia';

comment on column salve.ficha_pendencia_email.dt_envio is 'Data de envio do email, se null significa que nao houve envio';

comment on column salve.ficha_pendencia_email.tx_log is 'Log do envio realizado com o nome e email para quem foi enviado';

comment on column salve.ficha_pendencia_email.dt_inclusao is 'Data de criacao da pendencia';

alter table salve.ficha_pendencia_email
    owner to postgres;

create index if not exists ix_ficha_pend_email_ficha_pend_fk
    on salve.ficha_pendencia_email (sq_ficha_pendencia)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_pendencia_email to usr_salve;

create table if not exists salve.ficha_pesquisa
(
    sq_ficha_pesquisa    serial
        constraint pk_ficha_pesquisa
            primary key,
    sq_ficha             bigint not null
        constraint fk_ficha_pesq_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_tema_pesquisa     bigint not null
        constraint fk_ficha_pesq_tema_pesq
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_situacao_pesquisa bigint not null
        constraint fk_ficha_pesq_sit_pesq
            references salve.dados_apoio
            on update restrict on delete restrict,
    tx_ficha_pesquisa    text
);

alter table salve.ficha_pesquisa
    owner to postgres;

create index if not exists ix_ficha_pesquisa_ficha
    on salve.ficha_pesquisa (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_pesquisa_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_pesquisa to usr_salve;

create table if not exists salve.ficha_pessoa
(
    sq_ficha_pessoa    serial
        constraint pk_ficha_pessoa
            primary key,
    sq_ficha           bigint                   not null
        constraint fk_ficha_pessoa_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_papel           bigint                   not null
        constraint fk_ficha_pessoa_papel
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_pessoa          bigint                   not null
        constraint fk_ficha_pessoa_pessoa
            references salve.pessoa
            on update restrict on delete restrict,
    in_ativo           char default 'S'::bpchar not null,
    sq_web_instituicao bigint
        constraint fk_ficha_pessoa_instituicao
            references salve.instituicao
            on update restrict on delete restrict,
    sq_grupo           bigint
        constraint fk_ficha_pessoa_grupo
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on column salve.ficha_pessoa.in_ativo is 'Informação se a pessoa está ativa ou não';

comment on column salve.ficha_pessoa.sq_web_instituicao is 'Chave estrangeira da relação da pessoa_ficha com a instituição do coordenador de taxon';

comment on column salve.ficha_pessoa.sq_grupo is 'Chave estrangeira da tabela salve.dados_apoio - TB_GRUPO';

alter table salve.ficha_pessoa
    owner to postgres;

create index if not exists ix_ficha_pessoa_ficha
    on salve.ficha_pessoa (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_pessoa_ficha is 'Indice pelo id da ficha';

create index if not exists ix_ficha_pessoa_ficha_pessoa_papel_ativo
    on salve.ficha_pessoa (sq_ficha, sq_pessoa, sq_papel, in_ativo)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_pessoa to usr_salve;

create table if not exists salve.ficha_popul_estimada_local
(
    sq_ficha_popul_estimada_local serial
        constraint pk_ficha_popul_estimada_local
            primary key,
    sq_ficha                      bigint not null
        constraint fk_ficha_pop_estim_local_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_unid_abundancia_populacao  bigint
        constraint fk_ficha_pop_estim_local_unid_abund
            references salve.dados_apoio
            on update restrict on delete restrict,
    ds_local                      text,
    ds_mes_ano_inicio             text,
    ds_mes_ano_fim                text,
    ds_estimativa_populacao       text,
    de_valor_abundancia           text,
    sq_abrangencia                bigint
        constraint fk_ficha_pop_est_loc_abrangencia
            references salve.abrangencia
            on update restrict on delete restrict
);

comment on column salve.ficha_popul_estimada_local.de_valor_abundancia is 'valor ou faixa da abundância ex. 0,53 a 0,79';

comment on column salve.ficha_popul_estimada_local.sq_abrangencia is 'tipo da abrangencia';

alter table salve.ficha_popul_estimada_local
    owner to postgres;

create index if not exists ix_ficha_pop_est_ficha
    on salve.ficha_popul_estimada_local (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_pop_est_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_popul_estimada_local to usr_salve;

create table if not exists salve.ficha_sincronismo
(
    sq_ficha_sincronismo serial
        constraint pk_ficha_sincronismo
            primary key,
    sq_ficha             bigint                 not null
        constraint fk_ficha_sincronismo_ficha
            references salve.ficha
            on update cascade on delete cascade,
    sq_tipo_sincronismo  bigint                 not null
        constraint fk_ficha_sincronismo_tipo
            references salve.dados_apoio
            on update restrict on delete restrict,
    dt_sincronismo       timestamp default CURRENT_TIMESTAMP,
    nu_minutos_intervalo integer   default 1440 not null
);

comment on table salve.ficha_sincronismo is 'Tabela para controle dos intervalos dos sincronismos da ficha';

comment on column salve.ficha_sincronismo.sq_ficha_sincronismo is 'Chave primaria sequencial';

comment on column salve.ficha_sincronismo.sq_ficha is 'Chave estrangeira da tabela Ficha';

comment on column salve.ficha_sincronismo.sq_tipo_sincronismo is 'Tipo do sincronismo realizado. Ex: ocorrencias do portalbio';

comment on column salve.ficha_sincronismo.dt_sincronismo is 'Data que foi realizado pela última vez';

comment on column salve.ficha_sincronismo.nu_minutos_intervalo is 'Número mínimo de minutos permitido para o próximo sincronismo. Padrao 1 dia';

alter table salve.ficha_sincronismo
    owner to postgres;

create index if not exists ix_fk_ficha_sincronismo_ficha
    on salve.ficha_sincronismo (sq_ficha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_sincronismo to usr_salve;

create table if not exists salve.ficha_sinonimia
(
    sq_ficha_sinonimia serial
        constraint pk_ficha_sinonimia
            primary key,
    sq_ficha           bigint not null
        constraint fk_ficha_reference_ficha
            references salve.ficha
            on update restrict on delete cascade,
    no_sinonimia       text   not null,
    no_autor           text   not null,
    nu_ano             smallint
);

alter table salve.ficha_sinonimia
    owner to postgres;

create index if not exists ix_ficha_sinonimia_ficha
    on salve.ficha_sinonimia (sq_ficha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_sinonimia to usr_salve;

create table if not exists salve.ficha_uf
(
    sq_ficha_uf serial
        constraint pk_ficha_uf
            primary key,
    sq_ficha    bigint not null
        constraint fk_ficha_uf_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_estado   bigint not null
        constraint fk_ficha_uf_uf
            references salve.estado
            on update restrict on delete restrict
);

comment on table salve.ficha_uf is 'Tabela das Unidades da Federacao que a especie ocorre';

comment on column salve.ficha_uf.sq_ficha_uf is 'Chave primaria sequencial';

comment on column salve.ficha_uf.sq_ficha is 'Chave estrangeira da tabela Ficha';

comment on column salve.ficha_uf.sq_estado is 'Chave estrangeira da tabela Estado/UF';

alter table salve.ficha_uf
    owner to postgres;

create index if not exists ix_ficha_uf_uf
    on salve.ficha_uf (sq_estado)
    tablespace pg_default;

create unique index if not exists ixu_ficha_uf_ficha_uf
    on salve.ficha_uf (sq_ficha, sq_estado)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_uf to usr_salve;

create table if not exists salve.ficha_uso_regiao
(
    sq_ficha_uso_regiao serial
        constraint pk_ficha_uso_regiao
            primary key,
    sq_ficha_uso        bigint not null
        constraint fk_ficha_uso_uso
            references salve.ficha_uso
            on update restrict on delete cascade,
    sq_ficha_ocorrencia bigint
        constraint fk_ficha_uso_ocorrencia
            references salve.ficha_ocorrencia
            on update restrict on delete cascade
        constraint fk_ficha_uso_regiao_ocorrencia
            references salve.ficha_ocorrencia
            on update restrict on delete restrict
);

comment on table salve.ficha_uso_regiao is 'Registrar as regiões do Uso';

alter table salve.ficha_uso_regiao
    owner to postgres;

create index if not exists ix_ficha_uso_reg_uso
    on salve.ficha_uso_regiao (sq_ficha_uso)
    tablespace pg_default;

comment on index salve.ix_ficha_uso_reg_uso is 'Indice pelo id da ficha_uso';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_uso_regiao to usr_salve;

create table if not exists salve.ficha_variacao_sazonal
(
    sq_ficha_variacao_sazonal serial
        constraint pk_ficha_variacao_sazonal
            primary key,
    sq_ficha                  bigint not null
        constraint fk_tb_ficha_var_sazonal_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_fase_vida              bigint
        constraint fk_tb_ficha_var_sazonal_fase_vida
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_epoca                  bigint
        constraint fk_tb_ficha_var_sazonal_epoca
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_habitat                bigint
        constraint fk_tb_ficha_var_sazonal_habitat
            references salve.dados_apoio
            on update restrict on delete restrict,
    ds_ficha_variacao_sazonal text
);

alter table salve.ficha_variacao_sazonal
    owner to postgres;

create index if not exists ix_ficha_var_sazo_ficha
    on salve.ficha_variacao_sazonal (sq_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_var_sazo_ficha is 'Indice pelo id da ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_variacao_sazonal to usr_salve;

create table if not exists salve.iucn_transmissao_ficha
(
    sq_iucn_transmissao_ficha serial
        constraint pk_iucn_transmissao_ficha
            primary key,
    sq_iucn_transmissao       bigint not null
        constraint fk_iucn_transmissao_sq_iucn_transmissao
            references salve.iucn_transmissao
            on update cascade on delete cascade,
    sq_ficha                  bigint not null
        constraint fk_iucn_transmissao_sq_ficha
            references salve.ficha
            on update cascade on delete cascade
);

comment on column salve.iucn_transmissao_ficha.sq_iucn_transmissao_ficha is 'Chave primaria sequencial da tabela iucn_transmissao_ficha';

comment on column salve.iucn_transmissao_ficha.sq_ficha is 'Chave estrangeira da tabela iucn_transmissao';

alter table salve.iucn_transmissao_ficha
    owner to postgres;

create index if not exists ix_iucn_transmissao_sq_ficha
    on salve.iucn_transmissao_ficha (sq_ficha);

create index if not exists ix_iucn_transmissao_sq_iucn_transmissao
    on salve.iucn_transmissao_ficha (sq_iucn_transmissao);

grant delete, insert, references, select, trigger, truncate, update on salve.iucn_transmissao_ficha to usr_salve;

create table if not exists salve.oficina_ficha
(
    sq_oficina_ficha         serial
        constraint pk_oficina_ficha
            primary key,
    sq_oficina               bigint                not null
        constraint fk_oficina_ficha_oficina
            references salve.oficina
            on delete cascade,
    sq_ficha                 bigint                not null
        constraint fk_oficina_ficha_ficha
            references salve.ficha
            on delete cascade,
    sq_categoria_iucn        bigint
        constraint fk_oficina_ficha_criterio
            references salve.dados_apoio
            on update restrict on delete restrict,
    ds_criterio_aval_iucn    text,
    ds_justificativa         text,
    ds_agrupamento           text,
    dt_avaliacao             timestamp,
    dt_ultima_notificacao    timestamp,
    st_transferida           boolean default false not null,
    st_pf_convidado          boolean default false,
    st_ct_convidado          boolean default false,
    st_possivelmente_extinta char
        constraint chk_oficina_ficha_pex
            check (st_possivelmente_extinta = ANY (ARRAY ['S'::bpchar, 'N'::bpchar, 'D'::bpchar])),
    st_chat_alterado         boolean,
    sq_situacao_ficha        bigint
        constraint fk_oficina_ficha_situacao
            references salve.dados_apoio
            on update restrict on delete restrict
);

comment on column salve.oficina_ficha.sq_categoria_iucn is 'Manter histórico do critério iucn da ficha ao final da oficina';

comment on column salve.oficina_ficha.ds_criterio_aval_iucn is 'Manter histórico do criterio da ficha ao finalizar a oficina';

comment on column salve.oficina_ficha.ds_justificativa is 'Manter histórico da justificativa da ficha ao finalizar a oficina';

comment on column salve.oficina_ficha.dt_avaliacao is 'Data da avaliação';

comment on column salve.oficina_ficha.dt_ultima_notificacao is 'Quando a ficha esitver na situação AVALIADA/REVISADA, notificar semanalmente os administradores';

comment on column salve.oficina_ficha.st_transferida is 'Indicar quando a ficha for transferida para uma outra oficina.';

comment on column salve.oficina_ficha.st_pf_convidado is 'Campo para indicar se o PONTO FOCAL está ou não convidado para participar do chat com os validadores.';

comment on column salve.oficina_ficha.st_ct_convidado is 'Campo para indicar se o COORDENADOR DE TAXON está ou não convidado para participar do chat com os validadores.';

comment on column salve.oficina_ficha.st_possivelmente_extinta is 'Preenchido com S/N/D durante a oficina - para Sim, Não ou Desconhecido';

comment on column salve.oficina_ficha.st_chat_alterado is 'Campo para indentificar que existe nova mensagem no módulo bate papo.';

alter table salve.oficina_ficha
    owner to postgres;

create index if not exists ix_oficina_ficha_ficha
    on salve.oficina_ficha (sq_ficha)
    tablespace pg_default;

create index if not exists ix_oficina_ficha_oficina
    on salve.oficina_ficha (sq_oficina)
    tablespace pg_default;

comment on index salve.ix_oficina_ficha_oficina is 'Indice coluna id da ficha';

create unique index if not exists ixu_oficina_ficha_unique
    on salve.oficina_ficha (sq_oficina, sq_ficha)
    tablespace pg_default;

comment on index salve.ixu_oficina_ficha_unique is 'Não permitir adicionar a ficha duas vezes na mesma oficina';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_ficha to usr_salve;

create table if not exists salve.oficina_ficha_chat
(
    sq_oficina_ficha_chat serial
        constraint pk_oficina_ficha_chat
            primary key,
    sq_oficina_ficha      bigint    not null
        constraint fk_oficina_ficha_chat_oficina_ficha
            references salve.oficina_ficha
            on delete cascade,
    sq_pessoa             bigint    not null
        constraint fk_oficina_ficha_chat_pessoa
            references salve.pessoa_fisica,
    dt_mensagem           timestamp not null,
    de_mensagem           text      not null,
    sg_perfil             text      not null
);

comment on table salve.oficina_ficha_chat is 'Tabela para registro do bate-papo entre os validadores, ponto focal e coordenador de taxon durante a validação';

comment on column salve.oficina_ficha_chat.sg_perfil is 'Gravar a sigla do perfil que o usuario estava logado ao registrar a conversa.';

alter table salve.oficina_ficha_chat
    owner to postgres;

create index if not exists ix_oficina_ficha_chat_oficina_ficha
    on salve.oficina_ficha_chat (sq_oficina_ficha);

create index if not exists ix_oficina_ficha_chat_pessoa
    on salve.oficina_ficha_chat (sq_pessoa);

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_ficha_chat to usr_salve;

create table if not exists salve.oficina_ficha_participante
(
    sq_oficina_ficha_participante serial
        constraint pk_oficina_ficha_participante
            primary key,
    sq_oficina_ficha              bigint not null
        constraint fk_oficina_ficha_part_ofic_ficha
            references salve.oficina_ficha
            on update restrict on delete cascade,
    sq_oficina_participante       bigint not null
        constraint fk_oficina_ficha_part_part
            references salve.oficina_participante
            on update restrict on delete cascade,
    sq_papel_participante         bigint not null
        constraint fk_oficina_ficha_part_papel
            references salve.dados_apoio
            on update restrict on delete restrict
);

alter table salve.oficina_ficha_participante
    owner to postgres;

create index if not exists ix_ofic_ficha_part_oficina_ficha
    on salve.oficina_ficha_participante (sq_oficina_ficha)
    tablespace pg_default;

comment on index salve.ix_ofic_ficha_part_oficina_ficha is 'Indice coluna id da oficina_ficha';

create index if not exists ix_ofic_ficha_part_oficina_part
    on salve.oficina_ficha_participante (sq_oficina_participante)
    tablespace pg_default;

comment on index salve.ix_ofic_ficha_part_oficina_part is 'Indice coluna id da oficina_participante';

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_ficha_participante to usr_salve;

create table if not exists salve.planilha_ficha
(
    sq_planilha_linha          integer not null
        constraint pk_planilha_ficha
            primary key
        constraint fk_planilha_ficha_linha
            references salve.planilha_linha
            on update cascade on delete cascade,
    tx_autor_ano               text,
    tx_grupo                   text,
    tx_centro                  text,
    tx_cpf_pf                  text,
    tx_cpf_ct                  text,
    tx_nome_comum              text,
    tx_sinonimia               text,
    tx_notas_taxonomicas       text,
    tx_notas_morfologicas      text,
    tx_endemica_brasil         text,
    tx_distribuicao_global     text,
    tx_distribuicao_nacional   text,
    tx_altitude_maxima         text,
    tx_altitude_minima         text,
    tx_batimetria_maxima       text,
    tx_batimetria_minima       text,
    tx_historia_natural        text,
    tx_especie_migratoria      text,
    tx_padrao_deslocamento     text,
    tx_tendencia_populacional  text,
    tx_populacao               text,
    tx_ameaca                  text,
    tx_uso                     text,
    tx_presenca_lista_oficial  text,
    tx_acao_conservacao        text,
    tx_presenca_uc             text,
    tx_pesquisa                text,
    tx_tipo_avaliacao          text,
    tx_ano_avaliacao           text,
    tx_estado_avaliacao        text,
    tx_categoria_avaliacao     text,
    tx_criterio_avaliacao      text,
    tx_justificativa_avaliacao text,
    sq_ficha                   bigint
        constraint fk_planilha_ficha_ficha
            references salve.ficha
            on update cascade on delete cascade,
    tx_possivelmente_extinta   text,
    tx_pendencias              text,
    tx_subgrupo                text
);

comment on column salve.planilha_ficha.tx_cpf_pf is 'CPF do ponto focal ( PF )';

comment on column salve.planilha_ficha.tx_cpf_ct is 'CPF do coordenador de taxon ( CT )';

alter table salve.planilha_ficha
    owner to postgres;

create index if not exists ix_planilha_ficha_planilha_linha
    on salve.planilha_ficha (sq_planilha_linha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.planilha_ficha to usr_salve;

create table if not exists salve.planilha_ocorrencia
(
    sq_planilha_linha               integer not null
        constraint pk_planilha_ocorrencia
            primary key
        constraint fk_planilha_ocorrencia_linha
            references salve.planilha_linha
            on update cascade on delete cascade,
    tx_latitude                     text,
    tx_longitude                    text,
    tx_prazo_carencia               text,
    tx_datum                        text,
    tx_formato_original             text,
    tx_precisao_coordenada          text,
    tx_referencia_aproximacao       text,
    tx_metodo_aproximacao           text,
    tx_descricao_metodo_aproximacao text,
    tx_taxon_original_citado        text,
    tx_presenca_atual               text,
    tx_tipo_registro                text,
    tx_dia                          text,
    tx_mes                          text,
    tx_ano                          text,
    tx_hora                         text,
    tx_data_ano_desconhecidos       text,
    tx_localidade                   text,
    tx_caracteristica_localidade    text,
    tx_uc_federal                   text,
    tx_uc_estadual                  text,
    tx_rppn                         text,
    tx_pais                         text,
    tx_estado                       text,
    tx_municipio                    varchar(255),
    tx_ambiente                     text,
    tx_habitat                      varchar(255),
    tx_elevacao_minima              text,
    tx_elevacao_maxima              text,
    tx_profundidade_minima          text,
    tx_profundidade_maxima          text,
    tx_identificador                text,
    tx_data_identificacao           text,
    tx_indicacao_incerta            text,
    tx_tombamento                   text,
    tx_instituicao_tombamento       text,
    tx_compilador                   text,
    tx_data_compilacao              text,
    tx_revisor                      text,
    tx_data_revisao                 text,
    tx_validador                    text,
    tx_data_validacao               text,
    tx_usado_avaliacao              text,
    tx_justificativa_nao_uso        text,
    sq_ficha_ocorrencia             bigint,
    sq_ocorrencia                   bigint
        constraint fk_planilha_ocorrencia_ocorrencia
            references salve.ficha_ocorrencia
            on update cascade on delete cascade,
    id_origem                       text,
    tx_titulo_ref_bib               text,
    tx_autor_ref_bib                text,
    tx_ano_ref_bib                  text,
    tx_comunicacao_pessoal          text,
    tx_observacao                   text,
    id_ref_bib_salve                text,
    de_doi                          text
);

comment on column salve.planilha_ocorrencia.id_origem is 'Identificador único do registro na base de dados de origem';

comment on column salve.planilha_ocorrencia.tx_titulo_ref_bib is 'titulo da referencia bibliografica';

comment on column salve.planilha_ocorrencia.tx_autor_ref_bib is 'autor/autores da referencia bibliografica';

comment on column salve.planilha_ocorrencia.tx_ano_ref_bib is 'ano da referencia bibliografica';

comment on column salve.planilha_ocorrencia.tx_comunicacao_pessoal is 'comunicacao pessoal sobre a referencia bibliografica';

comment on column salve.planilha_ocorrencia.id_ref_bib_salve is 'Identificador da referencia bibliografica.';

comment on column salve.planilha_ocorrencia.de_doi is 'Numero do DOI da referência bibliográfica.';

alter table salve.planilha_ocorrencia
    owner to postgres;

create index if not exists ix_planilha_oco_planilha_linha
    on salve.planilha_ocorrencia (sq_planilha_linha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.planilha_ocorrencia to usr_salve;

create table if not exists salve.taxon_historico_avaliacao
(
    sq_taxon_historico_avaliacao serial
        constraint pk_taxon_historico_avaliacao
            primary key,
    sq_taxon                     bigint                not null
        constraint fk_taxon_hist_aval_taxon
            references taxonomia.taxon
            on update restrict on delete cascade,
    sq_tipo_avaliacao            bigint                not null
        constraint fk_icha_hist_aval_tipo_aval
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_abrangencia               bigint
        constraint fk_taxon_hist_aval_abrang
            references salve.abrangencia
            on update restrict on delete restrict,
    sq_categoria_iucn            bigint
        constraint fk_tb_taxon_hist_aval_cat_iucn
            references salve.dados_apoio
            on update restrict on delete restrict,
    nu_ano_avaliacao             smallint              not null,
    de_criterio_avaliacao_iucn   text,
    st_possivelmente_extinta     char,
    tx_justificativa_avaliacao   text,
    no_regiao_outra              text,
    de_categoria_outra           text,
    st_oficial                   boolean default false not null,
    id_origem                    bigint
);

comment on column salve.taxon_historico_avaliacao.sq_taxon_historico_avaliacao is 'chave primaria auto increment';

comment on column salve.taxon_historico_avaliacao.sq_taxon is 'chave estrangeira da tabela taxonomia.taxon';

comment on column salve.taxon_historico_avaliacao.sq_tipo_avaliacao is 'chave estrangeira da tabela salve.dados_apoio para informar o tipo da avaliacao. Ex.Nacional, estadual, municipal etc';

comment on column salve.taxon_historico_avaliacao.sq_abrangencia is 'chave estrangeira da tabela salve.dados_apoio para informar a abrangencia quando a avaliacao nao for nacional';

comment on column salve.taxon_historico_avaliacao.sq_categoria_iucn is 'chave estrangeira da tabela salve.dados_apoio para informar a categoria iucn que foi avaliada';

comment on column salve.taxon_historico_avaliacao.nu_ano_avaliacao is 'informar o ano da avaliacao';

comment on column salve.taxon_historico_avaliacao.de_criterio_avaliacao_iucn is 'informar o criterio da iucn recebido na avaliacao';

comment on column salve.taxon_historico_avaliacao.st_possivelmente_extinta is 'informar se o taxon pode estar ou não extinto';

comment on column salve.taxon_historico_avaliacao.tx_justificativa_avaliacao is 'texto com a justificativa da avaliacao';

comment on column salve.taxon_historico_avaliacao.no_regiao_outra is 'informar uma outra abrangencia no caso das avaliacoes regionais';

comment on column salve.taxon_historico_avaliacao.de_categoria_outra is 'informar a categoria e criterio especificos quando a avaliacao for estadual';

comment on column salve.taxon_historico_avaliacao.st_oficial is 'Indicar se o resultado da avaliacao esta oficializado pelo MMA';

comment on column salve.taxon_historico_avaliacao.id_origem is 'Id da avaliacao no banco de dados de origem';

alter table salve.taxon_historico_avaliacao
    owner to postgres;

create index if not exists ix_taxon_hist_aval_sq_taxon
    on salve.taxon_historico_avaliacao (sq_taxon, sq_tipo_avaliacao, nu_ano_avaliacao);

grant delete, insert, references, select, trigger, truncate, update on salve.taxon_historico_avaliacao to usr_salve;

create table if not exists salve.traducao_ficha_percentual
(
    sq_ficha                bigint            not null
        constraint pk_traducao_ficha_percentual
            primary key
        constraint fk_traducao_ficha_percentual_ficha
            references salve.ficha
            on update cascade on delete cascade,
    nu_percentual_traduzido integer default 0 not null,
    nu_percentual_revisado  integer default 0 not null
);

comment on table salve.traducao_ficha_percentual is 'Registrar o percentual traduzido e revisado da ficha';

comment on column salve.traducao_ficha_percentual.sq_ficha is 'chave estrangeira e primaria da tabela';

comment on constraint pk_traducao_ficha_percentual on salve.traducao_ficha_percentual is 'Chave primaria da tabela';

comment on constraint fk_traducao_ficha_percentual_ficha on salve.traducao_ficha_percentual is 'Chave estrangeira e primaria';

comment on column salve.traducao_ficha_percentual.nu_percentual_traduzido is 'valor percentual traduzido da ficha';

comment on column salve.traducao_ficha_percentual.nu_percentual_revisado is 'valor percentual revisado das traducoes';

alter table salve.traducao_ficha_percentual
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_ficha_percentual to usr_salve;

create table if not exists salve.traducao_fila
(
    sq_traducao_fila bigserial
        constraint pk_traducao_fila
            primary key,
    sq_ficha         bigint                              not null
        constraint fk_traducao_fila_ficha
            references salve.ficha
            on update cascade on delete cascade,
    dt_inclusao      timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao     timestamp default CURRENT_TIMESTAMP not null,
    nu_percentual    integer   default 0                 not null,
    st_executando    boolean   default false             not null
);

comment on table salve.traducao_fila is 'fila de fichas a serem traduzidas em lote';

comment on column salve.traducao_fila.sq_ficha is 'chave estrangeira da tabela ficha';

comment on column salve.traducao_fila.dt_inclusao is 'data e hora que a ficha entrou na fila';

comment on column salve.traducao_fila.nu_percentual is 'percentual da traducao concluido';

alter table salve.traducao_fila
    owner to postgres;

create unique index if not exists ixu_fk_traducao_fila_ficha
    on salve.traducao_fila (sq_ficha)
    tablespace pg_default;

create unique index if not exists ixu_pk_traducao_fila
    on salve.traducao_fila (sq_traducao_fila)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.traducao_fila to usr_salve;

create table if not exists salve.validador_ficha
(
    sq_validador_ficha           serial
        constraint pk_validador_ficha
            primary key,
    sq_ciclo_avaliacao_validador bigint not null
        constraint fk_val_ficha_ciclo
            references salve.ciclo_avaliacao_validador
            on update restrict on delete cascade,
    sq_oficina_ficha             bigint not null
        constraint fk_val_ficha_oficina
            references salve.oficina_ficha
            on update restrict on delete cascade,
    sq_categoria_sugerida        bigint
        constraint fk_val_ficha_categoria
            references salve.dados_apoio
            on update restrict on delete restrict,
    de_criterio_sugerido         text,
    tx_justificativa             text,
    dt_alteracao                 timestamp,
    sq_resultado                 bigint
        constraint fk_validador_ficha_resultado
            references salve.dados_apoio
            on update restrict on delete restrict,
    in_comunicar_alteracao       boolean default false,
    dt_ultimo_comunicado         timestamp,
    st_possivelmente_extinta     varchar(1)
);

comment on column salve.validador_ficha.dt_alteracao is 'Data que o validador alterou o registro';

comment on column salve.validador_ficha.sq_resultado is 'Resultado a,b ou c selecionado pelo validador';

comment on column salve.validador_ficha.in_comunicar_alteracao is 'informar ao validador que houve uma resposta e ele precisa rever a sua validacao';

comment on column salve.validador_ficha.dt_ultimo_comunicado is 'data de envio do ultimo email comunicando alteracoes na validacao';

comment on column salve.validador_ficha.st_possivelmente_extinta is 'informar se a espécie está possivelmente extinta. S ou N';

alter table salve.validador_ficha
    owner to postgres;

create index if not exists ix_valid_ficha_ciclo
    on salve.validador_ficha (sq_ciclo_avaliacao_validador)
    tablespace pg_default;

comment on index salve.ix_valid_ficha_ciclo is 'Indice coluna id do ciclo_avaliacao_validador';

create index if not exists ix_valid_ficha_ficha
    on salve.validador_ficha (sq_oficina_ficha)
    tablespace pg_default;

comment on index salve.ix_valid_ficha_ficha is 'Indice coluna id da oficina_ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.validador_ficha to usr_salve;

create table if not exists salve.validador_ficha_chat
(
    sq_validador_ficha_chat serial
        constraint pk_validador_ficha_chat
            primary key,
    sq_validador_ficha      bigint
        constraint fk_val_ficha_chat_ficha
            references salve.validador_ficha
            on update restrict on delete cascade,
    sq_pessoa               bigint    not null
        constraint fk_val_ficha_chat_pessoa
            references salve.pessoa_fisica
            on update restrict on delete restrict,
    dt_mensagem             timestamp not null,
    de_mensagem             text      not null,
    sg_perfil               varchar(2)
);

comment on column salve.validador_ficha_chat.sg_perfil is 'Sigla do perfil do usuário que enviou a mensagem. Ex: PF, AD, VL';

alter table salve.validador_ficha_chat
    owner to postgres;

create index if not exists ix_val_fi_chat_val_ficha
    on salve.validador_ficha_chat (sq_validador_ficha)
    tablespace pg_default;

comment on index salve.ix_val_fi_chat_val_ficha is 'Indice coluna id do validador_ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.validador_ficha_chat to usr_salve;

create table if not exists salve.validador_ficha_revisao
(
    sq_validador_ficha_revisao serial
        constraint pk_validador_ficha_revisao
            primary key,
    sq_validador_ficha         bigint                              not null
        constraint fk_validador_ficha_rev_val_ficha
            references salve.validador_ficha
            on update cascade on delete cascade,
    sq_resultado               bigint                              not null
        constraint fk_validador_ficha_revisao_resultado
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_categoria_sugerida      bigint
        constraint fk_validador_ficha_revisao_categoria
            references salve.dados_apoio
            on update restrict on delete restrict,
    st_possivelmente_extinta   varchar(1),
    tx_justificativa           text                                not null,
    de_criterio_sugerido       text,
    dt_inclusao                timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao               timestamp                           not null
);

comment on table salve.validador_ficha_revisao is 'Tabela utilizada quando o validador autoriza a coordenação a alterar a sua resposta ou por algum motivo não pode mais acessar o sistema';

comment on column salve.validador_ficha_revisao.sq_validador_ficha_revisao is 'Chave primária sequencial';

comment on column salve.validador_ficha_revisao.sq_validador_ficha is 'Chave estrangeira da tabela VALIDOR_FICHA';

comment on column salve.validador_ficha_revisao.sq_resultado is 'Chave estrangeira da tabela DADOS_APOIO';

comment on column salve.validador_ficha_revisao.sq_categoria_sugerida is 'Chave estrangeira da tabela DADOS_APOIO';

comment on column salve.validador_ficha_revisao.tx_justificativa is 'Justificativa da coordenação para interfência na resposta do validador';

comment on column salve.validador_ficha_revisao.de_criterio_sugerido is 'Critério avaliado';

comment on column salve.validador_ficha_revisao.dt_inclusao is 'Data de inclusão da revisão';

comment on column salve.validador_ficha_revisao.dt_alteracao is 'Data da última alteracao da revisão';

alter table salve.validador_ficha_revisao
    owner to postgres;

create index if not exists ix_fk_valid_ficha_rev_val_ficha
    on salve.validador_ficha_revisao (sq_validador_ficha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.validador_ficha_revisao to usr_salve;

create index if not exists idx_btree_taxonomia_json_trilha_classe_i
    on taxonomia.taxon (((json_trilha -> 'classe'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_especie_i
    on taxonomia.taxon (((json_trilha -> 'especie'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_familia_i
    on taxonomia.taxon (((json_trilha -> 'familia'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_filo_i
    on taxonomia.taxon (((json_trilha -> 'filo'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_genero_i
    on taxonomia.taxon (((json_trilha -> 'genero'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_ordem_i
    on taxonomia.taxon (((json_trilha -> 'ordem'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_reino_i
    on taxonomia.taxon (((json_trilha -> 'reino'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_subespecie_i
    on taxonomia.taxon (((json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_subfamilia_i
    on taxonomia.taxon (((json_trilha -> 'subfamilia'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_subfilo_i
    on taxonomia.taxon (((json_trilha -> 'subfilo'::text) ->> 'no_taxon'::text));

create index if not exists idx_btree_taxonomia_json_trilha_tribo_i
    on taxonomia.taxon (((json_trilha -> 'tribo'::text) ->> 'no_taxon'::text));

create index if not exists idx_trgm_taxonomia_taxon_json_especie
    on taxonomia.taxon using gin (((json_trilha -> 'especie'::text) ->> 'no_taxon'::text) public.gin_trgm_ops)
    tablespace pg_default;

create index if not exists idx_trgm_taxonomia_taxon_json_subespecie
    on taxonomia.taxon using gin (((json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text) public.gin_trgm_ops)
    tablespace pg_default;

create index if not exists idx_trgm_taxonomia_taxon_no_taxon
    on taxonomia.taxon using gin (no_taxon public.gin_trgm_ops)
    tablespace pg_default;

create index if not exists ix_taxon_nivel_taxon
    on taxonomia.taxon (sq_nivel_taxonomico, no_taxon)
    tablespace pg_default;

create index if not exists ix_taxon_no_autor
    on taxonomia.taxon (no_autor);

create index if not exists ix_taxon_nome_taxon
    on taxonomia.taxon (no_taxon)
    tablespace pg_default;

create index if not exists ix_taxon_pai
    on taxonomia.taxon (sq_taxon_pai)
    tablespace pg_default;

comment on index taxonomia.ix_taxon_pai is 'Indice coluna do auto relacionamento';

create index if not exists idx_trgm_taxonomia_taxon_json_familia
    on taxonomia.taxon using gin (((json_trilha -> 'familia'::text) ->> 'no_taxon'::text) public.gin_trgm_ops)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.taxon to usr_salve;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.situacao to usr_salve;

create table if not exists taxonomia.tipo_uso
(
    sq_tipo_uso serial
        constraint pk_tipo_uso
            primary key,
    de_tipo_uso text,
    co_tipo_uso text
);

comment on table taxonomia.tipo_uso is 'Exemplo de tipos de uso:
1) Científico
2) Não Científico
etc...';

alter table taxonomia.tipo_uso
    owner to postgres;

create table if not exists taxonomia.tipo_publicacao
(
    sq_tipo_publicacao serial
        constraint pk_tipo_publicacao
            primary key,
    sq_tipo_uso        bigint not null
        constraint fk_tipo_publicacao_tipo_uso
            references taxonomia.tipo_uso
            on update restrict on delete restrict,
    de_tipo_publicacao text   not null,
    co_tipo_publicacao text
);

comment on table taxonomia.tipo_publicacao is 'Tipos de Publicação:
1) Artigo
2) Revista
3) Jornal
etc...';

comment on column taxonomia.tipo_publicacao.co_tipo_publicacao is 'código único do tipo de publicação para uso interno nas regras de negócio';

alter table taxonomia.tipo_publicacao
    owner to postgres;

create table if not exists taxonomia.publicacao
(
    sq_publicacao            serial
        constraint pk_publicacao
            primary key,
    sq_tipo_publicacao       bigint
        constraint fk_publicacao_tipo_publicacao
            references taxonomia.tipo_publicacao
            on update restrict on delete restrict,
    de_titulo                text not null,
    no_autor                 text,
    nu_ano_publicacao        smallint,
    no_revista_cientifica    text,
    de_volume                text,
    de_issue                 text,
    de_paginas               text,
    de_ref_bibliografica     text,
    in_lista_oficial_vigente boolean,
    de_titulo_livro          text,
    de_editores              text,
    no_editora               text,
    no_cidade                text,
    nu_edicao_livro          text,
    de_url                   text,
    dt_acesso_url            date,
    no_universidade          text,
    de_doi                   text,
    de_issn                  text,
    de_isbn                  text,
    dt_publicacao            date
);

comment on column taxonomia.publicacao.in_lista_oficial_vigente is 'Este campo informará se é ou não uma lista oficial nacional vigente para alimentar as opções do campo SQ_REF_BIB_LISTA_VIGENTE da tabela especies.ficha';

comment on column taxonomia.publicacao.dt_publicacao is 'Data de publicação do documento';

alter table taxonomia.publicacao
    owner to postgres;

create table if not exists salve.ficha_colaboracao_ocorrencia
(
    sq_ficha_colaboracao_ocorrencia integer   default nextval('salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq'::regclass) not null
        constraint pk_colab_ocorrencia
            primary key,
    sq_ciclo_consulta_ficha         serial
        constraint fk_ficha_co_reference_ciclo_co
            references salve.ciclo_consulta_ficha
            on update restrict on delete restrict,
    sq_situacao                     bigint                                                                                                       not null
        constraint fk_ficha_co_reference_dados_ap
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_web_usuario                  bigint                                                                                                       not null
        constraint fk_colab_ocorr_ref_usuario
            references salve.pessoa
            on update restrict on delete restrict,
    sq_datum                        bigint                                                                                                       not null
        constraint fk_colab_ocorr_datum
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_precisao_coordenada          bigint                                                                                                       not null
        constraint fk_colab_ocorr_precisao
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_ref_aproximacao              bigint
        constraint fk_colab_ocorr_ref_aprox
            references salve.dados_apoio
            on update restrict on delete restrict,
    sq_publicacao                   bigint
        constraint fk_colab_ocorr_ref_bib
            references taxonomia.publicacao
            on update restrict on delete restrict,
    no_localidade                   text,
    tx_ref_bib                      text,
    tx_observacao                   text,
    ge_ocorrencia                   public.geometry                                                                                                     not null,
    dt_inclusao                     timestamp default now()                                                                                      not null,
    dt_alteracao                    timestamp default now()                                                                                      not null,
    nu_dia_registro                 integer,
    nu_mes_registro                 integer,
    nu_ano_registro                 integer,
    hr_registro                     text,
    in_data_desconhecida            char,
    tx_nao_aceita                   text
);

comment on column salve.ficha_colaboracao_ocorrencia.tx_nao_aceita is 'Campo utilizado para justificar a nao aceitacao da colaboracao.';

alter table salve.ficha_colaboracao_ocorrencia
    owner to postgres;

alter sequence salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq owned by salve.ficha_colaboracao_ocorrencia.sq_ficha_colaboracao_ocorrencia;

create index if not exists ix_ficha_col_oco_ciclo
    on salve.ficha_colaboracao_ocorrencia (sq_ciclo_consulta_ficha)
    tablespace pg_default;

comment on index salve.ix_ficha_col_oco_ciclo is 'Indice coluna id ciclo_consulta_ficha';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_colaboracao_ocorrencia to usr_salve;

create table if not exists salve.ficha_ref_bib
(
    sq_ficha_ref_bib serial
        constraint pk_ficha_ref_bib
            primary key,
    sq_ficha         bigint not null
        constraint fk_ficha_ref_bib_ficha
            references salve.ficha
            on update restrict on delete cascade,
    sq_publicacao    bigint
        constraint fk_ficha_ref_bib_publicacao
            references taxonomia.publicacao
            on update restrict on delete restrict,
    no_tabela        varchar(50),
    sq_registro      bigint,
    no_coluna        varchar(50),
    de_rotulo        varchar(200),
    no_autor         varchar(100),
    nu_ano           integer,
    constraint ix_unique_ficha_ref_bib
        unique (sq_ficha, sq_publicacao, no_tabela, sq_registro, no_coluna, no_autor, nu_ano)
);

comment on table salve.ficha_ref_bib is 'Relação com a tabela taxonomia.publicacao';

comment on column salve.ficha_ref_bib.no_tabela is 'Registro o nome da tabela da tabela relacionada com a referência bibliográfica';

comment on column salve.ficha_ref_bib.sq_registro is 'id do registro da tabela que foi informada na coluna no_tabela';

comment on column salve.ficha_ref_bib.no_coluna is 'nome da coluna da tabela informada na coluna no_tabela';

comment on column salve.ficha_ref_bib.de_rotulo is 'descrição do rótulo do campo referenciado';

comment on column salve.ficha_ref_bib.no_autor is 'Nome do autor quando for uma comunicação pessoal';

comment on column salve.ficha_ref_bib.nu_ano is 'Ano da publicação quando for uma comunicação pessoal';

alter table salve.ficha_ref_bib
    owner to postgres;

create index if not exists ix_ficha_ref_bib_ficha
    on salve.ficha_ref_bib (sq_ficha, sq_registro, no_tabela, no_coluna)
    tablespace pg_default;

comment on index salve.ix_ficha_ref_bib_ficha is 'Indice pelo id da ficha, registro, tabela e coluna';

create index if not exists ix_ficha_ref_bib_tab_col_reg
    on salve.ficha_ref_bib (no_tabela, no_coluna, sq_registro)
    tablespace pg_default;

comment on index salve.ix_ficha_ref_bib_tab_col_reg is 'Indice para localizacao da referencia bibliografica';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ref_bib to usr_salve;

create table if not exists salve.ficha_ref_bib_tag
(
    sq_ficha_ref_bib_tag serial
        constraint pk_ficha_ref_bib_tag
            primary key,
    sq_ficha_ref_bib     bigint not null
        constraint fk_ficha_ref_bib_tag_ref
            references salve.ficha_ref_bib
            on update restrict on delete cascade,
    sq_tag               bigint not null
        constraint fk_ficha_ref_bib_tag
            references salve.dados_apoio
            on update restrict on delete restrict
);

alter table salve.ficha_ref_bib_tag
    owner to postgres;

create index if not exists ix_ficha_ref_bib_tag_ref_bib
    on salve.ficha_ref_bib_tag (sq_ficha_ref_bib)
    tablespace pg_default;

comment on index salve.ix_ficha_ref_bib_tag_ref_bib is 'Indice pelo id da ficha_ref_bib';

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ref_bib_tag to usr_salve;

create index if not exists ix_publicacao_tipo_titulo
    on taxonomia.publicacao (sq_tipo_publicacao, de_titulo)
    tablespace pg_default;

create index if not exists ix_trgm_publicacao_de_titulo
    on taxonomia.publicacao using gist (fn_remove_acentuacao(de_titulo::character varying) public.gist_trgm_ops)
    tablespace pg_default;

create index if not exists ix_trgm_publicacao_no_autor
    on taxonomia.publicacao using gist (fn_remove_acentuacao(no_autor::character varying) public.gist_trgm_ops)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.publicacao to usr_salve;

create table if not exists taxonomia.publicacao_arquivo
(
    sq_publicacao_arquivo serial
        constraint pk_publicacao_arquivo
            primary key,
    sq_publicacao         integer not null
        constraint fk_publicacao_arquivo_publicacao
            references taxonomia.publicacao
            on update restrict on delete cascade,
    no_publicacao_arquivo text,
    de_tipo_conteudo      text    not null,
    de_local_arquivo      text    not null
);

comment on column taxonomia.publicacao_arquivo.no_publicacao_arquivo is 'Nome original do arquivo adicionado pelo usuário';

comment on column taxonomia.publicacao_arquivo.de_tipo_conteudo is 'Tipo do conteúdo ( mime type) para informar ao navegador para exibir o preview';

comment on column taxonomia.publicacao_arquivo.de_local_arquivo is 'Local  de armazenamento no disco';

alter table taxonomia.publicacao_arquivo
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.publicacao_arquivo to usr_salve;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.tipo_publicacao to usr_salve;

grant delete, insert, references, select, trigger, truncate, update on taxonomia.tipo_uso to usr_salve;

create table if not exists salve.ficha_versao
(
    sq_ficha_versao     bigserial
        constraint pk_ficha_versao
            primary key,
    sq_ficha            bigint                              not null
        constraint fk_ficha_versao_ficha
            references salve.ficha,
    sq_contexto         bigint
        constraint fk_ficha_versao_contexto
            references salve.dados_apoio
            on update restrict on delete restrict,
    nu_versao           integer   default 1                 not null,
    js_ficha            jsonb                               not null,
    dt_inclusao         timestamp default CURRENT_TIMESTAMP not null,
    sq_usuario_inclusao bigint                              not null,
    st_publico          boolean   default false,
    st_publicada        boolean   default false,
    nu_ano_publicada    integer
);

comment on column salve.ficha_versao.sq_ficha_versao is 'chave primaria sequencial da tabela ficha_versao';

comment on column salve.ficha_versao.sq_ficha is 'chave estrangeira da tabela ficha';

comment on column salve.ficha_versao.sq_contexto is 'Registrar o contexto/motivo em que a ficha foi versionada';

comment on column salve.ficha_versao.nu_versao is 'Numero sequencial para controlar as versoes da versao da ficha';

comment on column salve.ficha_versao.js_ficha is 'Armazenar copia completa da ficha no formato jsonb';

comment on column salve.ficha_versao.dt_inclusao is 'Data de inclusao do registro';

comment on column salve.ficha_versao.sq_usuario_inclusao is 'Id do usuario que incluiu o regisrtro';

comment on column salve.ficha_versao.st_publico is 'Informar se a versao pode ser exibida no modulo publico';

comment on column salve.ficha_versao.st_publicada is 'Informar se a ficha (pdf) pode ou não ser exibida no SALVE publico';

comment on column salve.ficha_versao.nu_ano_publicada is 'Ano que a lista oficial foi publicada';

alter table salve.ficha_versao
    owner to postgres;

grant select, update, usage on sequence salve.ficha_versao_sq_ficha_versao_seq to usr_salve;

create unique index if not exists ix_ficha_versao_sq_ficha_fk
    on salve.ficha_versao (sq_ficha asc, nu_versao desc);

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_versao to usr_salve;

create table if not exists salve.ocorrencias_json
(
    sq_ocorrencias_json bigserial
        constraint pk_ocorrencias_json
            primary key,
    sq_ficha_versao     bigint not null
        constraint fk_ocorrencias_json_ficha_versao
            references salve.ficha_versao
            on update cascade on delete cascade,
    js_ocorrencias      jsonb  not null
);

comment on column salve.ocorrencias_json.sq_ocorrencias_json is 'chave primaria sequencial da tabela ocorrencias_json';

comment on column salve.ocorrencias_json.sq_ficha_versao is 'chave estrangeira da tabela ficha_versao';

comment on column salve.ocorrencias_json.js_ocorrencias is 'Armazenar copia completa dos registros de ocorrencias da ficha no formato jsonb';

alter table salve.ocorrencias_json
    owner to postgres;

grant select, update, usage on sequence salve.ocorrencias_json_sq_ocorrencias_json_seq to usr_salve;

create unique index if not exists ix_ocorrencias_json_sq_ficha_versao_fk
    on salve.ocorrencias_json (sq_ficha_versao);

grant delete, insert, references, select, trigger, truncate, update on salve.ocorrencias_json to usr_salve;

create table if not exists salve.ciclo_consulta_ficha_versao
(
    sq_ciclo_consulta_ficha integer not null
        constraint pk_ciclo_consulta_ficha_versao
            primary key
        constraint fk_ciclo_con_ficha_ver_ciclo_con_ficha
            references salve.ciclo_consulta_ficha
            on update cascade on delete cascade,
    sq_ficha_versao         integer not null
        constraint fk_ciclo_con_ficha_ver_ficha_ver
            references salve.ficha_versao
            on update cascade on delete cascade
);

comment on table salve.ciclo_consulta_ficha_versao is 'Armazenar qual foi a versão da ficha utilizada na consulta';

comment on column salve.ciclo_consulta_ficha_versao.sq_ciclo_consulta_ficha is 'Chave estrangeira e primário recebida da tabela ciclo_consulta_ficha';

comment on column salve.ciclo_consulta_ficha_versao.sq_ficha_versao is 'Chave estrangeira da tabela ficha_versao';

alter table salve.ciclo_consulta_ficha_versao
    owner to postgres;

create index if not exists ix_ciclo_con_ficha_ver_ficha_ve
    on salve.ciclo_consulta_ficha_versao (sq_ficha_versao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ciclo_consulta_ficha_versao to usr_salve;

create table if not exists salve.oficina_ficha_versao
(
    sq_oficina_ficha integer not null
        constraint pk_oficina_ficha_versao
            primary key
        constraint fk_oficina_ficha_ver_oficina_ficha
            references salve.oficina_ficha
            on update cascade on delete cascade,
    sq_ficha_versao  integer not null
        constraint fk_oficina_ficha_ver_ficha_versao
            references salve.ficha_versao
            on update cascade on delete cascade
);

comment on column salve.oficina_ficha_versao.sq_oficina_ficha is 'Chave estrangeira e primária recebida da tabela oficina_ficha';

comment on column salve.oficina_ficha_versao.sq_ficha_versao is 'Chave estrangeira da tabela ficha_versao';

alter table salve.oficina_ficha_versao
    owner to postgres;

create index if not exists ix_ofi_ficha_ver_ficha_versao_fk
    on salve.oficina_ficha_versao (sq_ficha_versao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.oficina_ficha_versao to usr_salve;

create table if not exists salve.taxon_historico_avaliacao_versao
(
    sq_taxon_historico_avaliacao integer not null
        constraint pk_taxon_historico_avaliacao_versao
            primary key
        constraint fk_tax_hist_aval_ver_tax_hist_aval
            references salve.taxon_historico_avaliacao
            on update cascade on delete cascade,
    sq_ficha_versao              integer not null
        constraint fk_tax_hist_aval_ver_ficha_versao
            references salve.ficha_versao
            on update cascade on delete cascade
);

comment on column salve.taxon_historico_avaliacao_versao.sq_taxon_historico_avaliacao is 'Chave estrangeira e primária recebida da tabela taxon_historico_avaliacao';

comment on column salve.taxon_historico_avaliacao_versao.sq_ficha_versao is 'Chave estrangeira da tabela ficha_versao';

alter table salve.taxon_historico_avaliacao_versao
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.taxon_historico_avaliacao_versao to usr_salve;

create table if not exists salve.ficha_ocorrencia_validacao_versao
(
    sq_ficha_ocorrencia_validacao bigint not null
        constraint pk_ficha_oco_valid_versao
            primary key
        constraint fk_ficha_oco_valid_ver_ficha_oco_valid
            references salve.ficha_ocorrencia_validacao
            on update cascade on delete cascade,
    sq_ficha_versao               bigint
        constraint fk_ficha_oco_valid_ver_ficha_versao
            references salve.ficha_versao
            on update cascade on delete cascade
);

comment on column salve.ficha_ocorrencia_validacao_versao.sq_ficha_ocorrencia_validacao is 'Chave estrangeira e primaria da tabela ficha_ocorrencia_validacao';

comment on column salve.ficha_ocorrencia_validacao_versao.sq_ficha_versao is 'Chave estrangeira da tabela ficha_versao';

alter table salve.ficha_ocorrencia_validacao_versao
    owner to postgres;

create index if not exists ix_ficha_oco_valid_ver_ficha_versao_fk
    on salve.ficha_ocorrencia_validacao_versao (sq_ficha_versao)
    tablespace pg_default;

create index if not exists ix_tax_hist_aval_ver_ficha_versao_fk
    on salve.ficha_ocorrencia_validacao_versao (sq_ficha_versao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_ocorrencia_validacao_versao to usr_salve;

create table if not exists salve.ficha_hist_sit_exc_versao
(
    sq_ficha_hist_situacao_excluida integer not null
        constraint pk_ficha_hist_sit_exc_versao
            primary key
        constraint fk_pk_ficha_his_sit_excl_versao
            references salve.ficha_hist_situacao_excluida
            on update cascade on delete cascade,
    sq_ficha_versao                 bigint  not null
        constraint fk_fic_his_sit_exc_ficha_versao
            references salve.ficha_versao
            on update cascade on delete cascade
);

comment on column salve.ficha_hist_sit_exc_versao.sq_ficha_hist_situacao_excluida is 'Chave estrangeira e primaria';

comment on column salve.ficha_hist_sit_exc_versao.sq_ficha_versao is 'Chave estrangeira da tabela ficha_versao';

alter table salve.ficha_hist_sit_exc_versao
    owner to postgres;

create index if not exists ix_ficha_his_sit_exc_fic_ver_fk
    on salve.ficha_hist_sit_exc_versao (sq_ficha_versao)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_hist_sit_exc_versao to usr_salve;

create table if not exists salve.ficha_versionamento
(
    sq_ficha    bigint                              not null
        constraint ficha_versionamento_pk
            primary key
        constraint fk_ficha_versionamento_ficha
            references salve.ficha
            on update cascade on delete cascade,
    in_status   char,
    ds_log      text,
    dt_inclusao timestamp default CURRENT_TIMESTAMP not null
);

alter table salve.ficha_versionamento
    owner to postgres;

create unique index if not exists ix_ficha_versionamento_pk
    on salve.ficha_versionamento (sq_ficha)
    tablespace pg_default;

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_versionamento to usr_salve;

create table if not exists salve.configuracao
(
    sq_estado         bigint                              not null
        constraint pk_configuracao
            primary key,
    no_instituicao    text                                not null,
    sg_instituicao    text                                not null,
    no_arquivo_brasao text,
    ds_link           text,
    dt_inclusao       timestamp default CURRENT_TIMESTAMP not null,
    dt_alteracao      timestamp default CURRENT_TIMESTAMP not null
);

comment on table salve.configuracao is 'Tabela de configuracoes gerais do sistema';

comment on column salve.configuracao.sq_estado is 'Chave estrangeira da tabela Estado';

comment on constraint pk_configuracao on salve.configuracao is 'Chave primária da tabela configuracao';

comment on column salve.configuracao.no_instituicao is 'Nome da instituição exibido no cabecalho das paginas';

comment on column salve.configuracao.sg_instituicao is 'Sigla da instituicao';

comment on column salve.configuracao.no_arquivo_brasao is 'Nome do arquivo imagem do brasao exibido no cabecalho dos PDF/DOC';

comment on column salve.configuracao.ds_link is 'Endereco do site da instituicao ao clicar sobre a logomarca do titulo da pagina';

comment on column salve.configuracao.dt_inclusao is 'Data de inclusao do registro no banco de dados';

comment on column salve.configuracao.dt_alteracao is 'Data da ultima alteracao realizada no registro';

alter table salve.configuracao
    owner to postgres;

create table if not exists salve.caverna
(
    sq_caverna           bigint not null
        constraint pk_caverna
            primary key,
    no_caverna           text   not null,
    no_estado            text,
    no_municipio         text,
    no_localidade        text,
    co_registro_nacional integer
);

alter table salve.caverna
    owner to postgres;

create table if not exists salve.ficha_amb_restrito_caverna
(
    sq_ficha_amb_restrito_caverna serial,
    sq_ficha_ambiente_restrito    bigint not null
        constraint fk_ficha_amb_rest_cav_ficha_amb_rest
            references salve.ficha_ambiente_restrito
            on update restrict on delete cascade,
    sq_caverna                    bigint not null
        constraint fk_ficha_amb_rest_cav_caverna
            references salve.caverna
            on update restrict on delete cascade
);

comment on table salve.ficha_amb_restrito_caverna is 'Armazenar a(s) caverna(s) quando o ambiente restrito da espeice for em caverna';

comment on column salve.ficha_amb_restrito_caverna.sq_ficha_amb_restrito_caverna is 'chave primária sequencial da tabela';

comment on column salve.ficha_amb_restrito_caverna.sq_ficha_ambiente_restrito is 'chave estrangeira da tabela ficha_ambiente_restrito';

comment on column salve.ficha_amb_restrito_caverna.sq_caverna is 'chave estrangeira da tabela salve.caverna';

alter table salve.ficha_amb_restrito_caverna
    owner to postgres;

create unique index if not exists ixu_ficha_amb_rest_cav_ficha_amb
    on salve.ficha_amb_restrito_caverna (sq_ficha_ambiente_restrito, sq_caverna);

grant delete, insert, references, select, trigger, truncate, update on salve.ficha_amb_restrito_caverna to usr_salve;

-- xxxxxx


-- functions

create or replace function salve.entity2char(t text) returns text
    immutable
    language plpgsql
as
$$
declare
    r record;
begin
    for r in
        select distinct ce.ch, ce.name
        from
            salve.character_entity ce
                inner join (
                select name[1] "name"
                from regexp_matches(t, '&([A-Za-z]+?);', 'g') r(name)
            ) s on ce.name = s.name
        loop
            t := replace(t, '&' || r.name || ';', r.ch);
        end loop;

    for r in
        select distinct
            hex[1] hex,
            ('x' || repeat('0', 8 - length(hex[1])) || hex[1])::bit(32)::int codepoint
        from regexp_matches(t, '&#x([0-9a-f]{1,8}?);', 'gi') s(hex)
        loop
            t := regexp_replace(t, '&#x' || r.hex || ';', chr(r.codepoint), 'gi');
        end loop;

    for r in
        select distinct
            chr(codepoint[1]::int) ch,
            codepoint[1] codepoint
        from regexp_matches(t, '&#([0-9]{1,10}?);', 'g') s(codepoint)
        loop
            t := replace(t, '&#' || r.codepoint || ';', r.ch);
        end loop;

    return t;
end;
$$;

comment on function salve.entity2char(text) is 'Converte todas as entidades HTML para os seus caracteres correspondentes';

alter function salve.entity2char(text) owner to postgres;

grant execute on function salve.entity2char(text) to usr_salve;

create or replace function salve.fn_calc_situacao_registro(in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean) returns text
    language plpgsql
as
$$
begin
    if in_utilizado_avaliacao='S' then
        if st_adicionado_apos_avaliacao then
            return 'Registro adicionado após a avaliação';
        else
            return 'Registro utilizado na avaliação';
        end if;
    else
        if in_utilizado_avaliacao='N' then
            return 'Registro não utilizado na avaliação';
        elseif in_utilizado_avaliacao='' then
            return 'Registro não confirmado';
        else
            return '';
        end if;
    end if;
end;
$$;

comment on function salve.fn_calc_situacao_registro(text, boolean) is 'Função para calcular a situação do registro em relação a sua utilização na avaliação';

alter function salve.fn_calc_situacao_registro(text, boolean) owner to postgres;

grant execute on function salve.fn_calc_situacao_registro(text, boolean) to usr_salve;

create or replace function salve.fn_ficha_bacias(p_sq_ficha bigint)
    returns TABLE(no_contexto text, no_origem text, no_bacia text, cd_bacia text, cd_sistema text, sq_bacia bigint, tx_trilha text, de_bacia_ordem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bacia bigint, co_nivel_taxonomico text, dt_carencia date, js_ref_bibs jsonb, st_em_carencia boolean)
    language plpgsql
as
$$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao

             ),
             bacias_ficha as (select 'ficha'::text                                              as no_contexto,
                                     'salve'::text                                              as no_origem,
                                     bacia.descricao::text                                      as no_bacia,
                                     bacia.codigo::text                                         as cd_bacia,
                                     bacia.codigo_sistema::text                                 as cd_sistema,
                                     bacia.id::bigint                                           as sq_bacia,
                                     bacia.trilha::text                                         as tx_trilha,
                                     bacia.ordem::text                                          as de_bacia_ordem,

                                     -- situacao avaliacao
                                     true::boolean                                               as st_utilizado_avalicao,
                                     false::boolean                                              as st_adicionado_apos_avaliacao,

                                     -- ids
                                     fb.sq_ficha_bacia::bigint                                  as sq_ficha_bacia,

                                     -- complemento
                                     passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,

                                     null::date as dt_carencia,

                                     -- ref bib
                                     salve.fn_ref_bib_json(fb.sq_ficha,fb.sq_ficha_bacia,'sq_ficha_bacia')::jsonb as js_ref_bibs

                              from salve.ficha_bacia fb
                                       inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                                       inner join salve.vw_bacia_hidrografica bacia on bacia.id = fb.sq_bacia

             ), bacias_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,

                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia')::jsonb as js_ref_bibs


            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- não considerar registros que forem centroide do Estado
              and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR ref_aprox.cd_sistema <> 'ESTADO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false

        ), bacias_ocorrencias_portalbio as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,

                   fo.dt_carencia::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia_portalbio,'sq_ficha_ocorrencia_portalbio')::jsonb as js_ref_bibs
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false

        )
        select a.*, false as st_em_carencia from bacias_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_portalbio as c;
end;
$$;

alter function salve.fn_ficha_bacias(bigint) owner to postgres;

grant execute on function salve.fn_ficha_bacias(bigint) to usr_salve;

create or replace function salve.fn_ficha_biomas(p_sq_ficha bigint)
    returns TABLE(no_bioma text, no_contexto text, no_origem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bioma bigint, sq_bioma_salve bigint, sq_bioma_corp bigint, co_nivel_taxonomico text, dt_carencia date, js_ref_bibs jsonb, st_em_carencia boolean)
    language plpgsql
as
$$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_ficha = p_sq_ficha
        ),

             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             -- ler biomas adicionados pelo usuario na ficha
             biomas_ficha as (

                 -- ler biomas das ocorrencias SALVE
                 select bioma_salve.ds_dados_apoio::text                              as no_bioma
                      , 'ficha'::text                                            as no_contexto
                      , 'salve'::text                                            as no_origem

                      -- situacao avaliacao
                      , true::boolean                                               as st_utilizado_avalicao
                      , false::boolean                                              as st_adicionado_apos_avaliacao

                      -- ids
                      , fb.sq_ficha_bioma::bigint                                  as sq_ficha_bioma
                      , bioma_salve.sq_dados_apoio::bigint                         as sq_bioma_salve
                      , bioma_corp.sq_bioma::bigint                               as sq_bioma_corp

                      -- complemento
                      , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico

                      ,null::date as dt_carencia

                      -- ref bib
                      ,salve.fn_ref_bib_json(fb.sq_ficha,fb.sq_ficha_bioma,'sq_ficha_bioma')::jsonb as js_ref_bibs

                 from salve.ficha_bioma fb
                          inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                          inner join salve.dados_apoio as bioma_salve on bioma_salve.sq_dados_apoio = fb.sq_bioma
                          left outer join salve.bioma as bioma_corp on bioma_corp.no_bioma = bioma_salve.ds_dados_apoio
             ),

             biomas_ocorrencias_salve as (
                 select bioma_corp.no_bioma                                                     as no_bioma,
                        'ocorrencia'                                                            as no_contexto,
                        'salve'::text                                                           as no_origem,
                        -- situacao avaliacao
                        (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                        (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                        -- ids
                        null::bigint                                                            as sq_ficha_bioma,
                        bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                        bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                        -- complemento
                        passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,

                        salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia,

                        -- ref bib
                        salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia')::jsonb as js_ref_bibs

                 from salve.ficha_ocorrencia_registro a
                          inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                          inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                          inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                          inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                          left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                          left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
                          left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                          left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                 where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
                   -- não considerar registros que forem centroide do Estado
                   and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR                   ref_aprox.cd_sistema <> 'ESTADO')
                   and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                   -- se nao for do grupo marinho não exibir o bioma marinho
                   and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )
             )
                , biomas_ocorrencias_portalbio as (
            select bioma_corp.no_bioma                                                     as no_bioma,
                   'ocorrencia'                                                            as no_contexto,
                   'portalbio'::text                                                       as no_origem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                   -- ids
                   null::bigint                                                            as sq_ficha_bioma,
                   bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                   bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                   -- complemento
                   passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,

                   fo.dt_carencia::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia_portalbio,'sq_ficha_ocorrencia_portalbio')::jsonb as js_ref_bibs

            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                     inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- se nao for do grupo marinho não exibir o bioma marinho
              and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )

        )
        select a.*, false as st_em_carencia from biomas_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_portalbio as c;


end;
$$;

alter function salve.fn_ficha_biomas(bigint) owner to postgres;

grant execute on function salve.fn_ficha_biomas(bigint) to usr_salve;

create or replace function salve.fn_ficha_estados(p_sq_ficha bigint)
    returns TABLE(no_contexto text, no_origem text, sq_estado bigint, no_estado text, sg_estado text, sq_regiao bigint, no_regiao text, nu_ordem_regiao integer, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_uf bigint, co_nivel_taxonomico text, dt_carencia date, js_ref_bibs jsonb, st_em_carencia boolean)
    language plpgsql
as
$$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , grupo.cd_sistema
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             estados_ficha as (select 'ficha'::text                                             as no_contexto,
                                      'salve'::text                                             as no_origem,
                                      uf.sq_estado::bigint                                      as sq_estado,
                                      uf.no_estado::text                                        as no_estado,
                                      uf.sg_estado::text                                        as no_estado,
                                      regiao.sq_regiao::bigint                                  as sq_regiao,
                                      regiao.no_regiao::text                                    as no_regiao,
                                      case when lower(regiao.no_regiao) = 'norte' then 1 else
                                          case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                                              case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                                                  case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                                      case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                                          end end end end end::integer                                      as nu_ordem_regiao
                                      -- situacao avaliacao
                                       , true::boolean                                               as st_utilizado_avalicao
                                       , false::boolean                                              as st_adicionado_apos_avaliacao

                                      -- ids
                                       , fu.sq_ficha_uf::bigint                                  as sq_ficha_uf

                                      -- complemento
                                       , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                       , null::date as dt_carencia

                                      -- ref bib
                                       , salve.fn_ref_bib_json(fu.sq_ficha,fu.sq_ficha_uf,'sq_ficha_uf')::jsonb as js_ref_bibs
                               from salve.ficha_uf fu
                                        inner join passo2 on passo2.sq_ficha = fu.sq_ficha
                                        inner join salve.estado as uf on uf.sq_estado = fu.sq_estado
                                        left outer join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
             ), estados_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as sg_estado,
                   regiao.sq_regiao::bigint                                  as sq_regiao,
                   regiao.no_regiao::text                                    as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia')::jsonb as js_ref_bibs

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao

            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false

        ), estados_ocorrencias_portalbio as (

            select 'ocorrencia'                                                            as no_contexto,
                   'salve'::text                                                           as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as no_estado,
                   regiao.sq_regiao                                          as sq_regiao,
                   regiao.no_regiao                                          as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia_portalbio,'sq_ficha_ocorrencia_portalbio')::jsonb as js_ref_bibs
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false
        )
        select a.*, false as st_em_carencia from estados_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_portalbio as c;
end;
$$;

alter function salve.fn_ficha_estados(bigint) owner to postgres;

grant execute on function salve.fn_ficha_estados(bigint) to usr_salve;

create or replace function salve.fn_ficha_grid_registros(p_sq_ficha bigint)
    returns TABLE(id bigint, id_row text, sq_ficha bigint, nm_cientifico text, cd_contexto text, no_bd text, no_fonte text, nu_x double precision, nu_y double precision, ds_precisao_coord text, no_datum text, nu_altitude integer, in_sensivel text, ds_sensivel text, sq_situacao_avaliacao bigint, ds_situacao_avaliacao text, cd_situacao_avaliacao text, ds_motivo_nao_utilizado text, tx_justificativa_nao_utilizado text, dt_carencia date, no_prazo_carencia text, no_localidade text, no_estado text, no_municipio text, in_presenca_atual text, ds_presenca_atual text, json_ref_bib text, id_origem text, co_nivel_taxonomico text, nu_grau_taxonomico integer, ds_data_hora text, dt_alteracao date, no_usuario_alteracao text, tx_observacao text, dt_ordenar text, no_autor text, no_projeto text, st_centroide boolean, sn_utilizado_avaliacao text, in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean)
    language plpgsql
as
$$
begin
    return query
        -- ler o taxon para poder processar as especies e subespecies
        with passo1 as (
            select ficha.sq_taxon, ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (
                 select ficha.sq_ficha
                 from salve.ficha
                          inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 where (ficha.sq_taxon = (select passo1.sq_taxon from passo1) or
                        taxon.sq_taxon_pai = (select passo1.sq_taxon from passo1))
                   and ficha.sq_ciclo_avaliacao = (select passo1.sq_ciclo_avaliacao from passo1)
             ),
             passoFinal as (
                 SELECT fo.sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_situacao_avaliacao
                      , 'salve'                                        as no_bd
                      , case
                            when consulta.sq_ficha_ocorrencia_consulta is null then 'SALVE'
                            else 'SALVE / Consulta' END                  as no_fonte
                      , 'REGISTRO_OCORRENCIA'                          as cd_contexto
                      , ge_ocorrencia                                  as ge_ocorrencia
                      , precisao.ds_dados_apoio                        as ds_precisao_coord
                      , datum.ds_dados_apoio                           as no_datum
                      , fo.nu_altitude                                 as nu_altitude
                      , fo.in_sensivel                                 as in_sensivel
                      , motivo.ds_dados_apoio                          as ds_motivo_nao_utilizado
                      , case
                            when fo.in_utilizado_avaliacao = 'N'
                                then salve.remover_html_tags(fo.tx_nao_utilizado_avaliacao)
                            else ''::text end                          as tx_justificativa_nao_utilizado
                      , salve.calc_carencia(fo.dt_inclusao::date,
                                            prazo_carencia.cd_sistema) as dt_carencia
                      , prazo_carencia.ds_dados_apoio                  as no_prazo_carencia
                      , fo.no_localidade
                      , uf.sg_estado                                   as no_estado
                      , case
                            when coalesce(metodologia.cd_sistema, ''::text) = 'CENTROIDE'::text and
                                 coalesce(referencia.cd_sistema, ''::text) = 'ESTADO'::text then ''::text
                            ELSE municipio.no_municipio end            as no_municipio
                      , fo.in_presenca_atual::text
                      , salve.snd(fo.in_presenca_atual)                as ds_presenca_atual
                      , ref_bib.json_ref_bib::text
                      , case
                            when fo.id_origem is null then concat('SALVE:', fo.sq_ficha_ocorrencia::text)
                            ELSE fo.id_origem::text end                as id_origem
                      , case
                            when fo.nu_dia_registro is not null and fo.nu_mes_registro is not null and
                                 fo.nu_ano_registro is not null then concat(lpad(fo.nu_dia_registro::text, 2, '0'), '/',
                                                                            lpad(fo.nu_mes_registro::text, 2, '0'), '/',
                                                                            fo.nu_ano_registro::text)
                            else
                                case
                                    when fo.nu_mes_registro is not null and fo.nu_ano_registro is not null
                                        then concat(lpad(fo.nu_mes_registro::text, 2, '0'), '/',
                                                    fo.nu_ano_registro::text)
                                    else case
                                             when fo.nu_ano_registro is not null then fo.nu_ano_registro::text
                                             else null end
                                    end
                     end                                               as ds_dia_mes_ano_registro
                      , fo.hr_registro                                 as ds_hora_registro
                      , fo.dt_alteracao::date
                      , fo.sq_pessoa_alteracao
                      , fo.tx_observacao::text
                      ,concat(fo.nu_ano_registro,'-',lpad(fo.nu_mes_registro::text,2,'0'),'-',fo.nu_dia_registro) as dt_ordenar
                      ,'' as no_autor
                      ,'' as no_projeto
                      , case when coalesce(metodologia.cd_sistema, ''::text) = 'CENTROIDE'::text and
                                  coalesce(referencia.cd_sistema, ''::text) = 'ESTADO'::text then true else false end as st_centroide
                      , fo.in_utilizado_avaliacao::text
                      , fo.st_adicionado_apos_avaliacao::boolean

                 from salve.ficha_ocorrencia fo
                          left join salve.ficha_ocorrencia_registro a on a.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     -- precisao
                          left join salve.dados_apoio precisao on precisao.sq_dados_apoio = fo.sq_precisao_coordenada
                     -- datum
                          left join salve.dados_apoio datum on datum.sq_dados_apoio = fo.sq_datum
                     -- prazo carencia
                          LEFT JOIN salve.dados_apoio prazo_carencia
                                    ON prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     -- Estado
                          left join salve.registro_estado reguf on reguf.sq_registro = a.sq_registro
                          left join salve.estado uf on uf.sq_estado = reguf.sq_estado
                     -- Municipio
                          left join salve.registro_municipio regmun on regmun.sq_registro = a.sq_registro
                          left join salve.municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                     -- referencia
                          LEFT JOIN salve.dados_apoio referencia ON referencia.sq_dados_apoio = fo.sq_ref_aproximacao
                     -- metodologia
                          LEFT JOIN salve.dados_apoio metodologia
                                    ON metodologia.sq_dados_apoio = fo.sq_metodo_aproximacao
                     -- motivo não utilizado
                          left join salve.dados_apoio motivo
                                    on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                     -- ocorrencia da consulta
                          left join salve.ficha_ocorrencia_consulta as consulta
                                    on consulta.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     -- referencia(s) bibliográfica(s)
                          left join lateral (
                     select json_object_agg(concat('rnd', trunc((random() * 100000))::text),
                                            json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano)
                                            )) as json_ref_bib
                     from salve.ficha_ref_bib x
                              left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                     where x.sq_ficha = fo.sq_ficha
                       and x.sq_registro = a.sq_ficha_ocorrencia
                       and x.no_tabela = 'ficha_ocorrencia'
                       and x.no_coluna = 'sq_ficha_ocorrencia' ) as ref_bib on true
                 where fo.ge_ocorrencia is not null

                 union
-- 1
                 select fo.sq_ficha_ocorrencia_portalbio as sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_situacao_avaliacao
                      , 'portalbio'                                     as no_bd
                      --, coalesce(coalesce(fo.no_base_dados, concat('Portalbio<br>', no_instituicao)),'Portalbio')  as no_fonte
                      , salve.calc_base_dados_portalbio(concat(fo.tx_ocorrencia, ' ', fo.no_instituicao),fo.de_uuid) as no_fonte
                      , 'PORTALBIO'                                     as cd_contexto
                      , ge_ocorrencia                                   as ge_ocorrencia
                      , case
                            when left(fo.tx_ocorrencia, 1) = '{' then fo.tx_ocorrencia::jsonb ->> 'precisaoCoord'::text
                            else null end                               as ds_precisao_coord
                      , null                                            as no_datum
                      , null                                            as nu_altitude
                      , null                                            as in_sensivel
                      , motivo.ds_dados_apoio                           as ds_motivo_nao_utilizado
                      , salve.remover_html_tags(fo.tx_nao_utilizado_avaliacao)::text as tx_justificativa_nao_utilizado
                      , fo.dt_carencia
                      , null                                            as no_prazo_carencia
                      , fo.no_local
                      , case
                            when reguf.sq_estado is not null then uf.sg_estado
                            else
                                case
                                    when left(tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(tx_ocorrencia::jsonb ->> 'estado',
                                                     tx_ocorrencia::jsonb ->> 'stateProvince')::text, uf.sg_estado)::text
                                    else uf.sg_estado end end           as no_estado
                      , case
                            when municipio.no_municipio is not null then municipio.no_municipio
                            else
                                case
                                    when left(fo.tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(fo.tx_ocorrencia::jsonb ->> 'municipio',
                                                     fo.tx_ocorrencia::jsonb ->> 'municipality')::text,
                                            municipio.no_municipio)::text
                                    else municipio.no_municipio end end as no_municipio


                      , fo.in_presenca_atual::text
                      , salve.snd(fo.in_presenca_atual)                  as ds_presenca_atual
                      , ref_bib.json_ref_bib::text
                      , fo.de_uuid                                      as id_origem
                      , case
                            when to_char(fo.dt_ocorrencia, 'dd') is not null and
                                 to_char(fo.dt_ocorrencia, 'MM') is not null and
                                 to_char(fo.dt_ocorrencia, 'yyyy') is not null then concat(
                                    lpad(to_char(fo.dt_ocorrencia, 'dd')::text, 2, '0'), '/',
                                    lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                            else
                                case
                                    when to_char(fo.dt_ocorrencia, 'MM') is not null and
                                         to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                        then concat(lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                                    else case
                                             when to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                                 then to_char(fo.dt_ocorrencia, 'yyyy')::text
                                             else null end
                                    end
                     end                                                as ds_dia_mes_ano_registro
                      , null                                            as ds_hora_registro
                      , fo.dt_alteracao::date
                      , null                                            as sq_pessoa_alteracao
                      , fo.tx_observacao::text
                      , to_char(fo.dt_ocorrencia, 'yyyy-MM-dd') as dt_ordenar
                      , fo.no_autor
                      , fo.tx_ocorrencia::jsonb->>'projeto'::text as no_projeto
                      , false as st_centroide
                      , fo.in_utilizado_avaliacao::text
                      , fo.st_adicionado_apos_avaliacao::boolean

                 from salve.ficha_ocorrencia_portalbio_reg a
                          join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     -- Estado
                          left join salve.registro_estado reguf on reguf.sq_registro = a.sq_registro
                          left join salve.estado uf on uf.sq_estado = reguf.sq_estado
                     -- Municipio
                          left join salve.registro_municipio regmun on regmun.sq_registro = a.sq_registro
                          left join salve.municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                     -- motivo não utilizado
                          left join salve.dados_apoio motivo
                                    on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao

                     -- referencia(s) bibliográfica(s)
                          left join lateral (
                     select json_object_agg(concat('rnd', trunc((random() * 100000))::text),
                                            json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano)
                                            )) as json_ref_bib
                     from salve.ficha_ref_bib x
                              left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                     where x.sq_ficha = fo.sq_ficha
                       and x.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                       and x.no_tabela = 'ficha_ocorrencia_portalbio'
                       and x.no_coluna = 'sq_ficha_ocorrencia_portalbio' ) as ref_bib on true

                 union


                 select fo.sq_ficha_ocorrencia_portalbio as sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_situacao_avaliacao
                      , 'portalbio'                                     as no_bd
                      , salve.calc_base_dados_portalbio(concat(fo.tx_ocorrencia, ' ', fo.no_instituicao),fo.de_uuid) as no_fonte
                      --, coalesce(coalesce(fo.no_base_dados, concat('Portalbio<br>', no_instituicao)),                              'Portalbio') as no_fonte
                      , 'PORTALBIO'                                     as cd_contexto
                      , ge_ocorrencia                                   as ge_ocorrencia
                      , case
                            when left(fo.tx_ocorrencia, 1) = '{' then fo.tx_ocorrencia::jsonb ->> 'precisaoCoord'::text
                            else null end                               as ds_precisao_coord
                      , null                                            as no_datum
                      , null                                            as nu_altitude
                      , null                                            as in_sensivel
                      , motivo.ds_dados_apoio                           as ds_motivo_nao_utilizado
                      , salve.remover_html_tags(fo.tx_nao_utilizado_avaliacao) as tx_justificativa_nao_utilizado
                      , fo.dt_carencia
                      , null                                            as no_prazo_carencia
                      , fo.no_local
                      , case
                            when reguf.sq_estado is not null then uf.sg_estado
                            else
                                case
                                    when left(tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(tx_ocorrencia::jsonb ->> 'estado',
                                                     tx_ocorrencia::jsonb ->> 'stateProvince')::text, uf.sg_estado)::text
                                    else uf.sg_estado end end           as no_estado
                      , case
                            when municipio.no_municipio is not null then municipio.no_municipio
                            else
                                case
                                    when left(fo.tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(fo.tx_ocorrencia::jsonb ->> 'municipio',
                                                     fo.tx_ocorrencia::jsonb ->> 'municipality')::text,
                                            municipio.no_municipio)::text
                                    else municipio.no_municipio end end as no_municipio

                      , fo.in_presenca_atual::text
                      , salve.snd(fo.in_presenca_atual)                  as ds_presenca_atual
                      , ref_bib.json_ref_bib::text
                      , fo.de_uuid                                      as id_origem
                      , case
                            when to_char(fo.dt_ocorrencia, 'dd') is not null and
                                 to_char(fo.dt_ocorrencia, 'MM') is not null and
                                 to_char(fo.dt_ocorrencia, 'yyyy') is not null then concat(
                                    lpad(to_char(fo.dt_ocorrencia, 'dd')::text, 2, '0'), '/',
                                    lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                            else
                                case
                                    when to_char(fo.dt_ocorrencia, 'MM') is not null and
                                         to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                        then concat(lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                                    else case
                                             when to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                                 then to_char(fo.dt_ocorrencia, 'yyyy')::text
                                             else null end
                                    end
                     end                                                as ds_dia_mes_ano_registro
                      , null                                            as ds_hora_registro
                      , fo.dt_alteracao::date
                      , null                                            as sq_pessoa_alteracao
                      , fo.tx_observacao::text
                      , to_char(fo.dt_ocorrencia, 'yyyy-MM-dd') as dt_ordenar
                      , fo.no_autor
                      , fo.tx_ocorrencia::jsonb->>'projeto'::text as no_projeto
                      , false as st_centroide
                      , fo.in_utilizado_avaliacao::text
                      , fo.st_adicionado_apos_avaliacao::boolean

                 from salve.ficha_ocorrencia_portalbio fo
                          left join salve.ficha_ocorrencia_portalbio_reg a
                                    on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     -- Estado
                          left join salve.registro_estado reguf on reguf.sq_registro = a.sq_registro
                          left join salve.estado uf on uf.sq_estado = reguf.sq_estado
                     -- Municipio
                          left join salve.registro_municipio regmun on regmun.sq_registro = a.sq_registro
                          left join salve.municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                     -- motivo não utilizado
                          left join salve.dados_apoio motivo
                                    on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao

                     -- referencia(s) bibliográfica(s)
                          left join lateral (
                     select json_object_agg(concat('rnd', trunc((random() * 100000))::text),
                                            json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano)
                                            )) as json_ref_bib
                     from salve.ficha_ref_bib x
                              left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                     where x.sq_ficha = fo.sq_ficha
                       and x.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                       and x.no_tabela = 'ficha_ocorrencia_portalbio'
                       and x.no_coluna = 'sq_ficha_ocorrencia_portalbio' ) as ref_bib on true
                 where a.sq_ficha_ocorrencia_portalbio_reg is null
             )
        select passoFinal.sq_ficha_ocorrencia::bigint                                    as id
             , concat(passoFinal.no_bd, '-', passoFinal.sq_ficha_ocorrencia::text)::text as id_row
             , passoFinal.sq_ficha::bigint
             , ficha.nm_cientifico::text
             , passoFinal.cd_contexto::text
             , passoFinal.no_bd::text
             , passoFinal.no_fonte::text
             , st_x(passoFinal.ge_ocorrencia)::double precision                          as nu_x
             , st_y(passoFinal.ge_ocorrencia)::double precision                          as nu_y
             , passoFinal.ds_precisao_coord::text
             , passoFinal.no_datum::text
             , passoFinal.nu_altitude::integer
             , passoFinal.in_sensivel::text
             , salve.snd(passoFinal.in_sensivel)::text                                   as ds_sensivel
             , passoFinal.sq_situacao_avaliacao
             , situacao_avaliacao.ds_dados_apoio as ds_situacao_avaliacao
             , situacao_avaliacao.cd_sistema as cd_situacao_avaliacao
             , passoFinal.ds_motivo_nao_utilizado::text
             , passoFinal.tx_justificativa_nao_utilizado::text
             , passoFinal.dt_carencia::date
             , passoFinal.no_prazo_carencia ::text
             , passoFinal.no_localidade::text
             , passoFinal.no_estado::text
             , passoFinal.no_municipio::text
             , passoFinal.in_presenca_atual::text
             , passoFinal.ds_presenca_atual::text
             , passoFinal.json_ref_bib::text
             , passoFinal.id_origem::text
             , nivel_taxonomico.co_nivel_taxonomico::text
             , nivel_taxonomico.nu_grau_taxonomico::integer
             , replace(trim(concat(passoFinal.ds_dia_mes_ano_registro, ' ', passoFinal.ds_hora_registro)), '00:00',
                       '')::text                                                         as ds_data_hora
             , passoFinal.dt_alteracao::date
             , pf.no_pessoa::text                                                        as no_usuario_alteracao
             , passoFinal.tx_observacao::text
             , passoFinal.dt_ordenar
             , passoFinal.no_autor
             , passoFinal.no_projeto
             , passoFinal.st_centroide
             , salve.snd(passoFinal.in_utilizado_avaliacao)::text as sn_utilizado_avaliacao
             , passoFinal.in_utilizado_avaliacao::text
             , passoFinal.st_adicionado_apos_avaliacao::boolean

        from passoFinal
                 inner join salve.ficha on ficha.sq_ficha = passoFinal.sq_ficha
                 inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                 inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = passoFinal.sq_situacao_avaliacao
                 left outer join salve.vw_pessoa_fisica pf on pf.sq_pessoa = passoFinal.sq_pessoa_alteracao;
end ;
$$;

alter function salve.fn_ficha_grid_registros(bigint) owner to postgres;

grant execute on function salve.fn_ficha_grid_registros(bigint) to usr_salve;

create or replace function salve.fn_ficha_ref_bibs(p_sq_ficha bigint)
    returns TABLE(sq_publicacao bigint, no_origem text, de_ref_bibliografica text, json_anexos text, no_tags text, no_tabela character varying, no_coluna character varying, co_nivel_taxonomico text, cd_situacao_avaliacao text, dt_carencia date, st_em_crencia boolean)
    language plpgsql
as
$$
begin
    return query
        with cte as (
            select ficha.sq_taxon,ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ),
             cte2 as (
                 select ficha.sq_ficha, nivel.co_nivel_taxonomico
                 from salve.ficha
                          inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                          inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                 where (ficha.sq_taxon = (select cte.sq_taxon from cte) or
                        taxon.sq_taxon_pai = (select cte.sq_taxon from cte))
                   and ficha.sq_ciclo_avaliacao = (select cte.sq_ciclo_avaliacao from cte )
             ),
             refFichas as (
                 select a.sq_publicacao
                      , case when a.no_tabela in ('ficha_uf','ficha_bioma','ficha_bacia') and
                                  p.de_titulo like '% - SISBIO' then 'ocorrencia'::text else 'ficha'::text end as no_origem
                      , p.de_ref_bibliografica
                      , anexos.json_anexos
                      , tags.no_tags
                      , a.no_tabela
                      , a.no_coluna
                      , cte2.co_nivel_taxonomico
                      , null as cd_situacao_sistema
                      , null::date as dt_carencia
                 from salve.ficha_ref_bib a
                          inner join cte2 on cte2.sq_ficha = a.sq_ficha
                          inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao

                          left join lateral (
                     select array_to_string(array_agg(dados_apoio.ds_dados_apoio), ',') AS no_tags
                     from salve.ficha_ref_bib_tag tag
                              inner join salve.dados_apoio on dados_apoio.sq_dados_apoio = tag.sq_tag
                     where tag.sq_ficha_ref_bib = a.sq_ficha_ref_bib
                     ) as tags on true
                          left join lateral (
                     select json_object_agg(anexo.sq_publicacao_arquivo::text, json_build_object('sq_publicacao_anexo',
                                                                                                 anexo.sq_publicacao_arquivo,
                                                                                                 'no_arquivo',
                                                                                                 anexo.no_publicacao_arquivo))::text as json_anexos
                     from taxonomia.publicacao_arquivo anexo
                     where anexo.sq_publicacao = a.sq_publicacao
                     ) as anexos on true
                 where a.sq_publicacao is not null
                   and p.de_ref_bibliografica is not null
                   and not a.no_tabela in ('ficha_ocorrencia', 'ficha_ocorrencia_portalbio','ficha_historico_avaliacao')
             ),
             refsOcoFicha as (
                 select a.sq_publicacao
                      , 'ocorrencia' as no_origem
                      , p.de_ref_bibliografica
                      , anexos.json_anexos
                      , tags.no_tags
                      , a.no_tabela
                      , a.no_coluna
                      , cte2.co_nivel_taxonomico
                      , situacao.cd_sistema as cd_situacao_sistema
                      , salve.calc_carencia(o.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

                 from salve.ficha_ref_bib a
                          inner join cte2 on cte2.sq_ficha = a.sq_ficha
                          inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao
                          inner join salve.ficha_ocorrencia o on o.sq_ficha_ocorrencia = a.sq_registro
                          inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = o.sq_situacao_avaliacao
                          left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = o.sq_prazo_carencia

                          left join lateral (
                     select array_to_string(array_agg(dados_apoio.ds_dados_apoio), ',') AS no_tags
                     from salve.ficha_ref_bib_tag tag
                              inner join salve.dados_apoio on dados_apoio.sq_dados_apoio = tag.sq_tag
                     where tag.sq_ficha_ref_bib = a.sq_ficha_ref_bib
                     ) as tags on true
                          left join lateral (
                     select json_object_agg(anexo.sq_publicacao_arquivo::text, json_build_object('sq_publicacao_anexo',
                                                                                                 anexo.sq_publicacao_arquivo,
                                                                                                 'no_arquivo',
                                                                                                 anexo.no_publicacao_arquivo))::text as json_anexos
                     from taxonomia.publicacao_arquivo anexo
                     where anexo.sq_publicacao = a.sq_publicacao
                     ) as anexos on true
                 where a.sq_publicacao is not null
                   and p.de_ref_bibliografica is not null
                   and a.no_tabela = 'ficha_ocorrencia'
                   and a.no_coluna = 'sq_ficha_ocorrencia'
                   and situacao.cd_sistema in ( 'REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                   and not exists(select null from refFichas where refFichas.sq_publicacao = p.sq_publicacao limit 1)
             ),
             refsOcoPortalbio as
                 (
                     select a.sq_publicacao
                          , 'ocorrencia' as no_origem
                          , p.de_ref_bibliografica
                          , anexos.json_anexos
                          , tags.no_tags
                          , a.no_tabela
                          , a.no_coluna
                          , cte2.co_nivel_taxonomico
                          , situacao.cd_sistema as cd_situacao_sistema
                          , o.dt_carencia as dt_carencia
                     from salve.ficha_ref_bib a
                              inner join cte2 on cte2.sq_ficha = a.sq_ficha
                              inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao
                              inner join salve.ficha_ocorrencia_portalbio o on o.sq_ficha_ocorrencia_portalbio = a.sq_registro
                              inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = o.sq_situacao_avaliacao
                              left join lateral (
                         select array_to_string(array_agg(dados_apoio.ds_dados_apoio), ',') AS no_tags
                         from salve.ficha_ref_bib_tag tag
                                  inner join salve.dados_apoio on dados_apoio.sq_dados_apoio = tag.sq_tag
                         where tag.sq_ficha_ref_bib = a.sq_ficha_ref_bib
                         ) as tags on true
                              left join lateral (
                         select json_object_agg(anexo.sq_publicacao_arquivo::text,
                                                json_build_object('sq_publicacao_anexo',
                                                                  anexo.sq_publicacao_arquivo,
                                                                  'no_arquivo',
                                                                  anexo.no_publicacao_arquivo))::text as json_anexos
                         from taxonomia.publicacao_arquivo anexo
                         where anexo.sq_publicacao = a.sq_publicacao
                         ) as anexos on true
                     where p.de_ref_bibliografica is not null
                       and a.no_tabela = 'ficha_ocorrencia_portalbio'
                       and a.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                       and situacao.cd_sistema in ( 'REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                       and not exists(select null
                                      from refFichas
                                      where refFichas.sq_publicacao = p.sq_publicacao
                                      limit 1)
                 )
        select a.*, false as st_em_carencia from refFichas as a
        union
        select b.*,case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from refsOcoFicha as b
        union
        select c.*,case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from refsOcoPortalbio as c
        order by no_origem, de_ref_bibliografica;
end;
$$;

alter function salve.fn_ficha_ref_bibs(bigint) owner to postgres;

grant execute on function salve.fn_ficha_ref_bibs(bigint) to usr_salve;

create or replace function salve.fn_ficha_registros(p_sq_ficha bigint)
    returns TABLE(bo_salve boolean, id_ocorrencia bigint, sq_ciclo_avaliacao bigint, sq_ficha bigint, sq_taxon bigint, nm_cientifico text, sq_categoria_iucn bigint, sq_categoria_final bigint, no_autor_taxon text, no_reino text, no_filo text, no_classe text, no_ordem text, no_familia text, no_genero text, no_especie text, no_subespecie text, ds_categoria_anterior text, ds_categoria_avaliada text, ds_categoria_validada text, ds_pan text, nu_longitude double precision, nu_latitude double precision, nu_altitude integer, de_endemica_brasil text, ds_sensivel text, sq_situacao_avaliacao bigint, ds_situacao_avaliacao text, cd_situacao_avaliacao text, tx_justificativa_nao_usado_avaliacao text, ds_motivo_nao_utilizado text, dt_carencia date, ds_prazo_carencia text, no_datum text, no_formato_original text, no_precisao text, no_referencia_aproximacao text, no_metodologia_aproximacao text, tx_metodologia_aproximacao text, no_taxon_citado text, in_presenca_atual text, ds_presenca_atual text, no_tipo_registro text, nu_dia_registro text, nu_mes_registro text, nu_ano_registro text, hr_registro text, nu_dia_registro_fim text, nu_mes_registro_fim text, nu_ano_registro_fim text, ds_data_desconhecida text, no_localidade text, ds_caracteristica_localidade text, no_uc_federal text, no_uc_estadual text, no_rppn text, no_pais text, no_estado text, no_municipio text, no_bioma text, no_ambiente text, no_habitat text, vl_elevacao_min numeric, vl_elevacao_max numeric, vl_profundidade_min numeric, vl_profundidade_max numeric, de_identificador text, dt_identificacao date, no_identificacao_incerta text, ds_tombamento text, ds_instituicao_tombamento text, no_compilador text, dt_compilacao date, no_revisor text, dt_revisao date, no_validador text, dt_validacao date, in_sensivel text, no_base_dados text, id_origem text, nu_autorizacao text, tx_observacao text, tx_ref_bib text, no_autor_registro text, no_projeto text, ds_utilizado_avaliacao text, ds_situacao_registro text, in_utilizado_avaliacao text)
    language plpgsql
as
$$
begin
    return query

        -- ler o taxon para poder processar as especies e subespecies
        with passo1 as (
            select ficha.sq_taxon, ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (
                 select ficha.sq_ficha
                 from salve.ficha
                          inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 where (ficha.sq_taxon = (select passo1.sq_taxon from passo1) or
                        taxon.sq_taxon_pai = (select passo1.sq_taxon from passo1))
                   and ficha.sq_ciclo_avaliacao = (select passo1.sq_ciclo_avaliacao from passo1)
             ),

             passo3 as (

                 SELECT TRUE                                                                                              AS bo_salve,
                        fo.sq_ficha_ocorrencia                                                                            AS id_ocorrencia,
                        ficha.sq_ciclo_avaliacao,
                        ficha.sq_ficha,
                        ficha.sq_taxon,
                        ficha.nm_cientifico,
                        ficha.sq_categoria_iucn,
                        ficha.sq_categoria_final,
                        taxon.no_autor                                                                                    AS no_autor_taxon,
                        (taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text                                         AS no_reino,
                        (taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text                                          AS no_filo,
                        (taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text                                        AS no_classe,
                        (taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text                                         AS no_ordem,
                        (taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text                                       AS no_familia,
                        (taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text                                        AS no_genero,
                        (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                       AS no_especie,
                        (taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text                                    AS no_subespecie,
                        categoria_anterior.ds_categoria_anterior,
                        CASE
                            WHEN ficha.sq_categoria_iucn IS NOT NULL THEN CONCAT(cat_avaliada.ds_dados_apoio, ' (', cat_avaliada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                              AS ds_categoria_avaliada,
                        CASE
                            WHEN ficha.sq_categoria_final IS NOT NULL THEN CONCAT(cat_validada.ds_dados_apoio, ' (', cat_validada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                              AS ds_categoria_validada,
                        pans.ds_pan,
                        st_x(fo.ge_ocorrencia)                                                                            AS nu_longitude,
                        st_y(fo.ge_ocorrencia)                                                                            AS nu_latitude,
                        fo.nu_altitude,
                        salve.snd(ficha.st_endemica_brasil::text)                                                         AS de_endemica_brasil,
                        salve.snd(fo.in_sensivel::text)                                                                   AS ds_sensivel,
                        fo.sq_situacao_avaliacao,
                        situacao_avaliacao.ds_dados_apoio                                                                 AS ds_situacao_avaliacao,
                        situacao_avaliacao.cd_sistema                                                                     AS cd_situacao_avaliacao,
                        fo.tx_nao_utilizado_avaliacao                                                                     AS tx_justificativa_nao_usado_avaliacao,
                        motivo.ds_dados_apoio                                                                             AS ds_motivo_nao_utilizado,
                        salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema)                              AS dt_carencia,
                        prazo_carencia.ds_dados_apoio                                                                     AS ds_prazo_carencia,
                        datum.ds_dados_apoio                                                                              AS no_datum,
                        formato_original.ds_dados_apoio                                                                   AS no_formato_original,
                        precisao_coord.ds_dados_apoio                                                                     AS no_precisao,
                        referencia_aprox.ds_dados_apoio                                                                   AS no_referencia_aproximacao,
                        metodo_aprox.ds_dados_apoio                                                                       AS no_metodologia_aproximacao,
                        fo.tx_metodo_aproximacao                                                                          AS tx_metodologia_aproximacao,
                        (taxon_citado.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                AS no_taxon_citado,
                        fo.in_presenca_atual,
                        salve.snd(fo.in_presenca_atual::text)                                                             AS ds_presenca_atual,
                        tipo_registro.ds_dados_apoio                                                                      AS no_tipo_registro,
                        fo.nu_dia_registro::text                                                                          AS nu_dia_registro,
                        fo.nu_mes_registro::text                                                                          AS nu_mes_registro,
                        fo.nu_ano_registro::text                                                                          AS nu_ano_registro,
                        fo.hr_registro::text,
                        fo.nu_dia_registro_fim::text                                                                      AS nu_dia_registro_fim,
                        fo.nu_mes_registro_fim::text                                                                      AS nu_mes_registro_fim,
                        fo.nu_ano_registro_fim::text                                                                      AS nu_ano_registro_fim,
                        salve.snd(fo.in_data_desconhecida::text)                                                          AS ds_data_desconhecida,
                        fo.no_localidade,
                        salve.remover_html_tags(fo.tx_local)                                                              AS ds_caracteristica_localidade,
                        uc_federal.no_uc_federal,
                        uc_estadual.no_uc_estadual,
                        INITCAP(rppn_pessoa.no_pessoa::text)                                                                       AS no_rppn,
                        pais.no_pais,
                        estado.no_estado,
                        municipio.no_municipio,
                        bioma.no_bioma,
                        BTRIM(REGEXP_REPLACE(habitat.ds_dados_apoio, '[0-9.]'::text, ''::text, 'g'::text))                AS no_ambiente,
                        habitat.ds_dados_apoio                                                                            AS no_habitat,
                        fo.vl_elevacao_min,
                        fo.vl_elevacao_max,
                        fo.vl_profundidade_min,
                        fo.vl_profundidade_max,
                        fo.de_identificador,
                        fo.dt_identificacao,
                        qualificador.ds_dados_apoio                                                                       AS no_identificacao_incerta,
                        fo.tx_tombamento                                                                                  AS ds_tombamento,
                        fo.tx_instituicao                                                                                 AS ds_instituicao_tombamento,
                        compilador.no_pessoa                                                                              AS no_compilador,
                        fo.dt_compilacao,
                        revisor.no_pessoa                                                                                 AS no_revisor,
                        fo.dt_revisao,
                        validador.no_pessoa                                                                               AS no_validador,
                        fo.dt_validacao,
                        fo.in_sensivel,
                        'salve'::text                                                                                     AS no_base_dados,
                        CASE
                            WHEN fo.id_origem IS NULL THEN CONCAT('SALVE:', fo.sq_ficha_ocorrencia::text)
                            ELSE fo.id_origem::text
                            END                                                                              AS id_origem,
                        ''::text                                                                                          AS nu_autorizacao,
                        salve.remover_html_tags(fo.tx_observacao)                                                         AS tx_observacao,
                        refbibs.json_ref_bibs                                                                             AS tx_ref_bib,
                        NULL::text                                                                                        AS no_autor_registro,
                        NULL::text                                                                                        AS no_projeto,
                        CASE
                            WHEN fo.st_adicionado_apos_avaliacao = TRUE THEN 'Não'::text
                            ELSE salve.snd(fo.in_utilizado_avaliacao::text)
                            END                                                                              AS ds_utilizado_avaliacao,
                        salve.fn_calc_situacao_registro(fo.in_utilizado_avaliacao::text, fo.st_adicionado_apos_avaliacao) AS ds_situacao_registro,
                        fo.in_utilizado_avaliacao
                 FROM salve.ficha_ocorrencia fo
                          JOIN passo2 on passo2.sq_ficha = fo.sq_ficha
                          JOIN salve.ficha_ocorrencia_registro freg ON freg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                          JOIN salve.ficha ON ficha.sq_ficha = fo.sq_ficha
                          JOIN salve.ciclo_avaliacao ciclo ON ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                          JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                          JOIN salve.dados_apoio situacao_avaliacao ON situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                          LEFT JOIN salve.dados_apoio motivo ON motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                          LEFT JOIN salve.dados_apoio cat_avaliada ON cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                          LEFT JOIN salve.dados_apoio cat_validada ON cat_validada.sq_dados_apoio = ficha.sq_categoria_final
                          LEFT JOIN salve.dados_apoio tipo_registro ON tipo_registro.sq_dados_apoio = fo.sq_tipo_registro
                          LEFT JOIN salve.dados_apoio prazo_carencia ON prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                          LEFT JOIN salve.dados_apoio datum ON datum.sq_dados_apoio = fo.sq_datum
                          LEFT JOIN salve.dados_apoio formato_original ON formato_original.sq_dados_apoio = fo.sq_formato_coord_original
                          LEFT JOIN salve.dados_apoio precisao_coord ON precisao_coord.sq_dados_apoio = fo.sq_precisao_coordenada
                          LEFT JOIN salve.dados_apoio referencia_aprox ON referencia_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                          LEFT JOIN salve.dados_apoio metodo_aprox ON metodo_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                          LEFT JOIN taxonomia.taxon taxon_citado ON taxon_citado.sq_taxon = fo.sq_taxon_citado
                          LEFT JOIN LATERAL ( SELECT h.sq_taxon,
                                                     CONCAT(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
                                              FROM salve.taxon_historico_avaliacao h
                                                       JOIN salve.dados_apoio cat_hist ON cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                                              WHERE h.sq_taxon = ficha.sq_taxon
                                                AND h.sq_tipo_avaliacao = 311
                                                AND h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)
                                              ORDER BY h.nu_ano_avaliacao DESC
                                              OFFSET 0 LIMIT 1) categoria_anterior ON TRUE
                          LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
                                                     ARRAY_TO_STRING(ARRAY_AGG(CONCAT(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')), ', '::text) AS ds_pan
                                              FROM salve.ficha_acao_conservacao acoes
                                                       JOIN salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                                       JOIN salve.dados_apoio apoio_acao_situacao ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                                              WHERE acoes.sq_ficha = ficha.sq_ficha
                                              GROUP BY acoes.sq_ficha) pans ON TRUE
                          LEFT JOIN salve.registro_rppn regrppn ON regrppn.sq_registro = freg.sq_registro
                          LEFT JOIN salve.unidade_conservacao as rppn ON rppn.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.pessoa as rppn_pessoa ON rppn_pessoa.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.registro_pais regpais ON regpais.sq_registro = freg.sq_registro
                          LEFT JOIN salve.pais pais ON pais.sq_pais = regpais.sq_pais
                          LEFT JOIN salve.registro_estado reguf ON reguf.sq_registro = freg.sq_registro
                          LEFT JOIN salve.estado estado ON estado.sq_estado = reguf.sq_estado
                          LEFT JOIN salve.registro_municipio regmun ON regmun.sq_registro = freg.sq_registro
                          LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = regmun.sq_municipio
                          LEFT JOIN salve.dados_apoio habitat ON habitat.sq_dados_apoio = fo.sq_habitat
                          LEFT JOIN salve.dados_apoio ambiente ON ambiente.sq_dados_apoio = habitat.sq_dados_apoio_pai
                          LEFT JOIN salve.dados_apoio qualificador ON qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon
                          LEFT JOIN salve.pessoa compilador ON compilador.sq_pessoa = fo.sq_pessoa_compilador
                          LEFT JOIN salve.pessoa revisor ON revisor.sq_pessoa = fo.sq_pessoa_revisor
                          LEFT JOIN salve.pessoa validador ON validador.sq_pessoa = fo.sq_pessoa_validador
                          LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
                          LEFT JOIN salve.bioma bioma ON bioma.sq_bioma = regbio.sq_bioma
                          LEFT JOIN LATERAL ( SELECT JSON_OBJECT_AGG(ficha_ref_bib.sq_ficha_ref_bib, JSON_BUILD_OBJECT('de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica), 'no_autores_publicacao', REPLACE(publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano', ficha_ref_bib.nu_ano))::text AS json_ref_bibs
                                              FROM salve.ficha_ref_bib
                                                       LEFT JOIN taxonomia.publicacao ON publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                                              WHERE ficha_ref_bib.no_tabela::text = 'ficha_ocorrencia'::text
                                                AND ficha_ref_bib.no_coluna::text = 'sq_ficha_ocorrencia'::text
                                                AND ficha_ref_bib.sq_registro = fo.sq_ficha_ocorrencia) refbibs ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(COALESCE(ucf_instituicao.sg_instituicao, ucf_pessoa.no_pessoa::character varying)), ', '::text) AS no_uc_federal
                                              FROM salve.registro_uc_federal x
                                                       JOIN salve.unidade_conservacao ucf ON ucf.sq_pessoa = x.sq_uc_federal
                                                       join salve.instituicao as ucf_instituicao on ucf_instituicao.sq_pessoa = x.sq_uc_federal
                                                       join salve.pessoa as ucf_pessoa on ucf_pessoa.sq_pessoa = x.sq_uc_federal
                                              WHERE x.sq_registro = freg.sq_registro) uc_federal ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(uce_pessoa.no_pessoa), ', '::text) AS no_uc_estadual
                                              FROM salve.registro_uc_estadual x
                                                       JOIN salve.unidade_conservacao uce ON uce.sq_pessoa = x.sq_uc_estadual
                                                       JOIN salve.pessoa as uce_pessoa on uce_pessoa.sq_pessoa = x.sq_uc_estadual
                                              WHERE x.sq_registro = freg.sq_registro) uc_estadual ON TRUE
                 UNION ALL
                 SELECT FALSE                                                                                             AS bo_salve,
                        oc.sq_ficha_ocorrencia_portalbio                                                                  AS id_ocorrencia,
                        ficha.sq_ciclo_avaliacao,
                        ficha.sq_ficha,
                        ficha.sq_taxon,
                        ficha.nm_cientifico,
                        ficha.sq_categoria_iucn,
                        ficha.sq_categoria_final,
                        taxon.no_autor                                                                                    AS no_autor_taxon,
                        (taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text                                         AS no_reino,
                        (taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text                                          AS no_filo,
                        (taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text                                        AS no_classe,
                        (taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text                                         AS no_ordem,
                        (taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text                                       AS no_familia,
                        (taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text                                        AS no_genero,
                        (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                       AS no_especie,
                        (taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text                                    AS no_subespecie,
                        categoria_anterior.ds_categoria_anterior,
                        CASE
                            WHEN ficha.sq_categoria_iucn IS NOT NULL THEN CONCAT(cat_avaliada.ds_dados_apoio, ' (', cat_avaliada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                                           AS ds_categoria_avaliada,
                        CASE
                            WHEN ficha.sq_categoria_final IS NOT NULL THEN CONCAT(cat_validada.ds_dados_apoio, ' (', cat_validada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                                           AS ds_categoria_validada,
                        pans.ds_pan,
                        st_x(oc.ge_ocorrencia)                                                                            AS nu_longitude,
                        st_y(oc.ge_ocorrencia)                                                                            AS nu_latitude,
                        NULL::integer                                                                                     AS nu_altitude,
                        salve.snd(ficha.st_endemica_brasil::text)                                                         AS de_endemica_brasil,
                        NULL::text                                                                                        AS ds_sensivel,
                        oc.sq_situacao_avaliacao,
                        situacao_avaliacao.ds_dados_apoio                                                                 AS ds_situacao_avaliacao,
                        situacao_avaliacao.cd_sistema                                                                     AS cd_situacao_avaliacao,
                        oc.tx_nao_utilizado_avaliacao                                                                     AS tx_justificativa_nao_usado_avaliacao,
                        motivo.ds_dados_apoio                                                                             AS ds_motivo_nao_utilizado,
                        oc.dt_carencia,
                        NULL::text                                                                                        AS ds_prazo_carencia,
                        NULL::text                                                                                        AS no_datum,
                        NULL::text                                                                                        AS no_formato_original,
                        CASE
                            WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'precisaoCoord'::text
                            ELSE ''::text
                            END                                                                                           AS no_precisao,
                        NULL::text                                                                                        AS no_referencia_aproximacao,
                        NULL::text                                                                                        AS no_metodologia_aproximacao,
                        NULL::text                                                                                        AS tx_metodologia_aproximacao,
                        NULL::text                                                                                        AS no_taxon_citado,
                        oc.in_presenca_atual,
                        salve.snd(oc.in_presenca_atual::text)                                                             AS ds_presenca_atual,
                        NULL::text                                                                                        AS no_tipo_registro,
                        TO_CHAR(oc.dt_ocorrencia::timestamp WITH TIME ZONE, 'DD'::text)                                   AS nu_dia_registro,
                        TO_CHAR(oc.dt_ocorrencia::timestamp WITH TIME ZONE, 'MM'::text)                                   AS nu_mes_registro,
                        TO_CHAR(oc.dt_ocorrencia::timestamp WITH TIME ZONE, 'yyyy'::text)                                 AS nu_ano_registro,
                        NULL::text                                                                                        AS hr_registro,
                        NULL::text                                                                                        AS nu_dia_registro_fim,
                        NULL::text                                                                                        AS nu_mes_registro_fim,
                        NULL::text                                                                                        AS nu_ano_registro_fim,
                        salve.snd(oc.in_data_desconhecida::text)                                                          AS ds_data_desconhecida,
                        oc.no_local                                                                                       AS no_localidade,
                        NULL::text                                                                                        AS ds_caracteristica_localidade,
                        uc_federal.no_uc_federal,
                        uc_estadual.no_uc_estadual,
                        INITCAP(rppn_pessoa.no_pessoa::text)                                                                       AS no_rppn,
                        pais.no_pais,
                        estado.no_estado,
                        municipio.no_municipio,
                        bioma.no_bioma,
                        NULL::text                                                                                        AS no_ambiente,
                        NULL::text                                                                                        AS no_habitat,
                        NULL::numeric                                                                                     AS vl_elevacao_min,
                        NULL::numeric                                                                                     AS vl_elevacao_max,
                        NULL::numeric                                                                                     AS vl_profundidade_min,
                        NULL::numeric                                                                                     AS vl_profundidade_max,
                        NULL::text                                                                                        AS de_identificador,
                        NULL::date                                                                                        AS dt_identificacao,
                        NULL::text                                                                                        AS no_identificacao_incerta,
                        NULL::text                                                                                        AS ds_tombamento,
                        NULL::text                                                                                        AS ds_instituicao_tombamento,
                        NULL::text                                                                                        AS no_compilador,
                        NULL::date                                                                                        AS dt_compilacao,
                        revisor.no_pessoa                                                                                 AS no_revisor,
                        oc.dt_revisao,
                        validador.no_pessoa                                                                               AS no_validador,
                        oc.dt_validacao,
                        NULL::bpchar                                                                                      AS in_sensivel,
                        salve.calc_base_dados_portalbio(oc.tx_ocorrencia, oc.de_uuid)                                     AS no_base_dados,
                        CASE
                            WHEN oc.id_origem IS NULL THEN
                                CASE
                                    WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'uuid'::text
                                    ELSE oc.de_uuid
                                    END
                            ELSE oc.id_origem::text
                            END                                                                                           AS id_origem,
                        CASE
                            WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'recordNumber'::text
                            ELSE ''::text
                            END                                                                                           AS nu_autorizacao,
                        NULL::text                                                                                        AS tx_observacao,
                        refbibs.json_ref_bibs                                                                             AS tx_ref_bib,
                        REPLACE(
                                CASE
                                    WHEN oc.no_autor IS NULL THEN
                                        CASE
                                            WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN
                                                CASE
                                                    WHEN (oc.tx_ocorrencia::json ->> 'autor'::text) IS NULL THEN oc.tx_ocorrencia::json ->> 'collector'::text
                                                    ELSE oc.tx_ocorrencia::json ->> 'autor'::text
                                                    END
                                            ELSE ''::text
                                            END
                                    ELSE oc.no_autor
                                    END, '
'::text, '; '::text)                                                             AS no_autor_registro,
                        oc.tx_ocorrencia::jsonb ->> 'projeto'::text                                                       AS no_projeto,
                        CASE
                            WHEN oc.st_adicionado_apos_avaliacao = TRUE THEN 'Não'::text
                            ELSE salve.snd(oc.in_utilizado_avaliacao::text)
                            END                                                                                           AS ds_utilizado_avaliacao,
                        salve.fn_calc_situacao_registro(oc.in_utilizado_avaliacao::text, oc.st_adicionado_apos_avaliacao) AS ds_situacao_registro,
                        oc.in_utilizado_avaliacao
                 FROM salve.ficha_ocorrencia_portalbio oc
                          JOIN passo2 on  passo2.sq_ficha = oc.sq_ficha
                          JOIN salve.ficha ON ficha.sq_ficha = oc.sq_ficha
                          JOIN salve.ciclo_avaliacao ciclo ON ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                          JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                          JOIN salve.dados_apoio situacao_avaliacao ON situacao_avaliacao.sq_dados_apoio = oc.sq_situacao_avaliacao
                          LEFT JOIN salve.dados_apoio motivo ON motivo.sq_dados_apoio = oc.sq_motivo_nao_utilizado_avaliacao
                          LEFT JOIN salve.dados_apoio cat_avaliada ON cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                          LEFT JOIN salve.dados_apoio cat_validada ON cat_validada.sq_dados_apoio = ficha.sq_categoria_final
                          LEFT JOIN LATERAL ( SELECT h.sq_taxon,
                                                     CONCAT(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
                                              FROM salve.taxon_historico_avaliacao h
                                                       JOIN salve.dados_apoio cat_hist ON cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                                              WHERE h.sq_taxon = ficha.sq_taxon
                                                AND h.sq_tipo_avaliacao = 311
                                                AND h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)
                                              ORDER BY h.nu_ano_avaliacao DESC
                                              OFFSET 0 LIMIT 1) categoria_anterior ON TRUE
                          LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
                                                     ARRAY_TO_STRING(ARRAY_AGG(CONCAT(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')), ', '::text) AS ds_pan
                                              FROM salve.ficha_acao_conservacao acoes
                                                       JOIN salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                                       JOIN salve.dados_apoio apoio_acao_situacao ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                                              WHERE acoes.sq_ficha = ficha.sq_ficha
                                              GROUP BY acoes.sq_ficha) pans ON TRUE
                          LEFT JOIN salve.ficha_ocorrencia_portalbio_reg freg ON freg.sq_ficha_ocorrencia_portalbio = oc.sq_ficha_ocorrencia_portalbio
                          LEFT JOIN salve.registro_rppn regrppn ON regrppn.sq_registro = freg.sq_registro
                          LEFT JOIN salve.unidade_conservacao rppn ON rppn.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.pessoa as rppn_pessoa on rppn_pessoa.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.registro_pais regpais ON regpais.sq_registro = freg.sq_registro
                          LEFT JOIN salve.pais pais ON pais.sq_pais = regpais.sq_pais
                          LEFT JOIN salve.registro_estado reguf ON reguf.sq_registro = freg.sq_registro
                          LEFT JOIN salve.estado estado ON estado.sq_estado = reguf.sq_estado
                          LEFT JOIN salve.registro_municipio regmun ON regmun.sq_registro = freg.sq_registro
                          LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = regmun.sq_municipio
                          LEFT JOIN salve.pessoa revisor ON revisor.sq_pessoa = oc.sq_pessoa_revisor
                          LEFT JOIN salve.pessoa validador ON validador.sq_pessoa = oc.sq_pessoa_validador
                          LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
                          LEFT JOIN salve.bioma bioma ON bioma.sq_bioma = regbio.sq_bioma
                          LEFT JOIN LATERAL ( SELECT JSON_OBJECT_AGG(ficha_ref_bib.sq_ficha_ref_bib, JSON_BUILD_OBJECT('de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica), 'no_autores_publicacao', REPLACE(publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano', ficha_ref_bib.nu_ano))::text AS json_ref_bibs
                                              FROM salve.ficha_ref_bib
                                                       LEFT JOIN taxonomia.publicacao ON publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                                              WHERE ficha_ref_bib.no_tabela::text = 'ficha_ocorrencia_portalbio'::text
                                                AND ficha_ref_bib.no_coluna::text = 'sq_ficha_ocorrencia_portalbio'::text
                                                AND ficha_ref_bib.sq_registro = oc.sq_ficha_ocorrencia_portalbio) refbibs ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(COALESCE(ucf_instituicao.sg_instituicao, ucf_pessoa.no_pessoa::character varying)), ', '::text) AS no_uc_federal
                                              FROM salve.registro_uc_federal x
                                                       JOIN salve.pessoa ucf_pessoa ON ucf_pessoa.sq_pessoa = x.sq_uc_federal
                                                       JOIN salve.unidade_conservacao ucf ON ucf.sq_pessoa = x.sq_uc_federal
                                                       JOIN salve.instituicao as ucf_instituicao on ucf_instituicao.sq_pessoa = x.sq_uc_federal
                                              WHERE x.sq_registro = freg.sq_registro) uc_federal ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(uce_pessoa.no_pessoa), ', '::text) AS no_uc_estadual
                                              FROM salve.registro_uc_estadual x
                                                       JOIN salve.unidade_conservacao uce ON uce.sq_pessoa = x.sq_uc_estadual
                                                       JOIN salve.pessoa as uce_pessoa on uce_pessoa.sq_pessoa = x.sq_uc_estadual
                                              WHERE x.sq_registro = freg.sq_registro) uc_estadual ON TRUE
             )
        select passo3.bo_salve::boolean
             ,passo3.id_ocorrencia::bigint
             ,passo3.sq_ciclo_avaliacao::bigint
             ,passo3.sq_ficha::bigint
             ,passo3.sq_taxon::bigint
             ,passo3.nm_cientifico::text
             ,passo3.sq_categoria_iucn::bigint
             ,passo3.sq_categoria_final::bigint
             ,passo3.no_autor_taxon::text
             ,passo3.no_reino::text
             ,passo3.no_filo::text
             ,passo3.no_classe::text
             ,passo3.no_ordem::text
             ,passo3.no_familia::text
             ,passo3.no_genero::text
             ,passo3.no_especie::text
             ,passo3.no_subespecie::text
             ,passo3.ds_categoria_anterior::text
             ,passo3.ds_categoria_avaliada::text
             ,passo3.ds_categoria_validada::text
             ,passo3.ds_pan::text
             ,passo3.nu_longitude::double precision
             ,passo3.nu_latitude::double precision
             ,passo3.nu_altitude::integer
             ,passo3.de_endemica_brasil::text
             ,passo3.ds_sensivel::text
             ,passo3.sq_situacao_avaliacao::bigint
             ,passo3.ds_situacao_avaliacao::text
             ,passo3.cd_situacao_avaliacao::text
             ,passo3.tx_justificativa_nao_usado_avaliacao::text
             ,passo3.ds_motivo_nao_utilizado::text
             ,passo3.dt_carencia::date
             ,passo3.ds_prazo_carencia::text
             ,passo3.no_datum::text
             ,passo3.no_formato_original::text
             ,passo3.no_precisao::text
             ,passo3.no_referencia_aproximacao::text
             ,passo3.no_metodologia_aproximacao::text
             ,passo3.tx_metodologia_aproximacao::text
             ,passo3.no_taxon_citado::text
             ,passo3.in_presenca_atual::text
             ,passo3.ds_presenca_atual::text
             ,passo3.no_tipo_registro::text
             ,passo3.nu_dia_registro::text
             ,passo3.nu_mes_registro::text
             ,passo3.nu_ano_registro::text
             ,passo3.hr_registro::text
             ,passo3.nu_dia_registro_fim::text
             ,passo3.nu_mes_registro_fim::text
             ,passo3.nu_ano_registro_fim::text
             ,passo3.ds_data_desconhecida::text
             ,passo3.no_localidade::text
             ,passo3.ds_caracteristica_localidade::text
             ,passo3.no_uc_federal::text
             ,passo3.no_uc_estadual::text
             ,passo3.no_rppn::text
             ,passo3.no_pais::text
             ,passo3.no_estado::text
             ,passo3.no_municipio::text
             ,passo3.no_bioma::text
             ,passo3.no_ambiente::text
             ,passo3.no_habitat::text
             ,passo3.vl_elevacao_min::numeric
             ,passo3.vl_elevacao_max::numeric
             ,passo3.vl_profundidade_min::numeric
             ,passo3.vl_profundidade_max::numeric
             ,passo3.de_identificador::text
             ,passo3.dt_identificacao::date
             ,passo3.no_identificacao_incerta::text
             ,passo3.ds_tombamento::text
             ,passo3.ds_instituicao_tombamento::text
             ,passo3.no_compilador::text
             ,passo3.dt_compilacao::date
             ,passo3.no_revisor::text
             ,passo3.dt_revisao::date
             ,passo3.no_validador::text
             ,passo3.dt_validacao::date
             ,passo3.in_sensivel::text
             ,passo3.no_base_dados::text
             ,passo3.id_origem::text
             ,passo3.nu_autorizacao::text
             ,passo3.tx_observacao::text
             ,passo3.tx_ref_bib::text
             ,passo3.no_autor_registro::text
             ,passo3.no_projeto::text
             ,passo3.ds_utilizado_avaliacao::text
             ,passo3.ds_situacao_registro::text
             ,passo3.in_utilizado_avaliacao::text
        from passo3;
end;
$$;

alter function salve.fn_ficha_registros(bigint) owner to postgres;

grant execute on function salve.fn_ficha_registros(bigint) to usr_salve;

create or replace function salve.fn_ficha_ucs(p_sq_ficha bigint)
    returns TABLE(sq_ficha bigint, sq_uc bigint, no_uc text, sg_uc text, cd_esfera text, in_presenca_atual text, de_presenca_atual text, sg_estado text, no_estado text, nu_ordem integer, co_nivel_taxonomico text, st_adicionado_apos_avaliacao boolean, st_utilizado_avaliacao boolean, st_em_carencia boolean, json_ref_bib text, co_cnuc text)
    language plpgsql
as
$$
begin
    return query
        with passo0 as (
            select ficha.sq_taxon,ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ), passo1 as (
            select ficha.sq_ficha, nivel_taxonomico.co_nivel_taxonomico
            from salve.ficha
                     inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                     inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
            where (ficha.sq_taxon = (select passo0.sq_taxon from passo0) or
                   taxon.sq_taxon_pai = (select passo0.sq_taxon from passo0))
              and ficha.sq_ciclo_avaliacao = (select passo0.sq_ciclo_avaliacao from passo0 )
        ), cte1 AS (
            WITH cte AS (
                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                         AS sq_uc,
                       instituicao.sg_instituicao           AS no_uc,
                       instituicao.sg_instituicao           AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'F'::text                            AS cd_esfera,
                       1                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia,
                       uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_uc_federal b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_federal
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_uc_federal
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                               AS sq_uc,
                       pessoa.no_pessoa                           AS no_uc,
                       null::text                                 AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'E'::text                            AS cd_esfera,
                       2                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia
                        ,uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_uc_estadual b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.pessoa ON pessoa.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                         AS sq_uc,
                       pessoa.no_pessoa                     AS no_uc,
                       null::text                           AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'R'::text                            AS cd_esfera,
                       3                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia
                        ,null::text as co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_rppn b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_rppn
                         JOIN salve.pessoa on pessoa.sq_pessoa = b.sq_rppn
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION

                -- terras indigenas pontos SALVE

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                              AS sq_uc,
                       pessoa.no_pessoa                          AS no_uc,
                       null::text                                AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'T'::text                            AS cd_esfera,
                       4                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia
                        ,NULL::TEXT AS co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_terra_indigena b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.pessoa ON pessoa.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION


                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa      AS sq_uc,
                       instituicao.sg_instituicao AS no_uc,
                       null              as sg_uc,
                       null              as in_presenca_atual,
                       ''                as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'F'::text         AS cd_esfera,
                       1                 AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_uc_federal b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_federal
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_uc_federal
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa               AS sq_uc,
                       instituicao.sg_instituicao AS no_uc,
                       null        as sg_uc,
                       null        as in_presenca_atual,
                       ''          as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'E'::text   AS cd_esfera,
                       2           AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_uc_estadual b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa AS sq_uc,
                       instituicao.sg_instituicao   AS no_uc,
                       null::text   AS sg_uc,
                       null         as in_presenca_atual,
                       ''           as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'R'::text    AS cd_esfera,
                       3            AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,null::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_rppn b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_rppn
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_rppn
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

                UNION

                -- TERRAS INDIGENAS PONTOS PORTALBIO
                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa AS sq_uc,
                       pessoa.no_pessoa AS no_uc,
                       null         as sg_uc,
                       null         as in_presenca_atual,
                       ''           as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'T'::text    AS cd_esfera,
                       4            AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,null::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_terra_indigena b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.pessoa ON pessoa.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')


            )
            SELECT cte.sq_ficha,
                   cte.sq_uc,
                   cte.no_uc,
                   cte.sg_uc,
                   cte.cd_esfera,
                   cte.in_presenca_atual,
                   cte.de_presenca_atual,
                   cte.no_estado,
                   cte.sg_estado,
                   cte.nu_ordem,
                   COALESCE(publicacao.de_titulo, ''::text)                  AS de_titulo,
                   COALESCE(publicacao.no_autor, x.no_autor::text)           AS no_autor,
                   COALESCE(publicacao.nu_ano_publicacao::integer, x.nu_ano) AS nu_ano,
                   cte.co_nivel_taxonomico,
                   cte.no_tabela,
                   cte.st_adicionado_apos_avaliacao,
                   cte.st_utilizado_avaliacao,
                   case when cte.dt_carencia is null or cte.dt_carencia < now() then false else true end as st_em_carencia,
                   cte.co_cnuc::text

            FROM cte
                     LEFT JOIN salve.ficha_ref_bib x
                               ON x.sq_ficha = cte.sq_ficha AND x.sq_registro = cte.sq_ficha_ocorrencia AND
                                  x.no_tabela::text = cte.no_tabela::text AND
                                  x.no_coluna::text = concat('sq_',cte.no_tabela)::text
                     LEFT JOIN taxonomia.publicacao ON publicacao.sq_publicacao = x.sq_publicacao
        ),
             passoFinal as (
                 SELECT distinct cte1.sq_ficha::bigint,
                                 cte1.sq_uc::bigint,
                                 cte1.no_uc::text,
                                 cte1.sg_uc::text,
                                 cte1.cd_esfera::text,
                                 cte1.in_presenca_atual::text,
                                 cte1.de_presenca_atual::text,
                                 cte1.sg_estado::text,
                                 cte1.no_estado::text,
                                 cte1.nu_ordem::integer,
                                 cte1.co_nivel_taxonomico,
                                 cte1.st_adicionado_apos_avaliacao,
                                 cte1.st_utilizado_avaliacao,
                                 cte1.st_em_carencia,
                                 json_object_agg(concat('rnd', trunc(random() * 100000::double precision)::text),
                                                 json_build_object('de_titulo', cte1.de_titulo, 'no_autor',
                                                                   cte1.no_autor, 'nu_ano',
                                                                   cte1.nu_ano))::text AS json_ref_bib,
                                 cte1.co_cnuc
                 FROM cte1

                 GROUP BY cte1.sq_ficha, cte1.nu_ordem, cte1.sq_uc, cte1.sg_uc, cte1.no_uc, cte1.cd_esfera, cte1.in_presenca_atual,
                          cte1.de_presenca_atual, cte1.sg_estado, cte1.no_estado, cte1.co_nivel_taxonomico,
                          cte1.st_adicionado_apos_avaliacao, cte1.st_utilizado_avaliacao, cte1.st_em_carencia,
                          cte1.co_cnuc
             )
        select *
        from passoFinal
        order by passoFinal.nu_ordem, public.fn_remove_acentuacao(upper(passoFinal.no_uc));

end;
$$;

alter function salve.fn_ficha_ucs(bigint) owner to postgres;

grant execute on function salve.fn_ficha_ucs(bigint) to usr_salve;

create or replace function salve.fn_info_registro_portalbio(p_sq_ficha_ocorrencia_portalbio bigint)
    returns TABLE(no_base_dados text, sq_ficha_ocorrencia_portalbio integer, sq_ficha bigint, sq_motivo_nao_utilizado_avaliacao bigint, nu_lat double precision, nu_lon double precision, no_local text, ds_precisao text, ds_autorizacao_sisbio text, no_pessoa_inclusao text, dt_carencia text, ds_situacao_avaliacao text, cd_situacao_avaliacao text, tx_justificativa_nao_utilizado text, dt_inclusao text, ds_uuid text, tx_ocorrencia text, tx_observacao text, nm_cientifico text, no_estado text, no_municipio text, ds_carencia text, ds_motivo_nao_utilizado text, in_subespecie text, json_ref_bib text, in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean, ds_utilizado_avaliacao text, in_centroide_estado boolean, ds_presenca_atual_coordenada text, in_presenca_atual text)
    language plpgsql
as
$$
declare existeRegistro bigint;
begin
    -- verificar se o id existe na nova tabela
    select sq_ficha_ocorrencia_portalbio_reg into existeRegistro from salve.ficha_ocorrencia_portalbio_reg where ficha_ocorrencia_portalbio_reg.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia_portalbio limit 1;
    if existeRegistro is not null then
        return query
            with cte as (
                SELECT salve.calc_base_dados_portalbio(concat(a.tx_ocorrencia,' ',a.no_instituicao),a.de_uuid) as no_base_dados
                     , a.sq_ficha_ocorrencia_portalbio
                     , a.sq_ficha
                     , a.sq_motivo_nao_utilizado_avaliacao
                     , st_y(a.ge_ocorrencia)                                                                           as nu_lat
                     , st_x(a.ge_ocorrencia)                                                                           as nu_lon
                     , a.no_local
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'precisaoCoord'::text)                                                                         as ds_precisao
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'recordNumber'::text)                                                                          as ds_autorizacao_sisbio
                     , a.no_autor                                                                                      as no_pessoa_inclusao
                     , to_char(a.dt_carencia, 'DD/MM/YYYY')                                                            as dt_carencia
                     , situacao_avaliacao.ds_dados_apoio                            as ds_situacao_avaliacao
                     , situacao_avaliacao.cd_sistema                                as cd_situacao_avaliacao
                     , coalesce(a.tx_nao_utilizado_avaliacao, '')                                                      as tx_justificativa_nao_utilizado
                     , to_char(a.dt_ocorrencia, 'DD/MM/YYYY')                                                          as dt_inclusao
                     , a.de_uuid                                                                                       as ds_uuid
                     , a.tx_ocorrencia
                     , a.tx_observacao
                     , ficha.nm_cientifico
                     , case
                           when a.sq_estado is null then
                               (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                               'estado'
                           else concat(estado.no_estado, ' (', estado.sg_estado, ')') end                              as no_estado
                     , case
                           when municipio.no_municipio is null then
                               (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                               'municipio'
                           else municipio.no_municipio end                                                             as no_municipio
                     , coalesce(motivo.ds_dados_apoio, '')                                                             as ds_motivo_nao_utilizado
                     , case when nivel.co_nivel_taxonomico = 'SUBESPECIE' then 'S' else 'N' end                        as in_subespecie
                     , ref_bib.json_ref_bib::text
                     , a.in_utilizado_avaliacao::text                                                                  as in_utilizado_avaliacao
                     , a.st_adicionado_apos_avaliacao::boolean                                                         as st_adicionado_apos_avaliacao
                     , false as in_centroide_estado
                     , a.in_presenca_atual
                from salve.ficha_ocorrencia_portalbio as a
                         inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                         inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = a.sq_situacao_avaliacao
                         left outer join salve.estado estado on estado.sq_estado = a.sq_estado
                         left outer join salve.municipio municipio on municipio.sq_municipio = a.sq_municipio
                         left outer join salve.dados_apoio as motivo on motivo.sq_dados_apoio = a.sq_motivo_nao_utilizado_avaliacao
                         left join lateral (
                    select json_object_agg(x.sq_ficha_ref_bib,
                                           json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                               , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                               , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
                    from salve.ficha_ref_bib x
                             left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                    where x.sq_ficha = a.sq_ficha
                      and x.sq_registro = a.sq_ficha_ocorrencia_portalbio
                      and x.no_tabela = 'ficha_ocorrencia_portalbio'
                      and x.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                    ) as ref_bib on true
                where a.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia_portalbio
            )
            select
                case when cte.no_base_dados ilike '%sisbio%' then 'ICMBIo/SISBio' else cte.no_base_dados end as no_base_dados,
                cte.sq_ficha_ocorrencia_portalbio,
                cte.sq_ficha,
                cte.sq_motivo_nao_utilizado_avaliacao,
                cte.nu_lat,
                cte.nu_lon,
                cte.no_local,
                cte.ds_precisao,
                cte.ds_autorizacao_sisbio,
                cte.no_pessoa_inclusao,
                cte.dt_carencia,
                cte.ds_situacao_avaliacao,
                cte.cd_situacao_avaliacao,
                cte.tx_justificativa_nao_utilizado,
                cte.dt_inclusao,
                cte.ds_uuid,
                cte.tx_ocorrencia,
                cte.tx_observacao,
                cte.nm_cientifico,
                cte.no_estado,
                cte.no_municipio,
                coalesce(cte.dt_carencia::text, 'Sem carência') as ds_carencia,
                cte.ds_motivo_nao_utilizado,
                cte.in_subespecie,
                cte.json_ref_bib,
                cte.in_utilizado_avaliacao,
                cte.st_adicionado_apos_avaliacao,
                salve.fn_calc_situacao_registro(cte.in_utilizado_avaliacao,cte.st_adicionado_apos_avaliacao) as ds_utilizado_avaliacao,
                cte.in_centroide_estado,
                salve.snd(cte.in_presenca_atual)                                               as ds_presenca_atual_coordenada,
                cte.in_presenca_atual::text
            from cte;
    else
        return query
            with cte as (
                SELECT  salve.calc_base_dados_portalbio(concat(a.tx_ocorrencia,' ',a.no_instituicao),a.de_uuid ) as no_base_dados
                     , a.sq_ficha_ocorrencia_portalbio
                     , a.sq_ficha
                     , a.sq_motivo_nao_utilizado_avaliacao
                     , st_y(a.ge_ocorrencia)                                                                           as nu_lat
                     , st_x(a.ge_ocorrencia)                                                                           as nu_lon
                     , a.no_local
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'precisaoCoord'::text)                                                                         as ds_precisao
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'recordNumber'::text)                                                                          as ds_autorizacao_sisbio
                     , a.no_autor                                                                                      as no_pessoa_inclusao
                     , to_char(a.dt_carencia, 'DD/MM/YYYY')                                                            as dt_carencia
                     , situacao_avaliacao.ds_dados_apoio                            as ds_situacao_avaliacao
                     , situacao_avaliacao.cd_sistema                                as cd_situacao_avaliacao
                     , coalesce(a.tx_nao_utilizado_avaliacao, '')                                                      as tx_justificativa_nao_utilizado
                     , to_char(a.dt_ocorrencia, 'DD/MM/YYYY')                                                          as dt_inclusao
                     , a.de_uuid                                                                                       as ds_uuid
                     , a.tx_ocorrencia
                     , a.tx_observacao
                     , ficha.nm_cientifico
                     , case
                           when a.sq_estado is null then
                               (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                               'estado'
                           else concat(estado.no_estado, ' (', estado.sg_estado, ')') end                              as no_estado
                     , case
                           when municipio.no_municipio is null then
                               (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                               'municipio'
                           else municipio.no_municipio end                                                             as no_municipio
                     , coalesce(motivo.ds_dados_apoio, '')                                                             as ds_motivo_nao_utilizado
                     , case when nivel.co_nivel_taxonomico = 'SUBESPECIE' then 'S' else 'N' end                        as in_subespecie
                     , ref_bib.json_ref_bib::text
                     , a.in_utilizado_avaliacao::text                                                                  as in_utilizado_avaliacao
                     , a.st_adicionado_apos_avaliacao::boolean                                                         as st_adicionado_apos_avaliacao
                     , false as in_centroide_estado
                     , a.in_presenca_atual
                from salve.ficha_ocorrencia_portalbio as a
                         inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                         inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = a.sq_situacao_avaliacao
                         left outer join salve.estado estado on estado.sq_estado = a.sq_estado
                         left outer join salve.municipio municipio on municipio.sq_municipio = a.sq_municipio
                         left outer join salve.dados_apoio as motivo on motivo.sq_dados_apoio = a.sq_motivo_nao_utilizado_avaliacao

                         left join lateral (
                    select json_object_agg(x.sq_ficha_ref_bib,
                                           json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                               , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                               , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
                    from salve.ficha_ref_bib x
                             left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                    where x.sq_ficha = a.sq_ficha
                      and x.sq_registro = a.sq_ficha_ocorrencia_portalbio
                      and x.no_tabela = 'ficha_ocorrencia_portalbio'
                      and x.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                    ) as ref_bib on true
                where a.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia_portalbio
            )
            select
                case when cte.no_base_dados ilike '%sisbio%' then 'SISBIO' else cte.no_base_dados end as no_base_dados,
                cte.sq_ficha_ocorrencia_portalbio,
                cte.sq_ficha,
                cte.sq_motivo_nao_utilizado_avaliacao,
                cte.nu_lat,
                cte.nu_lon,
                cte.no_local,
                cte.ds_precisao,
                cte.ds_autorizacao_sisbio,
                cte.no_pessoa_inclusao,
                cte.dt_carencia,
                cte.ds_situacao_avaliacao,
                cte.cd_situacao_avaliacao,
                cte.tx_justificativa_nao_utilizado,
                cte.dt_inclusao,
                cte.ds_uuid,
                cte.tx_ocorrencia,
                cte.tx_observacao,
                cte.nm_cientifico,
                cte.no_estado,
                cte.no_municipio,
                coalesce(cte.dt_carencia::text, 'Sem carência') as ds_carencia,
                cte.ds_motivo_nao_utilizado,
                cte.in_subespecie,
                cte.json_ref_bib,
                cte.in_utilizado_avaliacao,
                cte.st_adicionado_apos_avaliacao,
                salve.fn_calc_situacao_registro(cte.in_utilizado_avaliacao,cte.st_adicionado_apos_avaliacao) as ds_utilizado_avaliacao,
                cte.in_centroide_estado,
                salve.snd(cte.in_presenca_atual)                                               as ds_presenca_atual_coordenada,
                cte.in_presenca_atual::text

            from cte;

    end if;
end;
$$;

alter function salve.fn_info_registro_portalbio(bigint) owner to postgres;

grant execute on function salve.fn_info_registro_portalbio(bigint) to usr_salve;

create or replace function salve.fn_info_registro_salve(p_sq_ficha_ocorrencia bigint)
    returns TABLE(no_base_dados text, sq_ficha_ocorrencia bigint, sq_ficha bigint, sq_pessoa_inclusao bigint, sq_precisao_coordenada bigint, sq_prazo_carencia bigint, sq_motivo_nao_utilizado_avaliacao bigint, nu_lat double precision, nu_lon double precision, in_sensivel text, ds_presenca_atual_coordenada text, ds_tombamento text, no_instituicao_tombamento text, dt_carencia date, ds_situacao_avaliacao text, cd_situacao_avaliacao text, tx_justificativa_nao_utilizado text, ds_motivo_nao_utilizado text, dt_coleta text, hr_coleta text, dt_inclusao text, id_origem text, in_centroide_estado boolean, tx_observacao text, nm_cientifico text, in_subespecie text, no_estado text, no_municipio text, ds_carencia text, ds_sensivel text, no_usuario_consulta text, ds_tipo_consulta text, tx_observacao_usuario_web text, no_consulta text, sg_consulta text, ds_periodo text, no_pessoa_inclusao text, json_ref_bib text, ds_precisao text, in_utilizado_avaliacao text, ds_utilizado_avaliacao text, in_presenca_atual text)
    language plpgsql
as
$$
begin
    return query
        with cte2 as (
            select fo.sq_ficha_ocorrencia
            from salve.ficha_ocorrencia as fo
            where fo.sq_ficha_ocorrencia = p_sq_ficha_ocorrencia
        ),
             cte as (
                 SELECT fo.sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_pessoa_inclusao
                      , fo.sq_precisao_coordenada
                      , fo.sq_prazo_carencia
                      , fo.sq_motivo_nao_utilizado_avaliacao
                      , fo.sq_situacao_avaliacao
                      , st_y(fo.ge_ocorrencia)                                                        as nu_lat
                      , st_x(fo.ge_ocorrencia)                                                        as nu_lon
                      , upper(coalesce(fo.in_sensivel, 'S'))                                          as in_sensivel
                      , salve.snd(fo.in_presenca_atual)                                               as ds_presenca_atual_coordenada
                      , coalesce(fo.tx_tombamento, '')                                                as ds_tombamento
                      , coalesce(fo.tx_instituicao,'')                                                as no_instituicao_tombamento
                      , salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                      , coalesce(fo.tx_nao_utilizado_avaliacao, '')         as tx_justificativa_nao_utilizado
                      , case
                            when fo.nu_dia_registro is not null then
                                concat(lpad(fo.nu_dia_registro::text, 2, '0'), '/',
                                       lpad(fo.nu_mes_registro::text, 2, '0'), '/',
                                       fo.nu_ano_registro)
                            else case
                                     when fo.nu_mes_registro is not null then
                                         concat(lpad(fo.nu_mes_registro::text, 2, '0'), '/', fo.nu_ano_registro)
                                     else fo.nu_ano_registro::text end
                     end                                                                              as dt_coleta
                      , case
                            when fo.nu_dia_registro_fim is not null then
                                concat(lpad(fo.nu_dia_registro_fim::text, 2, '0'), '/',
                                       lpad(fo.nu_mes_registro_fim::text, 2, '0'), '/', fo.nu_ano_registro_fim)
                            else case
                                     when fo.nu_mes_registro_fim is not null then
                                         concat(lpad(fo.nu_mes_registro_fim::text, 2, '0'), '/', fo.nu_ano_registro_fim)
                                     else fo.nu_ano_registro_fim::text end
                     end                                                                              as dt_coleta_fim
                      , fo.hr_registro                                                                as hr_coleta
                      , to_char(fo.dt_inclusao, 'DD/MM/YYYY')                                         as dt_inclusao
                      , coalesce(fo.id_origem::text, concat('SALVE: ', fo.sq_ficha_ocorrencia::text)) as id_origem
                      , case
                            when coalesce(metodoAproximacao.cd_sistema, '') = 'CENTROIDE' and
                                 coalesce(refAproximacao.cd_sistema, '') = 'ESTADO' then true
                            else false end                                                            as in_centroide_estado
                      , fo.tx_observacao
                      , upper(case when fo.sq_contexto = 420 then
                                       coalesce( fo.in_utilizado_avaliacao, 'S') else
                                       coalesce( fo.in_utilizado_avaliacao ,'') end )
                                                                                                      as in_utilizado_avaliacao
                      ,fo.st_adicionado_apos_avaliacao
                      ,fo.in_presenca_atual
                 from salve.ficha_ocorrencia as fo
                          inner join cte2 on cte2.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                          left outer join salve.dados_apoio as prazo_carencia
                                          on prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                          left outer join salve.dados_apoio as metodoAproximacao
                                          on metodoAproximacao.sq_dados_apoio = fo.sq_metodo_aproximacao
                          left outer join salve.dados_apoio as refAproximacao
                                          on refAproximacao.sq_dados_apoio = fo.sq_ref_aproximacao
             )
        select case when consulta.sq_ficha_ocorrencia is null then 'ICMBio/SALVE' else 'ICMBio/SALVE/CONSULTA' end as no_base_dados
             , cte.sq_ficha_ocorrencia::bigint
             , cte.sq_ficha::bigint
             , cte.sq_pessoa_inclusao::bigint
             , cte.sq_precisao_coordenada::bigint
             , cte.sq_prazo_carencia::bigint
             , cte.sq_motivo_nao_utilizado_avaliacao::bigint
             , cte.nu_lat::double precision
             , cte.nu_lon::double precision
             , cte.in_sensivel::text
             , cte.ds_presenca_atual_coordenada::text
             , cte.ds_tombamento::text
             , cte.no_instituicao_tombamento::text
             , cte.dt_carencia::date
             , situacao_avaliacao.ds_dados_apoio::text                   as ds_situacao_avaliacao
             , situacao_avaliacao.cd_sistema::text                       as cd_situacao_avaliacao
             , cte.tx_justificativa_nao_utilizado::text                  as tx_justificativa_nao_utilizado
             , coalesce(motivo.ds_dados_apoio, '')::text                 as ds_motivo_nao_utilizado
             , case
                   when cte.dt_coleta is not null and cte.dt_coleta_fim is not null
                       then concat(cte.dt_coleta, ' a ', cte.dt_coleta_fim)
                   else
                       case when cte.dt_coleta is not null then cte.dt_coleta else '' end::text
            end                                                                          as dt_coleta
             , cte.hr_coleta::text
             , cte.dt_inclusao::text
             , cte.id_origem::text
             , cte.in_centroide_estado::boolean
             , cte.tx_observacao::text
             , ficha.nm_cientifico::text
             , case when nivel.co_nivel_taxonomico = 'SUBESPECIE' then 'S' else 'N' end::text  as in_subespecie
             , case
                   when regUf.sq_estado is null then ''
                   else concat(estado.no_estado, ' (', estado.sg_estado, ')') end::text        as no_estado
             , case when cte.in_centroide_estado then '' else municipio.no_municipio end::text as no_municipio
             , case when cte.dt_carencia is not null then to_char(cte.dt_carencia,'DD/MM/YYYY')  else 'Sem carência' end as ds_carencia
             , case when cte.in_sensivel = 'S' then 'Sim' else 'Não' end::text                 as ds_sensivel
             , usuario_pessoa.no_pessoa::text                                                        as no_usuario_consulta
             , tipo_consulta.ds_dados_apoio::text                                              as ds_tipo_consulta
             , consulta.tx_observacao::text                                                    as tx_observacao_usuario_web
             , ciclo_consulta.de_titulo_consulta::text                                         as no_consulta
             , ciclo_consulta.sg_consulta::text                                                as sg_consulta
             , case
                   when ciclo_consulta.dt_inicio is not null then
                       concat(to_char(ciclo_consulta.dt_inicio, 'DD/MM/YYYY'), ' a ',
                              to_char(ciclo_consulta.dt_fim, 'DD/MM/YYYY'))
                   else '' end::text                                                           as ds_periodo
             , pf_inclusao.no_pessoa::text                                                              as no_pessoa_inclusao
             , ref_bib.json_ref_bib::text                                                as json_ref_bib
             , coalesce(precisao.ds_dados_apoio, '')::text                                     as ds_precisao
             , cte.in_utilizado_avaliacao
             , salve.fn_calc_situacao_registro(cte.in_utilizado_avaliacao,cte.st_adicionado_apos_avaliacao) as ds_utilizado_avaliacao
             , cte.in_presenca_atual::text

        from cte
                 inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = cte.sq_situacao_avaliacao
                 left outer join salve.ficha_ocorrencia_consulta as consulta on consulta.sq_ficha_ocorrencia = cte.sq_ficha_ocorrencia
                 left outer join salve.usuario as usuario on usuario.sq_pessoa = consulta.sq_web_usuario
                 left outer join salve.pessoa as usuario_pessoa on usuario_pessoa.sq_pessoa = usuario.sq_pessoa
                 left outer join salve.ciclo_consulta_ficha as ciclo_consulta_ficha
                                 on ciclo_consulta_ficha.sq_ciclo_consulta_ficha = consulta.sq_ciclo_consulta_ficha
                 left outer join salve.ciclo_consulta as ciclo_consulta
                                 on ciclo_consulta.sq_ciclo_consulta = ciclo_consulta_ficha.sq_ciclo_consulta
                 left outer join salve.dados_apoio as tipo_consulta
                                 on tipo_consulta.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                 left join salve.ficha_ocorrencia_registro freg on freg.sq_ficha_ocorrencia = cte.sq_ficha_ocorrencia
                 left join salve.registro_estado regUf on regUf.sq_registro = freg.sq_registro
                 left join salve.registro_municipio regMu on regMu.sq_registro = freg.sq_registro
                 left outer join salve.estado estado on estado.sq_estado = regUf.sq_estado
                 left outer join salve.municipio municipio on municipio.sq_municipio = regMu.sq_municipio
                 inner join salve.ficha on ficha.sq_ficha = cte.sq_ficha
                 inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                 left outer join salve.pessoa_fisica as pf on pf.sq_pessoa = cte.sq_pessoa_inclusao
                 left outer join salve.pessoa as pf_inclusao on pf_inclusao.sq_pessoa = cte.sq_pessoa_inclusao
                 left outer join salve.dados_apoio as precisao on precisao.sq_dados_apoio = cte.sq_precisao_coordenada
                 left outer join salve.dados_apoio as motivo   on motivo.sq_dados_apoio = cte.sq_motivo_nao_utilizado_avaliacao

                 left join lateral (
            select json_object_agg(x.sq_ficha_ref_bib,
                                   json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                       , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                       , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
            from salve.ficha_ref_bib x
                     left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
            where x.sq_ficha = cte.sq_ficha
              and x.sq_registro = cte.sq_ficha_ocorrencia
              and x.no_tabela = 'ficha_ocorrencia'
              and x.no_coluna = 'sq_ficha_ocorrencia'
            ) as ref_bib on true;
end;
$$;

alter function salve.fn_info_registro_salve(bigint) owner to postgres;

grant execute on function salve.fn_info_registro_salve(bigint) to usr_salve;

create or replace function salve.fn_taxon_bacias(p_sq_taxon bigint)
    returns TABLE(no_contexto text, no_origem text, no_bacia text, cd_bacia text, cd_sistema text, sq_bacia bigint, tx_trilha text, de_bacia_ordem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bacia bigint, sq_ficha_ref_bib bigint, sq_publicacao bigint, de_ref_bib text, de_titulo text, no_autor text, nu_ano integer, co_nivel_taxonomico text, dt_carencia date, st_em_carencia boolean)
    language plpgsql
as
$$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_taxon = p_sq_taxon
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
                        -- bioma marinho não possui pontos em Estados
                        where passo1.st_marinho = false
             )
                ,
             bacias_ficha as (select 'ficha'::text                                              as no_contexto,
                                     'salve'::text                                              as no_origem,
                                     bacia.descricao::text                                      as no_bacia,
                                     bacia.codigo::text                                         as cd_bacia,
                                     bacia.codigo_sistema::text                                 as cd_sistema,
                                     bacia.id::bigint                                           as sq_bacia,
                                     bacia.trilha::text                                         as tx_trilha,
                                     bacia.ordem::text                                          as de_bacia_ordem,

                                     -- situacao avaliacao
                                     true::boolean                                               as st_utilizado_avalicao,
                                     false::boolean                                              as st_adicionado_apos_avaliacao,

                                     -- ids
                                     fb.sq_ficha_bacia::bigint                                  as sq_ficha_bacia

                                     -- ref bib
                                      , frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib
                                      , publicacao.sq_publicacao::bigint                         as sq_publicacao
                                      , publicacao.de_ref_bibliografica::text                    as de_ref_bib
                                      , publicacao.de_titulo::text                               as de_titulo
                                      , coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor
                                      , coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano
                                     -- complemento
                                      , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                      , null::date as dt_carencia

                              from salve.ficha_bacia fb
                                       inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                                       inner join salve.vw_bacia_hidrografica bacia on bacia.id = fb.sq_bacia
                                       left outer join salve.ficha_ref_bib frb on frb.sq_ficha = fb.sq_ficha
                                  and frb.no_coluna = 'sq_ficha_bacia' and frb.no_tabela = 'ficha_bacia'
                                  and frb.sq_registro = fb.sq_ficha_bacia
                                       left outer join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao

             ), bacias_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha

                and frb.no_tabela = 'ficha_ocorrencia'
                and frb.no_coluna = 'sq_ficha_ocorrencia'
                and frb.sq_registro = fo.sq_ficha_ocorrencia
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- não considerar registros que forem centroide do Estado
              and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR ref_aprox.cd_sistema <> 'ESTADO')

        ), bacias_ocorrencias_portalbio as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
        )
        select a.*, false as st_em_carencia from bacias_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_portalbio as c;
end;
$$;

alter function salve.fn_taxon_bacias(bigint) owner to postgres;

grant execute on function salve.fn_taxon_bacias(bigint) to usr_salve;

create or replace function salve.fn_taxon_biomas(p_sq_taxon bigint)
    returns TABLE(no_bioma text, no_contexto text, no_origem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bioma bigint, sq_bioma_salve bigint, sq_bioma_corp bigint, sq_ficha_ref_bib bigint, sq_publicacao bigint, de_ref_bib text, de_titulo text, no_autor text, nu_ano integer, co_nivel_taxonomico text, dt_carencia date, st_em_carencia boolean)
    language plpgsql
as
$$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_taxon = p_sq_taxon
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             biomas_ficha as (select bioma_salve.ds_dados_apoio::text                              as no_bioma
                                   , 'ficha'::text                                            as no_contexto
                                   , 'salve'::text                                            as no_origem
                                   -- situacao avaliacao
                                   , true::boolean                                               as st_utilizado_avalicao
                                   , false::boolean                                              as st_adicionado_apos_avaliacao
                                   -- ids
                                   , fb.sq_ficha_bioma::bigint                                  as sq_ficha_bioma
                                   , bioma_salve.sq_dados_apoio::bigint                         as sq_bioma_salve
                                   , bioma_corp.sq_bioma::bigint                               as sq_bioma_corp
                                   -- ref bib
                                   , frb.sq_ficha_ref_bib::bigint                               as sq_ficha_ref_bib
                                   , publicacao.sq_publicacao::bigint                           as sq_publicacao
                                   , publicacao.de_ref_bibliografica::text                   as de_ref_bib
                                   , publicacao.de_titulo::text                               as de_titulo
                                   , coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor
                                   , coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano
                                   -- complemento
                                   , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                   , null::date as dt_carencia
                              from salve.ficha_bioma fb
                                       inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                                       inner join salve.dados_apoio as bioma_salve on bioma_salve.sq_dados_apoio = fb.sq_bioma
                                       left outer join salve.ficha_ref_bib frb on frb.sq_ficha = fb.sq_ficha
                                  and frb.no_coluna = 'sq_ficha_bioma' and frb.no_tabela = 'ficha_bioma' and frb.sq_registro = fb.sq_ficha_bioma
                                       left outer join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
                                       left outer join salve.bioma as bioma_corp on bioma_corp.no_bioma = bioma_salve.ds_dados_apoio
             ), biomas_ocorrencias_salve as (


            select bioma_corp.no_bioma                                                     as no_bioma,
                   'ocorrencia'                                                            as no_contexto,
                   'salve'::text                                                           as no_origem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                   -- ids
                   null::bigint                                                            as sq_ficha_bioma,
                   bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                   bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                   -- ref bib
                   frb.sq_ficha_ref_bib                                                    as sq_ficha_ref_bib,
                   publicacao.sq_publicacao                                                as sq_publicacao,
                   publicacao.de_ref_bibliografica                                         as de_ref_bib,
                   publicacao.de_titulo                                                    as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)                             as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)                      as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                     inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia'
                and frb.no_coluna = 'sq_ficha_ocorrencia'
                and frb.sq_registro = fo.sq_ficha_ocorrencia
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              -- não considerar registros que forem centroide do Estado
              and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR
                   ref_aprox.cd_sistema <> 'ESTADO')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- se nao for do grupo marinho não exibir o bioma marinho
              and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )

        )
                , biomas_ocorrencias_portalbio as (
            select bioma_corp.no_bioma                                                     as no_bioma,
                   'ocorrencia'                                                            as no_contexto,
                   'portalbio'::text                                                       as no_origem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                   -- ids
                   null::bigint                                                            as sq_ficha_bioma,
                   bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                   bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                   -- ref bib
                   frb.sq_ficha_ref_bib                                                    as sq_ficha_ref_bib,
                   publicacao.sq_publicacao                                                as sq_publicacao,
                   publicacao.de_ref_bibliografica                                         as de_ref_bib,
                   publicacao.de_titulo                                                    as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)                             as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)                      as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                     inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- se nao for do grupo marinho não exibir o bioma marinho
              and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )
        )
        select a.*, false as st_em_carencia from biomas_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_portalbio as c;
end;
$$;

alter function salve.fn_taxon_biomas(bigint) owner to postgres;

grant execute on function salve.fn_taxon_biomas(bigint) to usr_salve;

create or replace function salve.fn_taxon_estados(p_sq_taxon bigint)
    returns TABLE(no_contexto text, no_origem text, sq_estado bigint, no_estado text, sg_estado text, sq_regiao bigint, no_regiao text, nu_ordem_regiao integer, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_uf bigint, sq_ficha_ref_bib bigint, sq_publicacao bigint, de_ref_bib text, de_titulo text, no_autor text, nu_ano integer, co_nivel_taxonomico text, dt_carencia date, st_em_carencia boolean)
    language plpgsql
as
$$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_taxon = p_sq_taxon
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             estados_ficha as (select 'ficha'::text                                             as no_contexto,
                                   'salve'::text                                             as no_origem,
                                   uf.sq_estado::bigint                                      as sq_estado,
                                   uf.no_estado::text                                        as no_estado,
                                   uf.sg_estado::text                                        as no_estado,
                                   regiao.sq_regiao::bigint                                  as sq_regiao,
                                   regiao.no_regiao::text                                    as no_regiao,
                                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                                       end end end end end::integer                                      as nu_ordem_regiao
                                    -- situacao avaliacao
                                    , true::boolean                                               as st_utilizado_avalicao
                                    , false::boolean                                              as st_adicionado_apos_avaliacao

                                    -- ids
                                    , fu.sq_ficha_uf::bigint                                  as sq_ficha_uf

                                    -- ref bib
                                    , frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib
                                    , publicacao.sq_publicacao::bigint                         as sq_publicacao
                                    , publicacao.de_ref_bibliografica::text                    as de_ref_bib
                                    , publicacao.de_titulo::text                               as de_titulo
                                    , coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor
                                    , coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano
                                    -- complemento
                                    , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                    , null::date as dt_carencia
                               from salve.ficha_uf fu
                                        inner join passo2 on passo2.sq_ficha = fu.sq_ficha
                                        inner join salve.estado as uf on uf.sq_estado = fu.sq_estado
                                        left outer join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                                        left outer join salve.ficha_ref_bib frb on frb.sq_ficha = fu.sq_ficha
                                   and frb.no_coluna = 'sq_ficha_uf' and frb.no_tabela = 'ficha_uf'
                                   and frb.sq_registro = fu.sq_ficha_uf
                                        left outer join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao

             ), estados_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as sg_estado,
                   regiao.sq_regiao::bigint                                  as sq_regiao,
                   regiao.no_regiao::text                                    as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia'
                and frb.no_coluna = 'sq_ficha_ocorrencia'
                and frb.sq_registro = fo.sq_ficha_ocorrencia
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false


        ), estados_ocorrencias_portalbio as (

            select 'ocorrencia'                                                            as no_contexto,
                   'salve'::text                                                           as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as no_estado,
                   regiao.sq_regiao                                          as sq_regiao,
                   regiao.no_regiao                                          as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false
        )
        select a.*, false as st_em_carencia from estados_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_portalbio as c;
end;
$$;

alter function salve.fn_taxon_estados(bigint) owner to postgres;

grant execute on function salve.fn_taxon_estados(bigint) to usr_salve;

create or replace function salve.fn_traducao_calcular_percentuais(p_sq_fichas text)
    returns TABLE(sq_ficha bigint, nu_percentual_traduzido integer, nu_percentual_revisado integer)
    language plpgsql
as
$$
    /*
      funcao para calcular o percentual traduzido e revisado de uma ou da lista de fichas
      Testar: select * from salve.fn_traducao_calcular_percentuais('19160,35824');
    */
declare
    contaColunas text;
    cmdSql text;
begin
    SET client_min_messages TO WARNING;

    -- cada coluna traduzivel da ficha vale 1 se estiver preenchida
    select  array_to_string( array_agg( distinct concat('case when ficha.',tt.no_coluna,' is null then 0 else 1 end') ),' + ' ) into contaColunas
    from salve.traducao_tabela tt
    where tt.no_tabela = 'ficha'
      and tt.no_coluna like 'ds_%';
    /*
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE 'Fichas %',p_sq_fichas;
    RAISE NOTICE 'Colunas %',contaColunas;
    */
    cmdSql = 'with passo1 as (
                    select ficha.sq_ficha
                         , 11 as nu_colunas_preenchidas
                         , coalesce(andamento.qtd_revisado,0) as qtd_revisado
                         , coalesce(andamento.qtd_traduzido,0) as qtd_traduzido
                    from salve.ficha
                        inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                        left join lateral (
                        select sum(case when tr.tx_traduzido is not null then 1 else 0 end) as qtd_traduzido
                             , sum(case when tr.dt_revisao is null then 0 else 1 end)         as qtd_revisado
                        from salve.traducao_revisao tr
                                 inner join salve.traducao_tabela tt on tt.sq_traducao_tabela = tr.sq_traducao_tabela
                        where tr.sq_registro = ficha.sq_ficha
                          and tt.no_tabela = ''ficha''
                          and tt.no_coluna like ''ds_%''
                        ) as andamento on true
                        where
                         situacao.cd_sistema in (''PUBLICADA'',''FINALIZADA'')
                         and ficha.sq_ficha = any( array['||p_sq_fichas||'])
                    ), passo2 as (
                        select ficha.nm_cientifico
                             , passo1.sq_ficha
                             , (case when nu_colunas_preenchidas = 0 then 1 else nu_colunas_preenchidas end)::decimal as nu_colunas_preenchidas
                             , qtd_traduzido::decimal
                             , qtd_revisado::decimal
                        from passo1
                                 inner join salve.ficha on ficha.sq_ficha = passo1.sq_ficha
                    )
                    select passo2.sq_ficha::bigint
                          , least(100,(passo2.qtd_traduzido / nu_colunas_preenchidas * 100)::int) as percentual_traduzido
                          , least(case when passo2.qtd_traduzido=0 then 0 else passo2.qtd_revisado / passo2.qtd_traduzido * 100 end::int) as percentual_revisado
                          from passo2;';

    return QUERY EXECUTE cmdSql;
end
$$;

comment on function salve.fn_traducao_calcular_percentuais(text) is 'funcao para calcular o percentual traduzido e revisado de uma ou varias fichas';

alter function salve.fn_traducao_calcular_percentuais(text) owner to postgres;

create or replace function salve.remover_html_tags(text) returns text
    language sql
as
$$
SELECT regexp_replace(regexp_replace($1, E'<.*?>', '', 'g' ), E'&nbsp;', ' ', 'g')
$$;

comment on function salve.remover_html_tags(text) is 'Função para remover as tags HTML do texto.';

alter function salve.remover_html_tags(text) owner to postgres;

grant execute on function salve.remover_html_tags(text) to usr_salve;

create or replace function salve.snd(text) returns text
    language sql
as
$$
SELECT case when $1='S' then 'Sim' else
    case when $1='N' then 'Não' else
        case when $1='D' then 'Desconhecido' else '' end
        end
           end
$$;

comment on function salve.snd(text) is 'Função retornar os valores S,N ou D em Sim, Não ou Desconhecido.';

alter function salve.snd(text) owner to postgres;

grant execute on function salve.snd(text) to usr_salve;

create or replace function taxonomia.criar_trilha(v_sq_taxon bigint) returns json
    language plpgsql
as
$$
DECLARE
    v_trilha json;
BEGIN
    WITH RECURSIVE subir_arvore_taxon(
                                      nivel,
                                      key,
                                      ds_caminho,
                                      sq_taxon,
                                      in_ocorre_brasil,
                                      sq_nivel_taxonomico,
                                      no_autor, no_taxon,
                                      nu_ano,
                                      sq_situacao_nome,
                                      st_ativo,
                                      sq_taxon_pai)
                       AS (
            SELECT 1::integer as nivel, taxon.sq_taxon as key, taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho, taxon.sq_taxon, taxon.in_ocorre_brasil,
                   taxon.sq_nivel_taxonomico, taxon.no_autor,  taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon
                    , taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
            FROM taxonomia.taxon
            where taxon.sq_taxon  = v_sq_taxon
            UNION
            SELECT t.nivel+1::integer as nivel, taxon.sq_taxon as key,
                   (t.ds_caminho::text) ||' < ' ||taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
                   taxon.sq_taxon, taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor , taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon ,
                   taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
            FROM taxonomia.taxon
                     JOIN  subir_arvore_taxon t  on taxon.sq_taxon = t.sq_taxon_pai
        )
    SELECT json_object_agg(lower(nivel_taxonomico.co_nivel_taxonomico), json_build_object(
            'sq_taxon',  t.sq_taxon,
            'no_taxon', t.no_taxon,
            'sq_taxon_pai', t.sq_taxon_pai,
            'st_ativo', t.st_ativo,
            'in_ocorre_brasil', t.in_ocorre_brasil,
            'sq_nivel_taxonomico', t.sq_nivel_taxonomico,
            'de_nivel_taxonomico', nivel_taxonomico.de_nivel_taxonomico,
            'nu_grau_taxonomico', nivel_taxonomico.nu_grau_taxonomico,
            'in_nivel_intermediario', nivel_taxonomico.in_nivel_intermediario,
            'co_nivel_taxonomico', nivel_taxonomico.co_nivel_taxonomico
                                                                        )
           )

               as trilha into v_trilha
    FROM subir_arvore_taxon t
             inner join taxonomia.nivel_taxonomico
                        on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico;

    return v_trilha;
END;
$$;

alter function taxonomia.criar_trilha(bigint) owner to postgres;

grant execute on function taxonomia.criar_trilha(bigint) to usr_salve;

create or replace function taxonomia.criar_trilha2(v_sq_taxon bigint) returns json
    language plpgsql
as
$$
DECLARE
    v_trilha json;
BEGIN
    select array_to_json(array_agg(row_to_json(tb1))) into v_trilha
    from (
             WITH RECURSIVE subir_arvore_taxon(
                                               nivel,
                                               key,
                                               ds_caminho,
                                               sq_taxon,
                                               in_ocorre_brasil,
                                               sq_nivel_taxonomico,
                                               no_autor, no_taxon,
                                               nu_ano,
                                               sq_situacao_nome,
                                               st_ativo,
                                               sq_taxon_pai,
                                               sq_taxon_alvo,
                                               nome_cientifico)
                                AS (
                     SELECT 1::integer                                    as nivel,
                            taxon.sq_taxon                                as key,
                            taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
                            taxon.sq_taxon,
                            taxon.in_ocorre_brasil,
                            taxon.sq_nivel_taxonomico,
                            taxon.no_autor,
                            taxonomia.formatar_nome(taxon.sq_taxon)       as no_taxon,
                            taxon.nu_ano,
                            taxon.sq_situacao_nome,
                            taxon.st_ativo,
                            taxon.sq_taxon_pai,
                            sq_taxon                                      as sq_taxon_alvo,
                            taxonomia.formatar_nome(sq_taxon)             as nome_cientifico
                     FROM taxonomia.taxon
                     where taxon.sq_taxon = v_sq_taxon
                     UNION
                     SELECT t.nivel + 1::integer                                                           as nivel,
                            taxon.sq_taxon                                                                 as key,
                            (t.ds_caminho::text) || ' < ' ||
                            taxonomia.formatar_nome(taxon.sq_taxon)::text                                  as ds_caminho,
                            taxon.sq_taxon,
                            taxon.in_ocorre_brasil,
                            taxon.sq_nivel_taxonomico,
                            taxon.no_autor,
                            taxonomia.formatar_nome(taxon.sq_taxon)                                        as no_taxon,
                            taxon.nu_ano,
                            taxon.sq_situacao_nome,
                            taxon.st_ativo,
                            taxon.sq_taxon_pai,
                            t.sq_taxon_alvo,
                            taxonomia.formatar_nome(taxon.sq_taxon)                                        as nome_cientifico
                     FROM taxonomia.taxon
                              JOIN subir_arvore_taxon t on taxon.sq_taxon = t.sq_taxon_pai
                 )
             SELECT t.nivel as nivel_recursivo,
                    t.key,
                    t.ds_caminho,
                    t.sq_taxon,
                    t.in_ocorre_brasil,
                    t.sq_nivel_taxonomico,
                    t.no_autor,
                    no_taxon,
                    t.nu_ano,
                    t.sq_situacao_nome,
                    t.st_ativo,
                    t.sq_taxon_pai,
                    t.sq_taxon_alvo,
                    t.nome_cientifico,
                    nivel_taxonomico.de_nivel_taxonomico,
                    nivel_taxonomico.nu_grau_taxonomico,
                    nivel_taxonomico.in_nivel_intermediario,
                    nivel_taxonomico.co_nivel_taxonomico
             FROM subir_arvore_taxon t
                      inner join taxonomia.nivel_taxonomico
                                 on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico
         ) tb1;


    return v_trilha;
END;
$$;

alter function taxonomia.criar_trilha2(bigint) owner to postgres;

grant execute on function taxonomia.criar_trilha2(bigint) to usr_salve;

create or replace function taxonomia.formatar_nome(v_sq_taxon bigint) returns text
    language plpgsql
as
$$
DECLARE
    v_nome_cientifico text := '';
BEGIN
    WITH RECURSIVE nome_cientifico(sq_taxon_pai,
                                   nivel,
                                   formatacao_nome,
                                   sq_taxon,
                                   sq_taxon_search,
                                   no_taxon,
                                   in_ocorre_brasil,
                                   sq_nivel_taxonomico,
                                   no_autor,
                                   nu_ano,
                                   sq_situacao_nome,
                                   st_ativo,
                                   co_nivel_taxonomico,
                                   de_nivel_taxonomico,
                                   in_nivel_intermediario,
                                   nu_grau_taxonomico
        )
                       AS (
            SELECT taxon.sq_taxon_pai, 1::integer as nivel,  taxon.no_taxon::text as formatacao_nome,
                   taxon.sq_taxon,
                   taxon.sq_taxon as sq_taxon_search,
                   taxon.no_taxon,
                   taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor,
                   taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo,
                   nivel_taxonomico.co_nivel_taxonomico,
                   nivel_taxonomico.de_nivel_taxonomico,
                   nivel_taxonomico.in_nivel_intermediario,
                   nivel_taxonomico.nu_grau_taxonomico
            FROM taxonomia.taxon
                     INNER JOIN taxonomia.nivel_taxonomico ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
            WHERE taxon.sq_taxon = v_sq_taxon
            UNION
            SELECT  taxon.sq_taxon_pai,
                    t.nivel+1::integer as nivel,
                    taxon.no_taxon::text  ||' ' || t.formatacao_nome as formatacao_nome,
                    taxon.sq_taxon,
                    t.sq_taxon_search,
                    taxon.no_taxon as no_taxon,
                    taxon.in_ocorre_brasil,
                    taxon.sq_nivel_taxonomico,
                    taxon.no_autor ,
                    taxon.nu_ano,
                    taxon.sq_situacao_nome,
                    taxon.st_ativo,
                    nivel_taxonomico.co_nivel_taxonomico,
                    nivel_taxonomico.de_nivel_taxonomico,
                    nivel_taxonomico.in_nivel_intermediario,
                    nivel_taxonomico.nu_grau_taxonomico
            FROM taxonomia.taxon
                     INNER JOIN taxonomia.nivel_taxonomico
                                ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                    AND nivel_taxonomico.nu_grau_taxonomico in (100,90,80)
                     INNER JOIN  nome_cientifico t on taxon.sq_taxon = t.sq_taxon_pai
        )
    SELECT t.formatacao_nome into v_nome_cientifico
    FROM nome_cientifico t
    WHERE t.nivel = (select max(t2.nivel) from nome_cientifico t2 Where t2.sq_taxon_search =t.sq_taxon_search);
    return v_nome_cientifico;
END;
$$;

alter function taxonomia.formatar_nome(bigint) owner to postgres;

grant execute on function taxonomia.formatar_nome(bigint) to usr_salve;

create or replace function salve.fn_ficha2json(p_sq_ficha bigint) returns json
    language plpgsql
as
$$begin
    return  (
        select json_build_object(
                       'sq_ficha', ficha.sq_ficha
                   , '_id', ficha.sq_ficha
                   , 'ds_doi',ficha.ds_doi
                   , 'sq_unidade_responsavel',ficha.sq_unidade_org
                   , 'sg_unidade_responsavel',uo.sg_unidade_org
                   , 'no_unidade_responsavel',uo.no_pessoa
                   , 'nm_cientifico',ficha.nm_cientifico
                   , 'taxon_trilha', taxon.json_trilha
                   , 'no_autor_taxon',taxon.no_autor
                   , 'nu_ano_taxon',taxon.nu_ano
                   , 'st_manter_lc',ficha.st_manter_lc
                   , 'dt_aceite_validacao',ficha.dt_aceite_validacao
                   -- aba 1
                   , 'classificacao_taxonomica', json_build_object('nomes_comuns',
                                                                   (select json_agg(json_build_object(
                                                                           'no_comum', ficha_nome_comum.no_comum
                                                                       ,'de_regiao_lingua',salve.ficha_nome_comum.de_regiao_lingua
                                                                       , 'referencias', salve.fn_ref_bib_json(
                                                                                   ficha_nome_comum.sq_ficha,
                                                                                   ficha_nome_comum.sq_ficha_nome_comum)
                                                                                    ))
                                                                    from salve.ficha_nome_comum
                                                                    where ficha_nome_comum.sq_ficha = ficha.sq_ficha
                                                                   )
                           -- sinonimias
                           , 'nomes_antigos',
                                                                   (select json_agg(json_build_object('no_antigo',
                                                                                                      sinonimia.no_sinonimia
                                                                       , 'no_autor', sinonimia.no_autor
                                                                       , 'nu_ano', sinonimia.nu_ano)) as json_sinonimia
                                                                    from salve.ficha_sinonimia as sinonimia
                                                                    where sinonimia.sq_ficha = ficha.sq_ficha)
                           , 'notas_taxonomicas', salve.entity2char(ficha.ds_notas_taxonomicas)
                           , 'notas_morfologicas', salve.entity2char(ficha.ds_diagnostico_morfologico)
                           , 'grupo',
                                                                   json_build_object('sq_grupo', grupo.sq_dados_apoio,
                                                                                     'ds_grupo', grupo.ds_dados_apoio,
                                                                                     'de_grupo_ordem', grupo.de_dados_apoio_ordem,
                                                                                     'cd_grupo_sistema', grupo.cd_sistema)
                           , 'subgrupo', json_build_object('sq_subgrupo', subgrupo.sq_dados_apoio, 'ds_subgrupo',
                                                           subgrupo.ds_dados_apoio)
                           , 'recorte_publico', json_build_object('sq_recorte_publico', grupo_recorte.sq_dados_apoio,
                                                                  'ds_recorte_publico', grupo_recorte.ds_dados_apoio,
                                                                  'de_recorte_publico_ordem', grupo_recorte.de_dados_apoio_ordem,
                                                                  'cd_recorte_publico_sistema', grupo_recorte.cd_sistema)
                                                 )
                   -- aba 2

                   ,'distribuicao',
                       json_build_object('st_endemica_brasil', ficha.st_endemica_brasil
                           , 'ds_distribuicao_geo_global', salve.entity2char(ficha.ds_distribuicao_geo_global)
                           , 'ds_distribuicao_geo_nacional', salve.entity2char(ficha.ds_distribuicao_geo_nacional)
                           , 'nu_min_altitude', ficha.nu_min_altitude::text
                           , 'nu_max_altitude', ficha.nu_max_altitude::text
                           , 'nu_min_batimetria', ficha.nu_min_batimetria::text
                           , 'nu_max_batimetria', ficha.nu_max_batimetria::text
                           -- mapas distribuicao
                           , 'mapas_distribuicao', (select json_agg(json_build_object('de_legenda', anexo.de_legenda
                           , 'no_arquivo', anexo.no_arquivo
                           , 'de_tipo_conteudo', anexo.de_tipo_conteudo
                           , 'de_local_arquivo', anexo.de_local_arquivo
                           , 'in_principal', anexo.in_principal
                           , 'dt_anexo', anexo.dt_anexo
                                                                    ))
                                                    from salve.ficha_anexo anexo
                                                    where sq_ficha = ficha.sq_ficha
                                                      and anexo.sq_contexto = 678
                                         )
                           -- ufs
                           , 'estados', (select json_agg( json_build_object('no_contexto',no_contexto
                           ,'no_origem',no_origem
                           ,'sq_estado',sq_estado
                           ,'no_estado',no_estado
                           ,'sg_estado',sg_estado
                           ,'no_regiao',no_regiao
                           ,'st_adicionado_apos_avaliacao',st_adicionado_apos_avaliacao
                           ,'st_em_carencia',st_em_carencia
                           ,'dt_carencia',dt_carencia
                           ,'referencias',js_ref_bibs
                                                          ) ) from salve.fn_ficha_estados(ficha.sq_ficha)
                                         )
                           -- biomas
                           , 'biomas', (select json_agg( json_build_object('no_contexto',no_contexto
                           ,'no_origem',no_origem
                           ,'sq_bioma',sq_bioma_salve
                           ,'sq_bioma_corp',sq_bioma_corp
                           ,'no_bioma',no_bioma
                           ,'st_adicionado_apos_avaliacao',st_adicionado_apos_avaliacao
                           ,'st_em_carencia',st_em_carencia
                           ,'dt_carencia',dt_carencia
                           ,'referencias',js_ref_bibs
                                                         ) ) from salve.fn_ficha_biomas(ficha.sq_ficha)
                                         )
                           -- bacias
                           , 'bacias', (select json_agg( json_build_object('no_contexto',no_contexto
                           ,'no_origem',no_origem
                           ,'sq_bacia',sq_bacia
                           ,'cd_sistema',cd_sistema
                           ,'cd_bacia',cd_bacia
                           ,'no_bacia',no_bacia
                           ,'tx_trilha',tx_trilha
                           ,'referencias',js_ref_bibs
                           ,'st_adicionado_apos_avaliacao',st_adicionado_apos_avaliacao
                           ,'st_em_carencia',st_em_carencia
                           ,'dt_carencia',dt_carencia
                                                         ) ) from salve.fn_ficha_bacias(ficha.sq_ficha))

                           -- ambiente restrito
                           , 'ambiente_restrito',( select json_agg(json_build_object('sq_restricao_habitat', f.sq_restricao_habitat
                           , 'no_restricao_habitat', apoio.ds_dados_apoio
                           , 'tx_ficha_restricao_habitat', f.tx_ficha_restricao_habitat
                           ,'cavernas',( select json_agg(json_build_object('sq_caverna',far.sq_caverna, 'no_caverna',cav.no_caverna, 'sq_estado',estado.sg_estado, 'sg_estado',estado.no_estado))
                                         from salve.ficha_amb_restrito_caverna far
                                                  inner join salve.caverna cav on cav.sq_caverna = far.sq_caverna
                                                  left outer join salve.estado on estado.sq_estado = cav.sq_estado
                                         where far.sq_ficha_ambiente_restrito = f.sq_ficha_ambiente_restrito
                                                                                     )
                           , 'referencias', salve.fn_ref_bib_json ( f.sq_ficha, f.sq_ficha_ambiente_restrito)
                                                                   ) )
                                                   from salve.ficha_ambiente_restrito f
                                                            inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = f.sq_restricao_habitat
                                                   where f.sq_ficha = ficha.sq_ficha
                                         )
                           -- areas relevantes
                           , 'areas_relevantes',( select json_agg(json_build_object('sq_tipo_relevancia', far.sq_tipo_relevancia
                           , 'ds_area_relevante', apoio.ds_dados_apoio
                           , 'sq_estado', uf.sq_estado
                           , 'no_estado', uf.no_estado
                           , 'sg_estado', uf.sg_estado
                           , 'tx_local', far.tx_local
                           , 'referencias', salve.fn_ref_bib_json ( far.sq_ficha, far.sq_ficha_area_relevancia)
                           , 'municipios', (select json_agg(json_build_object('sq_municipio', m.sq_municipio
                                   , 'no_municipio', m.no_municipio
                                   , 'tx_relevancia_municipio', salve.entity2char(farm.tx_relevancia_municipio)
                                                            ))
                                            from salve.ficha_area_relevancia_municipio farm
                                                     inner join salve.municipio m on m.sq_municipio = farm.sq_municipio
                                            where farm.sq_ficha_area_relevancia = far.sq_ficha_area_relevancia
                                                                                    )
                                                                  ))
                                                  from salve.ficha_area_relevancia far
                                                           inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = far.sq_tipo_relevancia
                                                           left outer join salve.estado uf on uf.sq_estado = far.sq_estado
                                                  where sq_ficha = ficha.sq_ficha
                                         )
                           -- registros de ocorrencia
                           , 'ocorrencias', 'gravadas na tabela ocorrencias_json'
                       )
                   -- fim  aba 2

                   ,'historia_natural',json_build_object( 'st_migratoria', ficha.st_migratoria
                           , 'ds_historia_natural', salve.entity2char(ficha.ds_historia_natural)
                           -- aba 3.1
                           , 'st_habito_aliment_especialista', ficha.st_habito_aliment_especialista
                           , 'tipos_habitos_alimentares', (select json_agg( json_build_object( 'sq_tipo_habito_alimentar', a.sq_tipo_habito_alimentar
                            , 'ds_tipo_habito_alimentar', apoio.ds_dados_apoio
                            , 'referencias',salve.fn_ref_bib_json( a.sq_ficha, a.sq_ficha_habito_alimentar )
                                                                            ))
                                                           from salve.ficha_habito_alimentar a
                                                                    inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = a.sq_tipo_habito_alimentar
                                                           where a.sq_ficha = ficha.sq_ficha
                                                          )
                           , 'ds_habito_alimentar', salve.entity2char(ficha.ds_habito_alimentar)
                           -- aba 3.2
                           , 'habitats', (select json_agg( json_build_object('sq_habitat_pai', apoio.sq_dados_apoio_pai, 'sq_habitat', a.sq_habitat
                            , 'ds_habitat', apoio.ds_dados_apoio
                            , 'referencias', salve.fn_ref_bib_json( a.sq_ficha,a.sq_ficha_habitat)
                                                           ))
                                          from salve.ficha_habitat a
                                                   inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = a.sq_habitat
                                          where a.sq_ficha = ficha.sq_ficha
                                                          )
                           , 'st_restrito_habitat_primario', ficha.st_restrito_habitat_primario
                           , 'st_especialista_micro_habitat', ficha.st_especialista_micro_habitat
                           , 'st_variacao_sazonal_habitat', ficha.st_variacao_sazonal_habitat
                           , 'st_difer_macho_femea_habitat', ficha.st_difer_macho_femea_habitat
                           , 'ds_uso_habitat', salve.entity2char(ficha.ds_uso_habitat)
                           -- aba 3.3
                           , 'interacao',json_build_object( 'ds_interacao', salve.entity2char(ficha.ds_interacao)
                                                              , 'interacoes', ( select json_agg(json_build_object('sq_tipo_interacao', a.sq_tipo_interacao
                                    , 'ds_tipo_interacao', apoio.ds_dados_apoio
                                    , 'taxon', a.sq_taxon
                                    , 'taxon_trilha', taxon.json_trilha
                                    , 'taxon_autor', taxon.no_autor
                                    , 'taxon_ano', taxon.nu_ano
                                    , 'sq_categoria_iucn', a.sq_categoria_iucn
                                    , 'ds_ficha_interacao', a.ds_ficha_interacao
                                    , 'referencias', salve.fn_ref_bib_json(   a.sq_ficha,a.sq_ficha_interacao)
                                                                                                ))
                                                                                from salve.ficha_interacao a
                                                                                         inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = a.sq_tipo_interacao
                                                                                         left outer join taxonomia.taxon on taxon.sq_taxon = a.sq_taxon
                                                                                where sq_ficha = ficha.sq_ficha
                                                            )
                                         )
                           -- aba 3.4
                           ,'reproducao',json_build_object('ds_reproducao', salve.entity2char(ficha.ds_reproducao)
                                                              , 'sq_modo_reproducao', ficha.sq_modo_reproducao::text
                                                              , 'ds_modo_reproducao', modo_reproducao.ds_dados_apoio
                                                              , 'sq_sistema_acasalamento', ficha.sq_sistema_acasalamento::text
                                                              , 'ds_sistema_acasalamento', sistema_acasalamento.ds_dados_apoio
                                                              , 'vl_intervalo_nascimento', ficha.vl_intervalo_nascimento::text::text
                                                              , 'sq_unid_intervalo_nascimento', ficha.sq_unid_intervalo_nascimento::text
                                                              , 'ds_unid_intervalo_nascimento', unid_intervalo_nascimento.ds_dados_apoio
                                                              , 'vl_tempo_gestacao', ficha.vl_tempo_gestacao::text
                                                              , 'sq_unid_tempo_gestacao', ficha.sq_unid_tempo_gestacao::text
                                                              , 'vl_tamanho_prole', ficha.vl_tamanho_prole::text
                                                              -- macho
                                                              , 'vl_maturidade_sexual_macho', ficha.vl_maturidade_sexual_macho::text
                                                              , 'sq_unid_maturidade_sexual_macho', ficha.sq_unid_maturidade_sexual_macho::text
                                                              , 'ds_unid_maturidade_sexual_macho', unid_maturidade_sexual_macho.ds_dados_apoio
                                                              , 'vl_peso_macho', ficha.vl_peso_macho::text
                                                              , 'sq_unidade_peso_macho', ficha.sq_unidade_peso_macho::text
                                                              , 'ds_unidade_peso_macho', unid_peso_macho.ds_dados_apoio
                                                              , 'vl_comprimento_macho_max', ficha.vl_comprimento_macho_max::text
                                                              , 'sq_medida_comprimento_macho_max', ficha.sq_medida_comprimento_macho_max::text
                                                              , 'ds_medida_comprimento_macho_max', unid_medida_comprimento_macho_max.ds_dados_apoio
                                                              , 'vl_comprimento_macho', ficha.vl_comprimento_macho::text
                                                              , 'sq_medida_comprimento_macho', ficha.sq_medida_comprimento_macho::text
                                                              , 'ds_medida_comprimento_macho', unid_medida_comprimento_macho.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_macho', ficha.vl_senilidade_reprodutiva_macho::text
                                                              , 'sq_unidade_senilid_rep_macho', ficha.sq_unidade_senilid_rep_macho::text
                                                              , 'ds_unidade_senilid_rep_macho', unid_senilid_rep_macho.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_macho', ficha.vl_senilidade_reprodutiva_macho::text
                                                              , 'sq_unidade_longevidade_macho', ficha.sq_unidade_senilid_rep_macho::text
                                                              , 'sq_unidade_longevidade_macho', unid_longevidade_macho.ds_dados_apoio

                                                              -- femea
                                                              , 'vl_maturidade_sexual_femea', ficha.vl_maturidade_sexual_femea::text
                                                              , 'sq_unid_maturidade_sexual_femea', ficha.sq_unid_maturidade_sexual_femea::text
                                                              , 'ds_unid_maturidade_sexual_femea', unid_maturidade_sexual_femea.ds_dados_apoio
                                                              , 'vl_peso_femea', ficha.vl_peso_femea::text
                                                              , 'sq_unidade_peso_femea', ficha.sq_unidade_peso_femea::text
                                                              , 'ds_unidade_peso_femea', unid_peso_femea.ds_dados_apoio
                                                              , 'vl_comprimento_femea_max', ficha.vl_comprimento_femea_max::text
                                                              , 'sq_medida_comprimento_femea_max', ficha.sq_medida_comprimento_femea_max::text
                                                              , 'ds_medida_comprimento_femea_max', unid_medida_comprimento_femea_max.ds_dados_apoio
                                                              , 'vl_comprimento_femea', ficha.vl_comprimento_femea::text
                                                              , 'sq_medida_comprimento_femea', ficha.sq_medida_comprimento_femea::text
                                                              , 'ds_medida_comprimento_femea', unid_medida_comprimento_femea.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_femea', ficha.vl_senilidade_reprodutiva_femea::text
                                                              , 'sq_unidade_senilid_rep_femea', ficha.sq_unidade_senilid_rep_femea::text
                                                              , 'ds_unidade_senilid_rep_femea', unid_senilid_rep_femea.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_femea', ficha.vl_senilidade_reprodutiva_femea::text
                                                              , 'sq_unidade_longevidade_femea', ficha.sq_unidade_senilid_rep_femea::text
                                                              , 'sq_unidade_longevidade_femea', unid_longevidade_femea.ds_dados_apoio
                                         )
                           -- 3.5
                           ,'sazonalidade',( select json_agg( json_build_object(
                                'sq_fase_vida', fase_vida.sq_dados_apoio,
                                'ds_fase_vida', fase_vida.ds_dados_apoio,
                                'sq_epoca', epoca.sq_dados_apoio,
                                'ds_epoca', epoca.ds_dados_apoio,
                                'sq_habitat_pai',habitat.sq_dados_apoio_pai,
                                'sq_habitat', habitat.sq_dados_apoio,
                                'ds_habitat',habitat.ds_dados_apoio,
                                'referencias',salve.fn_ref_bib_json( a.sq_ficha, a.sq_ficha_variacao_sazonal)
                                                              ))
                                             from salve.ficha_variacao_sazonal a
                                                      inner join salve.dados_apoio as fase_vida on fase_vida.sq_dados_apoio = a.sq_fase_vida
                                                      inner join salve.dados_apoio as epoca on epoca.sq_dados_apoio = a.sq_epoca
                                                      inner join salve.dados_apoio as habitat on habitat.sq_dados_apoio = a.sq_habitat
                                             where a.sq_ficha = ficha.sq_ficha
                                                          )
                                       )
                   -- fim aba 3
                   -- inicio aba 4
                   ,'populacao',json_build_object('sq_tendencia_populacional',ficha.sq_tendencia_populacional
                           ,'ds_tendencia_populacional',tendencia_populacional.ds_dados_apoio
                           ,'ds_populacao', salve.entity2char(ficha.ds_populacao)
                           ,'vl_tempo_geracional',ficha.vl_tempo_geracional
                           ,'sq_medida_tempo_geracional',ficha.sq_medida_tempo_geracional
                           ,'ds_unidade_tempo_geracional',unid_tempo_geracional.ds_dados_apoio
                           ,'ds_metod_calc_tempo_geracional',salve.entity2char(ficha.ds_metod_calc_tempo_geracional)
                           ,'anexos', ( select json_agg( json_build_object('de_legenda', a.de_legenda, 'no_arquivo', a.no_arquivo
                            , 'dt_anexo', a.dt_anexo
                            , 'de_local_arquivo', a.de_local_arquivo
                            , 'de_tipo_conteudo', a.de_tipo_conteudo
                            , 'in_principal', a.in_principal
                                                         ))
                                        from salve.ficha_anexo a
                                        where a.sq_ficha = ficha.sq_ficha
                                          and a.sq_contexto = 677
                                                  )
                           -- aba 4.1
                           ,'informacao_local_regional',( select json_agg(json_build_object('sq_abrangencia', a.sq_abrangencia
                            , 'ds_unidade_abrangencia', unid.ds_dados_apoio
                            , 'ds_local', a.ds_local
                            , 'ds_mes_ano_inicio', a.ds_mes_ano_inicio
                            , 'ds_mes_ano_fim', a.ds_mes_ano_fim
                            , 'ds_estimativa_populacao', a.ds_estimativa_populacao
                            , 'de_valor_abundancia', a.de_valor_abundancia
                            , 'referencias', salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_popul_estimada_local)
                                                                          ) )
                                                          from salve.ficha_popul_estimada_local a
                                                                   inner join salve.abrangencia abrangencia on abrangencia.sq_abrangencia = a.sq_abrangencia
                                                                   left outer join salve.dados_apoio unid on unid.sq_dados_apoio = a.sq_unid_abundancia_populacao
                                                          where a.sq_ficha = ficha.sq_ficha
                                                  )
                           -- aba 4.2
                           ,'area_vida',( select json_agg( json_build_object('sq_unidade_area',a.sq_unidade_area
                            ,'ds_unidade_area',unid.ds_dados_apoio
                            ,'tx_local',a.tx_local
                            ,'tx_area', a.tx_area
                            ,'vl_area', a.vl_area
                            ,'nu_ano',a.nu_ano
                            ,'referencia',salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_area_vida)
                                                           ) )
                                          from salve.ficha_area_vida a
                                                   left outer join salve.dados_apoio unid on unid.sq_dados_apoio = a.sq_unidade_area
                                          where a.sq_ficha = ficha.sq_ficha
                                                  )
                                ) -- fim aba 4

            -- inicio aba 5
                   ,'ameacas',json_build_object('tx_ameaca',salve.entity2char(ficha.ds_ameaca)
                           ,'vetores',(select json_agg( json_build_object('sq_criterio_ameaca_iucn', a.sq_criterio_ameaca_iucn
                            , 'ds_ameaca', vw_ameacas.descricao
                            , 'cd_ameaca', vw_ameacas.codigo
                            , 'de_ordem', vw_ameacas.ordem
                            , 'nu_nivel', vw_ameacas.nivel
                            , 'tx_ameaca_trilha',vw_ameacas.trilha
                            , 'tx_ficha_ameaca', a.tx_ficha_ameaca
                            , 'nu_peso', a.nu_peso
                            , 'sq_ref_termporal',ref_temporal.sq_dados_apoio
                            , 'ds_ref_termporal',ref_temporal.ds_dados_apoio
                            , 'cd_ref_termporal_sistema',ref_temporal.cd_sistema
                            , 'regioes', (
                                                                              select json_agg( json_build_object(
                                                                                      'sq_abrangencia',fo.sq_abrangencia
                                                                                  ,'tx_local,',fo.tx_local) )
                                                                              from salve.ficha_ameaca_regiao b
                                                                                       left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                       inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fo.sq_contexto
                                                                                       left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                              where b.sq_ficha_ameaca = a.sq_ficha_ameaca
                                                                                and contexto.cd_sistema='AMEACA_REGIAO'
                                                                          )
                            , 'georeferencia', (
                                                                              select json_agg( json_build_object('tx_local',fo.tx_local
                                                                                  ,'ge_ocorrencia',fo.ge_ocorrencia
                                                                                  ,'x',st_x(fo.ge_ocorrencia)
                                                                                  ,'y',st_y(fo.ge_ocorrencia) ) )
                                                                              from salve.ficha_ameaca_regiao b
                                                                                       left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                       inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fo.sq_contexto
                                                                                       left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                              where b.sq_ficha_ameaca = a.sq_ficha_ameaca
                                                                                and contexto.cd_sistema = 'AMEACA_GEO'
                                                                          )
                            , 'referencias', salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_ameaca)
                                                        ) )

                                       from salve.ficha_ameaca a
                                                inner join salve.vw_ameacas on vw_ameacas.id = a.sq_criterio_ameaca_iucn
                                                left outer join salve.dados_apoio as ref_temporal on ref_temporal.sq_dados_apoio = a.sq_referencia_temporal
                                       --left outer join salve.dados_apoio ameaca on ameaca.sq_dados_apoio = a.sq_criterio_ameaca_iucn
                                       where a.sq_ficha = ficha.sq_ficha )
                              )
                   -- fim  aba 5



                   -- inicio aba 6
                   ,'usos',json_build_object('tx_uso',salve.entity2char(ficha.ds_uso)
                           ,'usos',(
                                                 select json_agg( json_build_object('sq_uso', a.sq_uso
                                                     , 'ds_uso', uso.descricao
                                                     , 'cd_uso',uso.codigo
                                                     , 'de_ordem',uso.ordem
                                                     , 'nu_nivel',uso.nivel
                                                     , 'tx_uso_trilha', uso.trilha
                                                     , 'sq_ameaca_iucn',uso.id_ameaca_iucn
                                                     , 'tx_ficha_uso', a.tx_ficha_uso
                                                     , 'tx_ameaca_trilha',ameaca.trilha
                                                     , 'regioes', (
                                                                                        select json_agg( json_build_object(
                                                                                                'sq_abrangencia',fo.sq_abrangencia
                                                                                            ,'tx_local,',fo.tx_local) )
                                                                                        from salve.ficha_uso_regiao b
                                                                                                 left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                                 left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                                        where b.sq_ficha_uso = a.sq_ficha_uso
                                                                                          and fo.sq_contexto = 497
                                                                                    )
                                                     , 'georeferencia', (
                                                                                        select json_agg( json_build_object('tx_local',fo.tx_local
                                                                                            ,'ge_ocorrencia',fo.ge_ocorrencia
                                                                                            ,'x',st_x(fo.ge_ocorrencia)
                                                                                            ,'y',st_y(fo.ge_ocorrencia) ) )
                                                                                        from salve.ficha_uso_regiao b
                                                                                                 left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                                 left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                                        where b.sq_ficha_uso = a.sq_ficha_uso
                                                                                          and fo.sq_contexto = 1006
                                                                                    )
                                                     , 'referencias', salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_uso)
                                                                  )
                                                        )
                                                 from salve.ficha_uso a
                                                          left outer join salve.vw_usos as uso on uso.id = a.sq_uso
                                                          left outer join salve.vw_ameacas as ameaca on ameaca.id = uso.id_ameaca_iucn
                                                 where a.sq_ficha = ficha.sq_ficha
                                             )
                           ,'anexos',( select json_agg( json_build_object('de_legenda',fa.de_legenda
                            ,'no_arquivo',fa.no_arquivo
                            ,'de_local_arquivo',fa.de_local_arquivo
                            ,'de_tipo_documento',fa.de_tipo_conteudo
                            ,'in_principal', fa.in_principal
                            ,'dt_anexo',fa.dt_anexo ) )
                                       from salve.ficha_anexo fa
                                       where fa.sq_ficha = ficha.sq_ficha
                                         and fa.sq_contexto = 874
                                             )
                           )
                   -- fim aba 6



                   -- inicio aba 7
                   ,'historico_avaliacao',json_build_object('st_presenca_lista_vigente',ficha.st_presenca_lista_vigente
                           ,'ds_historico_avaliacao',salve.entity2char(ficha.ds_historico_avaliacao)
                           ,'avaliacoes',( select json_agg( json_build_object('sq_tipo_avaliacao',h.sq_tipo_avaliacao
                            ,'ds_tipo_avaliacao',tipo.ds_dados_apoio
                            ,'cd_tipo_avaliacao_sistema',tipo.cd_sistema
                            ,'sq_abrangencia',h.sq_abrangencia
                            ,'sq_categoria_iucn',sq_categoria_iucn
                            ,'ds_categoria_iucn',categoria.ds_dados_apoio
                            ,'cd_categoria_iucn',categoria.cd_dados_apoio
                            ,'cd_categoria_iucn_sistema',categoria.cd_sistema
                            ,'nu_ano_avaliacao',h.nu_ano_avaliacao
                            ,'de_criterio_avaliacao_iucn',h.de_criterio_avaliacao_iucn
                            ,'st_possivelmente_extinta',h.st_possivelmente_extinta
                            ,'tx_justificativa_avaliacao',h.tx_justificativa_avaliacao
                            ,'no_regiao_outra',h.no_regiao_outra
                            ,'de_categoria_outra',h.de_categoria_outra))
                                           from salve.taxon_historico_avaliacao h
                                                    inner join salve.dados_apoio as categoria on categoria.sq_dados_apoio = h.sq_categoria_iucn
                                                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = h.sq_tipo_avaliacao
                                                    left outer join salve.abrangencia on categoria.sq_dados_apoio = h.sq_abrangencia
                                           where h.sq_taxon =ficha.sq_taxon
                                                            )),
                       'listas_convencoes',(
                           select json_agg( json_build_object('sq_lista_convencao', a.sq_lista_convencao, 'ds_lista_convencao',listas.ds_dados_apoio, 'nu_ano', a.nu_ano) )
                           from salve.ficha_lista_convencao a
                                    inner join salve.dados_apoio as listas on listas.sq_dados_apoio = a.sq_lista_convencao
                           where a.sq_ficha =ficha.sq_ficha
                       ),'acoes_conservacao',json_build_object('ds_acao_conservacao', salve.entity2char( ficha.ds_acao_conservacao)
                           , 'acoes_conservacao',(
                                                                   select json_agg(
                                                                                  json_build_object('sq_acao_conservacao', a.sq_acao_conservacao
                                                                                      , 'ds_acao_conservacao', acao.ds_dados_apoio
                                                                                      , 'sq_situacao_acao_conservacao', a.sq_situacao_acao_conservacao
                                                                                      , 'ds_situacao_acao_conservacao', situacao.ds_dados_apoio
                                                                                      , 'sq_plano_acao', a.sq_plano_acao
                                                                                      , 'tx_plano_acao', pan.tx_plano_acao
                                                                                      , 'sq_tipo_ordenamento', a.sq_tipo_ordenamento
                                                                                      , 'ds_tipo_ordenamento', ordenamento.ds_dados_apoio
                                                                                      ,'referencias',salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_acao_conservacao ) )
                                                                          )
                                                                   from salve.ficha_acao_conservacao a
                                                                            inner join salve.dados_apoio as acao on acao.sq_dados_apoio = a.sq_acao_conservacao
                                                                            inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao_acao_conservacao
                                                                            left outer join salve.plano_acao as pan on pan.sq_plano_acao = a.sq_plano_acao
                                                                            left outer join salve.dados_apoio as ordenamento on ordenamento.sq_dados_apoio = a.sq_tipo_ordenamento
                                                                   where a.sq_ficha = ficha.sq_ficha
                                                               ))
                   ,'presenca_uc',json_build_object('ds_presenca_uc', salve.entity2char(ficha.ds_presenca_uc )
                           ,'ucs',( select json_agg(json_build_object('sq_uc',a.sq_uc
                            ,'no_uc',a.no_uc
                            ,'cd_esfera',a.cd_esfera
                            ,'in_presenca_atual',a.in_presenca_atual
                            ,'no_estado',a.no_estado
                            ,'sg_estado',a.sg_estado
                            ,'nu_ordem',a.nu_ordem
                            ,'co_nivel_taxonomico',a.co_nivel_taxonomico
                            ,'st_utilizado_avaliacao',a.st_utilizado_avaliacao
                            ,'st_adicionado_apos_avaliacao',a.st_adicionado_apos_avaliacao
                            ,'st_em_carencia',a.st_em_carencia
                            ,'co_cnuc',a.co_cnuc
                            ,'referencias',a.json_ref_bib::jsonb))
                                    from salve.fn_ficha_ucs(ficha.sq_ficha) a
                                                    ))
                   -- fim aba 7



                   -- inicio aba 8
                   ,'pesquisas',(
                           select json_agg( json_build_object('tx_ficha_pesquisa',a.tx_ficha_pesquisa
                               ,'sq_tema_pesquisa',a.sq_tema_pesquisa
                               ,'ds_tema_pesquisa',tema.ds_dados_apoio
                               ,'sq_situacao_pesquisa',a.sq_situacao_pesquisa
                               ,'ds_situacao_pesquisa',situacao.ds_dados_apoio
                               ,'referencias',salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_pesquisa)
                                            ) )
                           from salve.ficha_pesquisa a
                                    inner join salve.dados_apoio as tema on tema.sq_dados_apoio = a.sq_tema_pesquisa
                                    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao_pesquisa
                           where a.sq_ficha = ficha.sq_ficha )
                   -- fim aba 8

                   -- inicio aba 9
                   ,'referencias',( select json_agg( json_build_object('sq_publicacao',r.sq_publicacao
                    ,'no_origem', r.no_origem
                    ,'de_ref_bibliografica',r.de_ref_bibliografica
                    ,'json_anexos',r.json_anexos::jsonb
                    ,'no_tags',r.no_tags
                    ,'no_tabela',r.no_tabela
                    ,'no_coluna',r.no_coluna
                                                     ))
                                    from salve.fn_ficha_ref_bibs(ficha.sq_ficha) as r
                       )
                   -- fim aba 9

                   -- inicio aba 10
                   ,'multimidia',( select json_agg( json_build_object(
                        'sq_tipo',a.sq_tipo,'ds_tipo',tipo.ds_dados_apoio,
                        'sq_datum',a.sq_datum,'ds_datum',datum.ds_dados_apoio,
                        'sq_situacao',a.sq_situacao,'ds_situacao', situacao.ds_dados_apoio,
                        'de_legenda',a.de_legenda,
                        'tx_multimidia',a.tx_multimidia,
                        'no_autor',a.no_autor,
                        'de_email_autor',a.de_email_autor,
                        'dt_elaboracao',a.dt_elaboracao,
                        'no_arquivo',a.no_arquivo,
                        'no_arquivo_disco',a.no_arquivo_disco,
                        'dt_inclusao',a.dt_inclusao,
                        'in_principal',a.in_principal,
                        'de_url',a.de_url,
                        'ge_multimidia',a.ge_multimidia,
                        'dt_aprovacao',a.dt_aprovacao,
                        'sq_pessoa_aprovou',a.sq_pessoa_aprovou,
                        'tx_nao_aceito',a.tx_nao_aceito,
                        'dt_reprovacao',a.dt_reprovacao,
                        'sq_pessoa_reprovou',a.sq_pessoa_reprovou,
                        'in_destaque',a.in_destaque) )
                                   from salve.ficha_multimidia  a
                                            left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo
                                            left outer join salve.dados_apoio as datum on datum.sq_dados_apoio = a.sq_datum
                                            left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao
                                   where sq_ficha = ficha.sq_ficha
                       )
                   -- fim aba 10



                   -- inicio aba 11
                   ,'avaliacao',json_build_object(
                           -- 11.1
                               'distribuicao_taxonomica', json_build_object (
                                'st_ocorrencia_marginal', ficha.st_ocorrencia_marginal
                            , 'st_elegivel_avaliacao', ficha.st_elegivel_avaliacao
                            , 'st_limitacao_taxonomica_aval', ficha.st_limitacao_taxonomica_aval
                            , 'st_localidade_tipo_conhecida', ficha.st_localidade_tipo_conhecida
                            , 'st_regiao_bem_amostrada', ficha.st_regiao_bem_amostrada
                            , 'vl_eoo', ficha.vl_eoo
                            , 'sq_tendencia_eoo', ficha.sq_tendencia_eoo
                            , 'ds_tendencia_eoo', tendencia_eoo.ds_dados_apoio
                            , 'st_flutuacao_eoo', ficha.st_flutuacao_eoo
                            , 'vl_aoo', ficha.vl_aoo
                            , 'sq_tendencia_aoo', ficha.sq_tendencia_aoo
                            , 'ds_tendencia_aoo', tendencia_aoo.ds_dados_apoio
                            , 'ds_justificativa_aoo_eoo', salve.entity2char(ficha.ds_justificativa_aoo_eoo)
                                                          )

                           -- aba 11.2
                           ,'populacao',json_build_object('nu_reducao_popul_passada_rev',ficha.nu_reducao_popul_passada_rev
                                   ,'sq_tipo_redu_popu_pass_rev',ficha.sq_tipo_redu_popu_pass_rev
                                   ,'ds_tipo_redu_popu_pass_rev',tipo_redu_popu_pass_rev.ds_dados_apoio
                                   ,'nu_reducao_popul_passada',ficha.nu_reducao_popul_passada
                                   ,'sq_tipo_redu_popu_pass',ficha.sq_tipo_redu_popu_pass
                                   ,'ds_tipo_redu_popu_pass',tipo_redu_popu_pass.ds_dados_apoio
                                   ,'nu_reducao_popul_futura',ficha.nu_reducao_popul_futura
                                   ,'sq_tipo_redu_popu_futura',ficha.sq_tipo_redu_popu_futura
                                   ,'ds_tipo_redu_popu_futura',tipo_redu_popu_futura.ds_dados_apoio
                                   ,'declinios_populacionais',( select json_agg( json_build_object('sq_declinio_populacional', a.sq_declinio_populacional
                                    ,'ds_declinio_populacional', declinio.ds_dados_apoio
                                                                                 ))
                                                                from salve.ficha_declinio_populacional a
                                                                         inner join salve.dados_apoio as declinio on declinio.sq_dados_apoio = a.sq_declinio_populacional
                                                                where sq_ficha = ficha.sq_ficha
                                                          )
                                   ,'sq_perc_declinio_populacional',ficha.sq_perc_declinio_populacional
                                   ,'ds_perc_declinio_populacional',perc_declinio_populacional.ds_dados_apoio
                                   ,'st_populacao_fragmentada',ficha.st_populacao_fragmentada
                                   ,'nu_individuo_maduro',ficha.nu_individuo_maduro
                                   ,'sq_tenden_num_individuo_maduro',ficha.sq_tenden_num_individuo_maduro
                                   ,'ds_tenden_num_individuo_maduro',tenden_num_individuo_maduro.ds_dados_apoio
                                   ,'st_flut_extrema_indiv_maduro',ficha.st_flut_extrema_indiv_maduro
                                   ,'nu_subpopulacao',ficha.nu_subpopulacao
                                   ,'sq_tendencia_num_subpopulacao',ficha.sq_tendencia_num_subpopulacao
                                   ,'ds_tendencia_num_subpopulacao',tendencia_num_subpopulacao.ds_dados_apoio
                                   ,'st_flut_extrema_sub_populacao',ficha.st_flut_extrema_sub_populacao
                                   ,'nu_indivi_subpopulacao_max',ficha.nu_indivi_subpopulacao_max
                                   ,'nu_indivi_subpopulacao_perc',ficha.nu_indivi_subpopulacao_perc
                                        )

                           -- aba 11.3
                           ,'ameacas_analise_quantitativa',json_build_object(
                                       'nu_localizacoes', ficha.nu_localizacoes
                                   ,'sq_tenden_num_localizacoes', ficha.sq_tenden_num_localizacoes
                                   ,'ds_tenden_num_localizacoes', tenden_num_localizacoes.ds_dados_apoio
                                   ,'st_flutuacao_num_localizacoes', ficha.st_flutuacao_num_localizacoes
                                   ,'sq_tenden_qualidade_habitat',ficha.sq_tenden_qualidade_habitat
                                   ,'ds_tenden_qualidade_habitat',tenden_qualidade_habitat.ds_dados_apoio
                                   ,'st_ameaca_futura',ficha.st_ameaca_futura
                                   ,'sq_prob_extincao_brasil',ficha.sq_prob_extincao_brasil
                                   ,'ds_prob_extincao_brasil',prob_extincao_brasil.ds_dados_apoio
                                   ,'ds_metodo_calc_perc_extin_br', salve.entity2char(ficha.ds_metodo_calc_perc_extin_br )
                                                           )


                           -- aba 11.4 - esta na aba 11-avaliacao
                           -- aba 11.5
                           ,'avaliacao_expressa',json_build_object (
                                       'st_favorecido_conversao_habitats',ficha.st_favorecido_conversao_habitats
                                   ,'st_tem_registro_areas_amplas',ficha.st_tem_registro_areas_amplas
                                   ,'st_possui_ampla_dist_geografica',ficha.st_possui_ampla_dist_geografica
                                   ,'st_frequente_inventario_eoo',ficha.st_frequente_inventario_eoo)

                           -- aba 11.6
                           ,'resultado_avaliacao',json_build_object('sq_categoria_iucn',ficha.sq_categoria_iucn
                                   ,'ds_categoria_iucn',categoria_iucn.ds_dados_apoio
                                   ,'cd_categoria_iucn', categoria_iucn.cd_dados_apoio
                                   ,'cd_categoria_iucn_sistema', categoria_iucn.cd_sistema
                                   ,'de_categoria_ordem', categoria_iucn.de_dados_apoio_ordem
                                   ,'ds_justificativa', salve.entity2char( ficha.ds_justificativa )
                                   ,'st_possivelmente_extinta',ficha.st_possivelmente_extinta
                                   ,'ds_criterio_aval_iucn',ficha.ds_criterio_aval_iucn
                                   ,'ajuste_regional',json_build_object('sq_tipo_conectividade',ficha.sq_tipo_conectividade
                                                                        ,'ds_tipo_conectividade',tipo_conectividade.ds_dados_apoio
                                                                        ,'sq_tendencia_imigracao',ficha.sq_tendencia_imigracao
                                                                        ,'ds_tendencia_imigracao',tendencia_imigracao.ds_dados_apoio
                                                                        ,'st_declinio_br_popul_exterior',ficha.st_declinio_br_popul_exterior
                                                                        ,'ds_conectividade_pop_exterior',salve.entity2char(ficha.ds_conectividade_pop_exterior )
                                                      )
                                   ,'ds_citacao',salve.entity2char(ficha.ds_citacao)
                                   ,'ds_colaboradores',salve.entity2char(ficha.ds_colaboradores)
                                   ,'ds_equipe_tecnica',salve.entity2char(ficha.ds_equipe_tecnica )
                                   ,'dt_fim_avaliacao',oficina_avaliacao.dt_fim
                                   ,'nu_mes_ano_ultima_avaliacao', to_char(oficina_avaliacao.dt_fim,'MM/YYYY')
                                                  )

                           -- aba 11.7
                           ,'resultado_validacao',json_build_object('sq_categoria_final',ficha.sq_categoria_final
                                   ,'ds_categoria_final',categoria_iucn_final.ds_dados_apoio
                                   ,'cd_categoria_final',categoria_iucn_final.cd_dados_apoio
                                   ,'cd_categoria_final_sitema',categoria_iucn_final.cd_sistema
                                   ,'de_categoria_final_ordem',categoria_iucn_final.de_dados_apoio_ordem
                                   ,'ds_justificativa_final',salve.entity2char(ficha.ds_justificativa_final)
                                   ,'st_possivelmente_extinta_final',ficha.st_possivelmente_extinta_final
                                   ,'ds_criterio_aval_iucn_final',ficha.ds_criterio_aval_iucn_final
                                                  )
                           ,'motivos_mudanca_categoria',( select json_agg( json_build_object('sq_motivo_mudanca',a.sq_motivo_mudanca,'ds_motivo_mudanca', motivo_mudanca_categoria.ds_dados_apoio ) )
                                                          from salve.ficha_mudanca_categoria a
                                                                   inner join salve.dados_apoio as motivo_mudanca_categoria on motivo_mudanca_categoria.sq_dados_apoio = a.sq_motivo_mudanca
                                                          where a.sq_ficha = ficha.sq_ficha
                               )
                                )
                   -- fim aba 11

                   -- aba 12 - pendencias
                   , 'pendencias',
                       (  select json_agg(
                                         json_build_object(
                                                 'sq_ficha_pendencia',pendencia.sq_ficha_pendencia
                                             , 'sq_contexto'       , pendencia.sq_contexto
                                             , 'sq_usuario', pendencia.sq_usuario
                                             , 'ds_contexto', contexto.ds_dados_apoio
                                             , 'dt_pendencia',pendencia.dt_pendencia
                                             , 'de_assunto', pendencia.de_assunto
                                             , 'sq_usuario_resolveu', pendencia.sq_usuario_resolveu
                                             , 'dt_resolvida',pendencia.dt_resolvida
                                             , 'sq_usuario_revisou', pendencia.sq_usuario_revisou
                                             , 'dt_revisao',pendencia.dt_revisao
                                             , 'st_pendente', pendencia.st_pendente
                                             , 'tx_pendencia',salve.remover_html_tags(salve.entity2char(pendencia.tx_pendencia ) )
                                         )
                                 )

                          from salve.ficha_pendencia as pendencia
                                   left outer join salve.dados_apoio as contexto on contexto.sq_dados_apoio = pendencia.sq_contexto
                          where pendencia.sq_ficha = ficha.sq_ficha
                       ) -- fim aba 12

            -- Modulo Função / papéis
                   , 'funcoes',
                       (  select json_agg(
                                         json_build_object(
                                                 'sq_ficha_pessoa',ficha_pessoa.sq_ficha_pessoa
                                             ,'sq_papel',ficha_pessoa.sq_papel
                                             ,'ds_papel',papel.ds_dados_apoio
                                             ,'cd_papel_sistema',papel.cd_sistema
                                             ,'sq_pessoa',ficha_pessoa.sq_pessoa
                                             ,'no_pessoa',pf.no_pessoa
                                             ,'in_ativo',ficha_pessoa.in_ativo
                                             ,'sq_web_instituicao',ficha_pessoa.sq_web_instituicao
                                             ,'no_instituicao',web_instituicao.no_instituicao
                                             ,'sg_instituicao',web_instituicao.sg_instituicao
                                             ,'sq_grupo',ficha_pessoa.sq_grupo
                                             ,'ds_grupo',grupo.ds_dados_apoio
                                             ,'cd_grupo_sistema',grupo.cd_sistema
                                         )
                                 )
                          from salve.ficha_pessoa
                                   inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                                   left outer join salve.web_instituicao on web_instituicao.sq_web_instituicao = ficha_pessoa.sq_web_instituicao
                                   left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
                                   left outer join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                          where ficha_pessoa.sq_ficha = ficha.sq_ficha
                            and ficha_pessoa.in_ativo = 'S'
                       ) -- fim funcoes/papeis

               )::json
        from salve.ficha
                 left outer join salve.vw_unidade_org uo on uo.sq_pessoa = ficha.sq_unidade_org
                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                 left outer join salve.dados_apoio as subgrupo on subgrupo.sq_dados_apoio = ficha.sq_subgrupo
                 left outer join salve.dados_apoio as grupo_recorte on grupo_recorte.sq_dados_apoio = ficha.sq_grupo_salve
                 left outer join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 left outer join salve.dados_apoio as modo_reproducao on modo_reproducao.sq_dados_apoio = ficha.sq_modo_reproducao
                 left outer join salve.dados_apoio as sistema_acasalamento on sistema_acasalamento.sq_dados_apoio = ficha.sq_sistema_acasalamento
                 left outer join salve.dados_apoio as unid_intervalo_nascimento on unid_intervalo_nascimento.sq_dados_apoio = ficha.sq_unid_intervalo_nascimento
                 left outer join salve.dados_apoio as unid_tempo_gestacao on unid_tempo_gestacao.sq_dados_apoio = ficha.sq_unid_tempo_gestacao

                 left outer join salve.dados_apoio as unid_maturidade_sexual_macho on unid_maturidade_sexual_macho.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_macho
                 left outer join salve.dados_apoio as unid_peso_macho on unid_peso_macho.sq_dados_apoio = ficha.sq_unidade_peso_macho
                 left outer join salve.dados_apoio as unid_medida_comprimento_macho_max on unid_medida_comprimento_macho_max.sq_dados_apoio = ficha.sq_medida_comprimento_macho_max
                 left outer join salve.dados_apoio as unid_medida_comprimento_macho on unid_medida_comprimento_macho.sq_dados_apoio = ficha.sq_medida_comprimento_macho
                 left outer join salve.dados_apoio as unid_senilid_rep_macho on unid_senilid_rep_macho.sq_dados_apoio = ficha.sq_unidade_senilid_rep_macho
                 left outer join salve.dados_apoio as unid_longevidade_macho on unid_longevidade_macho.sq_dados_apoio = ficha.sq_unidade_longevidade_macho

                 left outer join salve.dados_apoio as unid_maturidade_sexual_femea on unid_maturidade_sexual_femea.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_femea
                 left outer join salve.dados_apoio as unid_peso_femea on unid_peso_femea.sq_dados_apoio = ficha.sq_unidade_peso_femea
                 left outer join salve.dados_apoio as unid_medida_comprimento_femea_max on unid_medida_comprimento_femea_max.sq_dados_apoio = ficha.sq_medida_comprimento_femea_max
                 left outer join salve.dados_apoio as unid_medida_comprimento_femea on unid_medida_comprimento_femea.sq_dados_apoio = ficha.sq_medida_comprimento_femea
                 left outer join salve.dados_apoio as unid_senilid_rep_femea on unid_senilid_rep_femea.sq_dados_apoio = ficha.sq_unidade_senilid_rep_femea
                 left outer join salve.dados_apoio as unid_longevidade_femea on unid_longevidade_femea.sq_dados_apoio = ficha.sq_unidade_longevidade_femea

                 left outer join salve.dados_apoio as tendencia_populacional on tendencia_populacional.sq_dados_apoio = ficha.sq_tendencia_populacional
                 left outer join salve.dados_apoio as unid_tempo_geracional on unid_tempo_geracional.sq_dados_apoio = ficha.sq_medida_tempo_geracional

                 left outer join salve.dados_apoio as tendencia_eoo on tendencia_eoo.sq_dados_apoio = ficha.sq_tendencia_eoo
                 left outer join salve.dados_apoio as tendencia_aoo on tendencia_aoo.sq_dados_apoio = ficha.sq_tendencia_aoo

                 left outer join salve.dados_apoio as tipo_redu_popu_pass_rev on tipo_redu_popu_pass_rev.sq_dados_apoio = ficha.sq_tipo_redu_popu_pass_rev
                 left outer join salve.dados_apoio as tipo_redu_popu_pass on tipo_redu_popu_pass.sq_dados_apoio = ficha.sq_tipo_redu_popu_pass
                 left outer join salve.dados_apoio as tipo_redu_popu_futura on tipo_redu_popu_futura.sq_dados_apoio = ficha.sq_tipo_redu_popu_futura
                 left outer join salve.dados_apoio as perc_declinio_populacional on perc_declinio_populacional.sq_dados_apoio = ficha.sq_perc_declinio_populacional
                 left outer join salve.dados_apoio as tenden_num_individuo_maduro on tenden_num_individuo_maduro.sq_dados_apoio = ficha.sq_tenden_num_individuo_maduro
                 left outer join salve.dados_apoio as tendencia_num_subpopulacao on tendencia_num_subpopulacao.sq_dados_apoio = ficha.sq_tendencia_num_subpopulacao

                 left outer join salve.dados_apoio as tenden_num_localizacoes on tenden_num_localizacoes.sq_dados_apoio = ficha.sq_tenden_num_localizacoes
                 left outer join salve.dados_apoio as tenden_qualidade_habitat on tenden_qualidade_habitat.sq_dados_apoio = ficha.sq_tenden_qualidade_habitat
                 left outer join salve.dados_apoio as prob_extincao_brasil on prob_extincao_brasil.sq_dados_apoio = ficha.sq_prob_extincao_brasil

                 left outer join salve.dados_apoio as categoria_iucn on categoria_iucn.sq_dados_apoio = ficha.sq_categoria_iucn
                 left outer join salve.dados_apoio as categoria_iucn_final on categoria_iucn_final.sq_dados_apoio = ficha.sq_categoria_final
                 left outer join salve.dados_apoio as tipo_conectividade on tipo_conectividade.sq_dados_apoio = ficha.sq_tipo_conectividade
                 left outer join salve.dados_apoio as tendencia_imigracao on tendencia_imigracao.sq_dados_apoio = ficha.sq_tendencia_imigracao
            -- ler a última oficina de avaliação
                 left join lateral (
            select oficina.dt_fim
            from salve.oficina_ficha
                     inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                     inner join salve.dados_apoio tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
            where oficina_ficha.sq_ficha = ficha.sq_ficha
              and tipo.cd_sistema = 'OFICINA_AVALIACAO'
            order by oficina.dt_fim desc limit 1
            ) as oficina_avaliacao on true
        where ficha.sq_ficha = p_sq_ficha )::json;
end;
$$;

alter function salve.fn_ficha2json(bigint) owner to postgres;
-- xxxxxx

grant execute on function salve.fn_ficha2json(bigint) to usr_salve;

create or replace function salve.fn_ocorrencia_bacias(p_sq_ficha_ocorrencia bigint, p_portalbio boolean DEFAULT false)
    returns TABLE(sq_bacia bigint, sq_bacia_pai bigint, cd_bacia_pai text, cd_bacia_pai_sistema text, cd_bacia text, cd_bacia_sistema text, no_bacia_pai text, no_bacia text, cd_geo text, de_ordem text, tx_geo text)
    language plpgsql
as
$$
    /*
     select * from salve.fn_ocorrencia_bacias(289287)
    */
begin
    if not p_portalbio then
        return query
            select bacia.sq_dados_apoio::bigint as sq_bacia
                 ,bacia_pai.sq_dados_apoio::bigint as sq_bacia_pai
                 ,bacia_pai.cd_dados_apoio::text as cd_bacia_pai
                 ,bacia_pai.cd_sistema::text as cd_bacia_pai_sistema
                 ,bacia.cd_dados_apoio::text as cd_bacia
                 ,bacia.cd_sistema::text as cd_bacia_sistema
                 ,bacia_pai.ds_dados_apoio::text as no_bacia_pai
                 ,bacia.ds_dados_apoio::text as no_bacia
                 ,geo.cd_geo::text as cd_geo
                 ,bacia.de_dados_apoio_ordem::text as de_ordem
                 ,geo.tx_geo::text as tx_geo
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_bacia reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.dados_apoio_geo geo on geo.sq_dados_apoio_geo = reg.sq_bacia_geo
                     inner join salve.dados_apoio bacia on bacia.sq_dados_apoio = geo.sq_dados_apoio
                     inner join salve.dados_apoio bacia_pai on bacia_pai.sq_dados_apoio = bacia.sq_dados_apoio_pai
            where fo.sq_ficha_ocorrencia = p_sq_ficha_ocorrencia;
    else
        return query
            select bacia.sq_dados_apoio::bigint as sq_bacia
                 ,bacia_pai.sq_dados_apoio::bigint as sq_bacia_pai
                 ,bacia_pai.cd_dados_apoio::text as cd_bacia_pai
                 ,bacia_pai.cd_sistema::text as cd_bacia_pai_sistema
                 ,bacia.cd_dados_apoio::text as cd_bacia
                 ,bacia.cd_sistema::text as cd_bacia_sistema
                 ,bacia_pai.ds_dados_apoio::text as no_bacia_pai
                 ,bacia.ds_dados_apoio::text as no_bacia
                 ,geo.cd_geo::text as cd_geo
                 ,bacia.de_dados_apoio_ordem::text as de_ordem
                 ,geo.tx_geo::text as tx_geo
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_bacia reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.dados_apoio_geo geo on geo.sq_dados_apoio_geo = reg.sq_bacia_geo
                     inner join salve.dados_apoio bacia on bacia.sq_dados_apoio = geo.sq_dados_apoio
                     inner join salve.dados_apoio bacia_pai on bacia_pai.sq_dados_apoio = bacia.sq_dados_apoio_pai
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia;

    end if;
end
$$;

comment on function salve.fn_ocorrencia_bacias(bigint, boolean) is 'Função para listar as bacias de um determiando registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_bacias(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencia_bacias_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) returns jsonb
    language plpgsql
as
$$
    /*
     select * from salve.fn_ocorrencia_bacias_json(289287)
     */
begin
    return (
        select json_agg(json_build_object('sq_bacia_pai',bacia.sq_bacia_pai
            ,'cd_bacia_pai',bacia.cd_bacia_pai
            ,'cd_sistema_pai',bacia.cd_bacia_pai_sistema
            ,'sq_bacia',bacia.sq_bacia
            ,'cd_bacia',bacia.cd_bacia
            ,'cd_sistema',bacia.cd_bacia_sistema
            ,'no_bacia_pai',bacia.no_bacia_pai
            ,'no_bacia',bacia.no_bacia
            ,'cd_geo', bacia.cd_geo
            ,'de_ordem',bacia.de_ordem
            ,'tx_geo',bacia.tx_geo
                        )) as js_bacias
        from salve.fn_ocorrencia_bacias(p_sq_ocorrencia, p_portalbio) as bacia
    )::jsonb;
end;
$$;

comment on function salve.fn_ocorrencia_bacias_json(bigint, boolean) is 'Função para gerar objeto json das bacias de um registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_bacias_json(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencia_bioma_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) returns jsonb
    language plpgsql
as
$$
    /*
     select * from salve.fn_ocorrencia_bioma_json(289287)
     */
begin

    if not p_portalbio then
        return (

            select json_agg(json_build_object('sq_bioma',bioma.sq_bioma
                ,'no_bioma', bioma.no_bioma
                            )) as js_bioma
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_bioma reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.bioma bioma on bioma.sq_bioma = reg.sq_bioma
            where fo.sq_ficha_ocorrencia = p_sq_ocorrencia

        )::jsonb;
    else
        return (

            select json_agg(json_build_object('sq_bioma',bioma.sq_bioma
                ,'no_bioma', bioma.no_bioma
                            )) as js_bioma
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_bioma reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.bioma bioma on bioma.sq_bioma = reg.sq_bioma
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia
        )::jsonb;
    end if;
end
$$;

comment on function salve.fn_ocorrencia_bioma_json(bigint, boolean) is 'Função para gerar objeto json do Bioma de um registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_bioma_json(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencia_municipio_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) returns jsonb
    language plpgsql
as
$$
    /*
     select * from salve.fn_ocorrencia_municipio_json(289287)
     */
begin

    if not p_portalbio then
        return (

            select json_agg(json_build_object('sq_municipio',municipio.sq_municipio
                ,'no_municipio', municipio.no_municipio
                ,'co_ibge'  ,municipio.co_ibge
                            )) as js_municipio
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_municipio reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.municipio municipio on municipio.sq_municipio = reg.sq_municipio
            where fo.sq_ficha_ocorrencia = p_sq_ocorrencia

        )::jsonb;
    else
        return (

            select json_agg(json_build_object('sq_municipio',municipio.sq_municipio
                ,'no_municipio', municipio.no_municipio
                ,'co_ibge'  ,municipio.co_ibge
                            )) as js_municipio
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_municipio reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.municipio municipio on municipio.sq_municipio = reg.sq_municipio
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia
        )::jsonb;
    end if;
end;
$$;

comment on function salve.fn_ocorrencia_municipio_json(bigint, boolean) is 'Função para gerar objeto json do Municipio de um registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_municipio_json(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencia_estado_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) returns jsonb
    language plpgsql
as
$$
    /*
     select * from salve.fn_ocorrencia_estado_json(289287)
     */
begin

    if not p_portalbio then
        return (

            select json_agg(json_build_object('sq_estado',uf.sq_estado
                ,'no_estado', uf.no_estado
                ,'sg_estado',uf.sg_estado
                ,'sq_regiao',uf.sq_regiao
                ,'no_regiao',regiao.no_regiao
                ,'co_ibge'  ,''
                            )) as js_estado
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_estado reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.estado uf on uf.sq_estado = reg.sq_estado
                     left join salve.regiao regiao on regiao.sq_regiao =uf.sq_regiao
            where fo.sq_ficha_ocorrencia = p_sq_ocorrencia

        )::jsonb;
    else
        return (

            select json_agg(json_build_object('sq_estado',uf.sq_estado
                ,'no_estado', uf.no_estado
                ,'sg_estado',uf.sg_estado
                ,'sq_regiao',uf.sq_regiao
                ,'no_regiao',regiao.no_regiao
                ,'co_ibge'  ,''
                            )) as js_estado
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_estado reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.estado uf on uf.sq_estado = reg.sq_estado
                     left join salve.regiao regiao on regiao.sq_regiao =uf.sq_regiao
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia
        )::jsonb;
    end if;
end
$$;

comment on function salve.fn_ocorrencia_estado_json(bigint, boolean) is 'Função para gerar objeto json do Estado de um registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_estado_json(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencia_uc(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false)
    returns TABLE(sq_uc bigint, in_esfera text, no_uc text, sg_uc text)
    language plpgsql
as
$$
    /*
    select * from salve.fn_ocorrencia_uc(588907)
     */
begin

    if not p_portalbio then
        return query
            select ucf.sq_uc_federal::bigint as sq_uc
                 , 'F'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_federal ucf
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_federal
            where foreg.sq_ficha_ocorrencia = p_sq_ocorrencia
            UNION
            select ucf.sq_uc_estadual::bigint as sq_uc
                 , 'E'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_estadual ucf
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_estadual
            where foreg.sq_ficha_ocorrencia = p_sq_ocorrencia
            UNION
            select ucf.sq_rppn::bigint as sq_uc
                 , 'R'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_rppn ucf
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_rppn
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_rppn
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_rppn
            where foreg.sq_ficha_ocorrencia = p_sq_ocorrencia;

    else
        return query
            select ucf.sq_uc_federal::bigint
                 , 'F'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_federal ucf
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_federal
            where foreg.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia

            UNION

            select ucf.sq_uc_estadual::bigint
                 , 'E'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_estadual ucf
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_estadual
            where foreg.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia

            UNION

            select ucf.sq_rppn::bigint
                 , 'R'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_rppn ucf
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_rppn
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_rppn
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_rppn
            where foreg.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia;

    end if;
end
$$;

comment on function salve.fn_ocorrencia_uc(bigint, boolean) is 'Função para listar as unidades de conservacao de um registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_uc(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencia_uc_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) returns jsonb
    language plpgsql
as
$$
    /*
        select * from salve.fn_ocorrencia_uc_json(588907)
     */
begin
    return (
        select json_agg(json_build_object(
                'sq_uc',uc.sq_uc
            ,'in_esfera',uc.in_esfera
            ,'no_uc',uc.no_uc
                        )) as js_uc
        from salve.fn_ocorrencia_uc(p_sq_ocorrencia, p_portalbio) as uc
    )::jsonb;
end;
$$;

comment on function salve.fn_ocorrencia_uc_json(bigint, boolean) is 'Função para gerar objeto json das unidades de conservacao e rppn de um registro de ocorrencia';

grant execute on function salve.fn_ocorrencia_uc_json(bigint, boolean) to usr_salve;

create or replace function salve.fn_ocorrencias2json(p_sq_ficha bigint) returns json
    language plpgsql
as
$$begin
    return  (
        select json_build_object(
                       'sq_ficha', ficha.sq_ficha
                   ,'ocorrencias_salve', (
                           select json_agg(
                                          json_build_object(
                                                  'sq_ficha_ocorrencia', fo.sq_ficha_ocorrencia
                                              , 'in_utilizado_avaliacao', fo.in_utilizado_avaliacao
                                              , 'sq_motivo_nao_utilizado_avaliacao', fo.sq_motivo_nao_utilizado_avaliacao
                                              , 'ds_motivo_nao_utilizado_avaliacao', motivo_nao_utilizado.ds_dados_apoio
                                              , 'tx_nao_utilizado_avaliacao', case when fo.sq_motivo_nao_utilizado_avaliacao is null then null::text else salve.entity2char(fo.tx_nao_utilizado_avaliacao) end
                                              , 'dt_invalidado', fo.dt_invalidado
                                              , 'tx_nao_aceita', salve.entity2char(fo.tx_nao_aceita )
                                              , 'dt_alteracao',fo.dt_alteracao
                                              , 'sq_pessoa_alteracao',fo.sq_pessoa_alteracao
                                              , 'no_pessoa_alteracao', pf_alteracao.no_pessoa
                                              , 'dt_inclusao',fo.dt_inclusao
                                              , 'sq_pessoa_inclusao',fo.sq_pessoa_inclusao
                                              , 'no_pessoa_inclusao', pf_inclusao.no_pessoa
                                              , 'sq_situacao',fo.sq_situacao
                                              , 'ds_situacao',situacao.ds_dados_apoio
                                              , 'cd_situacao_sistema',situacao.cd_sistema
                                              , 'in_data_desconhecida',fo.in_data_desconhecida
                                              , 'sq_contexto',fo.sq_contexto
                                              , 'de_contexto', contexto.ds_dados_apoio
                                              , 'cd_contexto_sistema',contexto.cd_sistema
                                              -- aba 2.6.1
                                              ,'georeferenciamento', json_build_object('sq_tipo_registro'     ,fo.sq_tipo_registro
                                                      ,'ds_tipo_registro'     ,tipo_registro.ds_dados_apoio
                                                      ,'nu_dia_registro'      ,fo.nu_dia_registro
                                                      ,'nu_mes_registro'      ,fo.nu_mes_registro
                                                      ,'nu_ano_registro'      ,fo.nu_ano_registro
                                                      ,'nu_dia_registro_fim'  ,fo.nu_dia_registro_fim
                                                      ,'nu_mes_registro_fim'  ,fo.nu_mes_registro_fim
                                                      ,'nu_ano_registro_fim'  ,fo.nu_ano_registro_fim
                                                      ,'dt_registro'          ,fo.dt_registro
                                                      ,'hr_registro'          ,fo.hr_registro
                                                      ,'in_presenca_atual'    ,fo.in_presenca_atual
                                                      ,'in_sensivel'          ,fo.in_sensivel
                                                      ,'sq_prazo_carencia'    ,fo.sq_prazo_carencia
                                                      ,'ds_prazo_carencia'    ,carencia.ds_dados_apoio
                                                      ,'dt_carencia'          ,salve.calc_carencia(fo.dt_inclusao::date,carencia.cd_sistema)
                                                      ,'sq_datum'             ,fo.sq_datum
                                                      ,'ds_datum'             ,datum.ds_dados_apoio
                                                      ,'nu_lat'               , st_y(fo.ge_ocorrencia)
                                                      ,'nu_lon'               , st_x(fo.ge_ocorrencia)
                                                      ,'ge_ocorrencia'        , fo.ge_ocorrencia
                                                      ,'sq_formato_coord_original', fo.sq_formato_coord_original
                                                      ,'ds_formato_coord_original', formato_original.ds_dados_apoio
                                                      ,'sq_precisao_coordenada'   , fo.sq_precisao_coordenada
                                                      ,'ds_precisao_coordenada'   , precisao_coord.ds_dados_apoio
                                                      ,'sq_ref_aproximacao'       , fo.sq_ref_aproximacao
                                                      ,'ds_ref_aproximacao'       , ref_aproximacao.ds_dados_apoio
                                                      ,'sq_metodo_aproximacao', fo.sq_metodo_aproximacao
                                                      ,'ds_metodo_aproximacao', met_aproximacao.ds_dados_apoio
                                                      ,'tx_metodo_aproximacao', salve.entity2char(fo.tx_metodo_aproximacao )
                                                      ,'sq_situacao_avaliacao', fo.sq_situacao_avaliacao
                                                      ,'ds_situacao_avaliacao', situacao_avaliacao.ds_dados_apoio
                                                      ,'cd_situacao_avaliacao_sistema', situacao_avaliacao.cd_sistema
                                                      ,'id_origem', fo.id_origem
                                                      ,'tx_observacao',salve.entity2char(fo.tx_observacao )
                                                      ,'sq_estado', fo.sq_estado
                                                      ,'sq_municipio', fo.sq_municipio
                                                      ,'no_localidade', fo.no_localidade
                                                      ,'estado',salve.fn_ocorrencia_estado_json(fo.sq_ficha_ocorrencia)
                                                      ,'municipio',salve.fn_ocorrencia_municipio_json(fo.sq_ficha_ocorrencia)
                                                      ,'bioma',salve.fn_ocorrencia_bioma_json(fo.sq_ficha_ocorrencia)
                                                      ,'bacias',salve.fn_ocorrencia_bacias_json(fo.sq_ficha_ocorrencia)
                                                      ,'uc',salve.fn_ocorrencia_uc_json(fo.sq_ficha_ocorrencia)
                                                                     ) -- fim aba 2.6.1
                                          -- aba 2.6.2
                                              ,'dados_registro',json_build_object(
                                                          'sq_taxon_citado', fo.sq_taxon_citado
                                                      ,'json_trilha_taxon_citado',taxon_citado.json_trilha
                                                                )

                                              -- aba 2.6.3
                                              ,'localidade',json_build_object('de_local', fo.tx_local
                                                      ,'vl_elevacao_min', fo.vl_elevacao_min
                                                      ,'vl_elevacao_max', fo.vl_elevacao_max
                                                      ,'vl_profundidade_min', fo.vl_profundidade_min
                                                      ,'vl_profundidade_max', fo.vl_profundidade_max
                                                      ,'nu_altitude',fo.nu_altitude
                                                      ,'sq_pais', fo.sq_pais
                                                      ,'no_pais', pais.no_pais
                                                      ,'sq_estado', fo.sq_estado
                                                      ,'no_estado', estado.no_estado
                                                      ,'sg_estado', estado.sg_estado
                                                      ,'sq_municipio', fo.sq_municipio
                                                      ,'no_municipio', municipio.no_municipio
                                                      ,'sq_habitat', fo.sq_habitat
                                                      ,'no_habitat', habitat.ds_dados_apoio

                                                            ) -- fim aba 2.6.3
                                          -- aba 2.6.4
                                              ,'qualificador',json_build_object( 'de_identificador'         , fo.de_identificador
                                                      ,'dt_identificacao'         , fo.dt_identificacao
                                                      ,'sq_qualificador_val_taxon', fo.sq_qualificador_val_taxon
                                                      ,'ds_qualificador_val_taxon', qualificador.ds_dados_apoio
                                                      ,'tx_tombamento'            , fo.tx_tombamento
                                                      ,'tx_instituicao'           , fo.tx_instituicao
                                                              ) -- fim aba 2.6.4

                                          -- aba 2.6.5
                                              ,'responsaveis',json_build_object('sq_pessoa_compilador', fo.sq_pessoa_compilador
                                                      , 'dt_compilacao', fo.dt_compilacao
                                                      , 'sq_pessoa_revisor', fo.sq_pessoa_revisor
                                                      , 'dt_revisao', fo.dt_revisao
                                                      , 'sq_pessoa_validador', fo.sq_pessoa_validador
                                                      , 'no_pessoa_validador', pf_validador.no_pessoa
                                                      , 'dt_validacao', fo.dt_validacao
                                                              ) -- fim aba 2.6.5
                                              ,'referencias',( salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia'))
                                              ,'imagens',(
                                                      select json_agg( json_build_object('sq_ficha_ocorrencia_multimidia', fom.sq_ficha_ocorrencia_multimidia,
                                                                                         'sq_ficha_multimidia', fom.sq_ficha_multimidia) )
                                                      from salve.ficha_ocorrencia_multimidia fom
                                                      where fom.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                                                  )

                                          ) )
                           from salve.ficha_ocorrencia fo
                                    left outer join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                                    left outer join salve.dados_apoio as motivo_nao_utilizado on motivo_nao_utilizado.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                                    left outer join salve.vw_pessoa_fisica as pf_alteracao on pf_alteracao.sq_pessoa = fo.sq_pessoa_alteracao
                                    left outer join salve.vw_pessoa_fisica as pf_inclusao on pf_inclusao.sq_pessoa = fo.sq_pessoa_inclusao
                                    left outer join salve.vw_pessoa_fisica as pf_validador on pf_validador.sq_pessoa = fo.sq_pessoa_validador
                                    left outer join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fo.sq_contexto
                                    left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao
                                    left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                                    left outer join salve.dados_apoio as formato_original on formato_original.sq_dados_apoio = fo.sq_formato_coord_original
                                    left outer join salve.dados_apoio as ref_aproximacao on ref_aproximacao.sq_dados_apoio = fo.sq_ref_aproximacao
                                    left outer join salve.dados_apoio as met_aproximacao on met_aproximacao.sq_dados_apoio = fo.sq_metodo_aproximacao
                                    left outer join salve.dados_apoio as precisao_coord on precisao_coord.sq_dados_apoio = fo.sq_precisao_coordenada
                                    left outer join taxonomia.taxon   as taxon_citado on taxon_citado.sq_taxon = fo.sq_taxon_citado
                                    left outer join salve.vw_pais as pais on pais.sq_pais = fo.sq_pais
                                    left outer join salve.vw_estado as estado on estado.sq_estado = fo.sq_estado
                                    left outer join salve.vw_municipio as municipio on municipio.sq_municipio = fo.sq_municipio
                                    left outer join salve.dados_apoio as habitat on habitat.sq_dados_apoio = fo.sq_habitat
                                    left outer join salve.dados_apoio as qualificador on qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon
                                    left outer join salve.dados_apoio as datum on datum.sq_dados_apoio = fo.sq_datum
                                    left outer join salve.dados_apoio as tipo_registro on tipo_registro.sq_dados_apoio = fo.sq_tipo_registro


                           where fo.sq_ficha = ficha.sq_ficha
                       )

                   ,'ocorrencias_portalbio',(
                           select json_agg(
                                          json_build_object(
                                                  'sq_ficha_ocorrencia_portalbio', fop.sq_ficha_ocorrencia_portalbio
                                              , 'no_base_dados',coalesce(fop.no_base_dados,salve.calc_base_dados_portalbio( fop.tx_ocorrencia, fop.de_uuid ))
                                              , 'de_uuid', fop.de_uuid
                                              , 'no_cientifico', fop.no_cientifico
                                              , 'no_local', fop.no_local
                                              , 'no_instituicao', fop.no_instituicao
                                              , 'no_autor', fop.no_autor
                                              , 'dt_ocorrencia', fop.dt_ocorrencia
                                              , 'dt_carencia', fop.dt_carencia
                                              , 'de_ameaca', fop.de_ameaca
                                              , 'nu_lat', st_y(fop.ge_ocorrencia)
                                              , 'nu_lon', st_x(fop.ge_ocorrencia)
                                              , 'ge_ocorrencia', fop.ge_ocorrencia
                                              , 'sq_situacao', fop.sq_situacao
                                              , 'ds_situacao', situacao.ds_dados_apoio
                                              , 'cd_situacao_sistema', situacao.cd_sistema
                                              , 'sq_pessoa_validador', fop.sq_pessoa_validador
                                              , 'no_pessoa_validador', pf_validador.no_pessoa
                                              , 'dt_validacao', fop.dt_validacao
                                              , 'sq_pessoa_revisor', fop.sq_pessoa_revisor
                                              , 'no_pessoa_revisor', pf_revisor.no_pessoa
                                              , 'dt_revisao', fop.dt_revisao
                                              , 'in_utilizado_avaliacao', fop.in_utilizado_avaliacao
                                              , 'tx_nao_utilizado_avaliacao', case when fop.sq_motivo_nao_utilizado_avaliacao is null then null::text else salve.entity2char(fop.tx_nao_utilizado_avaliacao) end
                                              , 'tx_observacao', fop.tx_observacao
                                              , 'id_origem', fop.id_origem
                                              , 'dt_alteracao', fop.dt_alteracao
                                              , 'sq_pais', fop.sq_pais
                                              , 'sq_estado', fop.sq_estado
                                              , 'sq_municipio', fop.sq_municipio
                                              , 'sq_bioma', fop.sq_bioma
                                              , 'sq_uc_federal', fop.sq_uc_federal
                                              , 'sq_uc_estadual', fop.sq_uc_estadual
                                              , 'sq_rppn', fop.sq_rppn
                                              , 'in_processamento', fop.in_processamento
                                              , 'sq_bacia_hidrografica', fop.sq_bacia_hidrografica
                                              , 'sq_motivo_nao_utilizado_avaliacao', fop.sq_motivo_nao_utilizado_avaliacao
                                              , 'ds_motivo_nao_utilizado_avaliacao', motivo_nao_utilizado.ds_dados_apoio
                                              , 'in_data_desconhecida', fop.in_data_desconhecida
                                              , 'tx_nao_aceita', fop.tx_nao_aceita
                                              , 'st_adicionado_apos_avaliacao', fop.st_adicionado_apos_avaliacao
                                              , 'sq_situacao_avaliacao', fop.sq_situacao_avaliacao
                                              , 'ds_situacao_avaliacao', situacao_avaliacao.ds_dados_apoio
                                              , 'cd_situacao_avaliacao_sistema', situacao_avaliacao.cd_sistema
                                              , 'in_presenca_atual', fop.in_presenca_atual)
                                  )
                           from salve.ficha_ocorrencia_portalbio as fop
                                    left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fop.sq_situacao
                                    left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fop.sq_situacao_avaliacao
                                    left outer join salve.vw_pessoa_fisica as pf_validador on pf_validador.sq_pessoa = fop.sq_pessoa_validador
                                    left outer join salve.vw_pessoa_fisica as pf_revisor on pf_revisor.sq_pessoa = fop.sq_pessoa_validador
                                    left outer join salve.dados_apoio as motivo_nao_utilizado on motivo_nao_utilizado.sq_dados_apoio = fop.sq_motivo_nao_utilizado_avaliacao
                           where fop.sq_ficha = ficha.sq_ficha )
               )::jsonb
        from salve.ficha
        where ficha.sq_ficha = p_sq_ficha )::json;
end;
$$;

grant execute on function salve.fn_ocorrencias2json(bigint) to usr_salve;

create or replace function salve.fn_ficha_limpar_avaliacao(p_sq_ficha bigint) returns boolean
    language plpgsql
as
$$
/*
  Função para limpar todas as informações referentes a Avaliação (aba 11) gravadas na ficha
*/
BEGIN
    -- limpar a aba/subabas 11-Avaliação
    update salve.ficha set
                         -- aba 11.1
        st_ocorrencia_marginal=null
                         ,st_elegivel_avaliacao=null
                         ,st_limitacao_taxonomica_aval=NULL
                         ,st_localidade_tipo_conhecida=null
                         ,st_regiao_bem_amostrada=NULL
                         ,vl_eoo=NULL
                         ,sq_tendencia_eoo=null
                         ,st_flutuacao_eoo=NULL
                         ,vl_aoo=NULL
                         ,sq_tendencia_aoo=null
                         ,st_flutuacao_aoo=null
                         ,ds_justificativa_eoo=null
                         ,ds_justificativa_aoo=null
                         -- aba 11.2
                         ,nu_reducao_popul_passada_rev=null
                         ,sq_tipo_redu_popu_pass_rev=NULL
                         ,nu_reducao_popul_passada=NULL
                         ,sq_tipo_redu_popu_pass=null
                         ,nu_reducao_popul_futura=NULL
                         , sq_tipo_redu_popu_futura=NULL
                         ,nu_reducao_popul_pass_futura=NULL
                         ,sq_tipo_redu_popu_pass_futura=NULL
                         ,sq_declinio_populacional=NULL
                         ,sq_perc_declinio_populacional=NULL
                         ,st_populacao_fragmentada=NULL
                         ,nu_individuo_maduro=NULL
                         ,sq_tenden_num_individuo_maduro=NULL
                         ,st_flut_extrema_indiv_maduro=NULL
                         ,nu_subpopulacao=NULL
                         ,sq_tendencia_num_subpopulacao=NULL
                         ,st_flut_extrema_sub_populacao=NULL
                         ,nu_indivi_subpopulacao_max=NULL
                         ,nu_indivi_subpopulacao_perc=null
                         -- aba 11.3
                         ,nu_localizacoes=NULL
                         ,sq_tenden_num_localizacoes=null
                         ,st_flutuacao_num_localizacoes=NULL
                         ,sq_tenden_qualidade_habitat=NULL
                         ,st_ameaca_futura=NULL
                         ,sq_prob_extincao_brasil=NULL
                         ,ds_metodo_calc_perc_extin_br=NULL
                         -- aba 11.4
                         ,sq_tipo_conectividade=NULL
                         ,sq_tendencia_imigracao=NULL
                         ,st_declinio_br_popul_exterior=null
                         ,ds_conectividade_pop_exterior=NULL
                         -- aba 11.5
                         ,st_favorecido_conversao_habitats=NULL
                         ,st_tem_registro_areas_amplas=NULL
                         ,st_possui_ampla_dist_geografica=NULL
                         ,st_frequente_inventario_eoo=NULL
                         -- abas 11.6 e 11.7
                         , ds_justificativa=null
                         , ds_justificativa_final=null
                         , sq_categoria_iucn=NULL
                         , sq_categoria_final=NULL
                         , st_possivelmente_extinta=NULL
                         , st_possivelmente_extinta_final=NULL
                         , ds_criterio_aval_iucn=NULL
                         , ds_criterio_aval_iucn_final=null
                         , ds_just_mudanca_categoria=null
                         , st_manter_lc=null
                         , ds_citacao = NULL
                         , ds_colaboradores = NULL
                         , ds_equipe_tecnica= null
    where sq_ficha = p_sq_ficha;
    RAISE NOTICE '  - Campos da avaliação, citacao, colaboradores e equipe técnica foram limpos';

    -- Limpar mudança de categoria
    delete from salve.ficha_mudanca_categoria where sq_ficha = p_sq_ficha;
    RAISE NOTICE '  - Motivos mudança de categoria foram excluídos';

    -- Limpar declinio populacional
    delete from salve.ficha_declinio_populacional where sq_ficha = p_sq_ficha;
    RAISE NOTICE '  - Declínio populacional foi excluído';

    RETURN TRUE;
EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    RETURN FALSE;
END;
$$;

comment on function salve.fn_ficha_limpar_avaliacao(bigint) is 'Função para limpar todos os campo das subabas 11.x referentes a avaliação';

alter function salve.fn_ficha_limpar_avaliacao(bigint) owner to postgres;

grant execute on function salve.fn_ficha_limpar_avaliacao(bigint) to usr_salve;

create or replace function salve.fn_ref_bib_json(p_sq_ficha bigint, p_sq_registro bigint DEFAULT NULL::bigint, p_no_coluna text DEFAULT NULL::text) returns jsonb
    language plpgsql
as
$$
begin

    return ( select json_agg(json_build_object(
            'sq_publicacao', refs.sq_publicacao,
            'de_titulo',publicacao.de_titulo,
            'de_ref_bibliografica',publicacao.de_ref_bibliografica,
            'no_autor', coalesce( publicacao.no_autor, refs.no_autor),
            'nu_ano'  , coalesce( publicacao.nu_ano_publicacao, refs.nu_ano),
            'tags', ( select json_agg(json_build_object(
                    'sq_tag',ref_tag.sq_tag,
                    'ds_tag',tag.ds_dados_apoio))
                      from salve.ficha_ref_bib_tag as ref_tag
                               inner join salve.dados_apoio as tag on tag.sq_dados_apoio = ref_tag.sq_tag
                      where ref_tag.sq_ficha_ref_bib = refs.sq_ficha_ref_bib)
                             )
                    )
             from salve.ficha_ref_bib as refs
                      left outer join taxonomia.publicacao on publicacao.sq_publicacao = refs.sq_publicacao
             where refs.sq_ficha = p_sq_ficha
               and (refs.sq_registro = p_sq_registro or p_sq_registro is null)
               and (refs.no_coluna = p_no_coluna or p_no_coluna is null)
    )::jsonb;


end;
$$;

comment on function salve.fn_ref_bib_json(bigint, bigint, text) is 'Função para gerar objeto json das referencias bibliograficas';

alter function salve.fn_ref_bib_json(bigint, bigint, text) owner to postgres;

grant execute on function salve.fn_ref_bib_json(bigint, bigint, text) to usr_salve;

create or replace function salve.fn_versionar_ficha_ciclo_1(p_sq_ficha bigint, p_sq_pessoa bigint, p_sq_contexto bigint) returns json
    language plpgsql
as
$$
DECLARE
    v_context TEXT;
    v_msg TEXT;
    v_log text ARRAY;
    v_array_fichas text ARRAY;
    v_resultado JSON;

    -- ------------
    v_nu_versao_atual       integer;
    v_publicar boolean = false;
    v_sq_taxon bigint;
    v_sq_ficha_versao bigint;
    v_sq_taxon_hist_aval bigint;
    v_cd_contexto text;
    v_sq_avaliacao_naciona_brasil bigint;
    v_sq_categoria_validada bigint;
    v_ds_justificativa_validada text;
    v_cd_categoria_validada text;
    v_ds_criterio_validado text;
    v_pex_validado text;
    v_nu_ano integer;
    v_sq_ficha_ciclo_2 bigint;
    v_sq_ficha_json bigint;
    v_cd_situacao_ficha_ciclo_2 text;
    v_versionar_ficha_ciclo_2 boolean = false;

    --v_sq_situacao_compilacao bigint;
    v_sq_nova_situacao bigint;
    v_mesmo_taxon boolean;
BEGIN

    SET client_min_messages TO NOTICE;

    /*
    -- TESTE ARRAY
    -- select array_agg( json_build_array(sq_ficha,nm_cientifico) ) as fichas
    select array_agg(json_build_object('sq_ficha',sq_ficha,'nm_cientifico', nm_cientifico)) into v_array_fichas
    -- select array_agg( array[sq_ficha::text,nm_cientifico::text] ) into v_array_fichas
    from salve.ficha
    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
    where sq_ficha_ciclo_anterior=25128;

    RAISE NOTICE  ' ';
    RAISE NOTICE  ' ';
    RAISE NOTICE  ' ';
    raise notice 'len: %', array_length(v_array_fichas,1);
    --raise NOTICE 'array: %', v_array_fichas;
    raise NOTICE '1 id: %', (v_array_fichas[1]::json)->'sq_ficha';
    raise NOTICE '1 especie: %', (v_array_fichas[1]::json)->'nm_cientifico';
    raise NOTICE '2 id: %', (v_array_fichas[2]::json)->'sq_ficha';
    raise NOTICE '2 especie: %', (v_array_fichas[2]::json)->'nm_cientifico';
    v_msg = '';
    FOR counter IN 1..2
        LOOP
            if counter > 1 then
                v_msg = v_msg || ', ';
            END IF;
            v_msg = v_msg || ( (v_array_fichas[counter]::json )->>'nm_cientifico')::text;
        END LOOP;
    RAISE NOTICE 'Especies: %', v_msg;
    return
        */


    -- NO PRIMEIRO CICLO A VERSÃO SERÁ SEMPRE A PRIMEIRA
    v_nu_versao_atual = 0;-- ( SELECT COALESCE(MAX(nu_versao), 0) FROM salve.ficha_versao WHERE sq_ficha = p_sq_ficha);
    v_msg = format('  - Versão atual: %s',v_nu_versao_atual);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o contexto do versionamento: Publicação, Exclusão etc
    v_cd_contexto = ( SELECT cd_sistema FROM salve.dados_apoio where sq_dados_apoio = p_sq_contexto);
    v_msg = format('  - Contexto: %s',v_cd_contexto);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o taxon da ficha
    SELECT sq_taxon INTO v_sq_taxon FROM salve.ficha WHERE sq_ficha = p_sq_ficha;
    v_msg = format('  - Taxon (%s)', v_sq_taxon);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%', v_msg;

    IF v_cd_contexto = 'PUBLICACAO' THEN

        -- PARA O 1º CICLO NÃO TEM AS REGRAS PARA PUBLICAÇÃO RELACIONADA AS CATEGORIAS

        -- ler as informações da validação para gerar o registro no taxon_historico_avaliacao no final
        SELECT x.sq_categoria_final, cat_validada.cd_sistema, x.ds_justificativa_final
             , x.ds_criterio_aval_iucn_final, x.st_possivelmente_extinta_final, to_char(x.dt_aceite_validacao,'yyyy')::int
        INTO v_sq_categoria_validada, v_cd_categoria_validada, v_ds_justificativa_validada, v_ds_criterio_validado, v_pex_validado, v_nu_ano
        FROM salve.ficha x
                 LEFT JOIN salve.dados_apoio as cat_validada on cat_validada.sq_dados_apoio = x.sq_categoria_final
        WHERE x.sq_ficha = p_sq_ficha;

        v_msg = format( '  - Categoria Validada (%s) %s', v_cd_categoria_validada, v_sq_categoria_validada);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg = format( '  - Categoria Validada (%s) %s', v_cd_categoria_validada, v_sq_categoria_validada);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        v_msg = format( '  - Critério (%s)', v_ds_criterio_validado);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        v_msg = format( '  - PEX: %s', v_pex_validado);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        v_msg = format( '  - Ano aceite validação: %s', v_nu_ano);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        -- publicar as fichas do 1º ciclo se ela estiver publicada na visao mv_dados_modulo_publico
        SELECT exists ( select null from salve.mv_dados_modulo_publico
                        WHERE sq_ficha = p_sq_ficha limit 1 ) into v_publicar;

        -- não pode existir outra versão publicada para a ficha do 1º ciclo poder ser publicada
        if v_publicar  then
            SELECT not exists ( select null from salve.ficha_versao
                                WHERE sq_ficha = p_sq_ficha and st_publico = true limit 1 ) into v_publicar;
        END IF;
        v_msg = format('  - Publicar a versão? (%s)',v_publicar);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE  '%',v_msg;
    END IF; -- FIM CONTEXTO = PUBLICACAO

    -- REGRAS PARA VERSIONAMENTO DA FICHA DO 1º CICLO

    -- se a ficha tiver uma correspondente no ciclo 2, a versão deve ser gerada para esta ficha do ciclo 2
    -- e se a ficha do 2º ciclo estiver publicada tambem tem que ser versionada gerando a V2
    --SELECT sq_ficha, true into v_sq_ficha_ciclo_2, v_mesmo_taxon FROM salve.ficha
    SELECT array_agg(json_build_object('st_mesmo_taxon',true,'sq_ficha',sq_ficha,'nm_cientifico'
        , coalesce(taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon')
        ,'cd_situacao_sistema',situacao.cd_sistema)) into v_array_fichas
    FROM salve.ficha
             INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
             INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
    WHERE ficha.sq_ciclo_avaliacao = 2
      AND sq_ficha_ciclo_anterior = p_sq_ficha
      AND ficha.sq_taxon = v_sq_taxon;
    --AND situacao.cd_sistema <> 'EXCLUIDA';

    -- se existir somente 1 ficha no 2º ciclo inicializar as variaveis
    v_mesmo_taxon               = false;
    v_sq_ficha_ciclo_2          = null;
    v_cd_situacao_ficha_ciclo_2 = null;

    if v_array_fichas is not null then
        RAISE NOTICE 'MESMO TAXON %',((v_array_fichas[1])::json->>'st_mesmo_taxon');
        v_mesmo_taxon = ((v_array_fichas[1])::json->>'st_mesmo_taxon')::boolean;
        v_sq_ficha_ciclo_2 = ((v_array_fichas[1])::json->>'sq_ficha')::bigint;
        v_cd_situacao_ficha_ciclo_2 = ((v_array_fichas[1])::json->>'cd_situacao_sistema')::text;
    END IF;

    -- se não encontrou a ficha no 2º ciclo com o mesmo taxon, verificar se tem com outro taxon
    if v_array_fichas is null then
        SELECT array_agg(json_build_object('st_mesmo_taxon',false,'sq_ficha',sq_ficha,'nm_cientifico'
            , coalesce(taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon')
                         )) into v_array_fichas
        FROM salve.ficha
                 INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                 INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
        WHERE ficha.sq_ciclo_avaliacao = 2
          AND sq_ficha_ciclo_anterior = p_sq_ficha
          AND ficha.sq_taxon <> v_sq_taxon;
        --AND situacao.cd_sistema <> 'EXCLUIDA';
    END IF;

    v_msg = format( '  - Ficha(s) correspondente(s) no 2º ciclo: %s',v_array_fichas);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    v_msg = format( '  - Mesmo táxon? %s',v_mesmo_taxon);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- se não existir ficha correspondente no 2º ciclo, a ficha do primeiro ciclo deve ser versionadas com V1 apenas
    if v_sq_ficha_ciclo_2 is null then
        SELECT sq_ficha_versao into v_sq_ficha_versao from salve.ficha_versao
        WHERE sq_ficha = p_sq_ficha  and nu_versao = 1 limit 1;
    ELSE
        v_msg = format( '  - Situação da ficha do 2º ciclo %s',v_cd_situacao_ficha_ciclo_2);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        -- se existir a ficha correspondente no 2º ciclo com o mesmo taxon, considerar a V1 como sendo a ficha do 1º ciclo que já foi versionada 1 vez
        SELECT sq_ficha_versao into v_sq_ficha_versao from salve.ficha_versao
        WHERE sq_ficha = v_sq_ficha_ciclo_2  and nu_versao = 1 limit 1;
    END IF;

    -- criar a versão JSON da ficha quando não existir
    IF v_sq_ficha_versao is null THEN
        INSERT INTO salve.ficha_versao (sq_ficha, nu_versao, js_ficha, sq_usuario_inclusao, st_publico, sq_contexto)
        SELECT p_sq_ficha,
               (v_nu_versao_atual + 1),
               salve.fn_ficha2json(p_sq_ficha)
                ,
               p_sq_pessoa
                ,
               v_publicar
                ,
               p_sq_contexto
        RETURNING sq_ficha_versao INTO v_sq_ficha_versao;

        v_msg = format( '  - Versão json criada id: %s', v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%', v_msg;

        -- se não existir a ficha no 2ºciclo, apenas transferir a ficha para o 2º ciclo e alterar a situação para excluida
        if v_sq_ficha_ciclo_2 is null then

            /*
            SELECT dados_apoio.sq_dados_apoio into v_sq_situacao_compilacao
            FROM salve.dados_apoio
            INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
            WHERE pai.cd_sistema = 'TB_SITUACAO_FICHA'
              AND dados_apoio.cd_sistema = 'COMPILACAO'
            LIMIT 1;*/
            SELECT dados_apoio.sq_dados_apoio into v_sq_nova_situacao
            FROM salve.dados_apoio
                     INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
            WHERE pai.cd_sistema = 'TB_SITUACAO_FICHA'
              AND dados_apoio.cd_sistema = 'EXCLUIDA'
            LIMIT 1;

            -- transferir a ficha do 1º para o 2º ciclo
            --update salve.ficha set sq_ciclo_avaliacao = 2, sq_situacao_ficha = v_sq_situacao_compilacao where sq_ficha = p_sq_ficha;
            update salve.ficha set sq_ciclo_avaliacao = 2, sq_situacao_ficha = v_sq_nova_situacao where sq_ficha = p_sq_ficha;
            --v_msg = '  - Ficha transferida para o 2º ciclo e alterada a situação para compilação';
            v_msg = '  - Ficha transferida para o 2º ciclo na situação EXCLUIDA';
            v_log = array_append(v_log,v_msg);
            RAISE NOTICE '%',v_msg;


            -- gerar a mensagem do motivo da exclusão como : Alteração do nome científico para x, y, z...
            v_msg = '';
            if v_array_fichas is not null then
                FOR counter IN 1..array_length(v_array_fichas,1)
                    LOOP
                        if counter > 1 then
                            v_msg = v_msg || ', ';
                        END IF;
                        v_msg = v_msg || concat( '<i>',( (v_array_fichas[counter]::json )->>'nm_cientifico')::text,'</i>');
                    END LOOP;


                -- registrar o motivo da exclusção na tabela ficha_hist_situacao_excluida
                v_msg = concat('Alteração do nome científico para ',v_msg,'.');
                v_log = array_append(v_log,concat('Registrando histórico da exclusão como: ', v_msg ) );
                RAISE NOTICE '%',v_msg;

                INSERT INTO salve.ficha_hist_situacao_excluida( sq_ficha, sq_usuario_inclusao, ds_justificativa )
                VALUES (p_sq_ficha, p_sq_pessoa, v_msg );
            END IF;


            -- a ficha deve passar para o 2º ciclo sem a limpeza das abas e
            /*
            -- limpar dados da avaliação - aba 11, mudanca de categoria, citacao, autoria, colaboradores e equipe tecnica
            if not salve.fn_ficha_limpar_avaliacao(p_sq_ficha) then
                RAISE EXCEPTION 'Não foi possível limpar os dados da avaliação';
            END IF;

            -- desmarcar "Mapa distribuicao principal" da ficha
            update salve.ficha_anexo set in_principal = 'N'
            FROM salve.ficha_anexo fa
                     INNER JOIN salve.dados_apoio as contexto ON contexto.sq_dados_apoio = fa.sq_contexto
            WHERE fa.sq_ficha = p_sq_ficha
              AND contexto.cd_sistema = 'MAPA_DISTRIBUICAO'
              AND fa.in_principal = 'S'
              and fa.sq_ficha_anexo = ficha_anexo.sq_ficha_anexo;

            v_msg = '  - Mapa de distribuição principal foi desmarcado';
            v_log = array_append(v_log,v_msg);
            RAISE NOTICE '%', v_msg;
            */

        ELSE -- existe a ficha no 2º ciclo
        -- se existir a ficha no 2ºciclo com o mesmo taxon criar a V1 dela
            if v_mesmo_taxon then
                -- se existir apenas 1 ficha no 2º criar a V1 dela
                update salve.ficha_versao set sq_ficha = v_sq_ficha_ciclo_2
                                            , js_ficha = jsonb_set(js_ficha,'{_id}',to_jsonb(v_sq_ficha_ciclo_2),false)
                where sq_ficha_versao = v_sq_ficha_versao;
                v_msg = '  - JSON da ficha alterado para a ficha do 2º ciclo';
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;

                -- se a ficha estiver publicada, gerar a v2 dela
                if v_cd_situacao_ficha_ciclo_2 = 'PUBLICADA' then
                    v_versionar_ficha_ciclo_2 = true;
                END IF;
            ELSE

                -- se existir mais de 1 ficha no 2º ciclo com taxons diferentes, passar a ficha para o 2º ciclo como excluida
                -- e criar o historico do motivo da exclusão com o texto padrão
                if array_length(v_array_fichas,1) > 1 then
                    SELECT dados_apoio.sq_dados_apoio into v_sq_nova_situacao
                    FROM salve.dados_apoio
                             INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
                    WHERE pai.cd_sistema = 'TB_SITUACAO_FICHA'
                      AND dados_apoio.cd_sistema = 'EXCLUIDA'
                    LIMIT 1;

                    -- transferir a ficha do 1º para o 2º ciclo como excluida
                    update salve.ficha set sq_ciclo_avaliacao = 2, sq_situacao_ficha = v_sq_nova_situacao where sq_ficha = p_sq_ficha;
                    v_msg = '  - Ficha transferida para o 2º ciclo e alterada a situação para excluída';
                    v_log = array_append(v_log,v_msg);
                    RAISE NOTICE '%',v_msg;

                    -- criar o motivo da exclusão
                    -- concatenar os nomes das especies que ela virou
                    v_msg = '';
                    FOR counter IN 1..array_length(v_array_fichas,1)
                        LOOP
                            if counter > 1 then
                                v_msg = v_msg || ', ';
                            END IF;
                            v_msg = v_msg || concat( '<i>',( (v_array_fichas[counter]::json )->>'nm_cientifico')::text,'</i>');
                        END LOOP;
                    v_msg = '  - Registrando histórico do motivo da exclusão';
                    v_log = array_append(v_log,v_msg);
                    RAISE NOTICE '%',v_msg;

                    -- v_msg = concat('A espécie foi dividida em ',v_msg,'.');
                    v_msg = concat('Alteração do nome científico para ',v_msg,'.');
                    INSERT INTO salve.ficha_hist_situacao_excluida( sq_ficha, sq_usuario_inclusao, ds_justificativa )
                    VALUES (p_sq_ficha, p_sq_pessoa, v_msg);
                    v_log = array_append(v_log,'  - Motivao da exclusão: '||v_msg);
                    RAISE NOTICE '%',v_msg;
                end if;

            end if;
        END IF;
    ELSE -- existe a v_sq_ficha_versao

    -- se a ficha versionada não for a mesma, não sobrescrever o versionamento
        select (js_ficha->>'sq_ficha')::bigint into v_sq_ficha_json from salve.ficha_versao where sq_ficha_versao = v_sq_ficha_versao;
        if v_sq_ficha_json is not null AND v_sq_ficha_json <> p_sq_ficha then
            RAISE EXCEPTION 'Já existe a versão 1 de outra ficha no 2º ciclo';
        END IF;

        update salve.ficha_versao set js_ficha = salve.fn_ficha2json(p_sq_ficha)
                                    ,sq_usuario_inclusao = p_sq_pessoa
                                    ,dt_inclusao = current_timestamp
        where sq_ficha_versao = v_sq_ficha_versao;
        v_msg = format('  - Versão JSON atualizada id: %s',v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    END IF;

    -- garantir somente uma versão publicada
    IF v_publicar = TRUE THEN
        UPDATE salve.ficha_versao
        SET st_publico = FALSE
        WHERE sq_ficha = coalesce(v_sq_ficha_ciclo_2, p_sq_ficha)
          AND st_publico = TRUE
          AND nu_versao <> (v_nu_versao_atual+1);

        v_msg =  '  - Executado update para garantir somente uma versao publicada';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%', v_msg;
    END IF;

    -- versionar / atualizar o versionamento dos registros de ocorrencia
    if not exists ( select null from salve.ocorrencias_json where sq_ficha_versao = v_sq_ficha_versao limit 1) then
        INSERT INTO salve.ocorrencias_json (sq_ficha_versao, js_ocorrencias)  SELECT v_sq_ficha_versao, salve.fn_ocorrencias2json(p_sq_ficha);

        v_msg = '  - Ocorrências versionadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    ELSE
        UPDATE salve.ocorrencias_json set js_ocorrencias = salve.fn_ocorrencias2json(p_sq_ficha) where sq_ficha_versao = v_sq_ficha_versao;
        v_msg = '  - Ocorrências versionadas atualizadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%', v_msg;
    END IF;


    -- bloquear as consultas da ficha
    INSERT INTO salve.ciclo_consulta_ficha_versao(sq_ficha_versao, sq_ciclo_consulta_ficha)
    SELECT v_sq_ficha_versao, ciclo_consulta_ficha.sq_ciclo_consulta_ficha
    FROM salve.ciclo_consulta_ficha
    WHERE sq_ficha = p_sq_ficha
      AND NOT EXISTS(
        SELECT NULL FROM salve.ciclo_consulta_ficha_versao x WHERE x.sq_ciclo_consulta_ficha = ciclo_consulta_ficha.sq_ciclo_consulta_ficha LIMIT 1
    );

    v_msg =  '  - Consultas/Revisões bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- bloquear as oficinas
    insert into salve.oficina_ficha_versao( sq_ficha_versao, sq_oficina_ficha)
    select v_sq_ficha_versao, oficina_ficha.sq_oficina_ficha
    from salve.oficina_ficha
    where oficina_ficha.sq_ficha = p_sq_ficha
      AND NOT EXISTS (
        SELECT NULL FROM salve.oficina_ficha_versao x where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha limit 1
    );
    v_msg = '  - Oficinas bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as colaborações no gride das ocorrencias (Concordo/Discordo) durante as consultas
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = fov.sq_ficha_ocorrencia
    where fo.sq_ficha = p_sq_ficha and not exists (
        select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
    );
    v_msg =  '  - Colaborações Concordo/Discordo do gride de registros SALVE da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as colaborações nas ocorrencias do portalbio
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = fov.sq_ficha_ocorrencia_portalbio
    where fo.sq_ficha = p_sq_ficha and not exists (
        select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
    );

    v_msg= '  - Colaborações Concordo/Discordo do gride de registros PORTALBIO da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear ficha_hist_situacao_excluida
    IF v_cd_contexto = 'EXCLUSAO_OFICINA' or v_cd_contexto = 'EXCLUSAO_LOTE' or v_cd_contexto = 'EXCLUSAO_FICHA' THEN
        INSERT INTO salve.ficha_hist_sit_exc_versao( sq_ficha_versao, sq_ficha_hist_situacao_excluida)
        SELECT v_sq_ficha_versao, hist.sq_ficha_hist_situacao_excluida
        FROM salve.ficha_hist_situacao_excluida hist
        WHERE hist.sq_ficha = p_sq_ficha
        ORDER BY hist.sq_ficha_hist_situacao_excluida desc limit 1;

        v_msg= '  - Histórico dos motivos das exclusões da ficha versionado';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    END IF;


    -- garar registro na tabela taxon_historico_avaliacao
    IF v_cd_contexto = 'PUBLICACAO' THEN

        -- se existir o ano a categoria e a justificativa, adicionar no taxon_historico_avaliacao se não existir
        if v_sq_categoria_validada is not null AND v_ds_justificativa_validada is not null then

            -- pegar o ano da data final da(s) oficina(s) de validação da mesma versão
            if v_nu_ano is null then
                SELECT Max(to_char( oficina_ficha.dt_avaliacao,'yyyy'))::int into v_nu_ano
                FROM salve.oficina_ficha_versao a
                         INNER JOIN salve.oficina_ficha ON oficina_ficha.sq_oficina_ficha = a.sq_oficina_ficha
                         INNER JOIN salve.oficina ON oficina.sq_oficina = oficina_ficha.sq_oficina
                         INNER JOIN salve.dados_apoio AS tipo ON tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                WHERE a.sq_ficha_versao = v_sq_ficha_versao
                  AND tipo.cd_sistema = 'OFICINA_VALIDACAO';
                v_msg = format( '  - Ano da validação lido da ultima oficina de validação: %s',v_nu_ano);
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;
            END IF;

            if v_nu_ano is not null then
                SELECT dados_apoio.sq_dados_apoio into v_sq_avaliacao_naciona_brasil
                FROM salve.dados_apoio
                         INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
                WHERE pai.cd_sistema = 'TB_TIPO_AVALIACAO'
                  AND dados_apoio.cd_sistema = 'NACIONAL_BRASIL'
                LIMIT 1;

                if not exists( select null from salve.taxon_historico_avaliacao th
                               where th.sq_taxon = v_sq_taxon
                                 and th.nu_ano_avaliacao = v_nu_ano
                                 and th.sq_tipo_avaliacao = v_sq_avaliacao_naciona_brasil
                                 and th.sq_categoria_iucn = v_sq_categoria_validada
                                 and coalesce( th.de_criterio_avaliacao_iucn,'') = coalesce( v_ds_criterio_validado,'')
                                 and coalesce(th.st_possivelmente_extinta,'') = coalesce(v_pex_validado,'')
                                 and th.tx_justificativa_avaliacao = v_ds_justificativa_validada
                               limit 1
                ) then
                    INSERT INTO salve.taxon_historico_avaliacao ( sq_taxon,  nu_ano_avaliacao, sq_tipo_avaliacao
                                                                , sq_categoria_iucn, de_criterio_avaliacao_iucn
                                                                , st_possivelmente_extinta, tx_justificativa_avaliacao)
                    values ( v_sq_taxon, v_nu_ano, v_sq_avaliacao_naciona_brasil,v_sq_categoria_validada, v_ds_criterio_validado, v_pex_validado, v_ds_justificativa_validada)
                    RETURNING sq_taxon_historico_avaliacao into v_sq_taxon_hist_aval;
                    v_msg =  format('  - Criado registro na tabela Taxon Historico Avaliação (%s)', v_sq_taxon_hist_aval) ;
                    v_log = array_append(v_log,v_msg);
                    RAISE NOTICE '%',v_msg;
                    IF v_sq_taxon_hist_aval IS NOT NULL THEN
                        insert into salve.taxon_historico_avaliacao_versao(sq_taxon_historico_avaliacao, sq_ficha_versao) values (v_sq_taxon_hist_aval,v_sq_ficha_versao);
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;

    -- se a ficha do 2º ciclo ainda não estiver versionada, gerar a versão
    if v_versionar_ficha_ciclo_2 then
        v_msg = format( '  - iniciando o versionamento da ficha do 2º ciclo PUBLICADA e com o mesmo táxon');
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    END IF;

    v_msg = format( '  - FIM versionamento da ficha %s as %s',p_sq_ficha, current_timestamp);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- CANCELAR TUDO - PARA TESTE APENAS
    -- RAISE EXCEPTION 'Teste fn_versionar_ficha_ciclo_1';

    -- registrar log
    DELETE FROM salve.ficha_versionamento where sq_ficha = p_sq_ficha;
    INSERT INTO salve.ficha_versionamento (sq_ficha, in_status, ds_log, dt_inclusao) VALUES (p_sq_ficha, 0, array_to_string(v_log,chr(13)), current_timestamp);


    -- se a ficha do 2º ciclo ainda não estiver versionada, gerar a versão
    if v_versionar_ficha_ciclo_2 then
        if not exists( select null from salve.ficha_versao
                       where sq_ficha = v_sq_ficha_ciclo_2
                         and sq_ficha_versao <> v_sq_ficha_versao LIMIT 1) then
            v_resultado = salve.fn_versionar_ficha_ciclo_2(v_sq_ficha_ciclo_2,p_sq_pessoa,p_sq_contexto);
        end if;
    END IF;


    return json_build_object('status',0,'msg', 'Ficha versionada com SUCESSO','type','success','log',array_to_string(v_log,chr(13)) );

EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    get stacked diagnostics
        v_context = pg_exception_context;
    raise notice E'Got exception:
        context: %', v_context;
    return json_build_object('status',1,'type','error','msg', concat('Mensagem de erro: ',SQLERRM,CHR(13),v_context),'log',array_to_string(v_log,chr(13)) );
END
$$;

alter function salve.fn_versionar_ficha_ciclo_1(bigint, bigint, bigint) owner to postgres;

grant execute on function salve.fn_versionar_ficha_ciclo_1(bigint, bigint, bigint) to usr_salve;

create or replace function salve.fn_versionar_ficha_ciclo_2(p_sq_ficha bigint, p_sq_pessoa bigint, p_sq_contexto bigint) returns json
    language plpgsql
as
$$
DECLARE
    v_context text;
    v_msg TEXT;
    v_log text ARRAY;
    -- -----------------
    v_nu_versao_atual       integer;
    v_publicar boolean = false;
    v_sq_taxon bigint;
    v_sq_ficha_versao bigint;
    v_sq_taxon_hist_aval bigint;
    v_cd_contexto text;
    v_cd_categoria_hist text;
    v_sq_categoria_avaliada bigint;
    v_sq_categoria_validada bigint;
    v_cd_categoria_avaliada text;
    v_cd_categoria_validada text;
    v_ds_criterio_validado text;
    v_pex_validado text;
    v_nu_ano integer;
    v_sq_nacional_brasil bigint;
    v_sq_situacao_utilizado bigint; -- registros utilizado na avaliacao
    v_sq_situacao_apos_avaliacao bigint; -- registro adicionado apos a avaliação
    v_sq_situacao_publicada bigint;
    v_sq_situacao_compilacao bigint;

BEGIN

    -- FICHAS DO 2º CICLO CALCULAM A PROXIMA VERSAO
    v_nu_versao_atual = ( SELECT COALESCE(MAX(nu_versao), 0) FROM salve.ficha_versao WHERE sq_ficha = p_sq_ficha);
    v_msg = format('  - Versão atual: %s',v_nu_versao_atual);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o contexto do versionamento: Publicação, Exclusão etc
    v_cd_contexto = ( SELECT cd_sistema FROM salve.dados_apoio where sq_dados_apoio = p_sq_contexto);
    v_msg = format('  - Contexto: %s',v_cd_contexto);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o taxon da ficha
    SELECT sq_taxon INTO v_sq_taxon FROM salve.ficha WHERE sq_ficha = p_sq_ficha;
    v_msg= format('  - Taxon (%s)', v_sq_taxon);


    /************************************************************************************************
    REGRAS PARA PUBLICAR AUTOMATICAMENTE APÓS O VERSIONAMENTO
        - ficha tem que ser do segundo ciclo
        - se a ficha era ameaçada (EN, VU, CR) e permanece ameaçada na MESMA CATEGORIA pode publicar
        - se a ficha era não ameaçada e PERMANECE não ameaçada pode publicar
        - se a ficha era ameaçada e permanece ameaçada em outra categoria NÃO pode publicar
        - demais situações (entrada e saida da lista oficial ) não pode publicar
    **************************************************************************************************/

    IF v_cd_contexto = 'PUBLICACAO' THEN
        -- ler as categorias avaliada e validada
        v_msg = '  - Ler categorias Avaliada e Validada para aplicar as regras da publicação automática';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        SELECT ficha.sq_categoria_iucn, ficha.sq_categoria_final
             , ficha.st_possivelmente_extinta_final, cat_avaliada.cd_sistema, cat_validada.cd_sistema
        INTO v_sq_categoria_avaliada, v_sq_categoria_validada, v_pex_validado, v_cd_categoria_avaliada, v_cd_categoria_validada
        FROM salve.ficha
                 LEFT JOIN salve.dados_apoio as cat_avaliada on cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                 LEFT JOIN salve.dados_apoio as cat_validada on cat_validada.sq_dados_apoio = ficha.sq_categoria_final
        WHERE ficha.sq_ficha = p_sq_ficha;

        v_msg= format('  - Categoria Avaliada (%s) %s', v_cd_categoria_avaliada, v_sq_categoria_avaliada);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - Categoria Validada (%s) %s', v_cd_categoria_validada, v_sq_categoria_validada);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - Critério (%s)', v_ds_criterio_validado);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - PEX: %s', v_pex_validado);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - Ano aceite validação: %s', v_nu_ano);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        IF v_sq_categoria_validada IS NULL THEN
            -- RAISE EXCEPTION 'Ficha não está validada (ABA 11.2 em branco)';
        END IF;

        -- encontrar a categoria atual no historico
        select a.sq_dados_apoio into v_sq_nacional_brasil from salve.dados_apoio a
                                                                   inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'NACIONAL_BRASIL'
          and pai.cd_sistema = 'TB_TIPO_AVALIACAO'
        LIMIT 1;
        v_msg = format( '  - Encontrado tipo NACIONAL BRASIL (%s)', v_sq_nacional_brasil);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        select cat.cd_sistema into v_cd_categoria_hist from salve.taxon_historico_avaliacao h
                                                                inner join salve.dados_apoio cat on cat.sq_dados_apoio = h.sq_categoria_iucn
        where h.sq_taxon = v_sq_taxon
          and h.sq_tipo_avaliacao = v_sq_nacional_brasil
        order by h.nu_ano_avaliacao desc, h.sq_taxon_historico_avaliacao desc limit 1;
        v_msg = format( '  - Categoria historico atual (%s)',v_cd_categoria_hist);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        if v_cd_categoria_hist in ( 'CR','VU','EN') then
            -- AMEAÇADA
            if v_cd_categoria_validada = v_cd_categoria_hist then
                -- PERMENECE AMEACADA NA MESMA CATEGORIA
                v_publicar=true;
            END IF;

        ELSE
            -- NÃO AMEAÇADA
            if not v_cd_categoria_validada in ( 'CR','VU','EN') then
                -- PERMANECE NÃO AMEAÇADA
                v_publicar=true;
            END IF;

        END IF;
        v_msg =   format('  - Publicar a versão? (%s)',v_publicar);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF; -- FIM CONTEXTO = PUBLICACAO

    -- se a versão já existir apenas fazer o update do json
    v_sq_ficha_versao=null;

    -- verificar se ja tem a versão criada
    select sq_ficha_versao into v_sq_ficha_versao from salve.ficha_versao
    where sq_ficha = p_sq_ficha
      and nu_versao = (v_nu_versao_atual+1) limit 1;

    if v_sq_ficha_versao is null then
        INSERT INTO salve.ficha_versao (sq_ficha, nu_versao, js_ficha, sq_usuario_inclusao, st_publico, sq_contexto)
        SELECT p_sq_ficha,
               (v_nu_versao_atual + 1),
               salve.fn_ficha2json(p_sq_ficha)
                ,
               p_sq_pessoa
                ,
               v_publicar
                ,
               p_sq_contexto
        RETURNING sq_ficha_versao INTO v_sq_ficha_versao;

        v_msg =  format('  - Versão json CRIADA id: %s', v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        -- garantir somente uma versão publicada
        IF v_publicar = TRUE THEN
            UPDATE salve.ficha_versao
            SET st_publico = FALSE
            WHERE sq_ficha = p_sq_ficha
              AND st_publico = TRUE
              AND nu_versao <> (v_nu_versao_atual+1);

            v_msg = '  - Executado update para garantir somente uma versao publicada';
            v_log = array_append(v_log,v_msg);
            RAISE NOTICE '%',v_msg;

        END IF;

    ELSE
        update salve.ficha_versao set js_ficha = salve.fn_ficha2json(p_sq_ficha)
                                    ,sq_usuario_inclusao = p_sq_pessoa
                                    ,dt_inclusao = current_timestamp
        where sq_ficha_versao = v_sq_ficha_versao;

        v_msg = format('  - Versão json ATUALIZADA id: %s', v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF;

    -- versionar / atualizar o versionamento dos registros de ocorrencia
    if not exists ( select null from salve.ocorrencias_json where sq_ficha_versao = v_sq_ficha_versao limit 1) then
        INSERT INTO salve.ocorrencias_json (sq_ficha_versao, js_ocorrencias)  SELECT v_sq_ficha_versao, salve.fn_ocorrencias2json(p_sq_ficha);

        v_msg = '  - Ocorrências versionadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    ELSE
        UPDATE salve.ocorrencias_json set js_ocorrencias = salve.fn_ocorrencias2json(p_sq_ficha) where sq_ficha_versao = v_sq_ficha_versao;

        v_msg =  '  - Ocorrências versionadas atualizadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF;


    -- bloquear as consultas da ficha
    INSERT INTO salve.ciclo_consulta_ficha_versao(sq_ficha_versao, sq_ciclo_consulta_ficha)
    SELECT v_sq_ficha_versao, ciclo_consulta_ficha.sq_ciclo_consulta_ficha
    FROM salve.ciclo_consulta_ficha
    WHERE sq_ficha = p_sq_ficha
      AND NOT EXISTS(
        SELECT NULL FROM salve.ciclo_consulta_ficha_versao x WHERE x.sq_ciclo_consulta_ficha = ciclo_consulta_ficha.sq_ciclo_consulta_ficha LIMIT 1
    );

    v_msg =  '  - Consultas/Revisões bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as oficinas
    insert into salve.oficina_ficha_versao( sq_ficha_versao, sq_oficina_ficha)
    select v_sq_ficha_versao, oficina_ficha.sq_oficina_ficha
    from salve.oficina_ficha
    where oficina_ficha.sq_ficha = p_sq_ficha
      AND NOT EXISTS (
        SELECT NULL FROM salve.oficina_ficha_versao x where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha limit 1
    );

    v_msg= '  - Oficinas bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- bloquear as colaborações no gride das ocorrencias (Concordo/Discordo) durante as consultas
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = fov.sq_ficha_ocorrencia
    where fo.sq_ficha = p_sq_ficha and not exists (
        select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
    );

    v_msg = '  - Colaborações Concordo/Discordo do gride de registros SALVE da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as colaborações nas ocorrencias do portalbio
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = fov.sq_ficha_ocorrencia_portalbio
    where fo.sq_ficha = p_sq_ficha and not exists (
        select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
    );

    v_msg = '  - Colaborações Concordo/Discordo do gride de registros PORTALBIO da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- bloquear ficha_hist_situacao_excluida
    IF v_cd_contexto = 'EXCLUSAO_OFICINA' or v_cd_contexto = 'EXCLUSAO_LOTE' or v_cd_contexto = 'EXCLUSAO_FICHA' THEN
        INSERT INTO salve.ficha_hist_sit_exc_versao( sq_ficha_versao, sq_ficha_hist_situacao_excluida)
        SELECT v_sq_ficha_versao, hist.sq_ficha_hist_situacao_excluida
        FROM salve.ficha_hist_situacao_excluida hist
        WHERE hist.sq_ficha = p_sq_ficha
        ORDER BY hist.sq_ficha_hist_situacao_excluida desc limit 1;

        v_msg = '  - Histórico dos motivos das exclusões da ficha versionado';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF;

    if v_cd_contexto = 'PUBLICACAO' then
        /*
        - CALCULAR O ANO DA AVALIAÇAO OU DA VALIDAÇÃO - VER REGRAS
        - ANO da gravação da aba 7.1 deve ser calculado com a seguinte regra:
        - aba 11.6 = aba 11.7 -> ano da gravação da justificativa da avaliação
        - aba 11.6 <> aba 11.7 -> ano data da validação da ficha
        */
        if v_sq_categoria_avaliada is not null and v_sq_categoria_validada is not null then
            if v_sq_categoria_avaliada = v_sq_categoria_validada then
                RAISE NOTICE '  - Categoria Aba 11.6 = aba 11.7 -> pegar o ano da ÚLTIMA avaliação';
                v_nu_ano = ( SELECT Max(to_char(oficina_ficha.dt_avaliacao,'yyyy'))::int
                             FROM salve.oficina_ficha_versao a
                                      INNER JOIN salve.oficina_ficha ON oficina_ficha.sq_oficina_ficha = a.sq_oficina_ficha
                                      INNER JOIN salve.oficina ON oficina.sq_oficina = oficina_ficha.sq_oficina
                                      INNER JOIN salve.dados_apoio AS tipo ON tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                             WHERE a.sq_ficha_versao = v_sq_ficha_versao
                               AND tipo.cd_sistema = 'OFICINA_AVALIACAO');

                v_msg = format( '  - Ano lido da ultima oficina de avaliação: %s',v_nu_ano);
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;
            ELSE
                v_nu_ano = (SELECT to_char(x.dt_aceite_validacao,'yyyy')::int from salve.ficha x where x.sq_ficha = p_sq_ficha);

                v_msg = format('  - Ano lido da ficha dt_aceite_validacao: %s',v_nu_ano);
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;
            END IF;

            if v_nu_ano > 0 then
                -- criar historico se não existir ainda
                with dados_historico as (SELECT ficha.sq_taxon
                                              , v_nu_ano as nu_ano -- TO_CHAR(NOW(), 'YYYY')::int as nu_ano_avaliacao
                                              , v_sq_nacional_brasil as sq_tipo_avaliacao
                                              , ficha.sq_categoria_final
                                              , ficha.ds_criterio_aval_iucn_final
                                              , ficha.st_possivelmente_extinta_final
                                              , ficha.ds_justificativa_final
                                         FROM salve.ficha
                                         WHERE sq_ficha = p_sq_ficha
                )
                INSERT INTO salve.taxon_historico_avaliacao ( sq_taxon,  nu_ano_avaliacao, sq_tipo_avaliacao
                                                            , sq_categoria_iucn, de_criterio_avaliacao_iucn
                                                            , st_possivelmente_extinta, tx_justificativa_avaliacao)
                select dh.sq_taxon,dh.nu_ano,dh.sq_tipo_avaliacao
                     ,dh.sq_categoria_final
                     ,dh.ds_criterio_aval_iucn_final
                     ,dh.st_possivelmente_extinta_final
                     ,dh.ds_justificativa_final
                from dados_historico dh
                where NOT exists (
                    select null from salve.taxon_historico_avaliacao th
                    where th.sq_taxon = dh.sq_taxon
                      and th.nu_ano_avaliacao = dh.nu_ano
                      and th.sq_tipo_avaliacao = dh.sq_tipo_avaliacao
                      and th.sq_categoria_iucn = dh.sq_categoria_final
                      and coalesce( th.de_criterio_avaliacao_iucn,'') = coalesce( dh.ds_criterio_aval_iucn_final,'')
                      and coalesce(th.st_possivelmente_extinta,'') = coalesce(dh.st_possivelmente_extinta_final,'')
                      and th.tx_justificativa_avaliacao = dh.ds_justificativa_final
                    limit 1
                ) RETURNING sq_taxon_historico_avaliacao into v_sq_taxon_hist_aval;
                v_msg = format('  - Criado registro na tabela Taxon Historico Avaliação (%s)', v_sq_taxon_hist_aval) ;
                IF v_sq_taxon_hist_aval IS NOT NULL THEN
                    insert into salve.taxon_historico_avaliacao_versao(sq_taxon_historico_avaliacao, sq_ficha_versao) values (v_sq_taxon_hist_aval,v_sq_ficha_versao);
                END IF;
            END IF;
        END IF;
        -- Regras após o versionamento

        -- 1) Desmarcar "Mapa distribuicao principal" da ficha
        update salve.ficha_anexo set in_principal = 'N'
        FROM salve.ficha_anexo fa
                 INNER JOIN salve.dados_apoio as contexto ON contexto.sq_dados_apoio = fa.sq_contexto
        WHERE fa.sq_ficha = p_sq_ficha
          AND contexto.cd_sistema = 'MAPA_DISTRIBUICAO'
          AND fa.in_principal = 'S'
          and fa.sq_ficha_anexo = ficha_anexo.sq_ficha_anexo;

        select a.sq_dados_apoio into v_sq_situacao_utilizado from salve.dados_apoio a
                                                                      inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO'
          and pai.cd_sistema = 'TB_SITUACAO_REGISTRO_OCORRENCIA'
        LIMIT 1;
        RAISE  NOTICE '  - Lendo situação registro utilizado (%)',v_sq_situacao_utilizado;

        select a.sq_dados_apoio into v_sq_situacao_apos_avaliacao from salve.dados_apoio a
                                                                           inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO'
          and pai.cd_sistema = 'TB_SITUACAO_REGISTRO_OCORRENCIA'
        LIMIT 1;
        RAISE  NOTICE '  - Lendo situação adicionado após avaliação (%)',v_sq_situacao_apos_avaliacao;

        -- registro "Adicionados após a avaliação" para "Utilizados na avaliação"
        update salve.ficha_ocorrencia
        set sq_situacao_avaliacao = v_sq_situacao_utilizado
        where sq_ficha = p_sq_ficha
          and sq_situacao_avaliacao = v_sq_situacao_apos_avaliacao;
        RAISE  NOTICE '  - Registros adicionado após avaliação convertidos para utilizados na avaliação';

        -- limpar dados da avaliação - aba 11, mudanca de categoria, citacao, autoria, colaboradores e equipe tecnica
        if not salve.fn_ficha_limpar_avaliacao(p_sq_ficha) then
            RAISE EXCEPTION 'Não foi possível limpar os dados da avaliação';
        END IF;

        -- se a ficha estiver PUBLICADA voltar para COMPILAÇÃO
        select a.sq_dados_apoio into v_sq_situacao_compilacao from salve.dados_apoio a
                                                                       inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'COMPILACAO'
          and pai.cd_sistema = 'TB_SITUACAO_FICHA'
        LIMIT 1;
        select a.sq_dados_apoio into v_sq_situacao_publicada from salve.dados_apoio a
                                                                      inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'PUBLICADA'
          and pai.cd_sistema = 'TB_SITUACAO_FICHA'
        LIMIT 1;
        update salve.ficha set sq_situacao_ficha = v_sq_situacao_compilacao
        Where ficha.sq_ficha = p_sq_ficha
          and ficha.sq_situacao_ficha = v_sq_situacao_publicada;

    END IF; -- fim if PUBLICACAO
    v_msg = format( '  - FIM versionamento da ficha %s as %s',p_sq_ficha, current_timestamp);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- registrar log
    DELETE FROM salve.ficha_versionamento where sq_ficha = p_sq_ficha;
    INSERT INTO salve.ficha_versionamento (sq_ficha, in_status, ds_log, dt_inclusao) VALUES (p_sq_ficha, 0, array_to_string(v_log,chr(13)), current_timestamp);


    -- RAISE EXCEPTION 'OK TUDO CERTO FICHA 2º CICLO';
    return json_build_object('status',0,'msg', 'Ficha versionada com SUCESSO','type','success','log',array_to_string(v_log,chr(13)) );

EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    get stacked diagnostics
        v_context = pg_exception_context;
    --- UPDATE SALVE.FICHA SET ds_diagnostico_morfologico = v_context WHERE SQ_FICHA=p_sq_ficha;
    raise notice E'Got exception:
            context: %', v_context;
    return json_build_object('status',1,'type','error','msg', concat('Mensagem de erro: ',SQLERRM,CHR(13),v_context),'log',array_to_string(v_log,chr(13)) );

END
$$;

alter function salve.fn_versionar_ficha_ciclo_2(bigint, bigint, bigint) owner to postgres;

grant execute on function salve.fn_versionar_ficha_ciclo_2(bigint, bigint, bigint) to usr_salve;

create or replace function salve.fn_versionar_ficha(p_sq_ficha bigint, p_sq_pessoa bigint, p_sq_contexto bigint) returns json
    language plpgsql
as
$$
DECLARE
    v_status integer;
    v_sq_ciclo_avaliacao bigint;
    v_nm_cientifico text;
    v_resultado JSON;
    v_msg text;
    v_array text ARRAY;
    v_type text = 'success';
BEGIN
    SET client_min_messages TO NOTICE;--WARNING;

    select sq_ciclo_avaliacao, nm_cientifico into v_sq_ciclo_avaliacao, v_nm_cientifico from salve.ficha where sq_ficha = p_sq_ficha;

    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE '--------------------------------------------------- ';
    v_msg =  format('Iniciando versionamento da ficha %s / sq_ficha: %s / %sº ciclo',TRIM(v_nm_cientifico), p_sq_ficha, v_sq_ciclo_avaliacao);
    RAISE NOTICE '%',v_msg;
    v_array = array_append(v_array,v_msg);
    v_status = 0;
    if v_sq_ciclo_avaliacao = 1 then
        v_msg = '  - versionando ficha do ciclo 1...';
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
        v_resultado = salve.fn_versionar_ficha_ciclo_1(p_sq_ficha, p_sq_pessoa , p_sq_contexto);
    ELSEIF v_sq_ciclo_avaliacao = 2 then
        v_msg = '  - versionando ficha do ciclo 2...';
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
        v_resultado = salve.fn_versionar_ficha_ciclo_2(p_sq_ficha, p_sq_pessoa , p_sq_contexto);
    END IF;

    if v_resultado->>'status'::text = '0'  then
        v_status = 0;
        v_type = 'success';
        v_msg = format('  - ficha do ciclo %s versionada com SUCESSO', v_sq_ciclo_avaliacao);
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
    ELSE
        v_status = 1;
        v_type = 'error';
        v_msg = format('  - ERRO ao versionar a ficha do ciclo %s', v_sq_ciclo_avaliacao);
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
    END IF;
    SET client_min_messages TO NOTICE;

    -- RAISE EXCEPTION 'TESTE fn_versionar_ficha()';


    return json_build_object('status',v_status
        ,'type',v_type
        ,'msg', array_to_string(v_array,chr(13))
        ,'data',v_resultado);

EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    get stacked diagnostics
        v_msg = pg_exception_context;
    raise notice E'Got exception:
            context: %', v_msg;
    SET client_min_messages TO NOTICE;
    return json_build_object('status',1
        ,'type',v_type
        ,'msg', CONCAT( SQLERRM,CHR(13),v_msg,CHR(13),array_to_string(v_array,chr(13)) )
        ,'data',v_resultado );
END
$$;

alter function salve.fn_versionar_ficha(bigint, bigint, bigint) owner to postgres;

grant execute on function salve.fn_versionar_ficha(bigint, bigint, bigint) to usr_salve;

create or replace function salve.fn_json_get_csv(p_jsonb jsonb, p_column text) returns text
    language plpgsql
as
$$
BEGIN
    IF p_jsonb::text = 'null' then
        return '';
    end if;
    return (select array_to_string( array_agg(values::text),', ') as values from
        (SELECT JSONB_ARRAY_ELEMENTS( p_jsonb ) ->> p_column) as t(values));
END
$$;

alter function salve.fn_json_get_csv(jsonb, text) owner to postgres;

grant execute on function salve.fn_json_get_csv(jsonb, text) to usr_salve;

create or replace function salve.fn_array_unique(a text[]) returns text[]
    language sql
as
$$
select array (
               select distinct v from unnest(a) as b(v)
       )
$$;

alter function salve.fn_array_unique(text[]) owner to postgres;

create or replace function salve.fn_ficha_select(p_json jsonb DEFAULT NULL::jsonb)
    returns TABLE(sq_ficha bigint, sq_taxon bigint, nm_cientifico text, st_transferida boolean, st_manter_lc boolean)
    language plpgsql
as
$$
    /*
      funcao com parametros dinamicos no formato json para filtragem das fichas. Exemplos:
      - selecionar as fichas do primeiro ciclo
        select * from salve.fn_ficha_select('{"sq_ciclo_avaliacao":1}');
      - selecionar as fichas do segundo ciclo das categorias 133, 134
        select * from salve.fn_ficha_select('{"sq_ciclo_avaliacao":2, 'sq_categoria_iucn','133,134'}');
    */
declare
    cmdSql text;
    sqlTemp text;
    strTemp text;
    arrCamposAbas text ARRAY;
    sqlWhere text ARRAY;
    sqlOr text ARRAY;
    sqlInnerJoin text ARRAY;
begin
    SET client_min_messages TO WARNING;
    cmdSql = 'select ficha.sq_ficha::bigint' ||
             ',ficha.sq_taxon::bigint' ||
             ',ficha.nm_cientifico::text' ||
             ',ficha.st_transferida::boolean' ||
             ',ficha.st_manter_lc::boolean' ||
             ' from salve.ficha';

    -- FILTRO PELO CICLO
    if p_json->>'sq_ciclo_avaliacao' is not null then
        sqlWhere = array_append(sqlWhere,format('ficha.sq_ciclo_avaliacao = %s',p_json->>'sq_ciclo_avaliacao'));
    end if;

    -- FILTRO PELO SQ_FICHA
    if p_json->>'sq_ficha' is not null then
        sqlWhere = array_append(sqlWhere,format('ficha.sq_ficha = %s',p_json->>'sq_ficha'));
    end if;

    -- FILTRO PELA UNIDADE ORG
    if p_json->>'sq_unidade_org' is not null then
        sqlWhere = array_append(sqlWhere, format('ficha.sq_unidade_org = %s',p_json->>'sq_unidade_org') );
    end if;

    -- FILTRO PELA(s) OFICINA(s) e PELA CATEGORIA DA OFICINA DE AVALIAÇÃO
    if p_json->>'sq_oficina' is not null  then
        sqlInnerJoin = array_append( sqlInnerJoin,'inner join salve.oficina_ficha on oficina_ficha.sq_ficha = ficha.sq_ficha');
        sqlWhere = array_append(sqlWhere, format('oficina_ficha.sq_oficina in (%s)',p_json->>'sq_oficina' ) );
        if p_json->>'sq_categoria_oficina' is not null then
            sqlWhere = array_append(sqlWhere, format('oficina_ficha.sq_categoria_iucn in (%s)',p_json->>'sq_categoria_oficina' ) );
        END IF;
        if p_json->>'ds_agrupamento' is not null then
            sqlWhere = array_append(sqlWhere, format('oficina_ficha.ds_agrupamento ILIKE ''%%%s%%''',trim( replace( p_json->>'ds_agrupamento','%','') ) ) );
        END IF;

    end if;

    -- FILTRAR SOMENTE AS FICHAS ATRIBUIDAS PARA A PESSOA NO MODULO FUNÇÃO
    if p_json->>'sq_pessoa' is not null then
        sqlTemp = format('ficha.sq_ficha in ( select ficha_pessoa.sq_ficha from salve.ficha_pessoa'
                             || ' inner join salve.dados_apoio papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel'
                             || ' where ficha_pessoa.sq_pessoa = %s and ficha_pessoa.in_ativo = ''S'')',p_json->>'sq_pessoa');
        sqlWhere = array_append(sqlWhere,sqlTemp);
    END IF;

    -- FILTRAR PELO(s) NOME(s) CIENTIFICO(s) OU PARTE DO NOME CIENTIFICO
    if p_json->>'nm_cientifico' is not null then
        -- o nome cientifico pode ser 1 ou vários separados por ;
        sqlOr = null;
        FOREACH strTemp IN ARRAY string_to_array(replace(p_json->>'nm_cientifico',',',';'), ';')
            LOOP
                sqlTemp = format('ficha.nm_cientifico ILIKE ''%%%s%%''',trim( replace( strTemp,'%','') ) );
                sqlOr = array_append(sqlOr, sqlTemp);
            END LOOP;
        sqlTemp = concat('(', array_to_string( salve.fn_array_unique(sqlOr ),chr(13)||' OR ' ) , ')');
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELO NOME COMUM
    if p_json->>'no_comum' is not null  then
        sqlTemp = format('exists( select null from salve.ficha_nome_comum where ficha_nome_comum.sq_ficha = ficha.sq_ficha ' ||
                         'and salve.fn_contem_palavra(''%s'',ficha_nome_comum.no_comum ) limit 1)',trim( replace( p_json->>'no_comum','%','' ) ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    end if;

    -- FILTRAR PELO NIVEL TAXONOMICO
    if p_json->>'co_nivel_taxonomico' is not null and p_json->>'sq_taxon' is not null then
        sqlInnerJoin = array_append( sqlInnerJoin,'inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon');
        sqlTemp = format('(json_trilha->''%s''->>''sq_taxon'')::int in ( %s )',lower(p_json->>'co_nivel_taxonomico'), p_json->>'sq_taxon');
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR COM / SEM PENDENCIA
    if p_json->>'st_com_pendencia' is not null then
        sqlInnerJoin = array_append( sqlInnerJoin,E'left join lateral (
                                select ficha_pendencia.sq_ficha_pendencia from salve.ficha_pendencia
                                where ficha_pendencia.sq_ficha = ficha.sq_ficha
                                and st_pendente = ''S''
                                limit 1
                                ) as pendencias ON TRUE
                            ');
        if (p_json->>'st_com_pendencia')::boolean = true then
            sqlTemp = 'pendencias.sq_ficha_pendencia is null';
        ELSE
            sqlTemp = 'pendencias.sq_ficha_pendencia is not null';
        END IF;
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELA(s) SITUACAO(oes) DA FICHA
    if p_json->>'sq_situacao_ficha' is not null then
        sqlTemp = format('ficha.sq_situacao_ficha in (%s)',trim( replace( p_json->>'sq_situacao_ficha',' ','') ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELO(s) GRUPO(s) AVALIADOS FICHA
    if p_json->>'sq_grupo_avaliado' is not null then
        sqlTemp = format('ficha.sq_grupo in (%s)',trim( replace( p_json->>'sq_grupo_avaliado',' ','') ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
        if p_json->>'sq_subgrupo_avaliado' is not null then
            sqlTemp = format('ficha.sq_subgrupo in (%s)',trim( replace( p_json->>'sq_subgrupo_avaliado',' ','') ) );
            sqlWhere = array_append(sqlWhere, sqlTemp);
        END IF;
    END IF;

    -- FILTRAR PELO(s) GRUPO(s) SALVE (RECORTE MODULO PUBLICO)
    if p_json->>'sq_grupo_salve' is not null then
        sqlTemp = format('ficha.sq_grupo_salve in (%s)',trim( replace( p_json->>'sq_grupo_salve',' ','') ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- PRESENÇA NA LISTA NACIONAL OFICIAL VIGENTE
    if p_json->>'st_lista_oficial' is not null then
        sqlTemp = format('ficha.st_presenca_lista_vigente = ''%s''',upper(trim( replace( p_json->>'st_lista_oficial',' ','') ) ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR POR PALAVRA CHAVE NOS CAMPOS ABERTOS DA ABA INFORMADA
    if p_json->>'nu_aba_localizar' is not null and p_json->>'ds_palavra_localizar' is not null then
        if p_json->>'nu_aba_localizar' = '1' then
            arrCamposAbas = ARRAY ['ds_notas_taxonomicas', 'ds_diagnostico_morfologico'];
        ELSEIF p_json->>'nu_aba_localizar' = '2' then
            arrCamposAbas = ARRAY ['ds_distribuicao_geo_global', 'ds_distribuicao_geo_nacional'];
        ELSEIF p_json->>'nu_aba_localizar' = '3' then
            arrCamposAbas = ARRAY ['ds_historia_natural', 'ds_habito_alimentar','ds_uso_habitat','ds_interacao','ds_reproducao'];
        ELSEIF p_json->>'nu_aba_localizar' = '4' then
            arrCamposAbas = ARRAY ['ds_populacao','ds_metod_calc_tempo_geracional','ds_caracteristica_genetica'];
        ELSEIF p_json->>'nu_aba_localizar' = '5' then
            arrCamposAbas = ARRAY ['ds_ameaca'];
        ELSEIF p_json->>'nu_aba_localizar' = '6' then
            arrCamposAbas = ARRAY ['ds_uso'];
        ELSEIF p_json->>'nu_aba_localizar' = '7' then
            arrCamposAbas = ARRAY ['ds_historico_avaliacao','ds_acao_conservacao','ds_presenca_uc'];
        ELSEIF p_json->>'nu_aba_localizar' = '8' then
            arrCamposAbas = ARRAY ['ds_pesquisa_exist_necessaria'];
        ELSEIF p_json->>'nu_aba_localizar' = '11' then
            arrCamposAbas = ARRAY ['ds_justificativa','ds_justificativa_final','ds_conectividade_pop_exterior','ds_justificativa_aoo_eoo'];
        END IF;
        if arrCamposAbas is not null then
            sqlOr = null;
            FOREACH strTemp IN ARRAY arrCamposAbas
                LOOP
                    sqlTemp = format('salve.fn_contem_palavra(''%s'',ficha.%s) = TRUE',trim( replace( p_json->>'ds_palavra_localizar','%','') ), strTemp );
                    sqlOr = array_append(sqlOr, sqlTemp);
                END LOOP;
            sqlTemp = concat('(', array_to_string( salve.fn_array_unique(sqlOr ),chr(13)||' OR ' ) , ')');
            sqlWhere = array_append(sqlWhere, sqlTemp);
        END IF;
    END IF;

    -- FILTRAR PELA CATEGORIA FINAL DA FICHA OU CATEGORIA DO HISTORICO DO TAXON
    if p_json->>'sq_categoria_final_ou_historico' is not null then
        sqlInnerJoin = array_append( sqlInnerJoin,E'left join lateral (
                                    SELECT th.sq_categoria_iucn
                                     FROM salve.taxon_historico_avaliacao th
                                    INNER JOIN salve.dados_apoio AS tipo_th ON tipo_th.sq_dados_apoio = th.sq_tipo_avaliacao
                                    INNER JOIN salve.dados_apoio AS categoria_th ON categoria_th.sq_dados_apoio = th.sq_categoria_iucn
                                    WHERE th.sq_taxon = ficha.sq_taxon
                                      AND tipo_th.cd_sistema = ''NACIONAL_BRASIL''
                                    ORDER BY th.nu_ano_avaliacao DESC LIMIT 1
                                    ) as taxon_historico on true');
        sqlTemp = format('( ficha.sq_categoria_final in (%s) or taxon_historico.sq_categoria_iucn in (%s) )', p_json->>'sq_categoria_final_ou_historico', p_json->>'sq_categoria_final_ou_historico');
        sqlWhere = array_append(sqlWhere, sqlTemp);
    ELSEIF p_json->>'sq_categoria_final' is not null then
        sqlTemp = format('ficha.sq_categoria_final in (%s)', p_json->>'sq_categoria_final' );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELA CATEGORIA AVALIADA NO CICLO
    if p_json->>'sq_categoria_ficha' is not null then
        sqlTemp = format('ficha.sq_categoria_iucn in (%s)',p_json->>'sq_categoria_ficha') ;
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELO GRUPO AVALIADO
    if p_json->>'sq_grupo' is not null then
        sqlTemp = format('ficha.sq_grupo in (%s)',p_json->>'sq_grupo') ;
        sqlWhere = array_append(sqlWhere, sqlTemp);
        if p_json->>'sq_subgrupo' is not null then
            sqlTemp = format('ficha.sq_subgrupo in (%s)',p_json->>'sq_subgrupo') ;
            sqlWhere = array_append(sqlWhere, sqlTemp);
        END IF;
    END IF;

    -- FILTRO MANTER LC
    if p_json->>'st_manter_lc' is not null then
        if (p_json->>'st_manter_lc')::boolean = true THEN
            sqlWhere = array_append(sqlWhere,format('ficha.st_manter_lc = %s',p_json->>'st_manter_lc'));
        ELSE
            sqlWhere = array_append(sqlWhere,format('(ficha.st_manter_lc is null OR ficha.st_manter_lc = %s)',p_json->>'st_manter_lc'));
        END IF;
    end if;

    -- -------------------------------------------------------------------------------------------------
    -- -------------------------------------------------------------------------------------------------
    -- -------------------------------------------------------------------------------------------------
    -- -------------------------------------------------------------------------------------------------
    -- FIM FILTROS
    if sqlInnerJoin is not null then
        cmdSql = concat(cmdSql, chr(13), array_to_string( salve.fn_array_unique(sqlInnerJoin ),chr(13) ) );
    END IF;

    if sqlWhere is not null then
        cmdSql = concat(cmdSql, chr(13),'where ', array_to_string( salve.fn_array_unique(sqlWhere ),chr(13)||' AND ') );
        RAISE NOTICE ' ';
        RAISE NOTICE ' ';
        RAISE NOTICE 'sqlWhere (%)',sqlWhere;
    END IF;

    RAISE NOTICE ' ';
    RAISE NOTICE 'SQL FINAL';
    RAISE NOTICE '%',cmdSql;

    return QUERY EXECUTE cmdSql;
end
$$;

alter function salve.fn_ficha_select(jsonb) owner to postgres;

grant execute on function salve.fn_ficha_select(jsonb) to usr_salve;

create or replace function salve.hierarquia_ordem(codigo text) returns text
    language plpgsql
as
$$
/*
  Teste: select salve.hierarquia_ordem ( '1.2.3')
*/
DECLARE
    s TEXT := '';
    x TEXT;
BEGIN
    FOREACH x IN ARRAY string_to_array( codigo, '.')
        LOOP
            s := s || lpad(trim(x),3,'0');
        END LOOP;
    RETURN s;
END;
$$;

comment on function salve.hierarquia_ordem(text) is 'Funcao para transformar o codigo de hierarquia ( 1, 1.1, 1.2 ... ) no formato 001, 011, 012... para permitir ordenar os registros corretamente em ordem crescente pelo codigo.';

alter function salve.hierarquia_ordem(text) owner to postgres;

grant execute on function salve.hierarquia_ordem(text) to usr_salve;


create or replace view salve.vw_pessoa(sq_pessoa, no_pessoa) as
SELECT pessoa.sq_pessoa,
       pessoa.no_pessoa
FROM salve.pessoa;

comment on view salve.vw_pessoa is 'Visão da tabela pessoa para manter compatibilidade com queries existentes';

alter table salve.vw_pessoa
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_pessoa to usr_salve;

create or replace view salve.vw_pessoa_fisica(sq_pessoa, no_pessoa, nu_cpf) as
SELECT pessoa.sq_pessoa,
       pessoa.no_pessoa,
       pf.nu_cpf
FROM salve.pessoa
         JOIN salve.pessoa_fisica pf ON pf.sq_pessoa = pessoa.sq_pessoa;

comment on view salve.vw_pessoa_fisica is 'Visão tabela pessoa física';

alter table salve.vw_pessoa_fisica
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_pessoa_fisica to usr_salve;



create or replace function salve.json_validadores(v_sq_ficha bigint, v_sq_oficina bigint) returns text
    language sql
as
$$
/**
testar: select salve.json_validadores(16729,57 );
        select salve.json_validadores(10064,114 );
*/
select
    coalesce(json_object_agg( coalesce( vw_pessoa.no_pessoa,''), json_build_object(
            'cd_situacao_convite',situacao_convite.cd_sistema
        ,'dt_ultimo_comunicado',to_char( validador_ficha.dt_ultimo_comunicado,'dd/mm/yyyy HH24:MI:SS')
        ,'dt_alteracao',case when validador_ficha.dt_alteracao is not null then to_char( validador_ficha.dt_alteracao,'dd/mm/yyyy HH24:MI:SS') else '' end
        ,'in_respondido', case when validador_ficha.sq_resultado is not null then 'S' else 'N' end
        ,'in_convite_vencido',case when ciclo_avaliacao_validador.dt_validade_convite > current_date then 'N' else 'S' end
        ,'in_comunicar_alteracao',case validador_ficha.in_comunicar_alteracao when true then 'S' else 'N' end
        ,'de_resposta', coalesce( resultado.ds_dados_apoio,'')
        ,'de_categoria_sugerida', case when categoria_sugerida.sq_dados_apoio is null then '' else categoria_sugerida.ds_dados_apoio||' ('|| categoria_sugerida.cd_dados_apoio ||')' end
        ,'de_criterio_sugerido', case when validador_ficha.de_criterio_sugerido is null then '' else validador_ficha.de_criterio_sugerido end
        ,'st_possivelmente_extinta',salve.snd( validador_ficha.st_possivelmente_extinta)
        ,'sq_revisao', coalesce( revisao.sq_validador_ficha_revisao,0)
        ,'de_resposta_revisao', coalesce( resultado_revisao.ds_dados_apoio,'')
        ,'de_categoria_revisao', case when categoria_revisao.sq_dados_apoio is null then '' else categoria_revisao.ds_dados_apoio||' ('|| categoria_revisao.cd_dados_apoio||')' end
        ,'de_criterio_revisao', case when revisao.de_criterio_sugerido is null then '' else revisao.de_criterio_sugerido end
        ,'st_possivelmente_extinta_revisao',salve.snd( revisao.st_possivelmente_extinta)
        ,'nu_chats',oficina_ficha_chat.nu_chats
        ,'sq_validador_ficha',validador_ficha.sq_validador_ficha
                                                                 )
             )::text,'{}')

from salve.oficina_ficha
         inner join salve.validador_ficha on validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
         inner join salve.ciclo_avaliacao_validador on ciclo_avaliacao_validador.sq_ciclo_avaliacao_validador = validador_ficha.sq_ciclo_avaliacao_validador
         inner join salve.vw_pessoa on vw_pessoa.sq_pessoa = ciclo_avaliacao_validador.sq_validador
         left outer join salve.dados_apoio as situacao_convite on situacao_convite.sq_dados_apoio = ciclo_avaliacao_validador.sq_situacao_convite
         left outer join salve.dados_apoio as resultado on resultado.sq_dados_apoio= validador_ficha.sq_resultado
         left outer join salve.dados_apoio as categoria_sugerida on categoria_sugerida.sq_dados_apoio = validador_ficha.sq_categoria_sugerida
         left outer join salve.validador_ficha_revisao as revisao on revisao.sq_validador_ficha = validador_ficha.sq_validador_ficha
         left outer join salve.dados_apoio as resultado_revisao on resultado_revisao.sq_dados_apoio= revisao.sq_resultado
         left outer join salve.dados_apoio as categoria_revisao on categoria_revisao.sq_dados_apoio = revisao.sq_categoria_sugerida
         LEFT JOIN lateral (
    select count(x.sq_oficina_ficha_chat) as nu_chats from salve.oficina_ficha_chat x
    where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
      and x.sq_pessoa = ciclo_avaliacao_validador.sq_validador
    ) oficina_ficha_chat on true
where oficina_ficha.sq_ficha = v_sq_ficha
  and oficina_ficha.sq_oficina = v_sq_oficina
$$;

comment on function salve.json_validadores(bigint, bigint) is 'Funcao para recuperar as respostas dos validadores e a revisao no formato json';

grant execute on function salve.json_validadores(bigint, bigint) to usr_salve;

create or replace function salve.calc_base_dados_portalbio(tx_ocorrencia text, uuid text) returns text
    language sql
as
$$
SELECT case when tx_ocorrencia ilike '%Atlas de Registros de Aves Brasileiras%' then 'ARA' else
    case when tx_ocorrencia ilike '%SIMMAM%' then 'SIMMAM' else
        case when tx_ocorrencia ilike '%Sisquelonios%' then 'SISQUELONIOS' else
            case when tx_ocorrencia ilike '%SITAMAR%' then 'SITAMAR' else
                case when tx_ocorrencia ilike '%SISBIO%' then 'SISBIO' else
                    case when uuid ilike '%SISBIO%' then 'SISBIO' else '' end
                    end
                end
            end
        end
           end
$$;

comment on function salve.calc_base_dados_portalbio(text, text) is 'Função calcular o nome da base dados original do registro de ocorrencia importados do PORTALBIO.';

alter function salve.calc_base_dados_portalbio(text, text) owner to postgres;

grant execute on function salve.calc_base_dados_portalbio(text, text) to usr_salve;

create or replace function salve.calc_carencia(data date, prazo text) returns date
    language sql
as
$$
SELECT CASE
           WHEN prazo IS NOT NULL
               AND prazo <> 'SEM_CARENCIA'::text
               AND data IS NOT NULL THEN CASE
                                             WHEN prazo = '1_ANO'::text
                                                 THEN data + '1 year'::interval
                                             ELSE CASE
                                                      WHEN prazo = '2_ANOS'::text
                                                          THEN data + '2 years'::interval
                                                      ELSE CASE
                                                               WHEN prazo = '3_ANOS'::text
                                                                   THEN data + '3 years'::interval
                                                               ELSE NULL::TIMESTAMP WITHOUT TIME ZONE
                                                          END
                                                 END
               END
           ELSE NULL::TIMESTAMP WITHOUT TIME ZONE
           END :: date
$$;

comment on function salve.calc_carencia(date, text) is 'Função calcular a data final da carencia baseado no periodo.';

alter function salve.calc_carencia(date, text) owner to postgres;

grant execute on function salve.calc_carencia(date, text) to usr_salve;
-- end functions



create materialized view if not exists salve.mv_registros as
SELECT true                                                                                              AS bo_salve,
       fo.sq_ficha_ocorrencia                                                                            AS id_ocorrencia,
       ficha.sq_ciclo_avaliacao,
       ficha.sq_ficha,
       ficha.sq_taxon,
       ficha.nm_cientifico,
       ficha.sq_categoria_iucn,
       ficha.sq_categoria_final,
       taxon.no_autor                                                                                    AS no_autor_taxon,
       (taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text                                         AS no_reino,
       (taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text                                          AS no_filo,
       (taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text                                        AS no_classe,
       (taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text                                         AS no_ordem,
       (taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text                                       AS no_familia,
       (taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text                                        AS no_genero,
       (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                       AS no_especie,
       (taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text                                    AS no_subespecie,
       categoria_anterior.ds_categoria_anterior,
       CASE
           WHEN ficha.sq_categoria_iucn IS NOT NULL THEN concat(cat_avaliada.ds_dados_apoio, ' (',
                                                                cat_avaliada.cd_dados_apoio, ')')
           ELSE ''::text
           END                                                                                           AS ds_categoria_avaliada,
       CASE
           WHEN ficha.sq_categoria_final IS NOT NULL THEN concat(cat_validada.ds_dados_apoio, ' (',
                                                                 cat_validada.cd_dados_apoio, ')')
           ELSE ''::text
           END                                                                                           AS ds_categoria_validada,
       pans.ds_pan,
       st_x(fo.ge_ocorrencia)                                                                            AS nu_longitude,
       st_y(fo.ge_ocorrencia)                                                                            AS nu_latitude,
       fo.nu_altitude,
       salve.snd(ficha.st_endemica_brasil::text)                                                         AS de_endemica_brasil,
       salve.snd(fo.in_sensivel::text)                                                                   AS ds_sensivel,
       fo.sq_situacao_avaliacao,
       situacao_avaliacao.ds_dados_apoio                                                                 AS ds_situacao_avaliacao,
       situacao_avaliacao.cd_sistema                                                                     AS cd_situacao_avaliacao,
       fo.tx_nao_utilizado_avaliacao                                                                     AS tx_justificativa_nao_usado_avaliacao,
       motivo.ds_dados_apoio                                                                             AS ds_motivo_nao_utilizado,
       salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema)                              AS dt_carencia,
       prazo_carencia.ds_dados_apoio                                                                     AS ds_prazo_carencia,
       datum.ds_dados_apoio                                                                              AS no_datum,
       formato_original.ds_dados_apoio                                                                   AS no_formato_original,
       precisao_coord.ds_dados_apoio                                                                     AS no_precisao,
       referencia_aprox.ds_dados_apoio                                                                   AS no_referencia_aproximacao,
       metodo_aprox.ds_dados_apoio                                                                       AS no_metodologia_aproximacao,
       fo.tx_metodo_aproximacao                                                                          AS tx_metodologia_aproximacao,
       (taxon_citado.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                AS no_taxon_citado,
       fo.in_presenca_atual,
       salve.snd(fo.in_presenca_atual::text)                                                             AS ds_presenca_atual,
       tipo_registro.ds_dados_apoio                                                                      AS no_tipo_registro,
       fo.nu_dia_registro::text                                                                          AS nu_dia_registro,
       fo.nu_mes_registro::text                                                                          AS nu_mes_registro,
       fo.nu_ano_registro::text                                                                          AS nu_ano_registro,
       fo.hr_registro,
       fo.nu_dia_registro_fim::text                                                                      AS nu_dia_registro_fim,
       fo.nu_mes_registro_fim::text                                                                      AS nu_mes_registro_fim,
       fo.nu_ano_registro_fim::text                                                                      AS nu_ano_registro_fim,
       salve.snd(fo.in_data_desconhecida::text)                                                          AS ds_data_desconhecida,
       fo.no_localidade,
       salve.remover_html_tags(fo.tx_local)                                                              AS ds_caracteristica_localidade,
       uc_federal.no_uc_federal,
       uc_estadual.no_uc_estadual,
       initcap(rppn.no_pessoa)                                                                           AS no_rppn,
       pais.no_pais,
       estado.no_estado,
       municipio.no_municipio,
       bioma.no_bioma,
       btrim(regexp_replace(habitat.ds_dados_apoio, '[0-9.]'::text, ''::text, 'g'::text))                AS no_ambiente,
       habitat.ds_dados_apoio                                                                            AS no_habitat,
       fo.vl_elevacao_min,
       fo.vl_elevacao_max,
       fo.vl_profundidade_min,
       fo.vl_profundidade_max,
       fo.de_identificador,
       fo.dt_identificacao,
       qualificador.ds_dados_apoio                                                                       AS no_identificacao_incerta,
       fo.tx_tombamento                                                                                  AS ds_tombamento,
       fo.tx_instituicao                                                                                 AS ds_instituicao_tombamento,
       compilador.no_pessoa                                                                              AS no_compilador,
       fo.dt_compilacao,
       revisor.no_pessoa                                                                                 AS no_revisor,
       fo.dt_revisao,
       validador.no_pessoa                                                                               AS no_validador,
       fo.dt_validacao,
       fo.in_sensivel,
       'salve'::text                                                                                     AS no_base_dados,
       CASE
           WHEN fo.id_origem IS NULL THEN concat('SALVE:', fo.sq_ficha_ocorrencia::text)
           ELSE fo.id_origem::text
           END                                                                                           AS id_origem,
       ''::text                                                                                          AS nu_autorizacao,
       salve.remover_html_tags(fo.tx_observacao)                                                         AS tx_observacao,
       refbibs.json_ref_bibs                                                                             AS tx_ref_bib,
       NULL::text                                                                                        AS no_autor_registro,
       NULL::text                                                                                        AS no_projeto,
       CASE
           WHEN fo.st_adicionado_apos_avaliacao = true THEN 'Não'::text
           ELSE salve.snd(fo.in_utilizado_avaliacao::text)
           END                                                                                           AS ds_utilizado_avaliacao,
       salve.fn_calc_situacao_registro(fo.in_utilizado_avaliacao::text,
                                       fo.st_adicionado_apos_avaliacao)                                  AS ds_situacao_registro,
       fo.in_utilizado_avaliacao
FROM salve.ficha_ocorrencia fo
         JOIN salve.ficha_ocorrencia_registro freg ON freg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
         JOIN salve.ficha ON ficha.sq_ficha = fo.sq_ficha
         JOIN salve.ciclo_avaliacao ciclo ON ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
         JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
         JOIN salve.dados_apoio situacao_avaliacao ON situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
         LEFT JOIN salve.dados_apoio motivo ON motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
         LEFT JOIN salve.dados_apoio cat_avaliada ON cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
         LEFT JOIN salve.dados_apoio cat_validada ON cat_validada.sq_dados_apoio = ficha.sq_categoria_final
         LEFT JOIN salve.dados_apoio tipo_registro ON tipo_registro.sq_dados_apoio = fo.sq_tipo_registro
         LEFT JOIN salve.dados_apoio prazo_carencia ON prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
         LEFT JOIN salve.dados_apoio datum ON datum.sq_dados_apoio = fo.sq_datum
         LEFT JOIN salve.dados_apoio formato_original ON formato_original.sq_dados_apoio = fo.sq_formato_coord_original
         LEFT JOIN salve.dados_apoio precisao_coord ON precisao_coord.sq_dados_apoio = fo.sq_precisao_coordenada
         LEFT JOIN salve.dados_apoio referencia_aprox ON referencia_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
         LEFT JOIN salve.dados_apoio metodo_aprox ON metodo_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
         LEFT JOIN taxonomia.taxon taxon_citado ON taxon_citado.sq_taxon = fo.sq_taxon_citado
         LEFT JOIN LATERAL ( SELECT h.sq_taxon,
                                    concat(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
                             FROM salve.taxon_historico_avaliacao h
                                      JOIN salve.dados_apoio cat_hist ON cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                             WHERE h.sq_taxon = ficha.sq_taxon
                               AND h.sq_tipo_avaliacao = 311
                               AND h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)
                             ORDER BY h.nu_ano_avaliacao DESC
                             OFFSET 0 LIMIT 1) categoria_anterior ON true
         LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
                                    array_to_string(array_agg(concat(plano_acao.tx_plano_acao, ' (',
                                                                     apoio_acao_situacao.ds_dados_apoio, ')')),
                                                    ', '::text) AS ds_pan
                             FROM salve.ficha_acao_conservacao acoes
                                      JOIN salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                      JOIN salve.dados_apoio apoio_acao_situacao
                                           ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                             WHERE acoes.sq_ficha = ficha.sq_ficha
                             GROUP BY acoes.sq_ficha) pans ON true
         LEFT JOIN salve.registro_rppn regrppn ON regrppn.sq_registro = freg.sq_registro
         LEFT JOIN salve.pessoa rppn ON rppn.sq_pessoa = regrppn.sq_rppn
         LEFT JOIN salve.registro_pais regpais ON regpais.sq_registro = freg.sq_registro
         LEFT JOIN salve.pais pais ON pais.sq_pais = regpais.sq_pais
         LEFT JOIN salve.registro_estado reguf ON reguf.sq_registro = freg.sq_registro
         LEFT JOIN salve.estado estado ON estado.sq_estado = reguf.sq_estado
         LEFT JOIN salve.registro_municipio regmun ON regmun.sq_registro = freg.sq_registro
         LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = regmun.sq_municipio
         LEFT JOIN salve.dados_apoio habitat ON habitat.sq_dados_apoio = fo.sq_habitat
         LEFT JOIN salve.dados_apoio ambiente ON ambiente.sq_dados_apoio = habitat.sq_dados_apoio_pai
         LEFT JOIN salve.dados_apoio qualificador ON qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon
         LEFT JOIN salve.pessoa compilador ON compilador.sq_pessoa = fo.sq_pessoa_compilador
         LEFT JOIN salve.pessoa revisor ON revisor.sq_pessoa = fo.sq_pessoa_revisor
         LEFT JOIN salve.pessoa validador ON validador.sq_pessoa = fo.sq_pessoa_validador
         LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
         LEFT JOIN salve.bioma bioma ON bioma.sq_bioma = regbio.sq_bioma
         LEFT JOIN LATERAL ( SELECT json_object_agg(ficha_ref_bib.sq_ficha_ref_bib, json_build_object('de_ref_bib',
                                                                                                      salve.entity2char(publicacao.de_ref_bibliografica),
                                                                                                      'no_autores_publicacao',
                                                                                                      replace(
                                                                                                              publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano',
                                                                                                      ficha_ref_bib.nu_ano))::text AS json_ref_bibs
                             FROM salve.ficha_ref_bib
                                      LEFT JOIN taxonomia.publicacao
                                                ON publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                             WHERE ficha_ref_bib.no_tabela::text = 'ficha_ocorrencia'::text
                               AND ficha_ref_bib.no_coluna::text = 'sq_ficha_ocorrencia'::text
                               AND ficha_ref_bib.sq_registro = fo.sq_ficha_ocorrencia) refbibs ON true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(COALESCE(instituicao.sg_instituicao,
                                                                       ucf.no_pessoa::character varying::text)),
                                                    ', '::text) AS no_uc_federal
                             FROM salve.registro_uc_federal x
                                      JOIN salve.pessoa ucf ON ucf.sq_pessoa = x.sq_uc_federal
                                      JOIN salve.instituicao ON instituicao.sq_pessoa = x.sq_uc_federal
                             WHERE x.sq_registro = freg.sq_registro) uc_federal ON true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(uce.no_pessoa), ', '::text) AS no_uc_estadual
                             FROM salve.registro_uc_estadual x
                                      JOIN salve.pessoa uce ON uce.sq_pessoa = x.sq_uc_estadual
                             WHERE x.sq_registro = freg.sq_registro) uc_estadual ON true
UNION ALL
SELECT false                                                                                             AS bo_salve,
       oc.sq_ficha_ocorrencia_portalbio                                                                  AS id_ocorrencia,
       ficha.sq_ciclo_avaliacao,
       ficha.sq_ficha,
       ficha.sq_taxon,
       ficha.nm_cientifico,
       ficha.sq_categoria_iucn,
       ficha.sq_categoria_final,
       taxon.no_autor                                                                                    AS no_autor_taxon,
       (taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text                                         AS no_reino,
       (taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text                                          AS no_filo,
       (taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text                                        AS no_classe,
       (taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text                                         AS no_ordem,
       (taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text                                       AS no_familia,
       (taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text                                        AS no_genero,
       (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                       AS no_especie,
       (taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text                                    AS no_subespecie,
       categoria_anterior.ds_categoria_anterior,
       CASE
           WHEN ficha.sq_categoria_iucn IS NOT NULL THEN concat(cat_avaliada.ds_dados_apoio, ' (',
                                                                cat_avaliada.cd_dados_apoio, ')')
           ELSE ''::text
           END                                                                                           AS ds_categoria_avaliada,
       CASE
           WHEN ficha.sq_categoria_final IS NOT NULL THEN concat(cat_validada.ds_dados_apoio, ' (',
                                                                 cat_validada.cd_dados_apoio, ')')
           ELSE ''::text
           END                                                                                           AS ds_categoria_validada,
       pans.ds_pan,
       st_x(oc.ge_ocorrencia)                                                                            AS nu_longitude,
       st_y(oc.ge_ocorrencia)                                                                            AS nu_latitude,
       NULL::integer                                                                                     AS nu_altitude,
       salve.snd(ficha.st_endemica_brasil::text)                                                         AS de_endemica_brasil,
       NULL::text                                                                                        AS ds_sensivel,
       oc.sq_situacao_avaliacao,
       situacao_avaliacao.ds_dados_apoio                                                                 AS ds_situacao_avaliacao,
       situacao_avaliacao.cd_sistema                                                                     AS cd_situacao_avaliacao,
       oc.tx_nao_utilizado_avaliacao                                                                     AS tx_justificativa_nao_usado_avaliacao,
       motivo.ds_dados_apoio                                                                             AS ds_motivo_nao_utilizado,
       oc.dt_carencia,
       NULL::text                                                                                        AS ds_prazo_carencia,
       NULL::text                                                                                        AS no_datum,
       NULL::text                                                                                        AS no_formato_original,
       CASE
           WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'precisaoCoord'::text
           ELSE ''::text
           END                                                                                           AS no_precisao,
       NULL::text                                                                                        AS no_referencia_aproximacao,
       NULL::text                                                                                        AS no_metodologia_aproximacao,
       NULL::text                                                                                        AS tx_metodologia_aproximacao,
       NULL::text                                                                                        AS no_taxon_citado,
       oc.in_presenca_atual,
       salve.snd(oc.in_presenca_atual::text)                                                             AS ds_presenca_atual,
       NULL::text                                                                                        AS no_tipo_registro,
       to_char(oc.dt_ocorrencia::timestamp with time zone, 'DD'::text)                                   AS nu_dia_registro,
       to_char(oc.dt_ocorrencia::timestamp with time zone, 'MM'::text)                                   AS nu_mes_registro,
       to_char(oc.dt_ocorrencia::timestamp with time zone, 'yyyy'::text)                                 AS nu_ano_registro,
       NULL::text                                                                                        AS hr_registro,
       NULL::text                                                                                        AS nu_dia_registro_fim,
       NULL::text                                                                                        AS nu_mes_registro_fim,
       NULL::text                                                                                        AS nu_ano_registro_fim,
       salve.snd(oc.in_data_desconhecida::text)                                                          AS ds_data_desconhecida,
       oc.no_local                                                                                       AS no_localidade,
       NULL::text                                                                                        AS ds_caracteristica_localidade,
       uc_federal.no_uc_federal,
       uc_estadual.no_uc_estadual,
       initcap(rppn.no_pessoa)                                                                           AS no_rppn,
       pais.no_pais,
       estado.no_estado,
       municipio.no_municipio,
       bioma.no_bioma,
       NULL::text                                                                                        AS no_ambiente,
       NULL::text                                                                                        AS no_habitat,
       NULL::numeric                                                                                     AS vl_elevacao_min,
       NULL::numeric                                                                                     AS vl_elevacao_max,
       NULL::numeric                                                                                     AS vl_profundidade_min,
       NULL::numeric                                                                                     AS vl_profundidade_max,
       NULL::text                                                                                        AS de_identificador,
       NULL::date                                                                                        AS dt_identificacao,
       NULL::text                                                                                        AS no_identificacao_incerta,
       NULL::text                                                                                        AS ds_tombamento,
       NULL::text                                                                                        AS ds_instituicao_tombamento,
       NULL::text                                                                                        AS no_compilador,
       NULL::date                                                                                        AS dt_compilacao,
       revisor.no_pessoa                                                                                 AS no_revisor,
       oc.dt_revisao,
       validador.no_pessoa                                                                               AS no_validador,
       oc.dt_validacao,
       NULL::bpchar                                                                                      AS in_sensivel,
       salve.calc_base_dados_portalbio(oc.tx_ocorrencia, oc.de_uuid)                                     AS no_base_dados,
       CASE
           WHEN oc.id_origem IS NULL THEN
               CASE
                   WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'uuid'::text
                   ELSE oc.de_uuid
                   END
           ELSE oc.id_origem::text
           END                                                                                           AS id_origem,
       CASE
           WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'recordNumber'::text
           ELSE ''::text
           END                                                                                           AS nu_autorizacao,
       NULL::text                                                                                        AS tx_observacao,
       refbibs.json_ref_bibs                                                                             AS tx_ref_bib,
       replace(
               CASE
                   WHEN oc.no_autor IS NULL THEN
                       CASE
                           WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN
                               CASE
                                   WHEN (oc.tx_ocorrencia::json ->> 'autor'::text) IS NULL
                                       THEN oc.tx_ocorrencia::json ->> 'collector'::text
                                   ELSE oc.tx_ocorrencia::json ->> 'autor'::text
                                   END
                           ELSE ''::text
                           END
                   ELSE oc.no_autor
                   END, '
'::text, '; '::text)                                                             AS no_autor_registro,
       oc.tx_ocorrencia::jsonb ->> 'projeto'::text                                                       AS no_projeto,
       CASE
           WHEN oc.st_adicionado_apos_avaliacao = true THEN 'Não'::text
           ELSE salve.snd(oc.in_utilizado_avaliacao::text)
           END                                                                                           AS ds_utilizado_avaliacao,
       salve.fn_calc_situacao_registro(oc.in_utilizado_avaliacao::text,
                                       oc.st_adicionado_apos_avaliacao)                                  AS ds_situacao_registro,
       oc.in_utilizado_avaliacao
FROM salve.ficha_ocorrencia_portalbio oc
         JOIN salve.ficha ON ficha.sq_ficha = oc.sq_ficha
         JOIN salve.ciclo_avaliacao ciclo ON ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
         JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
         JOIN salve.dados_apoio situacao_avaliacao ON situacao_avaliacao.sq_dados_apoio = oc.sq_situacao_avaliacao
         LEFT JOIN salve.dados_apoio motivo ON motivo.sq_dados_apoio = oc.sq_motivo_nao_utilizado_avaliacao
         LEFT JOIN salve.dados_apoio cat_avaliada ON cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
         LEFT JOIN salve.dados_apoio cat_validada ON cat_validada.sq_dados_apoio = ficha.sq_categoria_final
         LEFT JOIN LATERAL ( SELECT h.sq_taxon,
                                    concat(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
                             FROM salve.taxon_historico_avaliacao h
                                      JOIN salve.dados_apoio cat_hist ON cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                             WHERE h.sq_taxon = ficha.sq_taxon
                               AND h.sq_tipo_avaliacao = 311
                               AND h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)
                             ORDER BY h.nu_ano_avaliacao DESC
                             OFFSET 0 LIMIT 1) categoria_anterior ON true
         LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
                                    array_to_string(array_agg(concat(plano_acao.tx_plano_acao, ' (',
                                                                     apoio_acao_situacao.ds_dados_apoio, ')')),
                                                    ', '::text) AS ds_pan
                             FROM salve.ficha_acao_conservacao acoes
                                      JOIN salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                      JOIN salve.dados_apoio apoio_acao_situacao
                                           ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                             WHERE acoes.sq_ficha = ficha.sq_ficha
                             GROUP BY acoes.sq_ficha) pans ON true
         LEFT JOIN salve.ficha_ocorrencia_portalbio_reg freg
                   ON freg.sq_ficha_ocorrencia_portalbio = oc.sq_ficha_ocorrencia_portalbio
         LEFT JOIN salve.registro_rppn regrppn ON regrppn.sq_registro = freg.sq_registro
         LEFT JOIN salve.pessoa rppn ON rppn.sq_pessoa = regrppn.sq_rppn
         LEFT JOIN salve.registro_pais regpais ON regpais.sq_registro = freg.sq_registro
         LEFT JOIN salve.pais pais ON pais.sq_pais = regpais.sq_pais
         LEFT JOIN salve.registro_estado reguf ON reguf.sq_registro = freg.sq_registro
         LEFT JOIN salve.estado estado ON estado.sq_estado = reguf.sq_estado
         LEFT JOIN salve.registro_municipio regmun ON regmun.sq_registro = freg.sq_registro
         LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = regmun.sq_municipio
         LEFT JOIN salve.pessoa revisor ON revisor.sq_pessoa = oc.sq_pessoa_revisor
         LEFT JOIN salve.pessoa validador ON validador.sq_pessoa = oc.sq_pessoa_validador
         LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
         LEFT JOIN salve.bioma ON bioma.sq_bioma = regbio.sq_bioma
         LEFT JOIN LATERAL ( SELECT json_object_agg(ficha_ref_bib.sq_ficha_ref_bib, json_build_object('de_ref_bib',
                                                                                                      salve.entity2char(publicacao.de_ref_bibliografica),
                                                                                                      'no_autores_publicacao',
                                                                                                      replace(
                                                                                                              publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano',
                                                                                                      ficha_ref_bib.nu_ano))::text AS json_ref_bibs
                             FROM salve.ficha_ref_bib
                                      LEFT JOIN taxonomia.publicacao
                                                ON publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                             WHERE ficha_ref_bib.no_tabela::text = 'ficha_ocorrencia_portalbio'::text
                               AND ficha_ref_bib.no_coluna::text = 'sq_ficha_ocorrencia_portalbio'::text
                               AND ficha_ref_bib.sq_registro = oc.sq_ficha_ocorrencia_portalbio) refbibs ON true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(COALESCE(instituicao.sg_instituicao,
                                                                       ucf.no_pessoa::character varying::text)),
                                                    ', '::text) AS no_uc_federal
                             FROM salve.registro_uc_federal x
                                      JOIN salve.pessoa ucf ON ucf.sq_pessoa = x.sq_uc_federal
                                      JOIN salve.instituicao ON instituicao.sq_pessoa = x.sq_uc_federal
                             WHERE x.sq_registro = freg.sq_registro) uc_federal ON true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(uce.no_pessoa), ', '::text) AS no_uc_estadual
                             FROM salve.registro_uc_estadual x
                                      JOIN salve.pessoa uce ON uce.sq_pessoa = x.sq_uc_estadual
                             WHERE x.sq_registro = freg.sq_registro) uc_estadual ON true;

alter materialized view salve.mv_registros owner to usr_salve;

create index if not exists idx_mv_exp_reg_dt_carencia
    on salve.mv_registros (dt_carencia)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_in_situacao_avaliacao
    on salve.mv_registros (in_utilizado_avaliacao, dt_carencia)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_nm_cientifico
    on salve.mv_registros (nm_cientifico)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_sq_ciclo
    on salve.mv_registros (sq_ciclo_avaliacao)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_sq_situacao_avaliacao
    on salve.mv_registros (sq_situacao_avaliacao, dt_carencia)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_sq_taxon
    on salve.mv_registros (sq_taxon)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_sq_taxon_ciclo_carencia
    on salve.mv_registros (sq_taxon, sq_ciclo_avaliacao, dt_carencia)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_sq_ficha
    on salve.mv_registros (sq_ficha)
    tablespace pg_default;

create index if not exists idx_mv_exp_reg_ficha_situ_caren
    on salve.mv_registros (sq_ficha, sq_situacao_avaliacao, dt_carencia)
    tablespace pg_default;

grant select, references on salve.mv_registros to usr_salve;

create or replace view salve.vw_ameacas
            (id, id_pai, codigo, descricao, descricao_completa, descricao_sem_acento, nivel, ordem, trilha,
             trilha_csv) as
WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai, cd_dados_apoio, ds_dados_apoio, de_dados_apoio_ordem,
                      cd_sistema, depth, ds_trilha, ds_trilha_csv) AS (SELECT da.sq_dados_apoio,
                                                                              da.sq_dados_apoio_pai,
                                                                              da.cd_dados_apoio,
                                                                              da.ds_dados_apoio,
                                                                              da.de_dados_apoio_ordem,
                                                                              da.cd_sistema,
                                                                              0                 AS depth,
                                                                              da.ds_dados_apoio AS ds_trilha,
                                                                              ''::text          AS ds_trilha_csv
                                                                       FROM salve.dados_apoio da
                                                                       WHERE da.cd_dados_apoio = 'TB_CRITERIO_AMEACA_IUCN'::text
                                                                       UNION ALL
                                                                       SELECT dc.sq_dados_apoio,
                                                                              dc.sq_dados_apoio_pai,
                                                                              dc.cd_dados_apoio,
                                                                              dc.ds_dados_apoio,
                                                                              dc.de_dados_apoio_ordem,
                                                                              dc.cd_sistema,
                                                                              arvore_1.depth + 1,
                                                                              CASE
                                                                                  WHEN arvore_1.depth = 0 THEN ''::text
                                                                                  ELSE arvore_1.ds_trilha || '|'::text
                                                                                  END ||
                                                                              ((dc.cd_dados_apoio || ' - '::text) || dc.ds_dados_apoio) AS ds_trilha,
                                                                              CASE
                                                                                  WHEN arvore_1.depth = 0 THEN ''::text
                                                                                  ELSE arvore_1.ds_trilha_csv || '|'::text
                                                                                  END ||
                                                                              ((((dc.cd_dados_apoio || ';'::text) ||
                                                                                 replace(dc.ds_dados_apoio, ';'::text, ','::text)) ||
                                                                                ';'::text) ||
                                                                               dc.sq_dados_apoio)                                       AS ds_trilha_csv
                                                                       FROM arvore arvore_1
                                                                                JOIN salve.dados_apoio dc ON dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio)
SELECT arvore.sq_dados_apoio                                           AS id,
       arvore.sq_dados_apoio_pai                                       AS id_pai,
       arvore.cd_dados_apoio                                           AS codigo,
       arvore.ds_dados_apoio                                           AS descricao,
       (arvore.cd_dados_apoio || ' - '::text) || arvore.ds_dados_apoio AS descricao_completa,
       fn_remove_acentuacao(arvore.ds_dados_apoio::character varying)  AS descricao_sem_acento,
       arvore.depth                                                    AS nivel,
       arvore.de_dados_apoio_ordem                                     AS ordem,
       arvore.ds_trilha                                                AS trilha,
       arvore.ds_trilha_csv                                            AS trilha_csv
FROM arvore
WHERE arvore.depth > 0;

alter table salve.vw_ameacas
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_ameacas to usr_salve;

create or replace view salve.vw_bacia_hidrografica
            (id, id_pai, codigo, codigo_sistema, descricao, descricao_completa, descricao_sem_acento, nivel, ordem,
             trilha) as
WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai, cd_dados_apoio, ds_dados_apoio, de_dados_apoio_ordem,
                      cd_sistema, depth, ds_trilha) AS (SELECT da.sq_dados_apoio,
                                                               da.sq_dados_apoio_pai,
                                                               da.cd_dados_apoio,
                                                               da.ds_dados_apoio,
                                                               da.de_dados_apoio_ordem,
                                                               da.cd_sistema,
                                                               0                 AS depth,
                                                               da.ds_dados_apoio AS ds_trilha
                                                        FROM salve.dados_apoio da
                                                        WHERE da.cd_dados_apoio = 'TB_BACIA_HIDROGRAFICA'::text
                                                        UNION ALL
                                                        SELECT dc.sq_dados_apoio,
                                                               dc.sq_dados_apoio_pai,
                                                               dc.cd_dados_apoio,
                                                               dc.ds_dados_apoio,
                                                               dc.de_dados_apoio_ordem,
                                                               dc.cd_sistema,
                                                               arvore_1.depth + 1,
                                                               CASE
                                                                   WHEN arvore_1.depth = 0 THEN ''::text
                                                                   ELSE COALESCE(arvore_1.ds_trilha, ''::text) || '|'::text
                                                                   END ||
                                                               ((COALESCE(dc.cd_dados_apoio, ''::text) || ' - '::text) ||
                                                                COALESCE(dc.ds_dados_apoio, ''::text)) AS ds_trilha
                                                        FROM arvore arvore_1
                                                                 JOIN salve.dados_apoio dc ON dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio)
SELECT arvore.sq_dados_apoio                                          AS id,
       arvore.sq_dados_apoio_pai                                      AS id_pai,
       arvore.cd_dados_apoio                                          AS codigo,
       arvore.cd_sistema                                              AS codigo_sistema,
       arvore.ds_dados_apoio                                          AS descricao,
       concat(arvore.cd_dados_apoio, ' - ', arvore.ds_dados_apoio)    AS descricao_completa,
       fn_remove_acentuacao(arvore.ds_dados_apoio::character varying) AS descricao_sem_acento,
       arvore.depth                                                   AS nivel,
       arvore.de_dados_apoio_ordem                                    AS ordem,
       arvore.ds_trilha                                               AS trilha
FROM arvore
WHERE arvore.depth > 0;

alter table salve.vw_bacia_hidrografica
    owner to postgres;

grant select on salve.vw_bacia_hidrografica to usr_salve;

create or replace view salve.vw_efeito_prim(sq_efeito, sq_efeito_pai, ds_efeito, ds_trilha, nu_nivel, de_ordem) as
WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai, cd_dados_apoio, ds_dados_apoio, de_dados_apoio_ordem, depth,
                      ds_trilha) AS (SELECT da.sq_dados_apoio,
                                            da.sq_dados_apoio_pai,
                                            da.cd_dados_apoio,
                                            da.ds_dados_apoio,
                                            da.de_dados_apoio_ordem,
                                            0                 AS depth,
                                            da.ds_dados_apoio AS ds_trilha
                                     FROM salve.dados_apoio da
                                     WHERE da.cd_sistema = 'TB_EFEITO_PRIM'::text
                                     UNION ALL
                                     SELECT dc.sq_dados_apoio,
                                            dc.sq_dados_apoio_pai,
                                            dc.cd_dados_apoio,
                                            dc.ds_dados_apoio,
                                            dc.de_dados_apoio_ordem,
                                            arvore_1.depth + 1,
                                            ((dc.cd_dados_apoio || ' - '::text) || dc.ds_dados_apoio) ||
                                            CASE
                                                WHEN arvore_1.depth = 0 THEN ''::text
                                                ELSE '|'::text || arvore_1.ds_trilha
                                                END AS ds_trilha
                                     FROM arvore arvore_1
                                              JOIN salve.dados_apoio dc ON dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio)
SELECT arvore.sq_dados_apoio                                           AS sq_efeito,
       arvore.sq_dados_apoio_pai                                       AS sq_efeito_pai,
       (arvore.cd_dados_apoio || ' - '::text) || arvore.ds_dados_apoio AS ds_efeito,
       arvore.ds_trilha,
       arvore.depth                                                    AS nu_nivel,
       arvore.de_dados_apoio_ordem                                     AS de_ordem
FROM arvore
WHERE arvore.depth > 0;

alter table salve.vw_efeito_prim
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_efeito_prim to usr_salve;

create or replace view salve.vw_email(sq_pessoa, sq_tipo_email, tx_email) as
SELECT pessoa.sq_pessoa,
       2               AS sq_tipo_email,
       pessoa.de_email AS tx_email
FROM salve.pessoa;

comment on view salve.vw_email is 'Visão para manter compatiblidade das queries existentes';

alter table salve.vw_email
    owner to postgres;

create or replace view salve.vw_estado(sq_estado, ge_estado, no_estado, sq_pais, sg_estado, sq_regiao) as
SELECT estado.sq_estado,
       estado.ge_estado,
       estado.no_estado,
       estado.sq_pais,
       estado.sg_estado,
       estado.sq_regiao
FROM salve.estado;

alter table salve.vw_estado
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_estado to usr_salve;

create or replace view salve.vw_ficha
            (sq_ficha, sq_taxon, sq_ciclo_avaliacao, sq_unidade_org, sq_usuario_alteracao, sq_situacao_ficha,
             ds_situacao_ficha, cd_situacao_ficha_sistema, sq_grupo_salve, sq_grupo, sq_subgrupo, sq_categoria_iucn,
             ds_criterio_aval_iucn, st_possivelmente_extinta, sq_categoria_final, ds_criterio_aval_iucn_final,
             st_possivelmente_extinta_final, nm_cientifico, sg_unidade_org, no_usuario_alteracao, dt_alteracao,
             dt_publicacao, sq_usuario_publicacao, vl_percentual_preenchimento, st_manter_lc, st_transferida,
             dt_aceite_validacao, st_localidade_desconhecida, co_nivel_taxonomico, nu_grau_taxonomico, nu_pendencia)
as
SELECT f.sq_ficha,
       f.sq_taxon,
       f.sq_ciclo_avaliacao,
       f.sq_unidade_org,
       f.sq_usuario_alteracao,
       f.sq_situacao_ficha,
       sit.ds_dados_apoio           AS ds_situacao_ficha,
       sit.cd_sistema               AS cd_situacao_ficha_sistema,
       f.sq_grupo_salve,
       f.sq_grupo,
       f.sq_subgrupo,
       f.sq_categoria_iucn,
       f.ds_criterio_aval_iucn,
       f.st_possivelmente_extinta,
       f.sq_categoria_final,
       f.ds_criterio_aval_iucn_final,
       f.st_possivelmente_extinta_final,
       f.nm_cientifico,
       u.sg_instituicao             AS sg_unidade_org,
       initcap(p.no_pessoa)         AS no_usuario_alteracao,
       f.dt_alteracao,
       f.dt_publicacao,
       f.sq_usuario_publicacao,
       f.vl_percentual_preenchimento,
       f.st_manter_lc,
       f.st_transferida,
       f.dt_aceite_validacao,
       f.st_localidade_desconhecida,
       nivel.co_nivel_taxonomico,
       nivel.nu_grau_taxonomico,
       count(fp.sq_ficha_pendencia) AS nu_pendencia
FROM salve.ficha f
         JOIN salve.instituicao u ON u.sq_pessoa = f.sq_unidade_org
         JOIN taxonomia.taxon t ON t.sq_taxon = f.sq_taxon
         JOIN taxonomia.nivel_taxonomico nivel ON nivel.sq_nivel_taxonomico = t.sq_nivel_taxonomico
         JOIN salve.dados_apoio sit ON sit.sq_dados_apoio = f.sq_situacao_ficha
         LEFT JOIN salve.ficha_pendencia fp ON fp.sq_ficha = f.sq_ficha AND fp.st_pendente = 'S'::bpchar
         LEFT JOIN salve.pessoa p ON p.sq_pessoa = f.sq_usuario_alteracao
GROUP BY f.sq_ficha, f.sq_taxon, f.sq_ciclo_avaliacao, f.sq_unidade_org, f.sq_usuario_alteracao, f.sq_situacao_ficha,
         sit.ds_dados_apoio, sit.cd_sistema, f.sq_grupo_salve, f.sq_grupo, f.sq_subgrupo, f.sq_categoria_iucn,
         f.ds_criterio_aval_iucn, f.st_possivelmente_extinta, f.sq_categoria_final, f.ds_criterio_aval_iucn_final,
         f.st_possivelmente_extinta_final, f.nm_cientifico, u.sg_instituicao, p.no_pessoa, f.dt_alteracao,
         f.dt_publicacao, f.sq_usuario_publicacao, f.vl_percentual_preenchimento, f.st_manter_lc, f.st_transferida,
         f.dt_aceite_validacao, f.st_localidade_desconhecida, nivel.co_nivel_taxonomico, nivel.nu_grau_taxonomico;

alter table salve.vw_ficha
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_ficha to usr_salve;


create or replace view salve.vw_uc
            (sq_uc, sq_controle, no_uc, sg_uc, sg_unidade, sq_esfera, cd_esfera, cd_esfera_sistema, in_esfera,
             co_cnuc) as
SELECT uc.sq_pessoa               AS sq_uc,
       uc.sq_pessoa               AS sq_controle,
       pessoa.no_pessoa           AS no_uc,
       instituicao.sg_instituicao AS sg_uc,
       instituicao.sg_instituicao AS sg_unidade,
       uc.sq_esfera,
       esfera.cd_dados_apoio      AS cd_esfera,
       esfera.cd_dados_apoio      AS cd_esfera_sistema,
       esfera.cd_dados_apoio      AS in_esfera,
       uc.cd_cnuc                 AS co_cnuc
FROM salve.unidade_conservacao uc
         JOIN salve.instituicao ON instituicao.sq_pessoa = uc.sq_pessoa
         JOIN salve.pessoa ON pessoa.sq_pessoa = uc.sq_pessoa
         JOIN salve.dados_apoio esfera ON esfera.sq_dados_apoio = uc.sq_esfera;

alter table salve.vw_uc
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_uc to usr_salve;

create or replace view salve.vw_unidade_org
            (sq_pessoa, sq_pessoa_pai, sq_tipo, sg_instituicao, sg_unidade_org, nu_cnpj, no_contato, nu_telefone,
             dt_alteracao, no_pessoa)
as
SELECT instituicao.sq_pessoa,
       instituicao.sq_pessoa_pai,
       instituicao.sq_tipo,
       instituicao.sg_instituicao,
       instituicao.sg_instituicao AS sg_unidade_org,
       instituicao.nu_cnpj,
       instituicao.no_contato,
       instituicao.nu_telefone,
       instituicao.dt_alteracao,
       pessoa.no_pessoa
FROM salve.instituicao
         JOIN salve.pessoa ON pessoa.sq_pessoa = instituicao.sq_pessoa
WHERE instituicao.st_interna = true;

alter table salve.vw_unidade_org
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_unidade_org to usr_salve;

create or replace view salve.vw_usuario(sq_pessoa, no_pessoa, st_ativo, de_hash, de_senha, dt_ultimo_acesso) as
SELECT usuario.sq_pessoa,
       pessoa.no_pessoa,
       usuario.st_ativo,
       usuario.de_hash,
       usuario.de_senha,
       usuario.dt_ultimo_acesso
FROM salve.usuario
         JOIN salve.pessoa ON pessoa.sq_pessoa = usuario.sq_pessoa;

alter table salve.vw_usuario
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_usuario to usr_salve;

create or replace view salve.vw_usuario_externo
            (sq_pessoa, no_pessoa, de_email, tx_email, nu_cpf, sq_instituicao, sg_instituicao, nu_cnpj, st_ativo) as
SELECT pessoa.sq_pessoa,
       pessoa.no_pessoa,
       pessoa.de_email,
       pessoa.de_email AS tx_email,
       pf.nu_cpf,
       pf.sq_instituicao,
       instituicao.sg_instituicao,
       instituicao.nu_cnpj,
       usuario.st_ativo
FROM salve.usuario
         JOIN salve.pessoa ON usuario.sq_pessoa = pessoa.sq_pessoa
         JOIN salve.pessoa_fisica pf ON pf.sq_pessoa = usuario.sq_pessoa
         JOIN salve.instituicao ON instituicao.sq_pessoa = pf.sq_instituicao
WHERE instituicao.st_interna = false;

alter table salve.vw_usuario_externo
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_usuario_externo to usr_salve;

create or replace view salve.vw_ficha_modulo_publico_ssc
            (sq_ficha, nm_cientifico, sq_ficha_versao, nu_versao, ds_situacao_ficha, cd_situacao_ficha_sistema,
             st_endemica_brasil, st_presenca_lista_vigente, sq_taxon_anterior, no_cientifico_anterior,
             no_taxon_anterior, sq_taxon, no_taxon, co_nivel_taxonomico, sq_nivel_taxonomico, nu_grau_taxonomico,
             sq_unidade_org, sg_unidade_org, no_unidade_org, sq_categoria_final, ds_categoria_final, cd_categoria_final,
             cd_categoria_final_sistema, ds_criterio_aval_iucn_final, st_possivelmente_extinta_final,
             de_categoria_final_ordem, ds_citacao, dt_fim_avaliacao, de_periodo_avaliacao, sq_grupo_avaliado,
             ds_grupo_avaliado, cd_grupo_avaliado_sistema, de_grupo_avaliado_ordem, sq_grupo_salve, ds_grupo_salve,
             de_grupo_salve_ordem, cd_grupo_salve_sistema, ds_distribuicao_geo_global, ds_distribuicao_geo_nacional,
             nu_min_altitude, nu_max_altitude, nu_min_batimetria, nu_max_batimetria, no_comum, no_antigo)
as
SELECT ficha.sq_ficha::bigint                                                                                         AS sq_ficha,
       fv.js_ficha ->> 'nm_cientifico'::text                                                                          AS nm_cientifico,
       fv.sq_ficha_versao,
       fv.nu_versao,
       'Publicada'::text                                                                                              AS ds_situacao_ficha,
       'PUBLICADA'::text                                                                                              AS cd_situacao_ficha_sistema,
       (fv.js_ficha -> 'distribuicao'::text) ->> 'st_endemica_brasil'::text                                           AS st_endemica_brasil,
       (fv.js_ficha -> 'historico_avaliacao'::text) ->>
       'st_presenca_lista_vigente'::text                                                                              AS st_presenca_lista_vigente,
       versao_anterior.sq_taxon_anterior,
       versao_anterior.no_cientifico_anterior,
       versao_anterior.no_taxon_anterior,
       COALESCE(((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'sq_taxon'::text,
                ((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                'sq_taxon'::text)::bigint                                                                             AS sq_taxon,
       COALESCE(((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'no_taxon'::text,
                ((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                'no_taxon'::text)                                                                                     AS no_taxon,
       COALESCE(((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'co_nivel_taxonomico'::text,
                ((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                'co_nivel_taxonomico'::text)                                                                          AS co_nivel_taxonomico,
       COALESCE(((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'sq_nivel_taxonomico'::text,
                ((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                'sq_nivel_taxonomico'::text)::bigint                                                                  AS sq_nivel_taxonomico,
       COALESCE(((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'nu_grau_taxonomico'::text,
                ((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                'nu_grau_taxonomico'::text)::integer                                                                  AS nu_grau_taxonomico,
       (fv.js_ficha ->> 'sq_unidade_responsavel'::text)::bigint                                                       AS sq_unidade_org,
       fv.js_ficha ->> 'sg_unidade_responsavel'::text                                                                 AS sg_unidade_org,
       fv.js_ficha ->> 'no_unidade_responsavel'::text                                                                 AS no_unidade_org,
       (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
        'sq_categoria_final'::text)::bigint                                                                           AS sq_categoria_final,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
       'ds_categoria_final'::text                                                                                     AS ds_categoria_final,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
       'cd_categoria_final'::text                                                                                     AS cd_categoria_final,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
       'cd_categoria_final_sitema'::text                                                                              AS cd_categoria_final_sistema,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
       'ds_criterio_aval_iucn_final'::text                                                                            AS ds_criterio_aval_iucn_final,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
       'st_possivelmente_extinta_final'::text                                                                         AS st_possivelmente_extinta_final,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->>
       'de_categoria_final_ordem'::text                                                                               AS de_categoria_final_ordem,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_avaliacao'::text) ->>
       'ds_citacao'::text                                                                                             AS ds_citacao,
       (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_avaliacao'::text) ->>
        'dt_fim_avaliacao'::text)::date                                                                               AS dt_fim_avaliacao,
       ((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_avaliacao'::text) ->>
       'nu_mes_ano_ultima_avaliacao'::text                                                                            AS de_periodo_avaliacao,
       (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->>
        'sq_grupo'::text)::bigint                                                                                     AS sq_grupo_avaliado,
       ((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->>
       'ds_grupo'::text                                                                                               AS ds_grupo_avaliado,
       ((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->>
       'cd_grupo_sistema'::text                                                                                       AS cd_grupo_avaliado_sistema,
       ((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->>
       'de_grupo_ordem'::text                                                                                         AS de_grupo_avaliado_ordem,
       (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->>
        'sq_recorte_publico'::text)::bigint                                                                           AS sq_grupo_salve,
       ((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->>
       'ds_recorte_publico'::text                                                                                     AS ds_grupo_salve,
       ((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->>
       'de_recorte_publico_ordem'::text                                                                               AS de_grupo_salve_ordem,
       ((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->>
       'cd_recorte_publico_sistema'::text                                                                             AS cd_grupo_salve_sistema,
       (fv.js_ficha -> 'distribuicao'::text) ->>
       'ds_distribuicao_geo_global'::text                                                                             AS ds_distribuicao_geo_global,
       (fv.js_ficha -> 'distribuicao'::text) ->>
       'ds_distribuicao_geo_nacional'::text                                                                           AS ds_distribuicao_geo_nacional,
       ((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_min_altitude'::text)::integer                                   AS nu_min_altitude,
       ((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_max_altitude'::text)::integer                                   AS nu_max_altitude,
       ((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_min_batimetria'::text)::integer                                 AS nu_min_batimetria,
       ((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_max_batimetria'::text)::integer                                 AS nu_max_batimetria,
       nomes_comuns.no_comuns                                                                                         AS no_comum,
       nomes_antigos.no_antigos                                                                                       AS no_antigo
FROM salve.ficha
         JOIN salve.ficha_versao fv ON fv.sq_ficha = ficha.sq_ficha AND fv.st_publico = true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT
                                                              CASE
                                                                  WHEN
                                                                      (nomes_comuns_1.value ->> 'de_regiao_lingua'::text) ~~*
                                                                      'portu%'::text OR
                                                                      (nomes_comuns_1.value ->> 'de_regiao_lingua'::text) IS NULL OR
                                                                      (nomes_comuns_1.value ->> 'de_regiao_lingua'::text) =
                                                                      ''::text THEN nomes_comuns_1.value ->> 'no_comum'::text
                                                                  ELSE NULL::text
                                                                  END), ', '::text) AS no_comuns
                             FROM jsonb_array_elements(((fv.js_ficha -> 'classificacao_taxonomica'::text) ->>
                                                        'nomes_comuns'::text)::jsonb) nomes_comuns_1(value)) nomes_comuns
                   ON true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT nomes_antigos_1.value ->> 'no_antigo'::text),
                                                    ', '::text) AS no_antigos
                             FROM jsonb_array_elements(((fv.js_ficha -> 'classificacao_taxonomica'::text) ->>
                                                        'nomes_antigos'::text)::jsonb) nomes_antigos_1(value)) nomes_antigos
                   ON true
         LEFT JOIN LATERAL ( SELECT x.nu_versao                          AS nu_versao_anterior,
                                    x.js_ficha ->> 'nm_cientifico'::text AS no_cientifico_anterior,
                                    COALESCE(((x.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->>
                                             'sq_taxon'::text,
                                             ((x.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                                             'sq_taxon'::text)::bigint   AS sq_taxon_anterior,
                                    COALESCE(((x.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->>
                                             'no_taxon'::text,
                                             ((x.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->>
                                             'no_taxon'::text)           AS no_taxon_anterior
                             FROM salve.ficha_versao x
                             WHERE x.sq_ficha = fv.sq_ficha
                               AND x.nu_versao < fv.nu_versao
                               AND x.sq_contexto = fv.sq_contexto
                             ORDER BY x.sq_ficha, x.nu_versao DESC
                             LIMIT 1) versao_anterior ON true
WHERE ficha.sq_ciclo_avaliacao = 2
UNION
SELECT ficha.sq_ficha::bigint                                                         AS sq_ficha,
       ficha.nm_cientifico,
       NULL::bigint                                                                   AS sq_ficha_versao,
       NULL::integer                                                                  AS nu_versao,
       situacao_ficha.ds_dados_apoio                                                  AS ds_situacao_ficha,
       situacao_ficha.cd_sistema                                                      AS cd_situacao_ficha_sistema,
       ficha.st_endemica_brasil::text                                                 AS st_endemica_brasil,
       ficha.st_presenca_lista_vigente::text                                          AS st_presenca_lista_vigente,
       ficha_anterior.sq_taxon                                                        AS sq_taxon_anterior,
       ficha_anterior.nm_cientifico                                                   AS no_cientifico_anterior,
       COALESCE((taxon_anterior.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text,
                (taxon_anterior.json_trilha -> 'especie'::text) ->> 'no_taxon'::text) AS no_taxon_anterior,
       ficha.sq_taxon,
       COALESCE((taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text,
                (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text)          AS no_taxon,
       nivel_taxonomico.co_nivel_taxonomico,
       nivel_taxonomico.sq_nivel_taxonomico::bigint                                   AS sq_nivel_taxonomico,
       nivel_taxonomico.nu_grau_taxonomico,
       ficha.sq_unidade_org,
       instituicao.sg_instituicao                                                     AS sg_unidade_org,
       pessoa.no_pessoa                                                               AS no_unidade_org,
       ficha.sq_categoria_final,
       categoria_final.ds_dados_apoio                                                 AS ds_categoria_final,
       categoria_final.cd_dados_apoio                                                 AS cd_categoria_final,
       categoria_final.cd_sistema                                                     AS cd_categoria_final_sistema,
       ficha.ds_criterio_aval_iucn_final,
       ficha.st_possivelmente_extinta_final::text                                     AS st_possivelmente_extinta_final,
       categoria_final.de_dados_apoio_ordem                                           AS de_categoria_final_ordem,
       ficha.ds_citacao,
       oficina_avaliacao.dt_fim_avaliacao,
       oficina_avaliacao.de_periodo_avaliacao,
       ficha.sq_grupo                                                                 AS sq_grupo_avaliado,
       grupo.ds_dados_apoio                                                           AS ds_grupo_avaliado,
       grupo.cd_sistema                                                               AS cd_grupo_avaliado_sistema,
       grupo.de_dados_apoio_ordem                                                     AS de_grupo_avaliado_ordem,
       ficha.sq_grupo_salve,
       grupo_salve.ds_dados_apoio                                                     AS ds_grupo_salve,
       grupo_salve.de_dados_apoio_ordem                                               AS de_grupo_salve_ordem,
       grupo_salve.cd_sistema                                                         AS cd_grupo_salve_sistema,
       ficha.ds_distribuicao_geo_global,
       ficha.ds_distribuicao_geo_nacional,
       ficha.nu_min_altitude,
       ficha.nu_max_altitude,
       ficha.nu_min_batimetria,
       ficha.nu_max_batimetria,
       nome_comum.no_comum,
       nome_antigo.no_antigo
FROM salve.ficha
         JOIN salve.dados_apoio situacao_ficha ON situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha
         JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
         JOIN taxonomia.nivel_taxonomico ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
         LEFT JOIN salve.ficha ficha_anterior ON ficha_anterior.sq_ficha = ficha.sq_ficha_ciclo_anterior
         LEFT JOIN taxonomia.taxon taxon_anterior ON taxon_anterior.sq_taxon = ficha_anterior.sq_taxon
         JOIN salve.instituicao instituicao ON instituicao.sq_pessoa = ficha.sq_unidade_org
         JOIN salve.pessoa pessoa ON pessoa.sq_pessoa = ficha.sq_unidade_org
         LEFT JOIN salve.dados_apoio categoria_avaliada ON categoria_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
         LEFT JOIN salve.dados_apoio categoria_final ON categoria_final.sq_dados_apoio = ficha.sq_categoria_final
         LEFT JOIN salve.dados_apoio grupo ON grupo.sq_dados_apoio = ficha.sq_grupo
         LEFT JOIN salve.dados_apoio grupo_salve ON grupo_salve.sq_dados_apoio = ficha.sq_grupo_salve
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT fnc.no_comum), ', '::text) AS no_comum
                             FROM salve.ficha_nome_comum fnc
                                      JOIN salve.ficha ficha_1 ON ficha_1.sq_ficha = fnc.sq_ficha
                                      JOIN salve.ciclo_avaliacao ciclo_1
                                           ON ciclo_1.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
                                      JOIN taxonomia.taxon taxon_1 ON taxon_1.sq_taxon = ficha_1.sq_taxon
                             WHERE (taxon_1.sq_taxon = ficha.sq_taxon OR taxon_1.sq_taxon_pai = ficha.sq_taxon)
                               AND ciclo_1.in_publico = 'S'::bpchar
                               AND (fnc.de_regiao_lingua ~~* 'portu%'::text OR fnc.de_regiao_lingua IS NULL OR
                                    fnc.de_regiao_lingua = ''::text)) nome_comum ON true
         LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT fnc.no_sinonimia), ', '::text) AS no_antigo
                             FROM salve.ficha_sinonimia fnc
                                      JOIN salve.ficha ficha_1 ON ficha_1.sq_ficha = fnc.sq_ficha
                                      JOIN salve.ciclo_avaliacao ciclo_1
                                           ON ciclo_1.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
                                      JOIN taxonomia.taxon taxon_1 ON taxon_1.sq_taxon = ficha_1.sq_taxon
                             WHERE (taxon_1.sq_taxon = ficha.sq_taxon OR taxon_1.sq_taxon_pai = ficha.sq_taxon)
                               AND ciclo_1.in_publico = 'S'::bpchar) nome_antigo ON true
         LEFT JOIN LATERAL ( SELECT to_char(oficina.dt_fim::timestamp with time zone, 'MM/yyyy'::text) AS de_periodo_avaliacao,
                                    oficina.dt_fim                                                     AS dt_fim_avaliacao
                             FROM salve.oficina_ficha
                                      JOIN salve.oficina ON oficina.sq_oficina = oficina_ficha.sq_oficina
                                      JOIN salve.dados_apoio ON oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
                                      JOIN salve.ficha ficha_1 ON ficha_1.sq_ficha = oficina_ficha.sq_ficha
                                      JOIN salve.ciclo_avaliacao ciclo
                                           ON ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
                             WHERE oficina_ficha.sq_ficha = ficha.sq_ficha
                               AND dados_apoio.cd_sistema = 'OFICINA_AVALIACAO'::text
                               AND ciclo.in_publico = 'S'::bpchar
                             ORDER BY oficina.dt_fim DESC
                             OFFSET 0 LIMIT 1) oficina_avaliacao ON true
WHERE ficha.sq_ciclo_avaliacao = 2
  AND (situacao_ficha.cd_sistema = ANY (ARRAY ['POS-OFICINA'::text, 'VALIDADA'::text, 'FINALIZADA'::text]));

alter table salve.vw_ficha_modulo_publico_ssc
    owner to postgres;

create or replace view salve.vw_oficina_participante
            (sq_oficina_participante, sq_oficina, sq_pessoa, sq_situacao, sq_instituicao, de_email, dt_email,
             de_assunto_email, de_situacao, cd_situacao_sistema, no_pessoa, no_instituicao, sg_instituicao,
             dt_email_assinatura, dt_assinatura, de_hash_assinatura)
as
SELECT oficina_participante.sq_oficina_participante,
       oficina_participante.sq_oficina,
       oficina_participante.sq_pessoa,
       oficina_participante.sq_situacao,
       oficina_participante.sq_instituicao,
       oficina_participante.de_email,
       oficina_participante.dt_email,
       oficina_participante.de_assunto_email,
       situacao.ds_dados_apoio           AS de_situacao,
       situacao.cd_sistema               AS cd_situacao_sistema,
       pessoa.no_pessoa,
       pessoa_instituicao.no_pessoa      AS no_instituicao,
       instituicao.sg_instituicao,
       oficina_anexo_assinatura.dt_email AS dt_email_assinatura,
       oficina_anexo_assinatura.dt_assinatura,
       oficina_anexo_assinatura.de_hash  AS de_hash_assinatura
FROM salve.oficina_participante
         LEFT JOIN salve.oficina_anexo_assinatura
                   ON oficina_anexo_assinatura.sq_oficina_participante = oficina_participante.sq_oficina_participante
         LEFT JOIN salve.instituicao instituicao ON instituicao.sq_pessoa = oficina_participante.sq_instituicao
         LEFT JOIN salve.pessoa pessoa_instituicao ON pessoa_instituicao.sq_pessoa = oficina_participante.sq_instituicao
         LEFT JOIN salve.pessoa pessoa ON pessoa.sq_pessoa = oficina_participante.sq_pessoa
         LEFT JOIN salve.dados_apoio situacao ON situacao.sq_dados_apoio = oficina_participante.sq_situacao;

alter table salve.vw_oficina_participante
    owner to postgres;

grant select on salve.vw_oficina_participante to usr_salve;

create or replace view salve.vw_oficina_anexo
            (sq_oficina_anexo, sq_oficina, no_arquivo, de_legenda, de_tipo_conteudo, de_local_arquivo, in_doc_final,
             nu_assinaturas)
as
SELECT anexo.sq_oficina_anexo,
       anexo.sq_oficina,
       anexo.no_arquivo,
       anexo.de_legenda,
       anexo.de_tipo_conteudo,
       anexo.de_local_arquivo,
       anexo.in_doc_final,
       sum(
               CASE
                   WHEN ass.dt_assinatura IS NOT NULL THEN 1
                   ELSE 0
                   END) AS nu_assinaturas
FROM salve.oficina_anexo anexo
         LEFT JOIN salve.oficina_anexo_assinatura ass ON ass.sq_oficina_anexo = anexo.sq_oficina_anexo
GROUP BY anexo.sq_oficina_anexo, anexo.sq_oficina, anexo.no_arquivo, anexo.de_legenda, anexo.de_tipo_conteudo,
         anexo.de_local_arquivo, anexo.in_doc_final;

alter table salve.vw_oficina_anexo
    owner to postgres;

grant select on salve.vw_oficina_anexo to usr_salve;

create or replace view salve.vw_usos
            (id, id_pai, codigo, descricao, descricao_completa, id_ameaca_iucn, ordem, nivel, trilha, trilha_csv) as
WITH RECURSIVE usos AS (SELECT u.sq_uso,
                               u.sq_uso_pai,
                               u.co_uso,
                               u.ds_uso,
                               u.sq_criterio_ameaca_iucn,
                               salve.hierarquia_ordem(u.nu_ordem)    AS ordem,
                               0                                     AS nivel,
                               (u.co_uso || ' - '::text) || u.ds_uso AS ds_trilha,
                               (((u.co_uso || ';'::text) || replace(u.ds_uso, ';'::text, ','::text)) || ';'::text) ||
                               u.sq_uso::text                        AS ds_trilha_csv
                        FROM salve.uso u
                        WHERE u.sq_uso_pai IS NULL
                        UNION
                        SELECT pai.sq_uso,
                               pai.sq_uso_pai,
                               pai.co_uso,
                               pai.ds_uso,
                               pai.sq_criterio_ameaca_iucn,
                               salve.hierarquia_ordem(pai.nu_ordem)                                           AS ordem,
                               usos_1.nivel + 1,
                               (((usos_1.ds_trilha || '|'::text) || pai.co_uso) || ' - '::text) ||
                               pai.ds_uso                                                                     AS ds_trilha,
                               (((((usos_1.ds_trilha_csv || '|'::text) || pai.co_uso) || ';'::text) ||
                                 replace(pai.ds_uso, ';'::text, ','::text)) || ';'::text) ||
                               pai.sq_uso::text                                                               AS ds_trilha_csv
                        FROM usos usos_1
                                 JOIN salve.uso pai ON pai.sq_uso_pai = usos_1.sq_uso)
SELECT usos.sq_uso                                 AS id,
       usos.sq_uso_pai                             AS id_pai,
       usos.co_uso                                 AS codigo,
       usos.ds_uso                                 AS descricao,
       (usos.co_uso || ' - '::text) || usos.ds_uso AS descricao_completa,
       usos.sq_criterio_ameaca_iucn                AS id_ameaca_iucn,
       usos.ordem,
       usos.nivel,
       usos.ds_trilha                              AS trilha,
       usos.ds_trilha_csv                          AS trilha_csv
FROM usos;

alter table salve.vw_usos
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on salve.vw_usos to usr_salve;
