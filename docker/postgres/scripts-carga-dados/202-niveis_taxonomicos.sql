insert into taxonomia.nivel_taxonomico (sq_nivel_taxonomico, de_nivel_taxonomico, nu_grau_taxonomico, in_nivel_intermediario, co_nivel_taxonomico, de_dwca)
values  (9, 'Subfamília', 60, true, 'SUBFAMILIA', 'subfamily'),
        (10, 'Tribo', 70, true, 'TRIBO', 'tribe'),
        (11, 'Subfilo', 25, true, 'SUBFILO', 'subphylum'),
        (8, 'Subespécie', 99, false, 'SUBESPECIE', 'infraspecificEpithet'),
        (1, 'Reino', 10, false, 'REINO', 'kingdom'),
        (5, 'Família', 50, false, 'FAMILIA', 'family'),
        (3, 'Classe', 30, false, 'CLASSE', 'class'),
        (2, 'Filo', 20, false, 'FILO', 'phylum'),
        (7, 'Espécie', 90, false, 'ESPECIE', 'specificEpithet'),
        (4, 'Ordem', 40, false, 'ORDEM', 'order'),
        (6, 'Gênero', 80, false, 'GENERO', 'genus');