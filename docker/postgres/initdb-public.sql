-- criar funcao publica
CREATE OR REPLACE FUNCTION public.fn_remove_acentuacao(character varying) RETURNS character varying AS
$BODY$
SELECT TRANSLATE($1, 'áéíóúàèìòùãõâêîôôäëïöüçÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÄËÏÖÜÇ', 'aeiouaeiouaoaeiooaeioucAEIOUAEIOUAOAEIOOAEIOUC')
$BODY$
    LANGUAGE sql IMMUTABLE
                 COST 1;
ALTER FUNCTION public.fn_remove_acentuacao(character varying)
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.fn_normaliza_string(character varying) RETURNS character varying AS
$BODY$
select trim(regexp_replace(regexp_replace(regexp_replace(public.fn_remove_acentuacao(lower($1)), '(ª regiao)|( d*[eao][ms]* )|(-)|( -( [a-z]{1,}$))|([0-9]{1,})', ' ', 'gi'), '(s( |$))|( {2,})|( marinha )', ' ', 'g'), '( [a-z]{2}$)', '', 'g'));
$BODY$
    LANGUAGE sql VOLATILE
                 COST 100;
ALTER FUNCTION public.fn_normaliza_string(character varying)
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.fn_normaliza_string(text) RETURNS character varying AS
$BODY$
SELECT public.fn_normaliza_string($1)
$BODY$
    LANGUAGE sql IMMUTABLE
                 COST 100;
ALTER FUNCTION public.fn_normaliza_string(text)
    OWNER TO postgres;
GRANT EXECUTE ON FUNCTION public.fn_normaliza_string(text) TO public;
